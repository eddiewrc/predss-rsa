# README #

This is a simple LSTM model for the prediction of SS and RSA of proteins.


### How do I get set up? ###

It needs pytorch, numpy, scipy, sklearn, matplotlib to run. 
Make sure they are installed. (pip will do)

### How do I run it? ###

Just run it with the command:

python standaloneSS+RSA.py YOUR_FASTA_FILE

and the predictions will be put in the preds_dir/ folder


### Who do I talk to? ###

daniele.raimondi@vub.be