#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
verbosity = 2
import numpy as np

SS1 = {"E":[1,0,0],"C":[0,1,0],"H":[0,0,1]}
SS = {"E":0,"C":1,"H":2}
SS_inv = ["E", "C", "H"]

def readFASTA(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line[line.index(",")+1:].strip().replace(",",":").replace("#",""),""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded)
	return sl

def readRSAfloat(seqFile, MIN_LENGTH = 20, MAX_LENGTH=500):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line[line.index(",")+1:line.index(",",6)],[]] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + [float(c) for c in line.strip().split(" ") ]
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded)
	return sl
	


def buildVectorsRSA(uids, rsa, seqs):	
	X = []
	Y = []
	for u in uids:
		tmprsa = rsa[u]
		tmpseq = seqs[u]
		X.append(seq2vec(tmpseq))
		Y.append(rsa2label(tmprsa))
	return X, Y	
	
def buildVectorsRSAfloat(uids, rsa, seqs):	
	X = []
	Y = []
	for u in uids:
		tmprsa = rsa[u]
		tmpseq = seqs[u]
		X.append(seq2vec(tmpseq))
		Y.append(tmprsa)
	return X, Y		

def rsa2label(rsa):
	r = []
	for i in rsa:
		if i == "e":
			r.append(1)
		else:
			r.append(0)
	return r
	
def buildVectorsRSAfloat(uids, rsa, seqs):	
	X = []
	Y = []
	for u in uids:
		tmprsa = rsa[u]
		tmpseq = seqs[u]
		X.append(seq2vec(tmpseq))
		Y.append(tmprsa)
	return X, Y
	
def buildVectorsSSembed(uids, ss, seqs):	
	X = []
	Y = []
	for u in uids:
		tmpss = ss[u]
		tmpseq = seqs[u]
		X.append(getEmbeddingValues(tmpseq))
		Y.append(ss2label(tmpss))
	return X, Y	
	
def buildVectorsSS(uids, ss, seqs):	
	X = []
	Y = []
	for u in uids:
		tmprsa = ss[u]
		tmpseq = seqs[u]
		X.append(seq2vec(tmpseq))
		Y.append(ss2label(tmprsa))
	return X, Y	

def getEmbeddingValues(s):
	r = []
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in s:
		if i not in listAA:
			r.append(0)
		else:
			r.append(listAA.index(i)+1)	
	return r
	
def ss2label(ss):
	r = []
	for i in ss:
		r.append(SS[i])
	return r
	
def buildVectors(uids, db):	#db = {uid:(seq,label)}
	X = []
	Y = []
	for u in uids:
		tmp = db[u][0]
		X.append(seq2vec(tmp))
		Y.append(db[u][1])
	return X, Y
	
def seq2vec(s):
	r = []
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in s:
		tmp = np.zeros((20))	
		try:
			tmp[listAA.index(i)] = 1
		except:
			pass
		r.append(tmp.tolist())	
	return r
		

def readSCRATCH_DB(seqFile, rsaFileFloat, rsaFile, ssFile, MIN_LENGTH = 50, MAX_LENGTH=5000):
	
	seqs = readFASTA(seqFile, MIN_LENGTH, MAX_LENGTH)
	#print seqs.items()[:3]
	ss = readFASTA(ssFile, MIN_LENGTH, MAX_LENGTH)
	#print ss.items()[:3]
	rsafloat = readRSAfloat(rsaFileFloat, MIN_LENGTH, MAX_LENGTH)
	#print rsa.items()[:3]
	rsa = readFASTA(rsaFile, MIN_LENGTH, MAX_LENGTH)
	#print rsa.items()[:3]
	assert len(seqs) == len(ss) == len(rsa)
	print "Found %d proteins" % (len(seqs))
	return seqs, ss, rsafloat, rsa

	
	
def main():
	db = readSCRATCH_DB("../SCRATCH-1D_datasets/Proteins.fa", "../SCRATCH-1D_datasets/ACCpro20.dssp", "../SCRATCH-1D_datasets/ACCpro.dssp","../SCRATCH-1D_datasets/SSpro8.dssp")
	ofp = open("db.fasta", "w")
	for i in db[0].items():
		print i
		ofp.write( ">%s\n%s\n" %(i[0],i[1]))
	ofp.close()
		

if __name__ == '__main__':
	main()
