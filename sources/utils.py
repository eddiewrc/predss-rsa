#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
verbosity = 2
import numpy as np

def readDB(f):
	ifp = open(f)
	lines = ifp.readlines()
	ifp.close()
	lines.pop(0)
	discarded = 0
	db = {}
	for l in lines:
		tmp = l.strip().split(",")
		#print tmp
		uid = tmp[1]
		if "\"" in l:
			sigp = int(tmp[5])
			seq = tmp[7][sigp:]
			label = parseLabel(tmp[6])
		else:
			sigp = int(tmp[4])
			seq = tmp[6][sigp:]
			label = parseLabel(tmp[5])
		#print uid, sigp,  label
		if len(seq) > 50:
			db[uid] = (seq,label)
		else:
			discarded += 1
	print "Found %d proteins, discarded %d" % (len(db), discarded)
	return db	#db = {uid:(seq,label)}

def parseLabel(l):
	if l == "cytoplasmic":
		return 0
	elif l == "secreted":
		return 1
	else:
		raise Exception("Unknown label %s" % l)

def retrieveUID(l):
	uidList = []
	for i in l:
		ifp = open(i, "r")
		lines = ifp.readlines()
		for j in lines:
			uidList.append(j.strip())		
	if verbosity >= 2:
		print "Num of uid retrieved: " + str(len(uidList))
	return uidList	

def mse(s1, s2):
	r = 0
	i = 0
	assert len(s1) == len (s2)
	while i < len(s1):
		r += (s1[i] - s2[i])**2
		i+=1
	return r / float(len(s1))

def getScoresMulticlass(pred, real, N_CLASSES):
	from sklearn.metrics import accuracy_score, classification_report
	assert len(pred) == len(real)
	tmp = []
	for prot in pred:
		#print len(prot)		
		for r in prot:		
			#print len(r)
			tmp.append(getMaxPos(r, N_CLASSES))
	pred = tmp
	tmp = []
	for prot in real:
		tmp += prot		
	real = tmp	
	#print pred
	print classification_report(real, pred)	

def getMaxPos(v, N_CLASSES):
	#print v
	#print len(v)
	assert len(v) == N_CLASSES
	m = 0
	i = 0
	while i < len(v):
		if v[i] > v[m]:
			m = i
		i += 1
	assert m < N_CLASSES
	return m
	
def getScoresSVR(pred, real, threshold=None, invert = False, PRINT = False, CURVES = False, SAVEFIG=None):
	print type(pred[0])
	if type(pred[0]) == list or type(pred[0]) == np.ndarray:
		tmp = []
		for i in pred:
			if type(i) == np.ndarray:
				i = i.flatten().tolist()
				#print i
				#raw_input()
			tmp += i
		pred = tmp
		tmp = []
		for i in real:
			tmp += i
		real = tmp
	#print pred
	import math
	if len(pred) != len(real):
		raise Exception("ERROR: input vectors have differente len!")
	if PRINT:
		print "Computing scores for %d predictions" % len(pred)		
	from sklearn.metrics import roc_curve, auc, average_precision_score, precision_recall_curve
	
	if CURVES or SAVEFIG != None:
		import matplotlib.pyplot as plt		
		precision, recall, thresholds = precision_recall_curve(real, pred)
		#plt.plot(recall, precision)
		#plt.show()
		fpr, tpr, _ = roc_curve(real, pred)		
		fig, (ax1, ax2) = plt.subplots(figsize=[10.0, 5], ncols=2)
		ax1.set_ylabel("Precision")
		ax1.set_xlabel("Recall")
		ax1.set_title("PR curve")
		ax1.set_xlim(0,1)
		ax1.set_ylim(0,1)
		ax1.plot(recall, precision)
		ax1.grid()
		ax2.plot(fpr, tpr)
		ax2.set_ylim(0,1)
		ax2.set_xlim(0,1)
		ax2.plot([0,1],[0,1],"--",c="grey",)
		ax2.set_xlabel("FPR")
		ax2.set_ylabel("TPR")
		ax2.set_title("ROC curve")
		ax2.grid()
		if SAVEFIG != None:
			plt.savefig(SAVEFIG, dpi=400)
		plt.show()
		plt.clf()
		
	fpr, tpr, thresholds = roc_curve(real, pred)
	auprc = average_precision_score(real, pred)
	aucScore = auc(fpr, tpr)		
	i = 0
	r = []
	while i < len(fpr):
		r.append((fpr[i], tpr[i], thresholds[i]))
		i+=1	
	ts = sorted(r, key=lambda x:(1.0-x[0]+x[1]), reverse=True)[:3]	
	#if PRINT:
	#	print ts
	if threshold == None:
		if PRINT:
			print " > Best threshold: " + str(ts[0][2])
		threshold = ts[0][2]
	i = 0
	confusionMatrix = {}
	confusionMatrix["TP"] = confusionMatrix.get("TP", 0)
	confusionMatrix["FP"] = confusionMatrix.get("FP", 0)
	confusionMatrix["FN"] = confusionMatrix.get("FN", 0)
	confusionMatrix["TN"] = confusionMatrix.get("TN", 0)
	if invert == True:
		while i < len(real):
			if float(pred[i])>=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])<=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	else:
		while i < len(real):
			if float(pred[i])<=threshold and (real[i]==0):
				confusionMatrix["TN"] = confusionMatrix.get("TN", 0) + 1
			if float(pred[i])<=threshold and real[i]==1:
				confusionMatrix["FN"] = confusionMatrix.get("FN", 0) + 1
			if float(pred[i])>=threshold and real[i]==1:
				confusionMatrix["TP"] = confusionMatrix.get("TP", 0) + 1
			if float(pred[i])>=threshold and real[i]==0:
				confusionMatrix["FP"] = confusionMatrix.get("FP", 0) + 1
			i += 1
	#print "--------------------------------------------"
	#print confusionMatrix["TN"],confusionMatrix["FN"],confusionMatrix["TP"],confusionMatrix["FP"]
	if PRINT:
		print "      | DEL         | NEUT             |"
		print "DEL   | TP: %d   | FP: %d  |" % (confusionMatrix["TP"], confusionMatrix["FP"] )
		print "NEUT  | FN: %d   | TN: %d  |" % (confusionMatrix["FN"], confusionMatrix["TN"])	
	
	sen = (confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"]))))
	spe = (confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"]))))
	acc =  (confusionMatrix["TP"] + confusionMatrix["TN"])/max(0.00001,float((sum(confusionMatrix.values()))))
	bac = (0.5*((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FP"])))))))
	inf =((confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FN"])))+(confusionMatrix["TN"]/max(0.00001,float((confusionMatrix["TN"] + confusionMatrix["FN"])))-1.0)))
	pre =(confusionMatrix["TP"]/max(0.00001,float((confusionMatrix["TP"] + confusionMatrix["FP"]))))
	mcc =	( ((confusionMatrix["TP"] * confusionMatrix["TN"])-(confusionMatrix["FN"] * confusionMatrix["FP"])) / max(0.00001,math.sqrt((confusionMatrix["TP"]+confusionMatrix["FP"])*(confusionMatrix["TP"]+confusionMatrix["FN"])*(confusionMatrix["TN"]+confusionMatrix["FP"])*(confusionMatrix["TN"]+confusionMatrix["FN"]))) )  
	
	if PRINT:
		print "\nSen = %3.3f" % sen
		print "Spe = %3.3f" %  spe
		print "Acc = %3.3f " % acc
		print "Bac = %3.3f" %  bac
		#print "Inf = %3.3f" % inf
		print "Pre = %3.3f" %  pre
		print "MCC = %3.3f" % mcc
		print "#AUC = %3.3f" % aucScore
		print "#AUPRC= %3.3f" % auprc
		print "--------------------------------------------"	
	
	return sen, spe, acc, bac, pre, mcc, aucScore, auprc
	
	
def main():
	db = readDB("../datasets/test.csv")
	ofp = open("db.fasta", "w")
	for i in db.items():
		ofp.write( ">%s\n%s\n" %(i[0],i[1][0]))
	ofp.close()
		

if __name__ == '__main__':
	main()
