#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  parseBenchmark.py
#  
#  Copyright 2017 Daniele Raimondi <eddiewrc@arcturus>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import os
SS = {"C":[0,1,0],"H":[0,0,1],"E":[1,0,0]}

def parsePSIPRED(folder):
	db = {}
	filelist = os.listdir(folder)
	for f in filelist[:]:
		if not ".ss2" in f:
			continue
		#print folder+f	
		db[f[:f.index(".")]] = readPSIPRED4preds(folder+f)
		#raw_input()
	return db

def readPSIPRED4preds(f):
	ifp = open(f)
	lines = ifp.readlines()
	lines.pop(0)
	lines.pop(0)
	preds = []
	count = 1
	for l in lines:		
		tmp = l.split()
		assert int(tmp[0]) == count
		count += 1
		pred = SS[tmp[2]]
		C = float(tmp[3])
		H = float(tmp[4])
		E = float(tmp[5])
		preds.append([C, H, E])
	return preds	
		
import utilsSCRATCH as US
import utils as U
import random

def main():
	#print readPSIPRED4preds("../bench2/psipred4_scop153/d1nkd__.ss2")
	db = parsePSIPRED("../scratch_psipred/")
	seqs, ss, rsafloat, rsa = US.readSCRATCH_DB("../SCRATCH-1D_datasets/Proteins.fa", "../SCRATCH-1D_datasets/ACCpro20.dssp", "../SCRATCH-1D_datasets/ACCpro.dssp", "../SCRATCH-1D_datasets/SSpro.dssp")
	ids = seqs.keys()
	random.shuffle(ids)
	train = ids[:2500]
	test = ids[2500:]
	print "Len train %d, len test %d" % (len(train), len(test))
	_, Yss = US.buildVectorsSS(train, ss, seqs)
	_, yss = US.buildVectorsSS(test, ss, seqs)
	Yp = []
	y = []
	i = 0
	while i < len(train):
		#print db[train[i]]
		if not len(db[train[i]]) == len(Yss[i]):
			print train[i], len(db[train[i]]), len(Yss[i]) 
		Yp.append(db[train[i]])
		#raw_input()
		i += 1
	print len(Yp), len(Yss)	
	U.getScoresMulticlass(Yp, Yss, 3)

if __name__ == '__main__':
	main()
