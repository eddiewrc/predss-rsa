#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  pred1.py
#  
#  Copyright 2018 Daniele Raimondi <eddiewrc@vega>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import matplotlib.pyplot as plt
import os, copy, random, time, sys
from sys import stdout
import sources.utils as U
import numpy as np
import torch as t
from torch.autograd import Variable
from torch.nn.utils.rnn import PackedSequence, pack_padded_sequence, pad_packed_sequence
from torch.utils.data import Dataset, DataLoader
import sources.utilsSCRATCH as US
from scipy.stats import pearsonr
############################THIS IS THE MAIN METHOD, CHANGE STUFF HERE IF YOU WANT###########à
def mainPRE():
	if len(sys.argv) <= 1:
		print "\nUSAGE: python standaloneSS+RSA.py FASTA_FILE\n\n"
		exit(1)
	print "Reading fasta..."
	db = readFASTA(sys.argv[1], MIN_LENGTH = 10, MAX_LENGTH=13000)  # set here your thresholds!
	print "Building vectors..."
	x = buildVectorsSSembed(db.keys()[:], db)
	print "Predicting... (it may take a while)"
	model = t.load("models/lstmRSA+SS2.emb20.iter_40.t")
	wrapper = NNwrapper(model)
	FOLDERNAME= sys.argv[1]+"_preds_dir"
	print "Printing results in "+FOLDERNAME
	os.system("mkdir -p "+FOLDERNAME)
	yp = wrapper.predict(x)	
	printPreds(yp, db.keys()[:], db, FOLDERNAME)
	print "Done."
########################DON't CHANGE STUFF BELOW!!! #########################

class myDataset(Dataset):
    
	def __init__(self, X, Y1 = None, Y2 = None):
		if Y1 == None:
			Y1 = []
			for x in X:
				Y1.append([0]*len(x))	
			Y2 = Y1	
		assert len(Y1) == len(X)
		X, Y1, Y2, order = sortForPadding(X, Y1, Y2)
		X, lensX = pad_sequence(X)			
		Y1, _ = pad_sequence(Y1)
		Y2, _ = pad_sequence(Y2)
		self.X = X
		self.lensX = lensX
		self.Y1 = Y1	
		self.Y2 = Y2	
		self.order = order	
		
	def __len__(self):
		return len(self.X)

	def __getitem__(self, idx):
		return self.X[idx], self.Y1[idx], self.Y2[idx], self.lensX[idx]

class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	

class NNwrapper():

	def __init__(self, model):
		self.model = model
	
	def fit(self, originalX, originalY1, originalY2, epochs = 200, batch_size=250, save_model_every=10, warmStart = 0):

		########DATASET###########
		dataset = myDataset(originalX, originalY1, originalY2)
		
		#######MODEL##############		
		self.model.getNumParams()
			
		print "Start training"
		########LOSS FUNCTION######
		loss_fn1 = t.nn.BCELoss(size_average=False)
		loss_fn2 = t.nn.CrossEntropyLoss(size_average=False)
		#loss_fn = t.nn.MSELoss(size_average=True)
		#loss_fn = t.nn.CrossEntropyLoss(size_average=False)
		
		########OPTIMIZER##########	
		self.learning_rate = 1e-2
		optimizer = t.optim.Adam(self.model.parameters(), lr=self.learning_rate, weight_decay=0.0001)
		scheduler = t.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.5, patience=1, verbose=True, threshold=0.0001, threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08)
		
		########DATALOADER#########
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=0)
		
		e = 1 +warmStart
		minLoss = 1000000000
		ofp = open("models/"+self.model.name+".TrainLog","w",0)		
		while e < epochs+warmStart:			
			errTot = [0,0]
			i = 1
			start = time.time()
			for sample in loader:
				optimizer.zero_grad()
				x, y1, y2, lens = sample
				#print y.size()
				y_rsa, y_ss = self.model.forward(Variable(x), lens)	
				#print y_rsa.size(), y_ss.size()				
				y1 = t.cat(unpad(y1, lens)).unsqueeze(1)				
				loss1 = loss_fn1(y_rsa, Variable(y1))	
				y2 = t.cat(unpad(y2, lens)).long()
				#print y1.size(), y2.size()			
				loss2 = loss_fn2(y_ss, Variable(y2))		
				(loss1+loss2).backward()	
				optimizer.step()
				errTot[0] += loss1.data[0]
				errTot[1] += loss2.data[0]
				i += len(lens)								
				perc = (100*i/float(len(dataset))	)	
				stdout.write("\nepoch=%d %d (%3.2f%%), errBatch=%f, %f" % (e, i, perc, loss1.data[0]/float(sum(lens)), loss2.data[0]/float(sum(lens))))
				stdout.flush()				
			end = time.time()						
			print " epoch %d, ERRORTOT: %f (%fs)" % (e,sum(errTot)/float(len(dataset)), end-start)
			scheduler.step(sum(errTot))
			if e % save_model_every == 0:
				print "Store model ", e
				t.save(self.model, "models/"+self.model.name+".iter_"+str(e)+".t")				
			stdout.flush()	
										
			e += 1	
		t.save(self.model, "models/"+self.model.name+".final.t")
		ofp.close()		
	
	def predict(self, X, Y=None, batch_size=250, setEval=True):
		if setEval:
			self.model.eval()
		print "Training mode: ", self.model.training

		print "Predicting..."
		dataset = myDataset(X, Y, Y)
		loader = DataLoader(dataset, batch_size=batch_size, shuffle=False, sampler=None, num_workers=1)
		preds1 = []
		preds2 = []
		lensTot = []
		first = True
		for sample in loader:
			x, y1, y2, lens = sample
			y_pred = self.model.forward(Variable(x), lens)	
			preds1 += y_pred[0].data.squeeze().tolist()
			preds2 += y_pred[1].data.squeeze().tolist()
			lensTot += lens.tolist()
		assert len(lensTot) == len(dataset.lensX)
		start = 0
		tmp1 = []
		tmp2 = []
		for i in lensTot: # divide again the proteins
			#print i
			tmp1.append(preds1[start:start+i])
			tmp2.append(preds2[start:start+i])
			start += i		
		preds1 = tmp1	
		preds2 = tmp2
		
		tmp1 = [None]*len(preds1)
		tmp2 = [None]*len(preds1)
		i = 0
		while i < len(preds1): #reorder the proteins
			tmp1[dataset.order[i]] = preds1[i]
			tmp2[dataset.order[i]] = preds2[i]
			i+=1
		preds1 = tmp1
		preds2 = tmp2
		return preds1, preds2


def buildVectorsSSembed(uids, seqs):	
	X = []	
	for u in uids:		
		tmpseq = seqs[u]
		X.append(getEmbeddingValues(tmpseq))		
	return X

def getEmbeddingValues(s):
	r = []
	listAA=['A','C','D','E','F','G','H','I','K','L','M','N','P','Q','R','S','T','V','W','Y']
	for i in s:
		if i not in listAA:
			r.append(0)
		else:
			r.append(listAA.index(i)+1)	
	return r

def printPreds(preds, ids, db, FOLDERNAME):
	
	i = 0
	assert len(preds[0]) == len(preds[1]) == len(ids)
	while i < len(ids):
		lab = None		
		writeSSpreds(preds[0][i], preds[1][i], ids[i], db, FOLDERNAME)
		i+=1
		#raw_input()

def writeSSpreds(rsa, ss, name, seqs, FOLDERNAME):
	ofp = open(FOLDERNAME+"/"+name+".rnn.ss", "w")
	ofp.write("RSA+SS predictions (lstmRSA+SS2.emb20.iter_40.t)\n")
	ofp.write("AA\tSS\tE\tC\tH\tRSA\n")
	i = 0
	while i < len(rsa):
		#print "%s\t%s\t%1.3f\t%1.3f\t%1.3f" % (seqs[name][i], US.SS_inv[U.getMaxPos(preds[i], 3)], preds[i][0], preds[i][1], preds[i][2]),
		ofp.write("%s\t%s\t%1.3f\t%1.3f\t%1.3f\t%1.3f\n" % (seqs[name][i], US.SS_inv[U.getMaxPos(ss[i], 3)], ss[i][0], ss[i][1], ss[i][2], rsa[i]))
		i +=1
	ofp.close()	

def readFASTA(seqFile, MIN_LENGTH = 20, MAX_LENGTH=5000):
	ifp = open(seqFile, "r")
	sl = {}
	i = 0
	line = ifp.readline()
	discarded = 0
	
	while len(line) != 0:
		tmp = []
		if line[0] == '>':	
			#sl.append([line.strip().replace("|","").replace(">","")[:5]+"_",""])	#only for spx				
			tmp = [line[1:].strip(),""] #attenzione
			line = ifp.readline()
			while len(line) > 0 and line[0] != '>':
				tmp[1] = tmp[1] + line.strip()
				line = ifp.readline()
			#print len(tmp[1])
			#raw_input()	
			i = i + 1	
			if len(tmp[1]) > MAX_LENGTH or len(tmp[1]) < MIN_LENGTH:
				#print "discard"
				discarded += 1
				continue
			else:				
				sl[tmp[0]] = tmp[1]			
		else:
			raise Exception("Syntax error in the fasta file "+seqFile+"!")	
	print "Found %d sequences, added %d discarded %d" % (i, len(sl), discarded)
	return sl		

def pack_sequence(sequences,batch_first=True):
	return t.nn.utils.rnn.pack_padded_sequence(pad_sequence(sequences,batch_first=batch_first), [v.size(0) for v in sequences],batch_first=batch_first)

def pad_sequence(sequences, batch_first=True):
    max_size = sequences[0].size()
    max_len, trailing_dims = max_size[0], max_size[1:]
    prev_l = max_len
    if batch_first:
        out_dims = (len(sequences), max_len) + trailing_dims
    else:
        out_dims = (max_len, len(sequences)) + trailing_dims
    out_variable = sequences[0].new(*out_dims).zero_()
    lengths=[]
    for i, variable in enumerate(sequences): 
		length = len(sequences[i])         
		lengths.append(length)
		# temporary sort check, can be removed when we handle sorting internally
		if prev_l < length:
			raise ValueError("lengths array has to be sorted in decreasing order")
		prev_l = length
		# use index notation to prevent duplicate references to the variable
		if batch_first:
			out_variable[i, :length, ...] = variable
		else:
			out_variable[:length, i, ...] = variable
    return out_variable, lengths

def sortForPadding(X, Y1, Y2):
	assert len(X) == len(Y1) == len(Y2)
	tmp = []
	i = 0
	while i < len(X):
		tmp.append((X[i], Y1[i], Y2[i], i))
		i+=1
	tmp = sorted(tmp, key=lambda x: len(x[0]), reverse=True)
	i = 0
	X = []
	Y1 = []
	Y2 = []
	order = []
	while i < len(tmp):
		X.append(t.Tensor(tmp[i][0]))
		Y1.append(t.Tensor(tmp[i][1]))
		Y2.append(t.Tensor(tmp[i][2]))
		order.append(tmp[i][-1])
		i+=1
	return X, Y1, Y2, order #lists of tensors

def unpad(x, l):
	preds=[]
	for i in xrange(len(x)):
		preds.append(x[i][:l[i]])
	return preds


if __name__ == '__main__':
	mainPRE()
	#mainEVAL()
