��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      �G�>L�L>�U����=��Ƈ?��<�"��z>���@=#yý%I���Ac��A>�>t�?ޑ��(3=H+>�$�=ʻ��m_\���;?�n�>�7�>�=�j>k*�fȮ�zݠ��	=��B�!�n�x�>j����"<�lO>eN�>��Ͼ�iʾ�;^�
7=O��B��=���?���꽱�2�ۃH=�nݽ�R��ݾ�$	>Qد<��?��7>�=u=�r��4dT�&��=��X>|V�>o��=e�0����W�4=mK>�ey>��=�@=�ý�����ȾБr�:M!?�о��%��#�K	��
�ǽ�w��^d=�p�>W�����d�/�+>�&B��� ��z?�dK���Ҋ�O1�>��v�2K�>�݉�����"�Q���0?��.�����k�=��4�;ƽ�޾iT�>
%?�>;?�s�>�ZH>!>���>0\�D"?���>zh�k>ּ���RR��$��=�2?�[L�=����\�<���>�6Z�jp?,e?>�!*=��<OJ���#�]�l>�5�>󶒾m4>Qנ>C��N8��Z1e=ҽ�<��'��2R>����8xڽ*��o�#e�=�Y�>�%>�$���v�>���LL&��˞����>]|�>c�>�����U���w� 5w��������)��>p�� wʽgh�>����x���ʼH���Y�˽����r>�������>����f��vOx>�J�>�D���V�=��h>�2�>�yL�Ur>�)�=�1���r� �l=Tޠ��0V>o��=`E4>S#�����n�H梾��w=�'�Yн�=��=�z콕�
cs>
a-=OE�pP`>�dK>��#�	�������A���m�=���'�G�ݨC��������>�t=UZ<��=�o==c�>���r�Q�\��=�#� &���P�=���=�륽�>��=���a��>xw�>���>����GM�6A�>����+1>�9�=Yh�>_�dS�>HAK?��>֤�ن�^�>���Fg��w��B��}�:��>ę>?�Q>B_��0��H�#�d���=�HB=�AY;9����O��0����ʢ��Dힽ��U���`u>kr\>���>�8�lN�>bp�<�b���Ao<;06>�z�>��*>�>�sB>1�>�>b���a�mս��R=+g���>����}j�>U��>�L=�N�=B:���j[�*�7>d�p���>��=A\??9�=Z`6?���>Rr�>��>`��A>,m
��e����
�����uP=�9&>�a��i����|>u�>a�����)>��>3�=�H����;�����=��߾�� �T�>Gg�=�{:=H��_�=��==�V��h#>v��=5�>xr�kx��K?)��>9��=^]=�Ţ�4�ݽb��m�J>�۷=�'?'�:�~�>�T�>1'�=����,�̈́�=��3��SQ���?g~���xB�x%j��#?��>��r���	?|�
?B)�>���!�J�?"��>z z=��=v���Qq�k�'���(������E>��?�f�>q~>6~��,�~y�       �=i��^s�*1?���?n��<���j��2P?>f>�|>�����i�;�TK�L7߼C�/=�G��2=���>r�\=_U���E��r	��ۣ���������)����>RG"��S�=��8<�ɮ>t1F��돾�uS�)��>u��`��:v��/�����>W��>M�A=��Q>�7b>�Ѿ4E۽��B>}�/�A����fѾ��`=�}
�N?�=~��<��>[�>2�=�=>^�<�J=C2��hˣ��1��> �>`>��>��>�v�U�>f����>r�����>��>�T����>_V���&��>}�>����ý��<��s;������`���`*>���l炽L���2�~�=>L���:�>NU��Ĭ��\��̲=b��wuھÑ��6��=݅�>�V/������>��>����N�8$þYݩ=��p;bX?�w�����=煆��_`��)>t�R=�g�f�x>�
�����>�9?pX�=�V�=�g>� �>�k>pn�<zY>���=@?�=�u�x�V>n3�=[���R�>+��>�W>��w�B��V5?�>-6�<�R����;��u¾|qC���>�>��>�=����>�>�T?X�>x�0�U���D�=[�(=ný����-p���ya>Mx�<Qł>��I=�R��c�F��9�>�P�����\>�`�>J/����b�Y�ؽ�i|�?��0�>�z��zH�l?<������ھ����󄠼�P)�[$�=c6>�E�>
����+�=3ۋ>�'��D>^ �>ph�;�����&M���>=�P��?#ˌ��1<��K�gێ��bC=��<���#>\�3>i�$�����ɾ����>�k=0 �=O�>]�w�����U�E��A|=)��=!����>���=j#>:�>g<C>��k���3�>�缝�t��XO>YJ�=�M	>������l��]>m��='Hj>\B<>�T���1�w�=��==�뮾W�>&���yO�b�=# �>?虾~?<%A�_�oB4��>�5v=^��=��?[�޽��1>��K��4]=�&>��>#�]��yO<H+k�G_=�v>\ A=�m7=ɋ�>�I�=i������<9�B>
���D��=��D>��Q=2Ln>>�)=��>.PL>�k&�i� =}�����=!׽��4��<�>bs>nnk��=	ѵ<Vw>�\��CJ�>�������<~�=E�L�/�G��ʖ�g ���>4�o=�o=��*>�>��=z��=�����\;��O=��;���=U�2=�ۋ���#X<�=>�S>�J���+ջ|�>��?>�X&=L�I�o��<1��>�cK=���>Kx>���=��z�C��˷�>Z�N�>zَ�6��:p��_>�����<�O���!>N-ܽ��>�U�>/tk�x���#�ʾ��Ƞ�;�&"������>r��>����3$�����lW�<C�>�#>�/o�_!>e��a'g�R�7>U��>{L�>�)��O�ڼ�U���� ?�=C�ͽ���>x@�='���Y/?=w�<�����ia�>9b�<g��wl'>H�a��2�=oQ�>�t>����?>e)��W���ܾ6�6>�Kf>�@�=��v��ڌ=�6��`��w�>Z�>�4��V�=Ӭ�=���=�>7>�
?>������>���>}}>t1�vp�*ƾw��'U=��>�{y>9K>2�� �m>��>t�.G�=ڜ�=G6���6�FQ�Z(�����	����?��?S��<�	����=���>w�=�������>��D�u�j{ɾ�q����>��{>�'�>A��>�����i�>�=�c�=�d���t=��">��a>� > ���n��<���4��q��_�:=�U-�L���>�n>�Ns>�ű���>K{�=w(ʽ �
ݿ=��ཀX�;`C��|<*>��ǽ�r��<sb>x�0�����o��\��>�E���=�������_~�>b�>�߽}����d>�W̾�W>��=Y��><�������ߎ=Hk�=�>#1ݾC��I!>�c�>!�彖��-E��=��<���>��>�޾�"�mv2��2,��*��]e�����m>"!��?H��	����=`�{>ƚ���Q�>|w߾�`Ծ{d���K�o�Z>7K���7>�{�j��>X_�	�ϼ�y��t��9(>�����	;qz(����=�*%=��5=:c>�>�JF>
��>v���U�>>�N2�W���vٽfx��WE߽�����n�1��>��>%*�>�ޕ���W��ϾE8��F�2�e>�d����Q9>��=('�>��=e>����%>��R���ƽ�S�>m��=�a�=[��=������=Z�W���ѽ��#�����d�Y>A5�<!�>�v@=���X�-�m2?=\�}�n�F�JK��y�>�t���~X���oi>���=n�>�r�=�7���6�dQ=���=:���
�	�<���<v�-�?�a������\>��>ߚ[=���Ұ����������� 3��3�=I���%��ɕ>��>���<�ħ>"(J>�E>�s.�<�I> v��i>�x���>ei;>�g� ��>�C�>��:\��>�y��v1�<r�����u���=>]L>��H��=�;D�1>�D��q?�U�>���=*4�>aٗ=��b�,='�H<����^8�#�9��(��W��=/�\����=���+=�,ʾ�!<��.�7���{�ᾫ�Q>�&�=�3�>XV�>mu=0��>�[">4�M�º_�O�>8[B���>��=�)�<���>��������`>{�!�B�p>11н� �>�a��=i�<|��΁��lս�i>��^>�<d�>������(ܾ�����L>���=-4��:�9w�>�h�������b�����l��G�l��|$�	�ʽR;���:n�#��RD�>t}�>�j���+>{�>�N�>��<��Q>|�=�j0�ol>��ݽ�>ؽH�>Ocv>ր��b��=���=�f�>lpb>-j�>���>���:���LD=<�������A>�T�>W7ؽ|Yq��e��[Ɲ�2�=�;���׽����	?T� >�ə=o�>F�@=ԭ8��o��3h��R;�>/%>       R�&��4������=Jpp>G��>'6�>Z�ڽZ<X�Ƚ�=o!�w]>�սO<?7B=��@��$i�b����陽VI�=��콹���-�=�)޾L�Ӿ2���<e !��i}��<H�Ѧ�=T�߻+~�����{?I�=��q>#���sٱ>?�<��>��W>�耾�Zw>S�c>�ɂ<0Rn�P�=�������k=:��w,��b�>��B=Z>g��<�����-n�Q�"=�0Ծ��ؾ�W��;�=r�:�hB>�g���<e>�n1��A�<�O>��L/�㧄�J��<D�⾏z>m��Ӹ�=$K��>E��u��=�����J�Ь >�~D=�⽽f�(��2U>@wj>>4]n=\-�<?�>
A?&�>��#��:�<�W�}���>�Wн+@��Ū� �c>�C�>���>ոe���B>_u�iv==���7��>~�>�����V�x�>bV�==n����ؾ�N>A����>b��Q�!��? >��ֽuư=(�þ��n�������Zs ;p�>�ߞ���<X��<
��\P�>�+>Ԇ>ݟٽ� ž%G=��~�\p�=�+>g�%���*>-6F>?��=n��ݱ��r���wZ�nȃ>|�-�qV��ާb>l�!�9fƽ�W>���>f��� ����A�3=Z]���@ >��ϻ�l�N���Ѱ>u���*3�<��%���9�I���X��>��K��K�=�I3���ļ�ƾǅ�>���p���I����=T�h=���=̔���.��ֈ>'Ԣ=�K�s%���->(�-�þ|>�[� =]���=�~S>���*�g�=rd��ַ�t�ڽS���#)�>e	�,&>l����k>�l���?�>t��> ��ƾM8�=�R��"X��'~�o�=W�:�<��D���;衆�K�p�Q���ɶ����<!b��ÉY�P�%�˽k��=a�>ϸ��?�|��
�K?>�󱾯]�<JF��� �=�+s> ��j�벏>4�-�Ѐ����=��˽��=>	(�����K�=�@�=!5>o}��z�)�>��z����=���X9�	����>��v�;�+����>z�b���?�S��Q�U> h�>��#>� �>��c�9w��Q�< ���`��>h�e����=*ى�u��=�lS>�۽,�>ĵ�>�^�=C =��x>�����&>x�>����U=�탽�Na>)���T
���3�j�w>�Ѝ���h�� ���#D�E#��j�!>��)�,##�\��N�q>M�¼iM�>��>� >(��>G���!��=�� >��3�8�Ǽ]:�?�W����L�>L]9>|6��>S�)>��?�8�����%m��!*�E�L>p�d
�>^
x=˲A���>R�=?�_��謽6�#>���>��<�Փ�7�9>��>{%�=1֟�{��ɏ�_�>>���=��3���>��T>�ۦ>� >�*�<�>>��;�m�>�3��(���z#�3.�>��>'��>bL���C��N����[�0�>����8�>�T>�9�>Kc���#G�=��>tZ> ��<|���m@��<	���>��C=��׽Ţ�A�����	��=�����=H�n�4�*=�����>áپ6�����=鵢>R)c�ne>T_�[=�>[���6F�>=>6�x˾�z.>kqڽ����>w��B>�v��>��m��h�<[��>�>�a��ξa��>*�����g?=�t�<}!i��m)��+�>�y�=wN�=�w�����Vt�=�
������>����: >{�l>�i�>�O�*��F�ҽD3,�$��>���#�L>�}�:{�>��=O��> ��Z�(>g��=�[��F�><����₽�1>w9��o)����O>�G8=�IV�%��[��>T�[�b�>X?�ܵ�<�{�<�'�<��>�����>��>6�=�����4����D=4wa<���=������A�@%�=C��%���<��_�#=~]	>a��>���>�#��y
>��>�)>\�=���=�|>P��B���}׆����a[>��*�I$>���>�r>���>}/3�ə��dq�|�1>��8>�	��$��0"�>�L��O��\�z>�y%��hp=a=�=B.=0퐾�,�=�K>�D��'�� א�)2Ͼq��,=���=)��;����	��þ�W�"	�D���Y���=(0�>[�m�S�ս[�=�=g�i�=���Ҩ;��!��>b�2���W>���z�Z>O.=�ċ�>�a�>��x>���>qG���o>m|'��pռ�ȸ�¥
>L��=�(����}>:Rؾ�ࢾ��о�s��X�p>EH]>�,�e����U���P�q�>��='de�d�9��w� \m>�]e��>��>�@��?B>�T��>������#>!1�<	:>�m�r�">�'�=|���?܎�5až��¾�{�>���=6�I^�<��hI>N,�<��'>���;���G���0��3뱾���A�������U�&ձ=.p>/��<6�S>�a6=gU���v���_>��T>F�I��s�>�0�=`�E�s�>x�%>L�>��=�)>�������8֧��p+�>$�q���e<%�>�G�����=�a������;㹾�z.<S��Ye���������^��=--�>i�=#X.���=�V��Z� =ɇٽ��>��Z>m7ƽ��+�3��=s�>��C���'=��8;������=��=�ʠ�����x�>:�(>�����E��.Z>8#g���>|�����c��n��>Y��H?��5>���=��=�v
>L���%��')��mk�tՔ��T>"�>(�q> d\�FV�>q>�$��;��`�;�Ov��N�̾��U2�<����=A�۾�Zh�ճ�XX߽��Q>ū�=	��<{ei��=T�ʛd�;Ӥ>���=�K?��=b:��Sΐ>�����?Y>Ё�>�"7����.��=�R�9����XZi>#ߘ>�c;4�_>�XA�0�]�L"�>���ҝ�l�>�6F�>�Լ>Y`F>O?>�Y�=A>����<��w�><T�>ԩ�<��j>���=?��=�z��������(>?��<�rT=���7�ʾ�&>�ۧ>��j�U~$>i�>�O�<�      �u�YH�>0G�>2Vʾ���=A���(>��L<bc=Ȅ���>��>�u�>��>�ϲ=�sw����>�����~�j�Ľ��9>�"�>c��>�͂='�>����nY>��>���&��IxP�\�>l��=qv���V����ZB>�)?)#/�[9ʾ�]>��<R��>�|>���=1��>o�5=�|�>y]>��Ͼqɻ�?�#>'�ܾ<G����0>�f	>��.��l��ۓ�>��k=g�>ė>�N��=�V�ξ�>��5�>r����(u����Y:��2����>�����<|��>�ꍾ�A���t>��>?�e>P��@�p���C�in����d�>99�Q�н�p?�Ǎ��z>�$�:�پY�Ki�����^J-?K0?���'f��,&_�\W�be���?>\�� L�>��:p!����*/�>��|�D�	��-=2y��(�>�>�J">y�>���>њ���ۯ=^����*��<��=b���[�=�E�"ڗ���;���NR"�����rA>�;�F�B�� j��0
��>"R���>�I�>�u|�����Q&�=>]�����>�b����a>"�>���\9�=i.]>�l���<���>�#��!�E�'���>O�	����=��ʽ�R�>r����Y�>���=��=�m4�>�O��W�>q*���!�>b��=��F>
9�=���<c�����=J�y>�|�=q�?����m�?:�þV��>|p�G|l=u���m�>��(�S���/��C�<��tWp��H�=qX>o�߽�{��@�S͜>
 ����>����37��m�����>)O����3>��Bx>f)��󎈽�>��>��]�"����>�5=��)ھ��=��v>�>���>$��>p����Z<;�>I>�������l]�<���>��<�����y=�7>�N!��M ��]�>�,�"�D��0�>���>��~>�d>xSy�aW>�pӽ�w����4�����>g$8�*�=%@p=�'o�������=N��=r_���\>�L�&�CH*:Ra�M�>@��=�s��iq�>���"hQ��y$�h�~>��X�
=�a1�x��>54;>&��>�Q!����=#/5���=�ԾA�H>�L�<r�̾�;G>E�>�膻Xے>g%<!��{��ޓ��J����T>�����=Pы>��<������v������%>b6�<߯����>@V ��;�>#fҼ�1:�N���nU>h�>m�L�wJZ�B�>�t>�>�$K��`
���:�=�����V������f��> i��U�ھ49>~�i��"�o��>IJ����J�}�>M��>��>en�=�LW�JH9>���ͭ���Ǿ�r��wD���>+��_���)>t&���ƃ>��>�:�=�m�=~�#�v�~�����@�=�d���"8��h����<�P�L��=�'���y�0K�>'}���3�>~�R��<u>�p�="%	?���=23�=^Yʽ�������BW\= R����{>�-D<��J��������xY�<�f��T==��e��_*������><�?>𠽤      @�?C��\/�=|C;��T�=��?�����?�V����?��j��N$���+��U�����>�����?8��=,G;?f�`�j�?���?��@t��>���>	u@rO�?����K�>
�#?r+�,WI����?�a? h?��?�_ľ� @㴑>�?�WF?���	O?˽n�*�Is��D�����->X��O��%?Õ?�3^?W8@���>�I�>�&���>�?,��>�Y�a�N��=WT?��-?s�v?)��?�2�?��'�P?�/���o����>��4����K�;ʷ>PM�C9���49�\S;?�x�?�r�=�:��e-���@@g�?!��?�kO�x�k=t�;�?��ý~�>A*V>����d�S���%�bF&>����^���+���^? �&?�>�?�k�=z��=AB���ߔ�w?��D�����?����?,��?mz��J>K�2�Ĭ�k��>�����5�?��r��펿\x|�O�,?�v>q��?ay�<�޾�J�����?�}�?���?� ���l�?�a�=�U�>�ɲ�mB@���� �1[־�:�?|�)�V�?m��r�߾ت��1��=��T?�H��)A��y���ÿ��7�cY���5������Q��>��������5?�u�u/i��IV?Nh!�����?~?HC[�oh ?k�?E�?;{ȿ�t��y�?��z>6�>�j�����F0���ࢿ|�οqv���S׽E��>�8�6Jk>��4�ﭹ����]��?rǎ��n�PQ?��Fտ�o��B�������?*�@��>��j?1�A?������@A�� 0�=w/@ `Y���k�2ٰ���?�\y�[m�>-��>���?�-'>���o?=��v"?�����T@����¼��d���Y? �m�x�U��$���%��d��9�<�6�쿩�ӿp��>�bj=O�e?�s�]�ľH_���J?��y�tS?�;k��?��m?$��U|���Ԇ�}�Ӿ'���Í �8�?J��?��Lֆ>P[���b��U)��e7�¦���	��;�?�'?]��> �?\�?���?�=�!��~�i�c�+��PA>��?2�'�s�3�n>����{?�lE?Eہ��u��������gԜ?pɑ? ��?��/?�"�䬿��+�� ���g�?��c��E�?����/�4�ed�?��>o�P�2��o"�?�q�?�?#��=GZ��˾csc>Y�,߿�.Ϳ8٣?�2?��<��?u��?��?p���/�>�?�K;'�F��Mp�?ys�>#�>�;5>9�S�\Z?p30?�6c?�X�?ՐĿ�v�?���j�4>&Iv�{�g?'��?�����:��D����{�>-@���?�!�ᄂ?�j&@8�ʾJ�Ӿ��>���?�����J�]�{����?_0�?k"�?,�~?<S�?Z�?�4E?qa���u{�?�,?͍ܾ��f<!�=Jl߾�Ͽ�dl�Gװ?�W@L򟿱�*?�?�cM?��?6~k?�?\�?p����~�= �Ǿ��(�2
���IW?���?��>*)���}���E������˿Fİ?G���G��?��̿m��>b]�M�B���z?�
}�G�R?`k>��>@�мԦ	��0y?>Xþ��Ͽ�8?���?�?
�7=(       �̾���vq�ps,���>J�W=����&҈��o>�"��o�>�{#>��>;��<�+>w>�y`�')>yS����;F�<6~���;��G7=#k>�~K�g�s>��i�
�2>5'%���>�$Y=�#��@ܜ�}s���+N>���>���(���R'�(       Qx1>���=X������<�C>�{� �9={5!����>���Ѻz>@�	�H�9n��>@����N�Iti���Z�ǡ���5���D��8�X��3>C�m����>��>��;>����񽥲"�E]M��]>U�=J]���ǀ����T>s_]���.��Q�(       ��<���>d�=<�=1�/>FK�>���=�=�j���K$�ѐ>���.��0���函�^��|��>�KV>#_�>t�,�G|��x8�=Nuڽ�e� '޽���>�>�����>��7>�}
��0>${��A;�>?_�>һu>�L��L�=�v<C�>       �;��\���:׹���>��?1���`�>ZZ�>N��pc>;�9��i�/�>�� �߷��g�=S�===��*���s>�.t<�
�qs>P9����o�1B½�?�~> ��M�N=���>	��=^�f�y��>�yi��`�=��=-��=/��=v�,>�n��5��I�>�6�>@ʽ�Zr>�F��^��LX���R��5�<�P=>&=�!>jQ�>���>��꾈7���.�C~�\~�=���*9Y�޽1>��<^�G��
y>���8�@����>p�g�i=��H>gս��@�I⸾��/>$��>�>>��0��>i���;����چ���l��dν7��ϣ�?����d>N��:;�f>�;>�����P>�<�c��=�>�;�6�=� =87Q>sQu>�&�>q�?���>$j�=.��>����R��:�7>+�>\Y$��!��1_>\ǖ>�fȾ[��=��T��>k2�=��>$XD=4���\h>�Z9�>�-���>� )<�Ҿk<�=<�5�a=RU= J>\M��e=�����\=ઍ>M>�O�>܀�:���؃>K�A��L�>�-U��l��(�������r���A���Z>:�q>wZ��Oۅ>���<rG��{>��=���R�����v);>�>���]q���v>�w>�_k�.�����=ʨ�= H��W O>>[.>&b�LH>���>{Eľ%�t�6ZJ=`{M���I�C��M�>��>p�>��_d>�֎�>L>q��:>*&_��n�������$����r��=�e̾��o��w�=5���i?�5�=z�?~��=,��	�=X V= ��9���=�=�!�=\�<�Y�W����"=�[�>5�1��P���>d�	!�=/�����B�>*^ ��§>{�;>�ď>X��=ݻ1<r����b�4%�`��>p
�>�3n>�ٽu:=�@��맽6��>�
�>:e��Z��<4��=�#+>�B�����> �
���C�4���QHM�	J#����=/�{>\�ݾc�=K`>f���B�^���+���%�`u>�ib>�w�>(Q�=Y�E�c]�=4w=)�/����n$=O)>�3>�q�=z�>Z�������'>^C>����^c�������;}>��i<>�E�>�����B)>EU��*� ���2>o9>j�U>?A]=D�*��>������\��>g�"?��'��#G�3E�>�A��
�>=�Ҿ�9>V� >Vϖ=V�>�S�>�ő����2_O<]�#>+�?$>ܠ#�*(���?�zν`�	?��A>�&����D4�>�3�=��6?��=���h�^��\�>dw�=��>�D��5Ni��j>��=G_�=��=��8����>n\��D >�������>��_���S>��>�3�H������>��>�3�>�\1���&>�P�*�G�e>1s>���>��M���>�g��
zC� ��=6\&���O>̴�>��Z>��N�|q(��*>7���*��,�>@ٽ
2p>-h�>�V>
���F�?��>ׄ˾����>��=ų���B>�=tn�(Ny>�C�<���=9⽆�?=�L�����>b�G�=�1����n=��>İ��ʯ���@�=C�l���j>�X>����(\>��O�<��>���>�:���p="�-�Y���:j-�k5ϼ�ׄ�g�>�ֽ|�R�����T*>)F�<���>���=p��>y@>��u�*���`�˾S�=���=$���������T=I>Krc>�Q=����S>\�_=_>��Ľ P�=(���S�D>rz�>A���Y�U��:5�쇳��ܪ�r��=J��;����/�[>�W�<�Z	=�<-^�>aW	�r;��y#4=�, �\���K����RR���p�7{C�%���/=J�(>��;�I>��r=UP�+��>�;�>�s�#����>�\��C�
< ��=71A�����$c�=�e�dA�<nٛ�`^ڽ�oK>P��>�mJ>�g����~>��=#�ھ��~>�L>��>\��<�a��2�>��=~2=G'[>*�'=;V�>�
S<��)��&">�1=ՏE>�����ھ�h��(c>�:�>���=�Re>H�r��j����Bi>^�=���&ù=bd�>Z�O<��F>�>q�ݽ����!{=�ᗽ�r>>�^=��\j��%}>g��5y���$�T:M�B= �4ꣽE>[xL>�m�
:�>�>��Q�^�f>���=vEV=�"��Ǉ�y@4>|�>�����t>�z�,8!>��X�/`">ہk> ��<'�>U6�Ə��+3f>�d�=�>���+�;I>ȹ>V�.>��ʾ�z־[�'�<$a>
� ��₾%j�>��u�\ƻ��$>v�$�T=`>m4Ծ�T���S�>�K�;M�>}�Ͻ�_>]�<>�����W=)dG��$�=�\����8>ʉ�<&��>�:��5���Uǽ�w�=-�=K����i>�oƾn� ���s>k>ȋ?�<۽"�=�Ž�u��Dc>dʡ=���<���3�='���6ن���?>l������L�>�Hr=aŰ���n>��j���н������>�f���>vj�t�=��e�>�D=>�Ծ�4d�IG�>{ݽ��>E��= y����ƾ#�m>���=X Խ'h���r/>l�>>�LA�0	=`3�=����Y>���=&w =�/�y{�>+1��6ޫ>V55>����(����<宴���ۼ�H>���.��>|��@�>ͣ�������=���>2>���"--�"{�ͷ�=�,B>+�ڻ�
�6i?SP�>��<��W>V����>�| �=u�>�,7��6�=�8��4
��PA>, ��_��>�BS�R��=B�>��&=�	1=���ў��"�^�q��]
=��p� Y ;M�)>�^�:�P>�N=3��>^��>�#���Y~��[;�V:��}ID��I�T�i��i>��[>4gM>�I�>r/(��b����:=��=#e=�+��l3�>4Ŝ=��=q�"��J�=��>Y�>�V�=�^=�7�>"�u=T,�;x�QB3��K�>�p=y�����>�V�<j�=�`�5؈=�jJ>�'Ƽ�v>'��_��M�I=y�>�Rξ7�*��T�#k��o�>AH<��=��,�"yȽI8d>?����g�ZE�>#!-�����)�==��>(       �e�=|�ѽ^	��j�1��Aw>I�N=}xq>JvQ>�5����>�8Q>���>��=��=�Hr=�/�m�b��t>�v�>��KR<>s�>�‼J�˾(N����b�w~"=�ܽ��A>~f�$F:$s>Z%>= vＰq齆?�=s�ļ4���00���:�<(       ׅ��Ӻ۽�D-��KZ>n�(>�@O>ͽ��Z�3�ý>�>���<T�d�8�$=��F=Z',>>�>eڕ=�=[�l�=�mr�O�>H|��;u�Y�ݾ�
>�ˮ=K���v>�tk>��v>)kn��cq>�	G�#�r�J��>$�Ƚ��>��ҾFi��X{��      ����:�н+
�v�>%�<��5�m��>���=�>=g8�>>�z�yu>�M�z��z��<&%u>���e���U�y=}�(?={�*ߧ=�>Y�
޽�?�>hu������~ > �գ>�<�=�0��fc�6��>&�>���Kϛ=�b�=�+Q���"�E�s="t�>���ƾO2�ߋ%>0�;�\���}�9�ݏ>&n�%��<a;������n��h���}����#=��>9\�>յ�����̄��kT�=�y�>?��۱;��RO>����<�>�B���@�֘T���>c�M>�t+���R=�9=5�z>�?��M>l�O<��9=����������F�~*t>����c�s?�U>�<2�!EZ=Qeľ����h=,�ܾ������=�GŽ�l۾�_�!���4�rm?'G��<dI;�ƽ�$�����>�h��H��>k/�6�P�Ҥ
>�T>"����6�$ɚ>��=X�?>��f�>�Ԯ>kB>.�">r�a�#�
>e�V�ʔ�=9��3LѼ�(�=>,g���=�QZ�z>0����>�.>]�=Q��,]�>AC�>.���T�R���N;>�=/�?��)H�zdT>f1>��?��v=��b=���>E�j>��r>� >���Q(���`�0�X�d�(���2=�4>�U>.�������_�ܾ��=�o�8n(�>�>_��;w�/<���>A��={`^�0߹>�E%?^��??���߾�י>��x�4�����=YU"?���'��d�u>e�>��=�f�=.x������/sK��;���q�5�>�r���2���W+��Z����>���c���J>I��>�}h������v���>���\о[t>\�#=y�n>3b�>;��=dۧ<xW۽5t���O��b�>v�>**0>e�k>sH>q�t>x8R�ik8>Q�(�!��=�z<�;ώ=Y�u��KԾ��T��`�>$��O&<��>=u��|g��5�������mX>/y�<�'=/x���È>�N��H��]�=ǄX�8�
<�4���"�>�a���>��H�z)��lA-��=4�'�9C��_���`�=J@Ͻ^c�=\X���\g=�>r>\ <��>��:>2i=}��@��=C�Ѿ�K2�*��dMB����*��>��=v1�=�FC<&�/>��:>/#Z=|�>�=��Ӿ$�5�w���g�N�8�ξp��>n5*�T{�=�=�,>�+a�������>�Ϗ>�$<������>���>*���π��;h>G攽!�v<�)'>L%�k�X0��~�=>�9��N�C���p>P1������ ����>�Nv����>j=��M�I>>/�=�>��>�(>w5>��>�t��F%8��ӧ<j�����pK�>��
��6��Uޔ��v�>��->��н�꨽��-�3\n�q�L��n��a�.A;���V���W>�Z&> b½`��>2��=L�7>f\?��>�/m���R�>e��>hI�>f�='��>�X�=$<�;�>�5���Д>��������ej>��g��V|����=`>6�>�<�w1>����pg��J'=�M�g�r��^���K<��;       $�>����b>��>�Ŗ>�B5>�>��=� g��=׆�<��>ə2��d��9>�v����>э>����Iq��c�>�!�V�=��Z=C����͍=A��x�>I8�=�,��]��2��;=V�o(�>8I����=Nf��C�>\2�=����nž��M�>��>�Q���8�<�Žx��>L�<)�$�l��>��j<���>��T=Ydt��ʃ>�����,<�� �è��Q>}��=�+���`=�����
=5��=r,�aZ�*1]=��+�	"л����+8=h��>��/�T=�	?��=P��?<>��<=���<�����c>��=��Q����EnF��i>�޽V��=#]P>y�L��@��γ>|���y�н���-~�>fO	���>9R��UI�0nd�F��~�Y=�Y�����/��>���=���>s��^X8��g��s�>Y�=G�>�闾+�����>m�/������ >�R>�?>�{վ]7�>�*��)=��1>�'���`=� r����=X���ž�	�>q�Z�O=��ν3q�>	?=8l�>���m�@�|^�+]|=��|>n�>"���k��>zd���>�>0x�=Ȕ���T>�����]�<���=��8����=y�3�������;�뢾�䨾;G��l�����>��3�XE�>���;x&R�<%���{�u�(?�SʽҺ��z=�j���*�JU1���>pپ��?�ֲ���h>�a�<9�6�DIn��_�>�pԼ���=e�=W8>ڵ>>2Z���Ľ��h�>ʢؽm��&���8��r#�;�Ϡ�� >�R�>�_>���>i��z��|S>��>��B>��{=;.�Lc$��.������Z�,�ѡZ>�٪�}�4�-F�>a��=��>MM�=%$��P�m�h]�=�_ ?̙i�}���U��=
t>��>�->$��=��>c�r���P>�o���q�;�O[�gtA���=��EA�=􎴼���ͥ�Uq�=9g�b[S�{v��
5/�����qn�=�'5>�ф>����_n�?B}>en�;���������I�=����-��w�%>��>�k�>o��W��>��P>���+�j>�a��
4>�����-�XP�=�"@<����Z[��(�������>��H>i�M��������w]=�?�>�C<Z�>4v2��v��<�4�߽)�>��8���VZ;��	G����>�
�>���>M%<���4qԾ�t ��C'��F�;*&��O0>#�>HkD��3=!��>����#J=�=61�>} ��{Q>�F>����v}Z>�^O>���>�������=9�>��F�-L�<�񽼻��d��>�s���[N=EP?�)�>�p�=�>�>�a?�W�=t�=�XG�C�#��#�Y����"��҄=�F�y�?+E��జ�f��>0U����->�\�=���>�#�<�d>�6����T>9ֽ1�7� ��>{���Ɣ>����cM�=���=����d-V�R�>�Z>-؅>� ��w�>{^����>�5v>iע>����Ze��b>�@�>��?j��˸���t>6C �)�^>k��>�JR����ۆ��tW>�<�ug=-�>��Q�l��=i��b��H
M�DF;�!����+�qm.>�ޮ����(�>kS=^4A��r^���c���ļ���=��<y��=Xs��� ����:������~�;��>a��=�ȝ= ���;>Kq��%>�$��Ky>l��5�\ג���n�t��<�
>����\g>�F:>�*���}=�\���������=q�нx��>Y���!>���=�p����=y
>`��P�-�j�j�虝=w�N���_>��%=,�[>�ua��1>����4�H�U����;YC>�M��؉>��=TE�>������H>��e��l�>���=�j�>���<M3��)�)=���=c�N=9�{>�)��E��<-6�=�s����,�_3�=P���� >��>���>���>�պ=j	>Qz�<���>cfh�?��>�mL>m�>�OQ>��>�{>�XE��J�>���Z@=қN>z\���I� �B> >$�CiE�wg���$�=�>&��=V.V>�7ɾv>�}ٽ����.1/>�n�<��5>>5�:>�GQ>;�L>��>;�L��Ҋ�2�`>�f����Ɋ>�Zs��2	�W4�=74��I��%7�J#�.5k��AG>�ht�ϩ��+ ���={�0Ò>Aɫ>�1�=��L>??%<2�I���O�=��a=]z��l��>�#n= �=�[���C�1��>�v�=sd�>dsK��~���ݬ�pQ�<�C���>U$%=0�\=�z��`c=�	����=U�>qjP��\�>��=Z͛��6�>N��=����ˇ>_�H>\ڰ��$J>f����<�+�>P3�=��c>�<��=B>u�����>�D�/%>boE���<%>�>����y>`�R>���=��(���n�|Y���a=�E�>�6E>7�a>�<q.��C���>�G>� J<_*r=��q>���q�<>�]���(��Ђ�3h>�5��s֝�@ߥ��m����0<�Ȅ>�<n=�=P��t��fG�06����i������>��?�ܡ>�D�=L�]�%�^<�?�8�=k��=��&=/��>��̾D-���>��>5ϫ>�柾�I{>���S�e��@2>�#>��J�O&�>t۽j���p�=�5W����z>��I�z��>�,$=̼�<����!>�X���>���~<Խ�>�<!�D׊�����&�E;ށ�l�u���=k�n=�M�<?�t�U9*;|>���<�ƈ� ��=V ������<�<�h�YǊ�Y�>|����>��=�>��@�9:>oش>��?&�,��b{4�>0��I�v>��@>ݜپ�>/��@b��_A���=S�>��>�>�u��-���c�>�=�
��>����o���>:\/����'�� �6�D>�K����	�Z��\ �tR>^<��O�8���m�����Z ��5��=�Ţ�����N���">������}�Am�����T��"߽Xt!��&�����g���`��>sk"�:��=����7c>+��=��B� gǾ����<q>]�*>��?Mg�>o>�?�>���>cg>(       �B=���D"�>j��>�Y��i��ה�9J>�q>�xp�P������{[��PZ�<u��>І��T�<U]j�;>:9����=�h�=%gJ���������.М��Ce��%>��>�T�=�b>�{=��P�*U�>�d>_��>i>+�}>���(       �j�=�h���bI>�	K��y�>ٰ>Z����!�<M��=�IL�I�q�7>kL��ֺ���$����>�\J�J?���I�>��L=��=�����:G���>�u��}�r>4���͞>�a6=�3�Ń^�Vˑ>�膾�?߻ZTY=���2Q{��_��V:o��      �S��U1�W2?u����̨Ž��<�*=>H=b/����f�&�@�j�n?�b{�D�>�8�>���;�
h?7	���߱�G����ܮ>·��N��Ŗ?����w�>3?*>W~�=��Ƚ@P�!'=��$>Ţ2>�Az>�W�>̣�>_ż�.������Ya>6�N��Ur��^�>=A�>g4���>�;�bB>:��>ϓ��]�����>ė�������o>�����i��:L�5�=�ľ�`?npK�iv�=fy7�sH��q��",��uW�>�"�>�kI=��=żl�+��侎>� �z����;�[��Լ;��$���~*>�آ�Eǯ�--����=��޼v1�=:����雾��<��~> eb��
=t+0��m������T�����1e>n�U�����>i��ԡ�>�콻�	�����RV>+^���ܽ��o>��'>���<�R>�ͣ=�׹>�4 ��N���>�>x)9�N�E����9z>��d>2 �=���>V�=@>+�٢:>�]=�E�>���>�b�k�\>�����/�>$�=�x-����=�����Ѽ�?>����F
0�x�n>9��)*>@��7�<+�x���N>�$Ǿ��޾�]���kc�&���ϩ>5f>e�.�?�5>��`>|���(.*��}�<�Y�A}��$g=��	>ԥ��,>�c�=M��ʍ�=@�����=<����o�<�=�I�C=#�)?|r���c�M>x�!�i���lЗ�׀���߿�p����ⶾ��="@�=_f�<��~�¶���H;<j��Q�%�>�!�>�]=!��>ຘ=�>>�YŽe�$����>�0����<vπ>�H��"��&b� ��>Wܻ>�7
�#$н���=\G�=��^;9�>���I�>��"?����kɽu��=���>q��>;+�=�=L1�=��@��+i�㽬�>>��Ǿ]5�=S��1�w> v+>+4��ƅｈl��=@>D��> ��>��>���P�>Z�>�a>��9>τ��b1����={�m>���>����1�>H>������=&���˾>'�?��8>��N=
����>q�<�*���;>�-z���¾���>��/�8l(>;��>Uų�?��r�P>�D�;F>e>T��=�=޾����T����������)���Fw��z����-�j->��	�.�^=�{�����طO�=P�=�y��`4�>7�q:#���p�>/��>��۽�P�>��>���=;�>d6��]�����'>1�ǽ$� ���?+�~��v�>/�:�L��>�@�xe̾��ԾkK*>��.��4�i�x=��><
?1��=j��=�U����+�.%罄�%�ڡ/?vb��<[>U?¼���>0n�=4*�r��(�~��2���]�'׽LO����>2��&��z���ԑ�^�>�d>̈́U���/>,�[�*�ϺU�	�W�ʽ�0> ��>��.��e��)�=[�>�Le����><">�bT���$=�?�yx��]���k��d���|�=��G>��=LZx��F�=<��;�u=t��> �>���=&���ԙ�� @�       ����]��}u>��>NM>�ǜ=D��=)C���S&�>������05<�ԕ�� �>"�~=8���r
�=�<��ϽY���7<->�����>�1S�����+ ?'x�=�}>�i���u�>�À>�z���Ϛ����>s���m>BM>��}>&;�R�1�`q�#u�>�=V�+�N>��0>H�R>r̬�V��} ��{�<�5>f`�=����Ӌ��F߽��>��>��|>���k{>Bz�=��A�U��=�%��o�=o�=��=��;��w�н����ox>R�Y>���>Au;��C�5�����;�p�����>�Xc=~���x����r=�>�9L>yP��C�=�J�>tn�>=r������&�=�,�=ss�=��4>WM=��G��V�=�D>��==�7�=)o>��:�3�C>\�> ���T�>��>��i��RT>����J��=^�}��a>��_��F���>��'��w������(�����>���>�q�>�S>1� >�H�> IT�����-��>�ř=���=�M�>"�����>=�=�+἗��;��j>�Z>���>]Z�5j�>TϹ<�6#�y=p��|M9�Θ>f+辁�>A]{>��
>9��4X�XSN����|�8>6�O���a=�ͪ��2�h���<�h������F=5�0�u�>`b��ӂ�Ɣ>B䧾W���*h�>}�=![L<�Q�F�ּ|˽�B>x�=
=>��6�V٘:&h�>V_Ž��6�5�>�"�Z��>ܯ��#�>l>¾�x)��d���g>���`۽�<K��T?"��>w@�>Ma�>(���ߩ>ѫ�����>�=�|">ܙ�>��������6f`��|>F��9�A>a�R<����vK��C�=͊6�I*x<�6�mH��X�<͂Ǿm���=]�=a�Z>���=��`>�k
�B����>@�>�����j��]2>�j���}>�G���>mH¾�D�<��˾�������>�5���R��#d>Jʟ�\�����9>�aɽ_��=`y�>����	Z��}�Q�$?Ŝټ�P^<�;> �S�d�v>��������YY>�$�@�����>��->Vn�<�P��$�>щ����W�����3�'�Yƫ<H��^��.��U�꽫)����>XO"=�K���+����߾��o�))�M��>+�)�O���I݄>�t>lf!>	��>Jt��_7>��F_��.Z����=ބc>�sH�j<����^��,=�����4�=��">��A�6�_���>��^��C+�uJ>t�9��)��|�d<�����]�)�[K��!=�R=�|���S>C�u���n>L8�f��=!�2=5d>�@�hM=j�=���>e��=����=[�����>�����~�i����U4>`�	�~����r��==���<��>*�l<E��>�.=�n#;��=���=���=�鹽�(>YP>6��<��*>�b�'|�<�->q���U#g�^+>��e=�9�=��,=�\�����a�.������l��WG��Z=$6i���=V�8���7|<`D�����=]f.�^ܣ��jq>���>B�z=ݲY>�^h>�?�>;䡾��>�4^��>�X��?락����͚>Ȓ�=�[���z��<H>��D��|��=��>�x�>�t�=&�>�y������i�<xؾ���r�=��[ɉ��.>'S�>�\߽�
�N;�ʮ��;]>���}>Mv>n(��&���(j�\�����=��^���>oM>d=鳟��>����;����>
�>碓��ؼ�h{>h	��2f羸�c>b_ǽpL˽�ܢ>�B'>f:)�J��<Թh��>ʾ�"�>B��O@�j�{>}���F.�=���+������>��&>X���?�;\���o��w}�����.��Ps��Y>du�>R�n=z��� ��x�b��!��=�W�>�Ԟ���/����=����0ڣ=K���z�=mX���OQ��;=9��>=��=9
/>����<�k��������::	Z>�ʲ� \�>z2=ؔ���[�(ѿ�C1�s�>���>����������/=]>¢C>�]i>�������=��1�,��Í>�����~3��;ݽ�虾�ٮ� ⽅�(<6I2>U�:����)�;���>�"��jA4<�␾���W��>��+�Y�)>n�>>:��pF��^`�a�=����ݸ>'��>�zF>�ͽ�Л=Oǋ>W�=TR�:��-�SnZ=\j9�^�)�V�t� }_�o�=4��ߺ8>��c>�>�3���:>ĥ�=4��+��>4��=1ޮ���*�۳�>�L�pq6��3���F�=w>U�褋=��w=G菽�.�=�	a>F⽈3.����Z������>;��Bs>l��<wB�{�9>�(����o��;�>*��>=��3�[>%���+=�Mǽn>�>x�m=��^�u�?����F�>�(��!P=۴�>�B������>a괾pn��}7k�N��i:<Oڑ��c>ƛC���=�@ۼ.R���O>^�t�4�A>9�ٽy����.=�����7�>Nbv��ռsm�����>�1�>��K>z�����O�H=wH�<���:vf��B�>4S<X�F�#�>n[�:yh�>`Wݽ��>��>�}f>�b�>C��	G����>�� >"O��dZ>�^��|[�����>Z�ǽD�����E�P��K�>�1���;u�;�i>�]�r(>q0��S�=��>?�>)7�<��׽��>0ܽ��d>d闾x~q�Z��p"�%)=��~=���۾��|��c=6�!��E��^��P">`R½�ǽN"�=�i�s����9J�.�>�.��o�A�H��8m|�Gp��[!>�<ս}(��xr>����#�=I�}>k�->��>��>��N>^XL��T��lӪ=��o�G=������)��,k��q>��2���Ծb�#������=�о�8>�'��;��j�>���>4&нԯc=�˖��`�#��7ȍ�ڤ�=e�	�M�=q�{�`/��d����Q�=ǂ��l�=����[<�|q�y�{=k捽U��>'�X>����<��>�>��=5��������=���g
�<0�ۼdԃ��Q�<b��>�4>(       OR������
�=��:>6%��;�>����>>��/>cr�>oӽL\�%[V=<���V�?�>�iܽ�4�{��=@\)>��Y��.��>R�W�S����=G���H=���>�G�=��W��5>^$
>r�>�����~��Ч�Z_2>��	;;O�(       *�����d��G��ʴ=��{���O����Ͻ1�<I�>�ʶ>�T>ֺX>=�>���>�6��<�� �D<fT=�N۾�>s���8 ��M�ȼHg>��ľ[� >#�=�Pa�>�L>�d>�d?>r��_�ݽ��?W���֗��`�x�l��>X;>	`���      T����%>?���>��"6t>���>���4�Ӻ�(�Υɾ�c�=��νjN�=�{�����h�K�GuY�R��=Խ��!�D��>�z=>{����=�®=�\=���t�*���ڽ���=�OV��猾GI�>�Dn��.>>SK�>g��Z�d<�^��4ɽc��?����a�=�\O����>oS�>k�ּ3��)���b��}�{>/e=y\���>T�>"�=��7>���=������>hQ�OՑ��N��o���ue>�N���l������>�M�=խ>N��=o��<!O���8��>U^*�po�<4M��L�q�e羺
���W>@(�;/��zH���m"?%�˾B~�=��=BW��Eu�<�9�>�`>�:>rɾ���>[C�vm�>��N�zy��h!��Q������D><l����þڦ��Ɣ��!���;�> TW=�&$�����5.V�幇=/x'>�K>)����eE�5o�qyV>C�z=i����BѾ��=>r�F>B����v����>gw���-��	42�	��Ӗ�>;�	���^&�������>g�̽8/I>\1p>d�A�{1��ܽ�楾��$>�S����={m�>ج>�h��Ş><�<(W��L&9C1�=k<����X<��N��A�=8E�>�ӏ���u>�<���=؄��I�-<��>�>9�0>Mu��t(���G������҈<�g��B��r��p�C>��t&L�q��>W�5����>�W�=k���#�6>�^�B�=)m�W�:>��5�_]>7h����>�;_���վ��J�j�=�	ѽ��ʽ1&>�����ZսlV�}��>,x�>�܌>;�=Ҍ����>���=��Ƚ#I��קN�N�U�B�L>��b>��D>a�J�5|0>�Y>�󕽥�J=1�>�Yj�G�=ī�>F�˾��=4�>����(����u��.�>�;Q�\PN>m���n=����i�p;Ž
	W=OA���k�[�=>��=8p񽊔�>p�½	�c��%����m�>49��ȑ>�Qʽm+=�4>S�>R���q|>w˽~8���A��f����j�%�->J��������>cȾ�%�x;��o1>���=�x�=C���mvǽH`�=.�<Q�:2�>2���>�о���>`٘=寚�Z�>HΆ>u���������%��<]7�>dǦ���=�&=`�v"F>�5&����>���>H؅�!pc�8q���$�[9�>T�p�ɘ=q\���k�ט���>'U>:p����Ѿ�.>�q0���<'��>�}q�{>����#��fd����=� =�F�>��=0r���L�;.>�0�l���-z&��3��4J��>'<��W=��>�0�>�jA>bP��8�>}->�m�>�q��O>�ൾK޽=T��=2�
>�?�2F>O�
>)���b��*�*�o���>�ӾM�>^$�����>[�>!���=7>!�>�7�������2>� �>�����>Ia�&u����"���b���]��<P�<3�!>Qw�=�#�<G�೾4B|�㜤�Y�_=r]˽�9@�Q��>��<���>(       �8+>oK�>E��������=<�>k��=D�>���t�̽X�����|:X���= >w��>4�<ƙ:�C�Q>N�!=&�=��A������>��b*�>f��〓���>�߽ֆؾo*�>߹�>����nM>0r�<� >�u�>I�w�ddܾ���(       ���>��>,�>��K�g�=�+>�$�>j>���5<=�� >�}l����=Ŕ�>u��=�>!��g���%%>��A��?�R_�_����=4֠�~&�=�������>S��>L`���k���B>)f>`b�>�� >�B�K������>�z��^��h��<�      �V?R����g�>��s>�=T>�c����>�=�=C���݄���s>끁�XD>�(	�2-�>�bؾ�>AX���@�dY��K>󿛼�z�>?I�>���>Ρ4���>U��<��e>nνH�>V49�\��eD���>��!���?���>R>�ks>r�	=���!轵݈��*��y��ٸ�_ۙ>&��T���Hv�����=ۅ��sqk=~�'>��>��>�����o*����&��>f�¾�1|<&�<���>�E��$}�<���>�~>k�۾���g���"+d=�?`�W�>R�C��p��þ}|����j��a(���>���NV�>��=�P����}����>]�3���5�=>�����9������z>D?'�?J���1�
�^H?z�U��K>U9>P>
0�>S(��7J=��x��I>�8�U��=6x�=�B�>g������˦��o�=�WN=�|=wU�>���7��ۋ�=-CI>��{��>�>>]���Q=8�>�k�>-a�<b6@>ya	�lB�>���>���>]>#Y��X�������b�>;��=�}P>������=���>�1�>*�|L龠ޝ>���uK�>}�<=�>|�>7�h��;���>^�B>�a���">TH>?�>����>������={�P��$�;ϱ]���=�S�<F�w>���>�/�f�������rξ���5>�1����=��>\y=�I�=��>z��>@�ȼ�g^��7���ܾa
���j>��[�����w-	?��?�Ҭ�\�p=��>b'?�V���������þ�i��<��Q9�xX9�]��u�!?DZ�HѠ>t5�=
��=\�?��2?��#?3=��\u>2վ>S;˾v�&�#>Nu�>�W�<���>��>[(|��$��F�3�M���W�྆π������@>&�پ�ཱུ���=ø<Tb5>3�>�����:>P��>.}�=O=�.�>*ƾ��]>C���Tgy��=ǡ��*�%={���9>z�����U��!�>�3'�!}���>p��=��I��hѾ@X?�[�=n�>��:W_�����\�d����=��ܾu�.���7��j�=��Ͼ�]Q����=~D`>�X>֛=�������>�gn>����#�H�>y�=�&����>�8�;�>�Ͼ	�5���&>B$>�l�>h����>^V���>0�ҽƯ.?@?]��������2���F�?�Q��c=,T�xa2?���=�Ǚ>���Z.f�^LY<Id�>z�D>�vz���=b�>��=v�
=��R�ѭ	��?Y^%>��a����=P�m>��z>ڣ�<̢�=l<=q�)��x����>����D{��m���d�ѥ��s�iOq��q,��y�>X]���H$��p;�|�>�]�>Kɫ�����=!��<�0�핕>��?� �>��4>	�>��>FA�>F�=L�>i����>�!�D �>g�5�Z��3k5�z��>��D�/}�A`�=�ތ���kwt=@���">O�н�,�>>fU�� ?��#>���>��ľ�%>1Z?o�澍<�>W��=       �F�>0�> M��`A���X>�l�	!j>��N>�G�>�JC>ѹ8>��2B>��;܉>�T��g�L>��4�PE{>844���i���3>�
)�����5?0n��� �>�ߚ�dK�=��T�gA7>H��>�l��s��b���l���xz">*\����>v��&?
�o%�>��>J?�5�>j�v>�PY>f�J>QVH�6Ɯ����S�6��<��%c >k�=�N�=�>�9>_)<Z;(�)��>=�>b���~i$�� ������]&>�튼o �	�z�x>�������=�>L:S<�:��MP,>��ռvA0<swνx�X�)�z��=1%��^�2>�b�>AS��2=���I�w��>jӼ-�d>��;�2���>>.��>���=�nI>���$����k�>�(M>�<���a���6>ڎ;�m���v��c�l��=� D���޽�|Ž��F>'a��Z�>�:8�(�#�?)󅾙�5���>a44�aGK>::`>�0�=t�?m�>�����7�ߔ�>b�<�#����1=MW������k�=ލb��ON���߽,��>]杼U����؍�������삽.k�<>Q�h��{x���8���V>\`M�����ˁ�j��<ba>��>�J����v=���<i5f>��˻�p�>sW�>�Ѳ>y�н,fH�����L-���]��7��>�n��6��>���o>��=; ����
'@=�ቾ���� O�C���>Q��<� m��D�=��D���N�v��4�(��	˾�6h���I>�*��V~>nv>d�>d ���p�$��=&UH>�I	�}�=߅��݅>5��e{
����=H^��>�>cۏ�b���B��>|����a����#�>���<�^�>����>S�.>����&^n;b��>=����<wb_�<鎾�d><u� �ʯ�>��`���d���?���o��ma�����XE�>��:�=pp>c>D�/���?=�Iҽ���:r�>�j�ReE�٭>�b>�m0�^8��.#�{R'���4>��]��(=��S>�Sm�;X�=�p���=�׷9�8R�� ]>�V��s����<'=�\���b>ײ&��G>&�$=���=o�>�!,>�H�>��tn[=�N�>N>���CB���s�J�>}��>��>�w�p¾���>N᷾�%���=g2��߀>s�>|Z�>g��.���V�>��k>3�>媮=m���{��=��>��=�ϯ>�ؾ~Z����B>j8x=���=�F�`<�&Sx>4#[��y�J%���s��Of>���>��O���	�O��y�=P�=캾��޼�)��DA>{弪���4 ��Z�<�S=�*N>i=�=�D���8���=��v������̙������#�=V曽�o=cSG=!�w��3�����=���>���><���¾�H�=�#�Ɗ>�Z�=^�=�����>��>�\+>s8>�;�>?Wu��2 >�j��@���u��I?k~]>cx�� ��=�"��X�>�b���J">	�=v�@��'>���7�8��<�#6�6c4>߆<���=�1�Ϛ����� "�<����CZ>��>�F>���=0ľ,�<�]�>��=;5�=	9k<�rr>'X��[8��8�=��>\>������2�KY�>l�=�Ke��䠽�Z\�8U���T�>H��DоI:�=y�����>/b>��b=�e���d���i>�[/�\؎= �ݵFjo�ad�=��+��=�Cx:�+>��žk�"=�G�>޵����H>P�i2�>
����HK����>�T><G><Y�+<
R���^>��$>�I��j��C6�-��������=���>�t>N������>W��*�V��|A�=t�<��=���.^�>Qgy=����zݐ���v<,pd>Y+�>��>M��>��Q>�оq��>#���`��'�?�9p�0`�=I����);繾B@�>�*�Y��>DN��E<��,����M?�=Z>8����G=b�>n8��[5�)Ǭ���庿!D>����r=+>��4� �a>�g½��u>�{;�`I��?0A>�x� ���2�<8��L��=�@>�0�>��:��d��z�����`=�8>�?#�=��]b�D}1��3�:�B>	�s=�>,>�[U=��S>��3��R���9�<�m<=a���=����V�����=�� � ��=eTq���4�p�9��K�=��C>yN�=]��ΐ;I��4ɠ>�3�=r;�>�O0��.��(�=+�^>�1>:-G>��C>�d�>���<��=��þ[Q�>�<�D�"Y����=|2�/⮽zc>'�W>�Zp�Z�o��)�>*>O��;�.�=���e��l�?�.�>�e	�Th�=:s<=�y��Y�=���T%��D>bm�>��G��_�4�>%W�=8��<:��=��2/�R�=�7>[�k>ٚ>
۽�q����=ݞB��Y�=]d(?�V��#!����>�ѯ���>�:��9V��h�=ͤ�cc�>��$>ǐ�㿢��g����J���>D���˖>}g�>!放��1�a �B2���U��8�>�%�v�=��N�[j�i��=��=r�R>�`@>Ef�>Y�z>�% ��>���>��>>f��M%���6��s��=���wjN���<&.�<Ͼ�&�b-�ߘM>�F>�2>��>Q�>�O��R>w�x���>���=�ȇ>Bzr=�8þ�U��A�I�>�Uv>�
�<�T=�~��D'仮=��-*Խ�Ԩ�J2��cG�>�>��+>W�>&���$��>5/G��'�=(����T��>H��*"��`d>U=�~�)����o�N=1>7��<�>~sʾ��ǾO��磽��?���='o����>tƵ>��R�G->�{�cѕ>��}>ͮϽ�.j>]���*���H>7�2��>(¾�=׻��>h*B��d��k�<~����=-��ܾ��G���s>1�>��K�Y>�����U�=Ņ�=D��FG�
:c�"�>�5�/�=��{���>g�<��>I��>��k����b�W>�������>����ٽ�=��xH> �@�lSC>xq�=	�~>�������>�;ҽ��n�M�B�g}�=���͂>k���6�z�W>�'>6i��>�f>i>>i�W��1��(       ��=(5�>�����ͽ�.�ل��-�=et8�����;�>i���K���N���d���#>vﴽ��Y�v�>>&�=�Ⱦ󇁼��v����,�>����Ђ��-�=��	>�p��%b�3��<�,Y�+�$���[>�K����>Y��>��3���O>�G>       �Щ>R�$?�*H?��>?�F4?�;�>@��>'�\?���>%�.?�W?=��L?w=�>#�>�vc>��?�3>^�r?���>       Q�ʻw��<|�<�Tr<}��:�0U�j�T�I<�d<�d��C ;�R�;d<'�N�=���<���rg��(�f:��G<�H�       �K���}���I��O?��h������o��_o�>Ü0>�1C>��B��"�@!�>�F��b,}>�p��#�=��>�L=ZX��       �L;8;;u�:B`;���;�t�:L:<%��;�a�;)�6<>�=&(1=�)<�;��<��;@�b<���;�<�:8щ;�      �ަ=hs�=e1�>H���$~>��>�=��=j��3Jv>��ʻ'&�<�w���1f=�`>ŭ�>8�(>�>ת=�L\�<;�>�$�=�x���Gþ2h=#F��>e�� Ɯ;`�=�Z.>m��gQp=�Hľ�U:�8y">�s"�����^W�
�L�f�>��n>G>�=_l�=�O�rh�=:Oܽ�6>�c��q*�%�"�`R��M����1r�=��l<{t�z&ľ�9b��޽�x>@��=�)#>b�2>Y�߻�i��F�j=T���aU�=�2>4x�Fэ>TxV�$B�=��1��R=A���=H	�g=
f>�����>���;̌��z:��	:>l5>�K>q������;!8;T�	>w�Q>N�_��9M>����5qa>����龗폾/	>Ӣ��U7C=B����sf�`�|>MR?>�l���������\�?�˼=a��<�F ���>GY ����<���F⋾u����>�b�,�����W��?���U�=\.>�+���=�2O>�a_�"�>�s:�M�<>Ӧ:�Ntν���(�>CX(��.	��Yо��=�ǻ].�>1|�Ϧ��6_��P��T�>���<(�>Bˋ�����q��HE(��>��>l
>�t>Yl>`W���!>>�O��b�ƽ�>vz�o9�Æ@���=8Nk�P�M��=�徼�^�� h>�Ҍ�f5��0<��B.���=���9ؽ�R~=��<�z=�I-���C<��=R�<�����u��ʐ���B>z߫>%hݽ�>�?���>�>QC�;���=D)�Dm>�\�>P{�>=��&b����>�u?>E�=4>>�?P>����97�P�>�7=P�r�U�o>�:>�����>Æ�5K�*�=��=�νbNO>8so������2���].�<y��=�*X���:� -�>��3��3>�y�>�N��J�ڽ3����wKg�y@��˽�Ò=��(>�:̽��>� ��>�>C��==R����9�?�[>7wA�F�=�)Ӿ�W>�5�=�+3=P��`�)=D��I��<F��;;E=j��ǬW��i>��;����v>�ە��Ʌ����� ~���>pl�<Bע=�?>���=�)�=�������ь�;r�>���=
[���d��n8>�#�����>W�Y����\1�!_">3iӾ<;��=+�|'1�>Ӿ\�6<3ن>���=F��>1�.n=+��>���6�r����� >А��>oJ��;�=̭�>S�1�֐ҽ�=�X<���R~�Ts&>�{7>��=S����%:<�L<�`���La>̻,�rO�U��=��Q�H-�����WE������>PL���3>7|`>C�b��]<>
���u �>a1	���=�`�$>�#��ؽ�+">ÉS=P��w9�1:K�|��=�b>�j>���<�dý�ᓾ��9>�:�<�>>֌]=�e�#�<}�q�>�w���l>K�P>r�N>Ko/�~���.�P]T>�ά=���%�9� mL���q=v��>+�=��M�8r�=�IJ>ۊj��>� ƽ�J��6��>�>k�%�       3s�=q�=�{;��)u��`>���˅h���&�<�0��i�<��=�.�=p�=�=� d>:��<YĨ�/�
���=pO��       {�?;X�>�+k?�VE?�?��i?�~?RV�?�]?��>XK�?�F�?
r?��>��&?�:P?z:�?�ߌ?�fD?��h?       �>�v�>��>�y>��>wj>n�>!�>%0�>�͐=F�>`�=�E�>��>#�!>c3> ��>���>�&�>���>       ���= �<��s������ >Ą�g5]�Xi����'�f<��=n�X=�1�=-<�<��c>>��<�5��Ӓ��X=���       ʗ�>�?;�?��?^1�>���>!|T?}?��2?�?g��>a;n>��W?R�?w�?���>�`>��?���>�>       *¸>J��=$��=��<]>��_>+R0���>,����~>�(��x�>�廾C�'>Hx��������*�o�G�Ӯ��T9�       hU�=<       QY��+�,�jvɾ;��=8����;e��>|��宩��0��ՙ��ь<��?�[�w�N��P�>K>��A?� ��P!���e�>��?�
?^��`�>!U�>	OR���/�g��>� B�!r���I��Q ��	?u�'�������>�������Ӵ��?p�q����\P<��$�����g}�=��>au��[��>L��>~�>q�7>qy�w��>����X�v����>#��>       �<��䏽P�>