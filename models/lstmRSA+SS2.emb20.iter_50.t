��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      w�/���q?k&�>g�5�<wt�""o?�H?�K*��V��?��'tͿڴ����f=E��=��>������r?�q����>#�B�l`7?��\��Ǔ?�@?t.7?b��>*�0?R����r=����M'�>����M�� !
��j��/A;i�O>���>D~s�s��z�?
:>lL��\n�=��u�=�h�=[ƾ���>�ڝ�}���=� ?/����C?�D,�t-�=u/�H���J��� ?��/?�]M>4�|�Ȇ�DY����=W�`>�C%?�Z����}��=n�>�J�>��ʾ�\�?8:����k�`=i<��T1�>�F>R��ቲ=��
=;�
��Y�)&�=[=r~¾{��hS�����8!?��>!d?0�Ͼy�>�����Q>�1?�9ؾ�G��b�>��׽�	2?zȾ���>�v�>/i�?��;?J��?&��>~,�>�i����!?��.=�`�:E|>F���������>Y�>�'!>y��>�/�����><�=2�EzO>���2i�<�]�>������.?�>{F�<5�þ��>@#e>a���,�>]�}=U����E�;�2�>q������잾)cB�O����/B?�C>��g�>�P?��B����b��<�?�d�>ϡ?0m¾@��<�z������!� 澾<�?6g/�����4��>wp3>�d>��7��$��t��=�#v����Ec�>^�W?r_8='���nG�>W�>ظs�����ĺ�>B��>fg����*=�e�>��n�A�"Y�SHi�Z!_=�I.>��=�u>�����>l�B��ѽ��$���!��p�>Ɵa�z��=��><�Q�"�E��u�>3��=�7<3T��K���n���a��C�H��y��^���y>|�?_�?���>�!����<�|�>Dܟ�ԃ��/p=-:�{ ���M>W�=>�ti�4�>��
�<�(�(�>>p(?[��>h�b�����f�>��ľi��=���5O>�54�qF?�p? ��>@R��"I�A ?�ڔ>y�=��(� >��S=�f<+w�>�gD>u9���Ok�.���i�>0־'�X>�M�>���oB*�A2l��e+�U���L�=�ߍ����=�I�>tl�>cO�>���9+1? Ӄ>wT��O�<�f�>�.�>d�}>e�>�&�>�[>�i?��z���G>�>8>���}�S?I�-?�ܑ���?]�R?tP�?T��?� ��ڻ��bH�L績�G7?9�>]M�?qT?�|U?�[?X�v?qpd?Ջ=����>��Ͽ!��F�?���>��>��3?7�b>�.羌uϾ�۽��ξ(�l=���>��N>y�&�v>G���O����@����>�3x=tdO>s#1��M�>���=+����81>+��>���I����+��n?�+�>�A#�˹��-����=��Q�&�A>Mk8?���?tn��?��.>7�}���84��e>t���v��
�??{�'��]�>�[��9�?��G>�xO���4?�U?�0?KBu���>��8?{3�=�Q�;�#P�WXd���G���?dh<�[ |�UC�>lGr?��&?F6�>��=�Yb�d^/�       ��6r��'���Y�>Ⲿ=@�����#�d>�y>Si>%e������˽�ޤ�]�=Y�S�s�y=���>C�6����oߗ��$��þR�쾩V����J�wZ?��Y>7����^n<�y>�P��'�þ��н�p�> �Z�ML �4;�:���J��>���>����3>f�>��t�{;I��)�=�1��+���X�о�rp�u/O��`�=����Ʃ?�o�>(�>>�ȷ>�]j�>�ܾs<ʽ!�����>-��>� :>�ŝ>�&�>d>�����>�m=`��>��X���W>�Z�>�O�;Ei�>tO��-�P�|>�P>��=�7�ؼ���=<uv�sn��˸�,����4�>��Ľ��ý({=o����>]�)= ƶ>�+J�_D�����.m=(h������ƞ��I4=��\>=�}<����*�>�A�>�;%����x��%�>c�{���0>=Q�����=I���Ѧ>;�>�8��㵾K�>��V����=��.>��B?R�=��T���:>Y�&?��>RM=Ӹ�:��O�r4	�tþF>C�	>��K=��>r����=�̦<�d �S~��<��=|-�>��,>�������S"�vv?�`>z�?���=2�?YӚ>&�%?���>ݷ��������=���;��%������c>Շ�=��=>����H־	4��b�f>�����֝����<Q��>�o=��Y������_,��Z ��f>_�Ͼa
�p|��G��WH�;���>�@k��,�EB>���>������/нŹ�>|�����=	G�=�U��4O�<�����:����=Ȣ?@�ƾ�k�n�|��+>��>z6��e��=`��>5FT�\���;���>�
T�qb���Y ?���<�[� �<�]f>nO����>E�*�=��?���<�&�>�Gy�Y�D��?Q�t��HQ�$�ܼK2����;�=t>�ٺ��B�>���>��?w�=y���d����/h>$�>pP9�e�S>����gP��W
�z�T>�X1��{�>Qヾ�O�1☾�>;m�=R�@��'?[PM�'�=<~��瞽�ҚZ=T#�>�N��4�=mҍ<�[�OO����=��=�ʑ=���=�"G=`�_����=u�<^g��)�>��q>���=�,?�?�2x���|>�Z�>O�q>�J+��˼>{�#��1�<y�4?0�q>aH;.;1>��	>;�j<<_L��;�>��i��(@9���2>�>-��^o�7
%���>A���e��cY��w��>$ř�*�=��M��K��+=�aY>Y%.�u;��r��~b񽩴�<��=�i�=o��~����+?��>�em>`뮽f�;o�>p�"����>�rH>��5>��>i]>�=>�f2=���>Q���=Ѿ@�x�N?�ξ��"�M���1t?�R��4\R>�� ?2��ɪ�ZᾖMN�c�>Ka��o��p��>�?3W�=Ry"�Jl��}\!�g��>�6�=tʾK�>(����?�#<<� +?�m�>9Fؾ�)�j�k��������`׻XC�>���=a�����a��,B��$�=��/���>���	�=�(�=�����=ey�>�7F>,
�F�?>A}��^Aоp�Ͼ`Q>i�0>�Q">��R�}�!�% ���/�&\�>��>�ֽ�'<�,�=�:�v@>���=��>�׉���m���h>i�T>t�=>�.��RĽӜ���ZJ�k��={a>��h>
x>��f>đ�>z����>��)<N,�&)�p���x��<W
�����ä?}N?�w�=垹�U8���>�e�f�?�>�m]�b�,;�nоx�'����>���>��>�?�i~����>WC���`�P(!��^�<��Z>)�G>��=و����=��� |��?���`=L��<���η>���>[D�>Crؽ�˯>HE�=.�%��~��/��=?���v�会����>S��g�ѽ�-W>(��=�u�EP��n��>���M��=�<��k���.�>`�>�;5��!!��->P��Ax�=C�C=3��>n̄���+�1>�����>������:�cj>�G�>
�/�����i~<0����@	>�m�=���*}�=�̈��Kb��垾I��g��ޢ>���!��=��~�"��<(�>���;7�>ʕȾ�о��S>��!�9k>9K����vY=;�^���>3Q���=�c��:���X>v�Ծ��<^�>�	=%���1�=��>q3�>`�>;�?�t����=��н����|(� ^��X���=Ѿy]�NL�>�X�>D�>�_�����yξw�9"7��~>�/�'x���]>>E�<|>?��>�̄<�Q>�?��������>�>�<P5�>�jC�hr->�.�=��A�Љ��i�<���>&��>�?IO���>��xf���e=OyV����:{l�3��=)�;����[���>Ka�=��>"*�>.��� H�[�Ǽ�ä��y!�f����=j;�
�I��I;�Mм��>��:>Fḻ�t����][��y;�k��E�u�>v?>ۛ���徾ߐ>k}�>*34=���>��p>')�>V�;u��=�Fi��� �{>�Mi;���>�?P>�r���?%b�>�T���>���|�=��l��\�=�b�>^e">Rֽ��<q���%�<���;����>>D�h>ʍ�>��Y>8\;=��>�ID�v��=�;n��4�Խ�C-���������T��󞾽֗�2Ej=m��X��/��>ИU=0%�>,
�>�=��>yl�T�#��;ﾰ��>ߊ>8?�>�A>Ɯ�>�~<�R��|�q>s���� ��函�?삾��v=��>ջ��)��m�>m^�>2c���0��������۾OX���ϝ>x�;>�12<�KŽ��~>�G�#ҋ��G���5��%��j>�-��ѯd�áܾ�!��z���?Vw�>%R����>�4>U��>N]�=3B��Lƅ�{+-�m��>�M\��ݽ�(=v�z=$�+=�%=������>.i�>S��>r]�= r�=b�3���~������">�3>B����k�37�l�=�GR>~���w������=?{$�<�^}���>�>β���g��8�)����>^|>       n6]��Vѽj'��;>%�+<��m>9�>��+�+�ϻ.>���B�>�8�S"?d���8��ㆾF˽�[�oz>�G;Uw����ý�I�ߔؾ�i�����=VI���������d!>���=�ٽg���,&?]V��Ņ>�"��F�>���o�Z> �7=j2��͝>�	�>�!���Zl���=)k��ۅ;�6�=�X+��	վ9�f;�
���0�*ez��{½��v���!>���]�����׾FG =rK�ey�=b!F�4V��VM��ԩ=T�=���m\I�b�>������>�H�A�>%�U��Ô>�uξ_�9=�� �|z=��<�n�=���Pɚ�^�>aa��)�|>(&8�0=�����G?�=|=Vޔ�� �%�8���<�>�0���)��b��>�,�>���>�4��`�]>���9�����ܾ:��>��>�g�l�+��`�>�I�;�EO�H���=���V�_>8K'���>��n�>s�Q�1>C�a��n��y3�����J�"�γ޾�X�<i��>�Ћ�߉����:�޿$�Zr�>���>�?Ӡ>k����,�> `+������>�	��R��>u@�>���>]ʦ�is�����9>jF�>���(�оa��<�ܮ��ew>�*�>��?�Z�HĽ9(��n3��Qֽ.z=}W�ӓ׾��`<%-�>��=]RL>�+9�=4*���]���>jD��|��0���w������>M�佘�jv�=B2м�=-�n=3R���&����e>�Q>-�>r_c��>�rN�KX��0ϯ>Y����%����
����>�2z��>�=0>AU=�m����)�+��>&�?��>��L��0H>x=����.?��2>�w?X?l=R��x�]=E,��E���Wg��o/=��N��x���W��<'=��O�z潪�<Ŵ��5'>��̽��P��,5�#<��pig<�&>op� �"�l^���G,>q�����^n���籾�4�>U��<}�{d>$�t����ֽr*=ͺ½̽5���{=iɽ���=d�>><w־�̾�޶>cڎ���w>�0d><t���W����F?��>d5$>݈Խ03>>N���ü�����?>��>B�>́�>S���A9R�h�)���=d�>�(��	�;j`>�xH>��v>e��>7!>(�>��=�4>ޗj>�aL=��>f��>]|�,;�%>��>^~;�ʾ���O�>!K^����=z	���@����[�Hm�>H�@���8���ѽ�B>�Z�=�B�>�nV>�PC>5��>���/��=��o����N=����f���B�>{�=�p�P�>KɁ>���>�í�Cuv=�߃�uy�>�s�=`�.�^��>�To>P��G�>�	��?��=�� ���\>�?�T�fp����>��>���ׇT>k�e�;HϾ���T'�>X�I=9IE?�_e>i��>Wg=���=�J�=hfc���>4ݴ�bo�s�`>'�>���=Ӫ?�f���Q��y7������>6�$��[�=w^�>��:>�[�>>Զ����>���>�E�=�l^��w��?�W=���>�����w�Sv�Xw��~��U��=6����j�=���Uf�=�e����/>�l�y*��)�@>���>�V���/l>+�ག��>� ���K�>5�2>>VW<�
��#:�LC�>sTM��k8��	>�B��˺�>�8 =qK<��>T"v>H]��6ϾzY�>�#*��]Ծ���<ˬ=�����+�ێ�>h�-=#1r>yҾ!������`��@�׍!>�ۭ�?�/>t�>98�>r��=������O��=�[>>�Ͼ�(>>+��<+�=2/=���R>�T���>�| >�����>�����a���_=�谾�ž}��<H󡽜㕾\_.>���=�Z=�lm�>���<;$>e�5�{�U>Λt>J�X����<�Q ���>�UJ��:��&��>X>�>u�����(�8P�=Ⱦ�ȳ�6�����s<�<w)>�_�>A��>����V�="��>d�>���=P>B >p�������jܤ�y5�]�>��:�P~>���>#�Ԉ�>Э���!�<�K����>8u�>��u��"۽�Pv>
,ͼŷĻ��l>�������=�u�=/u=�<����y>��==U���|>�~���AƾG5�T��=�sC�b�R=����Y<�������)�>����%���ԾVk�= Ѯ>l ����]��]L>��B�w\z>w$����ȾN�<<���?r��<��=#|ݽ��J>Q����c>���>:�>���>
���aW>g]�{Kt<��˾綁>��<��|��>cu��L���G���,���Ϳ�>���>�6���l����������̾?d8�����HQ�=~��<�-�=\�}>�=�I>T4h�\�徉;�p��> �����==�w=0�~>ʛ�����=���>�J��Ɔ�=U}�B��!z�>"Ȟ>���=��<�����6�<'n>�b>{�_=1���7��;8:�;äվ�qὂ���{�^���cx��E>��>��>a�>u �=+�@���z���]<3�V�y?�=�O8>T��7N>Mc,>��>��BD�>���=��;��o��-�=��I>��H>��?�C ���r>��R��ڽ�~Ͼ>Ͳ>{��-r���=�YR>f��>�{&��ʽ��=sþXn�>" �n��>���>������i��2�=|�&?�L]���h�����@q��󌽵����ƈ>�n���,� 
�>0�V=�qb=�����X�>$�O<�d�>ǃ�� �����<�wV>)
¾<?G�>&*�<qE����>9�$��# ���U�KM>�F���Z>�I�=�~t>�0�����>��m><����I�@�`�BdA���7���;��A>�&>*�`��{þ�����)X�ԡ���P�>��]>�f�>�ꝾP)پh��o�>+J�=7��>���<wX+��c�>G���؝=�`�>)�=���$E��<>���(�-�&��>�z�>O<����>H[�������>5����$�Ǿ�4�>6��>��-> C�>0�YaQ���o�X<�H>SC�>�D�V� ?1�r>���>A������ʽEg�=��=��A>?Ƚc{"���>�Ƹ>ȅp�	?>*@=�"�<�      1��G�>p��bc��s���v[{��m>>'׾�Ɛ���D�n�>���>�C�>ȋa>7b�����Q'ݾ�v8��Y�C ����>>�h�
�?9>�V?����>mE�>'&>pj��|�H=�>$"3?6�1�h��>o�4�8f�>4D�?��{>��9�z�>.슾lh�?"?=-=�-L?ݝ>�~ʽ��>RvپLZ���M���
�[,ξ�+�=u?�C�&�c>�??m��R7�>�+�>G7ϼ��">��F�h��>և��ߐ��B�< �������V�ʾ6O�>�`>�%�=��>� z����=��?r��=0��<���$�>�F�=�} ?=��;L�*:˄��?I�>_�A?�R��K���P��G�<�>E�����ol�?hY�?2:��Xվ�B��f���G]�9��>�P��=|�6�ȫN�|�)�b��>���Sz���z������Ó>��>t!>���>!M>��M�s��D����	F�>t�ҽns>�EȽ�
�>U��.+ӽ=ƽ/b����[�7x�=��c���=�R㔼��=�����|S����=�ɜ>��=�*����<"�>���ǉ$?���=�>z�>����f�=k�~=߆Ҿ���=�t>+}��@l���d1��?I~]�%H>�Ն=NK>��j��	�$�>rQ=��8>�&c�R��>�#����>h�>�~ھ P#>7��>�ܾ�Pj>Ț>?�H>�`6? nE��͖?���}�>���m?���B��>}h�V
���?>׊�^ܜ�޾�?��>;_=����k� >��>���П?W��>o���!��lS�>��g��=$
�F�6>�#-��?½4�>���=+<#�w���۴>�Z���ɾ�w?=�j	=�an>�L�>��.?of;>���=��>��=R�*�9Ƚ�	l��b �>_P�<R&��z�$��>ҧQ��/�=,�=m��=�38>$ �>���>p���*t>��}= �Q>����6>����'�>�!m���?I��=d�>Rю=?I��8�k���t����=ǏN�����]J��ϕ��>�1>[~�>;�>�����o*?�&	��م�{�����込	h�}?K�Ҽ���>K";��w>�(�gU >�&>�K��\��;��>���>�坾p?�s>|���4�>��<�u>��D5��C<�+��찏>�ٖ����I�(>��=4|Q��R���a���=��@>����tC>BS��L�>nh�<������=�(=��=a�g��X�>Vr�>�`�<�v=��@�z D�.u6�t��=T��	�>�*+�]u׾�`�>@�j<���Z��>��徫͠=�{q<ż�>�[�=7��>,������>\f=A�{�`	�S��i�!��h�>���� �w��-�=���>5�>O�>���=��=�q��5ߺ��?�R<�k�=�k��E@u=W�=��Ͻ��=H����7=�tmH�L%>��^+?�5��?g?'9�?`��?pq4�������n1���I���D?@ X�E�彦��6��=��>��]�ہ#�� �����j�d�T$!�+�ܾ]9�>3'?At(��      ��?
	�3��� �	Պ>��g?򆊿BG?�"��j7?I� ��F��I�N(�����=�4�=K��? ��~9?`�����8?��?�B
@��=�W*?sd@�ĳ?ǐ��%�=>0)?���,�T�Y7�?�uv?y?Hc?�-ɾ�b@�'<=�32?��V?o� ��-?����i���۾�����(?�&����\��8�>`5�>�n?I�c@J�%?�Ɗ>Q�׿E��?ϐ�>��ܿfS�b7N>=��>WfE?֎f?Ȅ�?f�?NmK���O?B_�Tf��S�>r̽رZ�}�D���>�e��x���mO�N��>!�z?�¼��j��	.�j�@>��?f��?el��#�w�HH�?�@�zS����=�@澸?k�����>N���b����a�.��?�#|?�T�?h�����kȿg���(�L�:��*�?*4'>ٮ�>��`?M��яZ>M���)�D-�>�ο�=�?nTm��tT_�-?<�>'��?J�^=�H���[���/�?���?`�?C���a�?�&l=Pn�>�w˾RD@o&��͹��� �F�?$.���?8�������BI���9�>bK.?��t���p���S�}���3n�E��B����0z���>~.�Ra=�d?�hq� ہ�_?���j��8T�>\-d�c]?p$?&'7?K���0���=�?Sb?�U�c�P��$��C�q��7���8ǿ݅��RY�U1�>��@��H>�	M=�}��Z:����?�ᆿ��=�9<�ԑĿ)���׿�-�8�?w�@�٫>o5l?��\?��H��?Xf���p=|�9@��D��Z������&��?(-u����>���>|d�?,�>W��Kk��aþ��R?����� @�e���ʾ%����K=?��D���}�N�ǽ� ��i��;8c���������'?��=�D?���5"�|���,�;?'<�p$?$sþM��?�-a?��XÑ�9�u����@<��[	
�B>?��?��)�?�F>`�&����y�+^ �iu��Z$%�*�?� ?����؈?^��?u��?�	���X�+㛿9�7����>U��?�f�m�o4�>R������?�C?���7%��nB��ؿ���?b��?%��?��?�7��P����H&�L-����?�]�z��?|�ȿo 2�o�?p ?��|���'?]?��?]��>�xC=�Q!�;�Ծ�B�>_3�4�����п���?M�?��=	��?8�?�^�?��tM"?ۢU���+���i_�?���>�8(>���>��p��(V?mx?H�J?Q��?��Ϳ挤?W64�;<>F悿l�g?��?]ʼ��QxI�,����>��@�0�?�$3��m?�s#@Q���_���s�>�?��pJ���d�ad�?�9�?��?Xb?&��?^g�?sw5?��H�3�ؾ?��>��ʾ�$��<
<=�=w��(�ܿ��|�bƬ?c@|����?#4�>��:?�w�?�EO?��>V�?U�1�0u�4o���T��U��u"�?��+�3=?`���jv���7=�6��D3�����?Y�����?5���P�>E���n���n?_p��p���v?h�1>��F@�}�<�x����C?��˾�"I��˿��>�?���>L/�<(       ��b���mj��4U����<i�=����ct�0��>�I�<V�>���=WC�>=��1=>Vп=r�)��8>�6�ױ�<��j���9�>WQ��~=��g>��5�f�?>Du��i�$>?K�'�>�ؓ<�0�.j���J	�5<>�N?aW��F�>�4h=(       �a>@�z=NM(���ž��=�f�u]�=LUM=ȡ?pʆ=V��>�Q��E�>-�>�o��[St���2��*K�څ��qꔾs}��sZ�r
>!\�'B�>�K2>��>05�KG���H���T��B9>[">/��p��̉��cM�>���/�>��=(       I�M�ѕ�>3�>��,>Pj)>$�>�=���㘾�=?ϧ>N�뾱�H���^��}?��w��U�>f�>&n�>SU��쳾ι�=�/N�
����,3�>1�>˅�����>�^f>(��=uII>�<��+��>�z?j�P>�=�=h+�=r�>v|�>       ��MC����9�?XOW?R�J=���>��>�Ŧ��~�>�t� J>	?��?�c���7�>n�>|�� �z<�V۽�[��<��G6>Z䉾�˔��>���>�]L>_��>L���f�'?��=�W��/>�n>��U">i��=F�=�S(>�>�>�	��ݾt��>��>�%���*?�[����
?�f
�\���g�@��ԧ���
?�?p+?�A�>��(�����ؽ�$�&­:H�v Ӽz���K=�q=��D>�%�`�,��3->j�_�9��=��=q}����m���&�B@�>Ԝo>��5�t��>W�g��몾��~�0����<>R"���Z������Nk��^о����-�>�	>�TǽP#�>7*�=�2�<I�.�|N>�j�>�k?J�A���6>��b??���v�5>�X5�t3��p�B>KM+?2�>2��>QҶ�7�?0���b�=�=��y>a���̖>v�R=��<n���>�/���4>π��"J�a`q>Df��h;�+1���p>xȨ����>9H���
����>*<�=��>e,Q��B�N>U����>�Tپ=�I�ꮗ�cl��zvt����f��=b��>)a���W>�ޅ�7{������O<F��痽�P����>��s>&2�t%�J[p=�f<�V��KȾ���kD�<�{I��|�=�&�>��o� ��=˸=�q��wľ}L)=�Ⱥ^ˎ=��u�iм!S\=@�侍�>82�I吾R^�g�齶�ػ���<��";�7!;Vk*�RM=E����m5��`">$ZQ�Fc>?�9U=y�7?���?�~�2�'>�ۻUn>e�_=R���:>-�'�T�)��x��M>�ܕ>\U	=���N�U>3:I��ݣ=?��|h�����>���{�>��?"��>a+J<j�I����������>=�J�>�>���>�kj�F�׾��(�S"��?Ue��@��>I�'?^lR?c�(?n�S>a;?0��0i4���Z�z�!���9���=|b?��&���>�<>GGd�����*ƾ�.�����;���>[�>B�=�Q&����=�C�=0@�l�}��Y?�a�<>Ǎ�=��M��>?]�𽥎ӾB��=b�>K������qܺ�߼F�����m>$�>mҁ���>R"� ϼs�>�Wn>�I�>F�=Ϫн�缾�A���>P�p?vo�>�<3�?Ac��C�?*`��a#?q�D?��.������?�f	�mJ���?�,"��A?���>��W�Y��}/?8�ӽl%f?��7>�j�5Fq>�>O�a>�~?��7>2?������A?�Ai>�/t>a��q��a�=�� >���> �=��R���>�����4P>\k�fV>�՘����>R?�>�s��7��;��>��>z�o>�����?n�����=���>5bp>\�?S��<)�>TH6���u�D�x<��1=���>�׫>D[>0�Ⱦ��u��Mo>���������>a�>5+�>;W >Nd�>x���p�=T��_=�Z��6�f>:A=��}>���=��ν6��<�>�1�=��0>P��>΃�>�V�>U�L��J�>Wn��_<�����[����>�Y@��o���J=A��j��=c�K>��
��f�>aC�d�<$�n>�?�$�=H�f<sz�=j~���о�P5�A���R�>K���͜�U'���)�>�	9>�>U�Y=ax�>U�Z>����ʍ�a6��֖?��q>���љ�6��=�8>��=��������\>ӛT=�>}>+&|�`��=I���rZ�>�J�>5j:�2��>��l�-�о�]���Ry�$P>����ź>nA�<��>�n�K�>�Z����;��L��0���L$�� Բ�Ի!=�iԽ�k�t��	>�*[= � >BW���_*���)>�H�>e,��_��a[`>��<�ɔ���@=[�C�z7پ��=�\ݽ�=W'���S����=v>[�>^���l��>4?ooD�s%b>iIF>t��>�
>M{=zܟ>q��>fXټr�F>*<���>d w�������>'�=���=�fӽ�I��.����>U��=�F�<�NR���7>D��m�徦��=(A.>�f�6�>y>��,��5�=��>�k�l�[��Σ��Pe�&�c>�=v�8�~i�>B���2|�2X.���B�D�U�-Y����%>��0>d��W�>�c;��nq��n�>�Y�ޚ�=��g��C��\>�ٍ>R0ؾ�Q)>yǭ�
��=�I���X�=���=��@=k�0=%�����O�>z��=�rҾJۋ� �.>
��>J�k"�x| �%A�� �>��I�2ۣ�Ȉ�>6ۘ����a�Q>�5�=Ӓ�>�L����z�f>�l���>}�>�W�=�D>�����Ͻ�5�>�����t�>?�=�\?�u��U���>3O��[>�2����=�;�<�U&>`�y>����1����+�S�d=F+�$>U��>�������<6˽Q��v�=�>j㹠�K=$�ѽ�S�>�N> � >���;� �5l�T&!?�q ������?>V��>�>���������	�fUQ=!:�<ӓ�>�`�Uu2��i���q>��=�I>�t��v���'>H땾J1����2<�ʢ��yY>�Ȝ=��=��X�^�s>�vt���>��d>9z����b�<�gM���L�>�'?�c�><��,�>�:c��`;$�6��>i�E>f^ ��bj�d��yuP��t>��y��i�b0(?�.?�]���d콊��<G��^d�<f?�G�<r/�=)���)>��=�+4��b�A?��5�k%>;�=C'a� ;�=�c�<����h}>��}�>�~�r<�$$�&��>7`�=�Л���=J�/?�җ�bㅾ�_a�bNy�X}׾�����r=f�o>�3=�j�>�/?��D�.��=j]���>�V�=A�~����>KR>���=X� � L>�6�>���>>|�U=���>�>5<�'T�EF�����>�H�=K�#�d��>�r����y=����y�=N>14��!�]=^%���4�;���k�>����in��RQ��V���b��L(=X��Qfv��㯾�������%���IZ=�X	���<0�8�S=F?(       �8�=D`��J�=�N��U>��M�G)�>��q>�w�/�&>8�6>U'�>s�V>:IŻݖ�=t=F��@K=��>���>�|V���)=�Q�>OlS�@ ؾ4���=z���ڼ|��E�0>��ѽ��=-p�>�%�>�+q=K��.�<>���=���<jtc�:�O=(       �O���V#��U8>'h�>��>�4�=OQ��d0:�nU���Ո>����+=�`$>�;�;u�L>�~�>�]�>C�X�K��=:����>��	�/���7���>CS=dJ½��M>XZ>OB�>W�)���>2 %>����*�>�7��I��>����7�W����      �ཙv�=�:��,�>��z;�!>?8��>x̟��Ճ��<?�l�o�=_��=k?��ɓ�;_��<n��	 ==�0>TWs?V��\��=}bؾ�7?�R�P����>�"���{�]Ž���>���>���.����>� 4<L����}�>�U��蚰��>Ὢ>�B?�~�=��޾��S�g>u��ژj�Jr �Z��=/탼�·>^�N�Lk�釉�Q�o�O��б>��?�
?��.=�k��s��>��5<��>���ھ��=*�>�O�=��b=���(�=������>�=:���>�>od>�/�>��>�)d>�0[�]#�����@������)=l�>-��ؼ?�>Ԋn��+�%�����k�=�r����F�L��X�E<���JᖽA܉��O<���>&݉�^-��@�?=�hƾ���>x�����>m�������>m{h�nn;�6'��w�>�	
��1�>ǝ��k���	��=�>����6L���>�p�"��tC�=�D���<a�=��8�ؼ��>�;�=#�`?�=�SU�>L�>�)�7s��0e��#w4>��_=f������cۇ>o��>��?��>h7,���=���>k�\>��>��>q7��ᾛ��=�ū�S�ξ�v�C?A8>��ʾ]�f�x6߾C��=!��>�ز>Ł|<�۽~��>��>r��=����w�>��?ߡ��6R�"�����>�}��j��_c>�?x�H����з>Ê�> ��>%Ế��Y�MKV=#�!>��
?^����>�)	�<��X�@h����J� \?�� >a�վ�'}>��>�����þr���XL>G���׾�!�>g��=��V>j&?�1=�r>9���My���<[�>���>�K!>V�=��~>�+m>{���/4�>5V���3=i=�<Pg�=z�=���;��Z�T��N�>��a�+�$�<;�>O,x��q���B��T��:Rϓ>h~�=���<�S�hyx>2�(�+�z���="\X�U��cks���>���=��^��c��6��=�]�sy<˞>���iEA�t�þ�.m�^U->�H��Ԡ@���>m���*�S>Ĺ�=�� �qKj�ۡ>%
��#�P%v�}�o��f�C?��o�4!-=%��"��>Z,�a��>5o�>�f����B�	��{"���>�'(�W�>:�)���*?3L�>���=�~n�k�]=2�>��>E��̏��H5>4�>ӵ�뱛�f�>��c��T����$>xz��� ��ǅ�^VB?����N9/����>P�>�e>�.o�]fz>+����h�>��� o�>��=:�>���>##!?P2>�X�>t�������w	�=L���Xq��K>�ƛ>Ok����e� >��=�帽=z�f4��ʤ���tW��J����Ř��>[?36,�@ȭ�{Z�����?j�	�+<>�4?k1C�caS�Pj�>�,�>Ww�>8L���A?�=�
>r'G>MA��fB�>]���H�[G�>�ľ8�m��e�>^�y>�u���*=C(X����s�Xrd������5����jt���=       k->��ʌ}>XP>TA?z۲>�� >.��>�����5�>��H�̼�>u�>�~��q �8�I�>_=���<�1��`D?.��<Z2>��=Cp޾ņ[>�D�.��>��=t���k�U>z�#�GQ����?1�C���Qz��?4̬�O�Ҿ\��2+�/$>�ϰ>!�>{�p<���=��2>��>�溾k��=�i`>���>�w�:Bn�=��>p�g>0|��#>����E�_>����(mX><�=���äI>��^>&2��c��霽�p�S�)�L�Ⱦ�,�lMf>��`�h�ļB�P?��o�	�w�>:�P=v =�"�:g�=X3>:�c�,��5|;=�5�>�ь�	�׽PӉ>��ԟ�=G��>���	7|<=k��>��>���p�>_�ֽ�ɽ%�)��~i�w	��"Ov��	S?�m?�?,k���P��ĕ=�Q�=�E?�}�> �>	�u�K���&�=���F)$��o�> ؅>�*�=�.�A��>���s!?��#>��[�ii���L��9��5Q���:@�,��>��1���>�' ��>n"�=pE
>��P�����轝�(��>��=�S���E�>�m4�� ���>�6�=ccʾ���>
x��U5���=�v��	?L;��������=-`ԽW2���ɢ��#Z�>�S�`
s>�A�����������>�<��N���>��1��M���JMn=ކ��MɁ?����;>����׆8�c派ٗ�>KD&>���> P�%5�=ƍ�>�?��Y ��r=)
B>`�<�k�K������A�Z�����<��"�>L�?'��> ��=���<c=���=�6:>�Ɖ=Z3G�ŹǾR�����S���M�h>��b�������
?��8=r.�>`ğ<Gj���y>'#Y<R�?��\�Ҋ���]#���O>�t�>&~C=?8��+��>N�Ҿ�ؙ=�þ�ԫ��ˇ�G롾,��=����2ﯽ���>���;BТ�P��>Ζk�.�V=Z�j�	��c*�~+j>;x>���>�&.����-�a>mn�=R;(>�P5�����W�= �#�̹��$�=�j2>BMQ>q�n��2�?\K >�����=>�V�/>>u��������n>㴪<�]ܾL~ƾ�q�# >�w>��>��޽�aȻ��Ӿ����fS�>(s=d-�>����/�����������->�U̽�T����f=��hk&?��>���>�a��_����c����y��k�b�3W>���<U�s��,Խ���>z�ϼQٍ�!ژ=��k=�d�?e����?d?���>��d�2-����>fvP>{,��V�L?�8����D�?�t?��ѱ=������>��N�S�?���>ݔf>�El�����CG�����.�ey��*)%��bc����>��k�[�ν&�>�ϙ�1�'>��1<��$?�+>φ?��J�=;��=.��6]�>�R&�"?���Tؓ>�hW=����h��+�>��.��4�E*m�����މ8>��? $�=?���������">��?�{O?���蟰>vfs?U9I�Bg� S�>zJ3�����<}���!>V�ͽo�=&�;>6|Y��%>��F�k�^�My½QK�<�#<J.>Jن>Xے����<y#�=�Hҽo��z���N�L>=8�<�,�=`!ƾܹ���T𽯖�}>2֤>I!
=KIZ�-�����=���똠=�����\>�����R��ӧ�b���c"�S��>���M�>��n>&H���F�=��������֭>l�a<.>��T���>'{�=����_S=��=��ͽb>��i��
���P^����>H��=s��>�c�;W�P>�&�=4�"a��; +��4@>_����>�9���p�>�pG��M>wqѼ�M�>꧜>B�~>�� �(Q��:$=�`H>�A�=�9>�p���7��К>��5�9�����=�ˑ�a�=���>���>��>�>>��O>:2?@�d�"�>-.g>眉>�#�>�:4>?Ώ> �	��X>I�ƽ�z�����>���:R�?0�=��=>�:�4�˽�=)��>?|"<�}�=�\�>�w���>@��P�>� ���y>�}�=�xc>�j�=�>��z>�B��`��dt�>?���ڤ�bY>�i`�w'�h>�S��/iq�Sf�=��q���.���{>�����s�B��=Q�	�S�@��>z��>Ku@>��>��f=� ʻ�T��Ynm=s�?�<4?_����@���%��w�\��x>��)>u��>t�N�n׉���=�">WJ[�N?�F=L�C�m��
K�<�VP�,��=�
 >�L�:g�>%OF;�VU�T��=Xm������M�<�j?�����>�>az�<sڂ>���=�㊼�4�[4>�D��S��>��v=�=���� X���S>	��>~���t>4��>TE>���M�6��<U��=�{�=��½:F?�r>C/�;QB���}<p�\>">Y#�>���=on�����T
��������p>`�d��~��+�B��<r>�ޮ=��.>����hR�wy=�h����>���>��%>���>Ep/?Jmp���Q�z���$S ?k�^>�>H"߼sE?�RB�p����>�TT>���>;�Ͻ&I�>�V��񁋾��=�=�?
�>G>e��������إ�d���A�:�>�䣾�N>	��>�}>t���A_���IA������=��>	Ӂ��������h<��~=������O>o����N��F;F�O��'�>7B%>ϕ���٘>Γk���f���H�ӽ����Q�>
;d�W>V]�>�4�>�����pL>�^�>pm@?��&?�� ���:>:�>	�*>�阻��>�9�We�=I�v����>�c6�2�I>�D"?Qǃ>^�9�q|��y?�c���?1 ��\�K��>�%��N���I>�D���7>��a���پ�1뾳���w�S>]�n��&D��G���n��2Y��"F>p��=��"���{�Y?�>r�����;c\�=<>�d侉��=
�%>E<K�Y����¾;c�>[�?�����о��0?�5<�Ž�̂���������r�>/�C?��>�����?o��>y�>u m�(       ��=1Q=�m�>{X�><�ý�C����=�f�>t7?2��=�?�=�?i��fv���1>ap=�@?��,������/���G>�R��]��<W=q8վ}�z��4�½а=���>�!>6@�=P9�>w(�=�ld>�?��>�?���>��?���=(       /�0>L轹�g>`���ҟ�>�>��=Z9R>ԛ?�����EƾD�r>�kB�t�*>������>�����O��lq> �*=%��=tz"�H숾�>aĨ���_>
��ӏ�>N�A=�g�ę���a�>�����>��5>�J=}�F��8�>�ل��      ��>-�>�;�>�/��㟕���d�W����F>��,���X=�͔�r�7?��?�����>��>yl=W��?WP>�翾��m=�(8>D����˾��?O�W=_��>,��6�����=j�����>�$%>��	?��?�����(?��>fT�����?<>x>fժ�1!�>}ǂ>eB�=�ὖr��@0O?[��?Ln�^��$�?hu_�������T>�'ƾ����	�>?
v��+�?��>*��=��&�0����׽�j��:�L?�皽jZپ�?�`,�оy��>����|d>=�6�Z;�=��������	??��>dhA��2Y���ܾP]	?(FB>��(?9��*�o�%���E?�:&>����.�n���zI����ѽ1Eξ+ݐ>=�T�=�.����>��ɾ�J?A+�W�?���8),?�J$�'(�J�N>���<cG>��C>N�J>x�?m�����>9��>#9��r!��"���G>�6�>�a>E�?5� ��mվ���	S�[��>�R>"c����C���U�� �?=W����>l9>�Y,�ڧ�=���>�5%�N���}?�&���=����L�f̦��O�>]��aգ��N�@��}sP�"��>Mxm�/���>T�h>��>\���O�\��M�l�/����k�>6׃=ק,���?�C
>5����n>O�MO?�U��E>=&V���%��ō?�*�,��=[2�>������?\9q���%�W�T勾M���)=S٨>����}$��޺��?E=��>WȚ�8�?گ>,����*�>o=P>M�T>��>pu�>w��N������>�9�=��S��f;>?�D?��l�;�̾%�=p_>��=t�-?m�۽v�>:��7��i��1/9�g�?>W ?�R>_%ѽ�׺�?�=�غ��/����>Z�?�؆�?G�>m�Ľ�D�=��.>������ѽty>��=ȃ>ذ ?��?)¾/�>�L!?y)�>� �>4�|>hF���O>f�n>f0#>F���"�>E�4=�6��.>W
��>+u�?h���__�\�㾑dX?4=g����q�>
��lrY�vՆ>�S��2���bs?u����]���?�����M?����=�z����w���H�k��=N��u�������že
���>��3=��2?�K�=f�� �f>c�G�*1=��>���>���<����D?A67? �g���>���>�����a?�ϻ>Q�\�(o�<V�>�!��m+�$�?i�=�+��>��U�K���ta�>�4���厾$��>q��>��<?���=�O�?s��?�$�_�佔����*>�圾���)`?�9��Ѥ=�ʼ��?��>�@
�0���Z'���Oξt�6������<.%���l>��=�^�5I�>����\�>�K̼v_�m��>~H?��t���Q�ƐȽh��?�#6�X�W�T5�?���D��>엿 #P?����!�=ѷ�?ivM?-�M���̾T��%�.�ѱ�>��f?�EM�&_����ro�=׬>'L�>ǯ>��C>�ߔ��W�����       �?"�f|>�{�ޕ�<��=��.>��@>8X����M�&>�&�+�:�1K�=��>C&�秸��T*>�n��\��}�h�"�w>�[���h�ӡ-��V�Z/>��W?i7�>�u�>�������?�b>�����9�]�2��֎���>�>"U�>G�־N��Ky�<[�>�;f�U�>��.<3#>9D�������c���D=�=!��=�Ѿ���J���3�>�? ]�>#��w�@>��>/+����=������(i�;���a����f��i�>�w��n�>���>����Ig���r�>�۾�;?�zs>�-���0���#>��>1�>p��<Q�;>��4?]�? P߾��=�Y8>Pc&�׭�>�^�<4zQ>.�D>�Rh>�R
>4�=e;�=A�
?�䟽ۀ�>�/>���T��=b�1?�맾*���3>��=�8�m8�;��">���@c�>Z�j=�幽���z���?���>D�>5�>�r�>x{��_��+��"�=��>鏗=;�c=�:�����>opw���Y� �h��
S>��ξ}��>�B�4���;W>�G����žh���Aw> sN�#f�>PY���֪>����Q��j~��C�.6����G����>��r����������)��D->���$Z{�p�9��pv>? j=���k��>̑U�N+a�4*��n<x�K����:vd�I� ���Լ��N>���>�\S;�K�[�>��=5��fb�>(��;�>�{���2�>w�(���&��*�=�/W���Ҽ��?=��=�	?f�>��>F�?�W�<D��>����*n>;�>Ή	>��C?[�K�f�O=�(? ��=X%M=�g?7z�=HF>��g�x=	�>�	N><��=�����vd�)2(�M:Z�{�h���J�-`R>r�$?�3�>S�J�A���a��>}�=[2��m>�^?�����U�>�zJ�'N�=������>�&վ4�G�VMa�@\�>����?>���>͐��(̾+�>�)���Ϡ=�5�>��=�/�>Z>)����>"��=��]>���>�Re��.���Y���B����>����ž��m>h�*?��;>ݟ��C�>��=�L6��R}� �n܁����<㽇��0D�U	ž��.=ǽ�<�TG��"O�u����s�6.��h��S(����mĖ��F���*>���>-e߾�7>����&��|ݾ���=L>`�Z>:Yy���ξQBo���r��R߽E8>���=8�<Lio>_�c�oUE>r="DK��i>AL�=�">���� ﷽Y�k��<����(*>��;��B)���q�W�_>��콅e����9>��>#��=��>�Y0��!Y?*)�''��C�Ѿw� ?K,�����>�Y^�S(�@v?��> �S<���o�*?E�w>V�?��>P->�9Y�~d:
�>����U�>��	�O�'?���>�3��a�>�TD�X�>�C?y����:�hA��򜡼��սx(�<nL1��[+�,Wy��'	�)+>pQ��IC�>0.�nn�>��h�)�¾��<=�d��VZ��Y`
��>�H>�Y\�o��<X|j>ƫ�=
��=-l�����>*�۽��>
ۙ�
���1&޾xa�>���=BX�;@N׾�1��򣾺�>�z�=��><'l=��>Ղc�����Z�z��瑾�g���T>�]?�vdW�톂�ƽ>�l�=����t�Nؾ"�s=&��=bh=���>@QH���R���=�}���>�v?�4�>�5>�rD=�!v���>�
ξ�͊����>�ڈ>�"���żi��>�u������w>x����=mK�>�|S=�w����=�>�˾�D.�����A	���>
`��� ѽ��;�]G���
�;=S>ߟQ>�/J=\�;��ݾ����^��v����վ�Ʊ�YA�=�%?��x>�0����5���p�=D��>����v����=������=��Ͻ�\�= u=<��*}�>���>5 >�03>c��=g:_�����@ٽ�����*�=��w��>5*�#u ������=���=_Eb>;��>�v�3�(=Iy&�&S�=E�>�?���>Tמ�[L�>隍�,mٽ3�>�����㤲�l�㼴�M��t>Ϡ[>:�=����>bB�롥�!�
>�)a�o�<�K���<�=��>�c>�!
�yz>@���=���/��=���=�u=�/2>B^>�����n�������/��́�Z| >���=�CQ<�
��ybs=��&>��H��A[�^@=}y����Խ��>��e<�	����>��
�B�O�����ʜ>\�b��1�q O=��r�ĳ���{.>�F�et��ӧ>O`�>g �:أ="瞾�b�"�>���td�>�׊>����f�>�yU�b�]�6�kN?S�'>�f�"�?:��� &��%���R�=2��>ִ�Q燾�uƾ��#��H�>�%����s�����*=?�}`�0�">[6S�xpn��V��T���՗��m��t�l>�+-�?�o>��E�H���u��>���"Xּ��=~�	<�*J���+�(d�<�;?�e��=Ry����=��>r�>bW��G�#4X>H>���*?&D<բ�>HŽD链uT�>���>�j5?q�"��8�=kG�>;;?:�-?	�����c>�W?&�%?ݴ�;x�<�߾�b�=���>啼f���F6����\rm>�B�Ņ�ӵ�>��;�s�-�]�ՠ���>j�>��0?9-�>���뾾U0>��Y�"%V>�l��י˾i�꾂��N�>�c�����q���������<4���¼������WX���'�3����e=�1����������O>�:�q�et�w�
�=]�=���Q�= ��	t�����;�,\�^�+��m=�5Ѿ�=>
�"��ھ�rV=f���4<������ >Z�N���=8�6�<�>+"쾬� �nI���=��?��g7����>���ٽ��\�{g�>a�^>��=�放����[��g$�� A>��>�����ޅ�Z#���I=�ǽ�K�V���Yν���H�-���X=s����w>&H�>@н$ �>��>�q)��/����Y	>Ԑ��@�=8��� /�Z��>��?}¼(       X@I�}�D�rܓ>�ޠ>�gͽL�>���Y�=j�b>�w�>��n����Fk)>�����+��p�=��4=��^��1�=�� >�c���zM�=�?�7_��@��5�=�-&����<O��>�%>u�=�O>��>&E?�"=㴑�,8I�*�m>|�>�$�=(       �t��,��<�Z�4V�C�����������=Xo�>�n�>�
�=��>6�A?�>��<0�ż�=b�M=W	�\T�����\>�:���_Yf>�(��D�c>����b��>�Y:>r7>,�2>!&(=\���w�@?�\��º�ay=��>�?��ͽ�      ���(>N�a�:%�>����*J>�J�>�̾6���d��޾N�=$mr�'�
='������.t�7���.=ϑ��о���>Zj����90�>��>/�=�/���XۿP�� H���� /b���>����WD%>j���ȾlP�J���p>�_v��ѹ�ܭ�=�:��A��>���>��0�B�G��O�=�zK�XV>h�=c����/B�z=7>D�?wO���(�=��>��
z��l��'���<)?�Τ�c���B�`�
k�=f:�>���>Y�s>�Ҭ=�x���꾯CR>�c��f�L���㾒� �hfھX�˾0k�>IM��f��Z��N? `��'>�k)?g�>*�>q+>ܢ�>M�+��<�j>���,Ý>b2Ǽ���<�\�p�8��W�=��x���|�qXc��p���"D��Jj�\8�>�#>ˡy����w㳾�47�{t>��>�M���O�<QR�da����(?"�&��>����#?!��>�r�=�!�>/T��m>���3P������>P0d��׽�՞��,��k4H>��_�� >�s�>�l��4
�t�c�����@>�8Z�	RB=4ܙ��T > ��<��g�n�$�"!;��>>Y:�?cd��ݡ=!��=ӎ>32�>2�6�/�_�B�=[2>G��N������>\��>�H>R���u�����&�C7��3�ν��þ�C��@�S���>�s�>ġ����>�伍�>x'�='Q/�M�`>0g�>��>^��>$�c>�Q�=��龪�x��9�>�'�����Fo�䤖=_��=�N�Mbf��=-����Tٽ�X?�}�>�g>�+K>V�!��r�H�e>�W=s3�>s��������=S��>K9>��=#?>S�Z=bL�"�=�-�>�X�����=�/�=�&�G�c>o�>F־��ǽ�����~�>!�Z��ۢ>VUԽ�="۽�kҽ|��<�g�9*���!����֪>��S*��F�#>�R��e)�$<ľ��B>G�?O�Ӽ�c�=+�I����>'��>�F=Qbu��X?����<�䇾��c�S/i���=BS���s��
�>�� ��C	��|پ�nx>��->�>��c����^><��qZ=N<?��ѽ���>��~�?k�=d:�@��>U(Q>l<��WV=R�����<�k�>�Y�(���O���'���Y>�pO��-?��>�x��>���Ŏ�G�d�wM�>��F�K��<b��$��ώ:��ὰ�->ֳ��"���+�sվ5�=:�>=��þ���=����/���d�V�
�>X�d=1ܩ>z��>���� ��fv�����'�мd}��z��־Z�;2�=�>��.-?�4?u~>�AҾ<P�>���>Bs9?�:��Gs>��'�Sٌ��| �FSq>�N�?Ռ�>��;zP��8��-�:дB�$/�=�N7��g�>J�)���>�2<>ͅ]�<.H>�>�>K%~��20���i>�ˢ>�,���>����Ѿ7���8:��rW>�UZ�,4���<쓍> 
>�z�=b� =���=���=	���,��>0Q'�[>���>�a}�%̛>(       �1>��>j����Ć�O!��l�>5�h���>��ѽ�ˑ��T���#�=�B�:�l>���>P��<�O��{#>\=rS�<�Z-�qw��GM�>��9�I�>��M��`��('�<�=-����EѪ>U>>�P�=W#�>* ̽t�>���>�I����!�}�ֽ(       ���>�Hb>�)�<�,��F��;cVx>^Z�>�ۛ>E����Ϙ=,��=�����2>*��>!�>|� �@������=��O��/
?��|�n� ��ߌ=f2�J��=C��W�>g�">&��٥����i>��=�@?y\>)7�'� ?�>7���Z�>�N�=�      ��B?�X���>�̢>~k�>$cp= �H?]��>��?І�ʓS?����mG7>躾|��>�+��_ߟ?���=C��>U��r��=C�=t%�>�y_>�>R���E�>B#ܽ���VnM��@�=lׅ���m�!$<��/������>K�>��>�F>�����,���&�-3G>!���m���徘�+�sJC��D���o�?b�>��,�Q�=N=�z�>C�Y?����q{�>'�*��>� ���`���>]�>h@�����ZC�>Q��?-��������P��*�>Л�P$�>���-׬�^�A�:'�=��3��Nab?8�1�>3i|�� ſ�f>�JҿԻ�?��"�h�,�=�����N�#<'��=��?�o$?h����<�`��\��:�=_a�> �K>U�>�=�>��<����>pfI�'�j>���=���>��ڽ�6о���>��9=U�=�<N��>}�F=b82��UE��y�>�Rl��b�>���>fS��似�-�>��>���>�B>�)�>�û>c>c؈?�&?Jɾ�x�>�̓�f��>pA>��>� ?�iX8?��0?��0���%����]8�>��H����>���=�A�>�?FW��a��?��#>ǔ�=��>6�F?����4�?;0r�m�J>�����|>G�/���7>��>�"?-<w>�.Ӿ�H��|2����2-��%?+e�Ӛ�>���>0�>RO�>��6?��p?uee>����uP��C�dʰ�	��>��H�-��p?�	-?��>|�>cʽ�%�=�~��(8������о���y���aY�,?��;��B�>Q˽����� ?�4?�_U?9��>徢>���>��'���� ��>�>��2��T�>��>�b�Lɫ�/8�;9T>-9�(E4�*q��V�>~���������[[�>�]">�WK>!i�>}����=d>p�>�x�=a���b�c>)�ϾZl�>�A]�<8�=�WO�&!���y��z_��lk>��C>iʾ̥�>O�1�z����s�>�}L>ww(�h�ɾ�<?���=�[�>�ɣ� �,�J=�GI>���>� ;�=��R�?�,ž�̰�Lŉ>�/�>��Z>d\d>�)��^	?t�=�9!�L6/�G.�>=�>������_>�w�%�3?LC��<Pi�
�A>_6>���>}�羇�?��j�={���﫽�G�?v�>���J?+m���{�>�M�>�q+?��Ѿ�'�4UP�$�?��=b�?�޾&�� �=>?���@�������^>�>�k�=|7A�KO�	�f>���=�1�=���{��>L@j�4��>�L=���=9>�=����8�>�bv=Z3ھ�Ս��k��d�M�������ɾ�"$�9KN?y���y��I!���?0�?�%ܾy� �S�>����Cl����>{�J?�$�>��>���>�>5�~?~��<ԑ�>m�����J?��d>�m[�.i��ܾ��g?�ԋ�#3!��S>sĜ����z�e>g휾��>	�L=�?��>�̾""?�;L>���>���� �=i��?��վ�+�>���=       @��J�O�x�;�_�>�Zb�sq�>��=��l>��>�]>ܟ���z��/�½���>r2>(�n��}��cr=Fw޾\���/��?[�=F�=Ѳ�?"�L��?�=R���-�>�8���>��=�č>�r�	=O��S��Q�>�a���>��Ǿ�G<6bC>��T>q[��mK�>�o�>��S������v��2~þ5���LO��>��H�=#��=Gi>pWd�d���ǈv�~a?�?7@|��N�<�8b={>�����>s��$/�_֠��~~=Y֥����<q-�=u��;�Kd��D�>:e�=��ҽ�笾A�1>ߩg>aH?��4��;P>�ϓ>%��>d����0��/0���ʾW.Q�=�=�ž�Է��?��F?���>�1��徵�7�>�T>�b�Gf�RD>W���Q1�۾-����唾-�<�u�r�(��_>]�t�l>�*��\(ɾ@?��ǻ�>�.?0��=ye�=� �=s# >D 	?�\z?�O��	�z�J=�že!�>>A�<����CK�1K>é�=���G�T�I��>���>��s��̧�圾{ѩ���Q�َümef>���Wj�����@��>R�۾��a�����Y=�C1>�л>N*>��>I"�>�����>JƔ>�����Z�=���p�h��u��.mg>w�{>ӳ�>��=p+>�����쏽�d>*4�Lk��r�l=�j��� �83�;��ۼ�ꅾ��/������bb=!M����0��N��fu?���n��<��G>}nξ�2f=���=�>�2#?l]i>c/>d//?�-�JDc��7>R��>cc��#i=�o򼜗�r�?F4>����H��>/��>ŧ��3�T�ͱ>�+���];ɽQ�'=��>���::w>�L�>��+��c�>fc��=V��Ȟ�>2׬����>�L'�y�g�����9���>�>v?X�X�ㅚ=a!>	"��D�>��c=����ǀ�>�K-�"9`�-�/>V�>��K��2��:�G>k������=*w����C�=(D׾�`=c[��('��{,��A��=t��=ֲ��h[8��Ɇ��G5�ӆw>�c���&9>}m�=?�k=-.>� l;qv�n�>]>?��<{?.�o���;ŵƾ��q?�}�>�(�>J~�=:I���s�<r���� ��:��,0�J?���>3�/?v׉>����y>�t,> ?�̺�����j�i��)>߇�>��߾���Z�S>�>�>���C� ���"?w3�����K�d��W�=��=V��>�x����D�d���<�s�<
 ����~���s�==��=����al����;n�>�M�>�hV>�����K��MJo�#0>�`���?�%S�=- 3�
^���>�d�Jb׼���=E �>��?� ��'S�Gq��!<��5��>�*���ʽ�'����>��)>�<G?)>�b�>i񺾌�>���5ؾ@u�u?i5�=��.�:Д�8GB>:j?�����fJ>��3>���|�v���3��T���_���g>Y6�2�7>�y����+��'����=N'����=�>5�1���U�5�!�ӂ<7N�>ޟ>�
�= �:�ـ>�/�;GwN>��>���>�w�<��>��݅����=[
ż������K>������Ͻ{��>%�s�0����n<A��E�=+!�>&o�>�����C��_��=)S[=F��)!T=�)H� �E>���g��=$�:ŋv=v>|� {�>X�t>ڭ�����>T�>�S�=��c�~�8��#�>-�
>z(>m���E�=(�h>���=e�=�K��Ӣ����}�}�V�I�����>�j�>;n#�_��=.���o��#bh���:=x,E=�6��h??ɪ��wY���J9�qW�=��S>���> �5?f��>Y{Q>G��g�>�e�|�lNe?�9�B^< �� ������%<s>2�l�}��>�NA=�٦=L=%>�ʾ�ǽ�h>d"�>/
>�}�>ɕ��s���&�=�aW>�4b>$�Ҿ8�>MD���=d�:���>ⶍ<�C��ݣ���о{��!#�<���g��<�e%>�~>�
!��σ<☾�>����=��U��R�Uw��u<������6ܓ>��X�`Ħ;�q3>�Mq?��O�|հ����<l>婥>���>�-��l�Ҽ3Ｚ�i����>����E�m�;f����>�q~>�ʾ�)�=ܳ>j��>ƍ�=}�>^����0������>%�i�y�>4?>a�t>��Z<�m�=����q�>mD<�2q=����Ч>a�F>�@���a�=>g>�/�>)Z,�Ԙ`�9f�=�=��=�%�������b?7�=�F�$y+?(�.>����s4�zKپms�ߦ�>��?�7�>��L��5�>[�J<�j>jx>D,!�𕖾��;��=�Ӎ>���>�`�������%�J,���g �Dt�?;��<j�>�M�?�p��,=�0=����>�Y{�7��
3=��]?����\����$D�@6M=��ྼ�Y>��ߜ�>1�o>2�2��i��{&2�0�4��g�<4���t ��/����x"/�����&i�=)�b>��r>�>LLO>�q�0��>��>~?��7ƽC�;F�[����߆��j���� �úp=21���
������E>^�+�=&p�>&�=�=JG����s�����>&}/>i_=�
�&t%=�m<Jg�Kf4>�+�(����{�>t�����>�IV���?�!�<�>�,�=5M�>�p8?4 �v��>^;>��Y>�):�) �uY�>��&��?�����j�����վ����Ǫ>s`u�h%H>3�>uƇ�V��Mq�ˑj�X#�=*�ƾЌ?E[?+h��=��RG��Pp>aI�>fQ`��=>:��Z����.�>�J�W�v=j��S�J��]�>�	�C�%�L�:��żrQ�=�"���󗾕�,�C��=M>�����>^�Ǿ����c��>�K����-�&?[�ʘh=?t���E?F�R�+��E>6/>.l9>�5���K����>+� ���>2��N�>g>�{=zB��A�>b�W>S� >��^��"�>��O=�ř��U>�X'��s��j<�:_�Ǿ�`%<!>�%>�h�1*���.>�>�冾qR��(       q�׼:��>��&��2��x˾��Ҿ�&!=L=�	>Y��>י۽�
[�Z>�>u=����>r�-;NJ��=>w�� K���ռ�������
ث>v8�`�:,|_>C�r>v#��Ϋ���2�;�%�Ivؼ���>ނ��ֶS>��>z�F��,?l��=       Q�>)$)?��??���>Ђ?�J.?���>QM�>D�R?���> =2?I@B>e�<?ӎ�>�]�>mü=ad?%�^>�Ւ?I��>       O�?��T���Ի���»�s�b�:z�����Q����;"�:��=<~�/����;��:�S�;v��̻ |�;��;���       U�F��Ѿt㼁�2?$�6�FN{;��v��$�><�=��%>��=�Q���j�>nI��5 >�S4���t=�A�>�E�=�H��       A�<���;.�;ۿ�;��<�=�=��<&��<tH�<�Pk=	��=���<�I6<0�)=1�<�t�<.=�|�<]L<�      4��=�=D�m>6��{�O>�M*>���=>�.>�%ӽ��>N�E�f���P뒾��+> �?ߟ>>�4�>��Z>d�(=��m<���>�y�=��/��Ȁ��? ����=�rM��ҭ��V=��h>:*N�|�8>J	�M�W���<>s_=05�g���ƞ�-�>�O�>rmX>�%e>mg��m�=��ֈ�=���=��X��:�=�-\��!9�Yڬ����>d����Z�����E
ؽ|��+9{>�0�<sZ�><C<��>@���nQּe�)����=.��>/�����>�Ń�	���V��X�;�p��[�g�1��=P��=-��t?��W���%���B���w>ř>c�?����r�ŽJ�=�y]>��>���=�w>A�˾زy>fF��r���6Z�������d�D���,��`����>{'>�D�n����ʜ��J���>'��<d߽���>M��B���%ھ�Bj�����[>�b���M������BY�۞b���a>�cL<+�>��8:B��?тP=c'e>�r
>|�������ܥ>Ռ��""��b���=�?��>c���L�7��	ew�S�>��	=B�t>����y
>G <���]ұ>�X�>O�?8�]�N>��V=�4d=�<>T�����=ҵ��5��=�b���Ct��F�>��<��ż�ua>q׾��y���=�;O�.ؓ��-><��;�>^ F=%x�(>�ݤ��J[�x̽7V;�Ӧ>l�������;5N�>K\>$��s�~�k>�F	?�Ⱦ���>^XC����=QD��(�>�d�>+%�>�݅=��w�~>�ː<��O��>d�	��0�<�|��T�>,�M<6,��'��>#��>+����󳽁�2>�$=�yT��t">>�c½��)>�!d�}�s�Ϋ��0����'�Sb�>��\�o޽"��>�����P�>��>���Xi��z''�!;�E[�$"���E�.@>	rL>�S޽�D�>�O�I�>+7�����(���5O>�(��1��=��8�M>��>7�����8a�=��?=�uW;Ӫ8;�+�<H8W�կ���b�>�!��\�=Ȥ>�0\��6S�eĽi�O��K�>�M�=�b�>��>֑�>��d>A�{�[�i��
�=y�\>�ٳ��Kܽ�=����=�H��Z��=��so��oeh�=��>1�	�zY�;��>0��Z^�6�ϾR����s�>EH>�	n>�;��3e�=�Ȱ>�����7�p�̷�>�o�=�Ɛ=��L�I1�=;��>R㑽�/�;��d�&5���T�=w��j���+>��=�_r���0>��P���p}>tbZ����(;�>�Ͼv]�=<��`\ҽ�)��l=�:��>�>'X#�,�c=�����<�>���5<��.>09ھ� *�=�� =�~c��Z�ݯ6��W=��>��>�0�=��ҽ�Y}��{=�v罕��=��=fa޾�u=U#���@�<���E��>c�V=^K�>�=ž��&N��/o>YF>-���}�1��S��pmͽ�v>�y=��z$���N���>�ɤ�a��>�~ν��T�7%?��>#9\�       ��}�Z{+�Z
��\;u�����=i��� /��Ho�����R=~=u�=i0<$L�=�r�%E�=��=YO+=��<       �t.?�n�>���?��\?=�8?n:�?Y��?n�?S�t?��>�3�?m]�?	1?���>�7@?�_n?R=�?5f�?��u?g�?       U>�=�>�/�>�u7>��>���= }/>,�>V��>>��<�>l!�=3��>���>ذ>Z�>`��>z��>���>�ܢ>       p�|�l�ü/�#��a�;�������=y�� q��ވ��$�]���<xk=Ya�==�=��=E�l�O��=���=,�=yA=       �6�>�O?���>��'?���>@1�>�M?�`?�T?�x�>���>�z>z]?�?'�U?�Ǜ>���>�%&?���>o�.?       O�>&�=G�=t$ɾ�Qq>�Ɨ>:���+>\Ñ�R%u>�����>�þL[>OP���=��Z%�".0�����S�       =]�=<       �`��U�7�����=��>T=�>��/?��-�]p���1�:�P����<��/?xwO��4��-:�>�����8?�ǻ��,��o�>ٿF?Z.(?5>ƾ��>��>�b����Q���?�����꒾mG��kM���8?�0��'�<��?>X�����1���?^�Ҿ�6C��`=���@L������?xN.��Kh?��?9y�>� >E֘��k�>q&��핾�yc�o4?=~?       \�7��m��>