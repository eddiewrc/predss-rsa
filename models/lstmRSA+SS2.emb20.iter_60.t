��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      sx�#ۂ?���>i^.�5b���r?u�"?¾8�,��<K��?�ܿ�f��`�*=��=���>�0��qt?,sŽWf�>b�ؼ\�0?��5��믗?��H?��-?���>�b:?� ���B�=����	�>~/���S<��/���f�.p�;2�L>R
�>r������^�.I>"Ȏ��Uc��Z�k$�=�|=4����+�>����C�
������
?�V���G?�:8��6e=�K!��
���)ؾ��'?S�3?��H>��E���l<>�W� ��=�<�>!�!?�x���l��!���u>u��>�s⾟�?����t�=P����m?tVj>�k��\�=	?H=Ň��w�s\�=w�v=ۦ�n��箾�+�� ?�[�>�4 ?�tѾ,9�>aŢ���J>�c?�1��d�Ͼa��>�)�d�P?׭���x�>p�g>ߛ�?��9?�4`�a1*?0��>�D�>�֝�`�?��<���s�b>X?
�lF��l��>1D�>|�?>x�>����ݟ>�,�=`p��>;>�%�����<���>�'���+����>7�V�CX��j�>�z>L,����>�9=���=�{�>j������L£��?���ͽ�CL?�e*>oeh�X�W?������G�����
?�>��?f�Ͼ|x=�;ѷ��� ��r���'?j�4���׭>Lzm>ř>�6�T��h�=TZ��๼t��>tEp?�K�=� ���>Og�=鄾�5�&Q�>(,�>\�ā�<&��>�䀿����G;�	Iq����<#+>��=d�K��ӗ���>�m"�4�ν�"��w$�*��>a<��=��>ˀo�O�S����>�h�=.��<�>C���ʽ�����e���e��~������ٛ
>�?�~?H1�>�:	�¤����>���m��<g�=6r5�2���K>�i/>�]V�/{�=��-��U5��C>�(2?��>nr�za���>_���>��=×��{Bc>2F?�B�?�q?{��>�5��K���#?E��>���=��*���>d�=�黼�0�>�g`>����v�%�G�ľ�u�>�����S>s�>�����,���;�}.��H�/��=��8��>�>2��>h�O>}���E?��>x�w����<�{�>"�>y�>#��>T_�>�i>#�?lyw�%�=>�/M>�Z�SpW?M5?d��'5�?��H?щ?9̌?۽��¾fpV�~^˾|�D?B�>J��?�?��[?�0t?�So?VPq?Ί6�r��>"�ѿO%���v?@�?9�>�0?_�t>;a��Ѿw4ɽ�Ӿ�.�=E>�>��Q>���"y>���!N������ �����>0={�}>���$��>�J�=���{g>?ʗ>Dg����R}.���?'"�>�t#��b ��y2�IQ>6��{F>��@?� �?+��"�	?mH<>y��Q�J�=*��X�m>�׭��W���Z:?(����>������?�]Q>���-9?t K?(3?@�U���E>�q6?��b=C üx���M�#[P��!#?�m7�%A���-�>µx?�+?��>���=�b�6<�       �lؽ{�������>l��=�ｐ��9j>���>�k>?Cf����-�齁´�W�=��d�g:�=��>�<x����L����-�¾x��������4�O�?MNt>�q���=�/r>A+���-���!�A6�>��0������@e����>���>F��_�1>�ǻ>4y��AB��Z�=��ĽÓ��*bپ$L��q�Y�؄�=Ė���%?��>JA>7�>�Dd�D7,>��⾥Ӿ����^�>ME�>o�6>\~�>�ס>����1�>[�=w�>�Z��l]>4M�>@,<�Ԙ>"I��y�I��e>=�G>��L�&悼u�>E|n�"uu�1���	���{>�н�Mѽ�	=��S8�>-8=Ē�>�aI��>���	 ��g�=��*��	�-*��
Q=��F>���<�뭽 =�>ɚ�>9f>�x������]�>m����F>"(��Av�=>���jN�>��>ކB�����}�>$-S�[^�=>/>� A?�V�=߂9�qHB>L�'?�l�>�LG=�얻8\X����už��>�M>!}M=>�>R�t<�=���;b���t����wj=)��>��@>����뾠��/�?ū>��?��>�5?�>�C&?���>j6��5���$�=�~�<��P���֬�H�>v�=�I(>dڬ� �׾:�����f>>b~��8��HC<���>��<K�X�X���n6����
�R>�D;]\�P��Р���]� 鏾�(>�o�����	>���>����*�D��܆�>����\>�;�=Z��~.�< ����_�>fd=�Q?�*̾���0k�R1;>E�c>X���P9�=�X�>ĹE�����ދľ�S�>�V^���1�c~�>>ݿ;v����O���Q>]�K��3�>՗��&�=��?��O�6�>T�j��*��+?� ��weC��	���J��<k��>�
þ���>���>��?��=�U��[䧾)�l>�!>�)���a>Ɂ���"��v�o�5>��*��:K>)�����ν[(���(->���=9&ԼŎ?R�b�R�=�(���뽉�4=k�?��J�c�=�t<	m��϶j����=��=MVf= ��=�G�=�S��=�������"��>ˀ>>ݧ*?�=�6C��G">��>��{>?�*�̈́�>\8(���<�Z8?!�k>���<.B>�(�=R(�<��<�Ȗ>�oT�Ǜú����" >��?�UNl��%���>�_Ľ���},���շ>����^��=͉\��G!�F;�<�y>T�)�����&a�'��T�G����=:�=�T���aX��N/?xa�>��|>Ш����|����>�'����>ܫB>��E>��>�NU>�>0>ɠ=Ҩ>,I��u�ξ��4��U?By׾��6�+����?�����@->J�&?:P@��c��x8޾����	>�\���1�>*j�>��=(��/���T1��z�>��=	�ɾe�>�2���I$�����x/?��>&?پ�Df�
{�"��rَ�櫂;�+�>�~�=C��0z�:a2�*ѝ=@���>"n�z��=G�=3�����=�'�>�	P>4�,�A>A�ýAо��̾�0J>$�>E�#>�G��Z�u�#��@8��>pd�>�o�	��Oh�=�N<-nH>I��=���=�'��7cw��f>+N>��4>�A)�Lؽ<�ΛR��`	>��Y>��j>GZ�>8���3>N�>�Vƽ6�>P��Ga�a-������<��e��s�?L?7ev=�����GB��ٌ>-g�:%7�E�>T��
�Ѿ�6&��>y(�>���>�?�a}�N��>���������+�#ǅ<Wc>��B>�A=⬄�͌=4;������,��?�E=�6���G��^�>��>`�>S�꽤Q�>&2�=�"�!(��A��=;��!��E͇�ʨ>���ڽf>F>��=C؀�����Tń>>�����=ߪ�ֹľ���>�Ώ>z9�M2�c2�=��=n�=�y�=��>���-���2>���<�>s
���4;E�k>Qz�>~����*�dne<�C��W}>���=�k�!E�=�;�� Id�����R��� ��l�>hNܼ�˭=�bn��0�<y>\!<n��>�þ[�þW�`>s$"���u>!�H��&��13=��a�-��>�C�I�=�ZP�(Ir��_>R�Ծ�`�<�c�B/ =�w�gs�=�2�>�L�>/�=]�?�2z�U�=_����f��w�(���ｽ'ջ�̾�F����>�X ?�_�>V���dRݼ��ξ��1��,�E��>-�.��?��y�d>?�9<�?^?|>���<+`b>�Ԟ�Q���/f�>�>Y��;ݭ>��4���@>�>ݭO�*䔽 �<��w>K/�>��?[k���5�<Z��2`=B�k��+f�a"d���v=�Eƾ"��� �X��>���=� >��>�S����)���5��!ǽzN �+d���1<ۅ�-V��H������>�i>�\���܃� ��<�U_�^-Ⱦ���p|����;>�|���ʾ��>��>+Pk=21�>N�a>&�>�F�<�AC=�Se�(Җ���>�g&<��>�A>ͭ��P�?���>�����>W'Ѽ! �=.h[��S�=�P�>X�>#@߽��<=��?zf<��Ȼ���?�1>Qiq>�`�>�'P>�x;MMĻ�>�E��+�=�4�<�r��2rὣb3�挌��ȴ�y�������ͼ��q=��������>�)=A��>�h�>X�=���>��¼i�#�Y��.�>�>�
?��>>�H>!��>0q�<�r��*�u>��Q��f��:?�$���A�<�3(>�\�N	����>���>�Ƭ��՜�cY��xѾ,� �U̟>�;>LT<�FĽ�}�>7�������T��+I��H4�S >t&ƾ�R[����o�����x�?2j�>ҽm��>h'>ʩ�>�\�=V����i����+�ϸ�>�_�����<b15="*E=���<@�3��<�>���>�(�>���=Zw�=�A��K�7�+4�!�&>��!>�x �����]<,���<�^>f�E���o�?��+�?G�9_;�O�>2�">)0ƾw��h�$��>�>Ǚy>       K=S���߽�A	���A>l����{>ď�>4�����:�z)>C�� ��><��uS?'y�w�޾䀐���½I>�>�>�X�;���r����g�߾GG��A��=Q��\����Q��.>��=����/���-?;�X�}�>r���~�>:���L�U>f��<l������>]+�>Pٽn}b�h��=� ��H<s�x=�YF�ʂʾEE";�-�YX/�pf��j_����r��V,>�p�����mо#!=��O���=�kH�'�$��[��{�=��=����Z��7J�D 辥�>�5ż�h#>�tY��H�>��mr=+����=�V<m��=�-ƽ?���y�>��ŻEv`>��G�c=�-���6?IO= {���"��F���<��>�%��r['�0��>Ǝ�>�,�>�g����d>`R�>޴�޾m��>|,>�ݻ/�+�S��>�Μ��gm��W�ly��ae\>��"�4��>5�m�>�l�I�I>�!B��Ԙ�K����J��T8�H�
�7=���>�B��G���)��8c ���>��>5
?�>/!��k2�>;R"�cA/�b��>���p�>�n�>eN�>E�ۼ-��y`;�C'>�>��н0�;��=UF��Zz}>���>JL?�������#R'�)%7��Խ0<�V�Q�Ӿ��<y*�>O��=�`O>�F7��v(��a��͂>����d(��4���q����|���>��O��Э=��x�	=ӝd='y�1A����_>��>��.>�]��u>�JG�����$�>C+�� �� ����>����y��=�.>��[=��k���/�*�YP�>�(Ҿ�Y�>~3P�tO0>V~��)�.?�%>�|?�ѯ=�A�4o=ΧO�*�����u�,ʁ=�E���q�sz����=o�B�`�+]�<R��#E->B߽�2Z�FC�@���y8���>�G���"������[8>V����!�2����%���2�>��<� N�wV>���5ӽ��0��=x���0P��,j=���|3�=�$>9�=�mҾ0�ƾ.��>����E~>�x_>�J{<�［e�H?�!>sA>(����5>l�����=�W�?/ �>���>�,�>����G�$O*���7=yٝ>�'�v�]<Ӓ,>�`D>蠁>k֚�O�>!š>(8�=�a9>�/|>� c=!�>���>8���Uߨ;��2>��>p�<*�־>����>��5<��=����	'�<>���
�>�:�ЊN��~ɽ�L;>w}�=�R�>� O>�t>>�w?e����=�`.�=�!g��e���(=��>�����Ͱ>uF�=�<w��>�e>�M�>伾>�=I˂���>Ԍ�=��*����><�d>�Z����>�!��$W|=�c�D�r>Ƕ?ʵ���j�?���>.S��!QE>�vW���о ���鵾>��=3$P?6�c>Y��>ؒ�=��=t>�;u�ܣ>����v�Gc>뀻>>\&=�?%��%�M�"C;�v������>;O�Ƞ�=���>�9>�h�>����P��>�J�>!��=��M�B��G-=ȉ?��P�(z�Wrt�Aŀ��������=�ɀ�Q��=�����=Oǯ�xU>��p��:��^>���>	���`>�xڽ��>�⊾�z�>��0>J�M<յ�����z�>��J����׶��Q� ����>]=��\;�܌>w�r>��^��	;��>�M+��uѾ�=3�=N��_+��Ȓ>qg=�>�Ծy�#�ԇ/��h�L�h;��>����>�2>d(�>�v�>z�==*��
�.n�=ݭU>��Ҿ�:7>���<���=9����K>�����>Ƌ>
���\>_b��Ig�|�=�ܷ���Ǿ��<R���:I����N>mD�=��]�;��>#Ѕ<V/(>��7���e>��t>��O�P=�6��7>�D�Z���s��>�U>�>
>!;Ͻ��2�܏=���=��R������<���<*1>�7�>R��>r��K�=t��>��	>r�=@> >���=�T������7,��/3<���>�
<��>zK�>?��$Ā>)���;s=�&����>0o�>��ҽl�s>j����P��~y>������=
��={{)=�[���Ӂ>���=�ǎ�Ƌ>d�z�{����51�L��=N���1I=E࠾Q���aԸ��*�>헢���!�ʏӾ�[�=풰><��
fY���I>�D�S�v>�A����ƾ5�<�g����g��=F�߽��A>Fi���\>���>�.> ��>Z|���PZ>�T���<Z�̾Au�>و�<B{���>cC��A���s��zR	���>���>���t��
�y����ܾ�Y<�db��In�=[ =A��=&*s>�6 >W�>��5�C*徼�����>��T{�=��_=�q�>���� �=F��>0|�_-�=�N�����*y�>ao�>�	�=�h=�֥�J��;��=KVm>"��=t1���>�;��/<�Bվ�Jҽ���ä�-���;Vr��0>U	>M��>�c�>��=��L�OVq��V�<i�Q��!^=vzE>�(��/>�7>M� >W���b��>7��=�G��]�ڶ�=�X>��T>T�"?���=�v>�e?��DڽJ�оY�>X>�������sx4=�LU>i�>͜1�zJٽ��>%�žx'�>1�R����>���>�����s��ǀ=]�&?��M��u����dnk��d���@����>�����X� a�>�e'=k�=W���C[�>�<G<R��>򸾾�}��{�=�Q>4�ľ�5:?́>%�<C?�� �>n�%�h �`gZ��@&>�>��m>�t>3�>�t�v��>�a>�¼5��S]�C2�v�1�NA1�x�F>�8>�aۼ(Z��> ���qb�>勾Q��>�}g>1܆>�����bྎa��`�>�B�=��>���;��7�^�>�<��>Bd=b+�>`|=�$���"����3>)6��5o%��.�>��>�-w�� �>x��l2��U�>fʜ��)�rS�����>A�>��,>��>�u0�v'��{���zS7�T>ឦ>L8н��	?4�p>쳈>4���2��
竽7	�=��>��d>r_ݽ��%�I&>=��>�?���>��"=�=�      �Ϲ���>=�$��Ѻ��ų���w�
 �>)�о�od��I�5�g>J��><�>�\D>�3���ȼ�#վ��<�c)��U�x��>�m�X�?��V>l?;#�X�>8v�>�
0>��}���M�=��>(|D?>�%�D��>ŕ=��>؍�?5��>�c��As�>WƖ���?��$?���<��D?{��>���{�>����,a�ۿs�;����z��=�.?ɻ�8zP>��<?>O�$K�>}��>!��`�>}M�D�>�E�\[��
=J����썽ϾH~�>2�@�#�=�]>�ۃ���=�K$?ݪ�=}���B��>壳=Q�7?C�<�pr������"�>�B?n�Q�-㽴CR���L��#�>\o�%J�����?8��?@{���پ<�������oc�ҫ�>J�] �=��?�m@��4��A�>���ݚ�����e���,-�>33�>�:�=���>��@>:U����zٽ��M" ?%-⽓��>==ŽA��>���s����Ľɶ���(e�xߓ=��i��x�<���ֶ=8i¾{q)��$�=�=�>?Y�=}+1�+�o<��>���O!"?��^�>Ӫ�>�፽튺=rX=��Ҿ�>@�>�Ŗ�ީ��6�4��1"?�S�څ>Iɻ=ps?>�Nl����x��>'t=�(M>�nd���>G���>1�>����>���>��پ��P>��A?��A>M<?�]C��&�?8b�|1�>ŀ���J?L�����>�{����	�B>yC�=ݬ��%�F?��>��o=�����#>m��>���-I?��>����Ќ����>�Z���=G����:>�+���׽��>���=��(���-��^�>�˦�3پ`�B=09s;&�p>o�z>��4?'�d>���=Lm�>�j=�}4�ɟ��I�[�>=�*)��"����>pS{�_�=@�=U�=��H>��>{�>��=���>K�=��N>��:>��|�#>�s��`#?�E>R�>|��=�$��|����Jl�C��=O�C�4���$~�����=��X>IE�>���>��.<�q/?�� @��������\f��x?���<��>LO�;�lw>w|μ�>�C�D[��7���x�>g��>=;��"�?'+r>�Z�z��>?Җ<�KB��9���<�����>�w�����i)>�Z�=nT��}S��ob�A��=Ī5>���ġ(>�Ҵ�V��>��=+e3������=�n=,�=�w�Crs>Y��>H�<؁=�<��H�=?����=�D��,�>f�(�%ؾ��>9,�<׋���>N�ݾ��=������>�6�=�x�>�$���#{>���=�Jy�a7���l �|�>.]�2�x�{� >���D?�M�>D��=d��=rV��W�����D��땼��=������|=�=/��ٮ�=�i���e@�&Q�u�A��S)?-��_,j?���?��?�D�u�!�}���cC��v'��S�$?��\�C��C��B�=���=��\�	I����j�������Z"���Ծ r�>�g"?xJ*��      6�?�j ��"þb��Y�>�o?KI��r�D?7ԇ�L�1?�H��B�	!&�������=��>��?p��}�;?#Ǿ:?xߐ?6
@���=c/?)�@�+�?���L�3>h)-?��޾�cU���?T�w?T�?��e?��;�@�p=�8?s�Z?g��?��Ža�h���ʽ��"�-?󬬾��]�$��>5�>�m?/.h@�O*?KK�>��޿D�?���>Q�ݿV�R��h>�}�>��I?$@c?���?��?��K���O?m����k��>m���<_�[�F�]��>�qc������5R��#�>u�y?�e���k��-���@t��?��?�to���Џ��S��?6DE�}f����= �y�p�����}�>L|��vŞ��37����?X��?'��?a�T�k-��<ȿw���g�p�/����?DM%>���>�*`?q��b>y���A.����>Oο���?��k�.��E]��L?z�>���?�ą=��ž�5���!�?O��?3t�?!꾊{�?��u=���>��־9�D@�����"���"�:��?�0�60?�T���Ỿ���<6�>�-?�xt��<t��M��Ün�Ј��6����t����>��?SJ=�?A�q�b����]?A��&�����>H�c�6�?(?�6?����i߉�Ԃ�?�)	?\����R�)���[o��4����ǿГ��@�X�>%�N�\mC>N�`=����ڍ�w>�?m]����D�L<�o�ÿ0����ٿD|���s�?�<@IQ�>Cm?jV_?(\����?���ʂ~=i9@��E�����3��ĸ�?��t����>�P�>��?���>'���J����¾�\?3F��� @�ë��5ľyG���H=?`>H������bʽ��~>K�,Y�(
�J}��:�.?r�
>�A?�q�����]8?�.1��/$?�¾���?��a?�����o����v��y	������3�?߄�?)�$�lBV>��)�NϾ���r�q��&�\�?I!?�����z�?G��?���?X��O�B��f����7���>���?i����3�>\l���C�?z)D?����XO��[)���ؿ�ٜ?B�?�H�?)P?���8���b�%���}����?uh[�"y�?�dȿ4F3��/�?�@?6���B���
Z?�P�?��>�)=��#�H�ؾ�O�>2u3�0�����п���?�X?�(
>���?am�?��?M���#�!?$�U��c+��]���?Tc�>�%?>Ha�>h�u�gX?��?W
M?I�?^�Ϳ�ܤ?�>5�v�J>�n��/h? ��?U>���*��L�G����>�@[d�?��7�fxm?��#@5��k��O��>-�?x'!���J��\�/��?�V�?���?x|a?�p�?s�?��1?�DH�s\����?v��>��¾+��.x�;�}�=v���ݿ�]~�1�?�@t璿��?�'�>��9?e�?��M?'�>��?n.H�(+=�����qU�����oA�?�50���B?+ݖ�_�t�'yu=稿�.����?����?���j�>G��:���Rk?���眸�+�|?�>l�F@�P};%����_F?aȾ1PC�nF˿�n�>M�?���>��=(       ��۾˂������,L�W�=9�=S����W�<��>��<k��>	"�=���>eg�:��7>�k�=�+���<>+6���<��c���6���Y��C�=�i>��5�t�?>y3���t*>;�K�Qt�>��<&�,��J��B��=�@>_V?9u�JL�>��c=(       *�>��=�%����u�=I�W��&�=�i=G4?qݑ=r�>�
��ә>gԆ>�����r�4�4��F�3����)������zP��y>#[��J�>m2>�z>���\��I� dR��<>;&>V��A팾�C���\�>�1Ͻ�4�>ׁ=(       "�.��&�>/��>��>>�+>��>B�I=s<	������m�=vM�>H-���D��d��nK�P=�/��>�&
>���>��b�Aﳾ�=PG�֙�ٓ���P�>d��>䜐��μ>�;i>Ĳ>�8_>;y��=i�>�� ?�LY>�	�=Rk�=�(>&��>       �0߾�N����<?(�N?H�J=&��>:��>�ށ�څ�>��q4)>��?��?�n��1*�>�Ѡ>��ڽ�?�<���n̽��ڽ��D>q:��	ަ�P4>��>q;Z>i�>C)��I�)?�>���`�=h�@�߀>i��=�=0�=>��>��"���ξj�>ٌ�>�q��aE3?�]��S ?����{���}I�����Ɇ?�?�V2?�0�>1�%�Z#F�j����̽�])�?�#�pe&��nս��R=r4=��2>��J���>�4H�:��=�+�=2���mm��La��1��>�`x>&�,�V�>5?t��u��FW��8���)N>ܳ��Y��-�Tgr������E�>Ԭ�=,tν��y>�>���<e�(���U>��>u?R�X�:&.>��c?L?3���H>fU��2�.�@>�.?р	>C��>�졽��?㛻�w
�=&��=j��>ӊý�}�>�-'=�=~wľ�/�>�@���
>��"�1�o�>��f���<զ|��Fm>n˨��� ?��վ��"��	?�=�i>�RO�&�Z�S>�'����>Rݾ؉]��ݘ�oR׽]�x����@c�=r8�>)���GN>_��$���K.��_�Y����4���>��>���e �Õ�=>논o'C��rȾ�i��_�<`�/�~�=��>ݩ���yx="�x=h�Ӿ�&۾���=��a=���=/���rɿ��2�=���p�>�5'��;���i��1�����@��P=}E��2�;.�e<O�=�(�:-�����5��.%>��N�}�>?;�&= G;?�(�?gw���1>\��� >k^#=.9콐�7>��֖(�Z�V���h>n��>() =M��@1d>}�Q����=Tս�~b��	�>��޾��>�f?L��>���<W�-�&������"=��>�^�>���>�n�:m˾��*����p�?xŅ��8�>[C?�^U?l�4?r�G>�`9?�s�74��yX�z'0�K�B����=��d?o`;�r>k7>�]U�!f{�YZȾOH��A>�s@< �y>R�>��=G�-�,��=��=�t�h���;��o^>��=C���;>?o��zϾ/�=>����]����žB?�s�.��s>��>�݃���>&���'��K�>e�>��>�<��� eƾ�A���n>�ff?�@�>�s;6:!?1���?����0)?�b?QHO�T^����?�);�����
?�C�z�<?�~�>��R�����*3?��нt�h?��D>!��~|>�>��p>M{?֓1>��<�l� ���I?��J>;�n>4� �^��H�7=��>��>�/�=��Y�ޯ�>T���0�C>�Fj�x�D>൜�G�>� �>�.q�#6���v�>I�y>*+`>�秾�?�|p�>�=\D�>!h>��?܎�<��>n�6�lTv�s�<���= J	?B�>̅v>8̾�{�Q�v>�1|�:F��+��>�+2>u�h>�M>�J�>^���m> �k�Ӈ��?�����>Ч�=\Ή>Q�s=�E��	$��E�>�ȸ=B<>ڧ>ՠ�>8�>�R��W�>���T��;������8D�=��?�!L��KK(=
j�����=�N>��}��>��N��W�:��k>DA?�L�=+�;�Z�=�c��)�׾佸�X����=�֞�6���5L��L�>e^E>Ө�>�Eq=�\�>�xY>0���Ɛ�Pޒ�l�>�o>Z�	�TL��Vh�=��>�~�=FD
�����=?\>xI=vn>�߂�^��=XǬ�7��>�T�>�5��!�>��w��{վs�������-,>n,��#ȿ>�i<��>����/ǜ>B٬�A�:��׽亼d�ӽ�$�Qi��p�A=.\���Pg�Fz���>S�=T��=�,��,K�9>%��>[���~����g>�κ<�;��H2=��9���޾�(�=�t�g�=E���Ӗ ���=ۮ>8$�>������>K	?�>B��UZ>P�6>=�>^>SD�=UF�>��>f���0>䭽+=�>S��분���>J��=�׆=��ʽx��7���i�>��=Ք�:�i��z�2>Kn���a���=��,>���i^->Xg>"L9�Ɓ�=g�
>κ|��Y�3ќ��ls���r>= A=p�7���~>-㎾����;��PJ�BY?�&8׽�!>��.>b���B'�>�$D� b�k��>j�`��9�=`�a�.���Q+>JO�>�BҾ��">Xh����=����Dx�=ߖ�=��W=	�=�᣾��e�Z�>2�=�vӾ��`�
}*>7��>\���2�X������O�>��E������1�>쒾/�+�M>S@�=r��>l��0�r��a>#�j�aB�>���>^lk=�m*>�Ҽ���<�] �n��>r�ξڒ�>�5�=�)?҆s� �f�v>.��x�m>��+���=�nþ�N<QI#>��f>�ܽ�f?9w���p\5= i�%>��>&�B;���<f��ࣃ�w��<U3e>�1=�~=4��a��>�M>jl>�pɽ�����/?U��w�޽�)^>Շ�>��>���HN=��������=Й�>�^�"c�3����
>�=i�>Iy1�>Zݼ�>I���E��G<,u��u�k>���=d�=fK��YM>�sp�� ?|�p>�i�����k�<�I� �N����>�(D�mh>5E���>W�R�|م<JGǽ;��>�f@>ޗ�Tl��,��"j���y>Ȋ�C���a!?�9?ښ����Խ�<�8޽V����n?�T=�C�=�}�o">MEB�/>¾_L?O�8�nt>l�T=ܣ�;�̪=L?<Oo��	`>j��
)	>0F�x��<>3,�t��>��_;G=���n�=92?6��� ���<j�s�_4Ѿ�� ��X�=�-s>�$&=Yv�>q44?�|S���=�t9��$>P��=�b�:�>�5>{#�=���6�	>KѦ>%ބ>^D>��d=e� ?~>�`9<p�R��5b���~>�y�="��!��>������=X����T>h�Z>�Xǽ*&_=6~���<�����>��)�8n�8������ƽ��Z<ͭ�o�n�OZ���`1���:��*��<s � 
=�,��FL?(       /�>eR�B��=x����P>�Z���>�;�>sGs�}(>�04>퀯>%�_>�=-����=�CD���j=se>��>�>]��*=f3�>��^��;Ͼn����|�b��N)��->Z�ֽ�ϡ=� �>eݐ>VJ�=�˲���C>>��=T:=�V�(�s=(       Mi��I$��=>e1�>�4>��=�	���#�����=}�>�m:�gP.=��->���;nP>o{�>�G�>KM��s�=������>���9l�����=�>Iu=��ͽ�F>��V>��>�� �>xo@>A��<9�>������>LԲ��*���彐      �㨽P��=�>��n�>b	��J?�>�\�<�ʽ�?6�m�ɻ=<��=�b���w���9�ɽ+=�9>�s?�&�[��=�{ξ�66�4�����>�"/=9�o��7�����>Y�>k�þDe+���>aW��/���jd�>
Uܽ:bB�>*�>*?��>����W��&>�k����T�Mr�$�=ڑ���j�>"!_���������H�O�P�y*�>�(?�N$?�W�=�$��ʛ�>���;�=H��]�%�=qs�>;߅=Vx�<�I�?ѱ=�������>��Q=��ƾ3��>>Z�>���>-y�>�Ug���ɽ����� ������=��=��� 	�?�|�>QGj�����ft��?����=�%��r�F��H �4F�W�8F��Z)S���<�e�>����Wy��c��=
ݾ�^�>{�����>eV�����3�>\h��l;�L�<���>̰�Y��>e�4ƨ���=���>����YR����>�H���	��!	>�,���q����}�=���׳����=Kf=��v?y�)�~��>��>����d���l%����=>L��=�S���M���>���>6�?�#>�I0�|�=���>��_>V��>L%>��/����=�=\�����Ѿj�~��!?��R=Uо��e��%Ӿbf>�?�ҵ>������yF�>���>\�=Ӱ��l�>��?,����N�m���؈>Hf��6e��.�>�7?mC�f��e�>Zk�>�g?6"þ�����O>}�>(?������>-S�S�;oh�թ��sJL���??�2>4�Ծ��w>��>���'Ǿ�͞��u�=#/�~�ھ��>���=�V>��&?�Q=V�s>bV����*�H�=c��>� �>�>�nw=I܂>�*t>�!��uܾ>�wֽ��=l�Y<7�>>"@0��"ؾ�<�܂Q����>d�_�<��*��>�T��'ܓ�x菉r����U�>��=e��<YyW��Yn>0�+��xb�Ol�=_�Q�~��K�q��c�>5m�=��^��\I���=��K�*w,;x�>���FU�^R۾��x���5>� �򯆼��>j��g�W>�L�=��/)^�ݺ>W��� &��
��k�(�Hj#���??6|��d;=��þ[��>�Z	�� �>��>��/����N�~� �#�>u?#�ɬ>"�/��[3?�ԟ>�>kZ^�-J�=�\�>]�>������I.>=�,>�P���[���h�>êE����k�C>j���n:�������F?�X�=�5��Q�>�X3>��>��b�L�g>L�����>�N����>�8�=��>���>�:.?��&>@h�>}~��1��H� >Zkؽ��z�J�Q>?��>`��:���iw�=���<B�Խk�ח�����Bqɼ�퇾xQ��������?����*��������?�gG�?z>�g	?��6���V�2��>_�>q�m>T�v���\?¿�=�">�H>�B���*�>���mR��`�>{u���{w��l�>��>3��~,�=��;=��Q˽����܍,��z��ޙ��������z=       ��3>s	��;t>8�>'B?Vħ>Q�>���>�] �Q�>��B�M�>>p>�m��Q�+������>���<��~=1�Ǩ?��<�s>��=�3�Y�h>�>V�v^�>��=K���3�_>��_Ӛ��*?��a���&�@r{��?{c���AоD����Ľzu>ӷ�>���>���Bף=�6>���>�Ȣ�	[*>�r>���>���= M�>R�f>� �:)6>H�W__>�R0���+>58=z��k\m>2�^>�c���_��v��`z0���3��FϾ��>���D>/�ŲE���S?�f��>�߀�>�
=��E=)k�<R5=�&>q@e����� X=(G�>�����ӽ(�>M���W;�=F��>W6½�=M���>g+�m�>��½%ʚ��Q(�2-M�V�|�v;��^?)�?�}?,���e;�.��=0�=�9?�	�>�l�>�_�k� ����=�[������ �>؝�>�+�=�����>k����,?�E'>��N�����@�{�[�9����8^�ꭰ>��3�`��>�9B��ɟ>wnB=Վ>�8�_� �:#+��8�*j�>,4�=|v�d��>̵A���'�@z�>���=����N�>�n�@X��F�=�[����?�d7��kn�q��=�̟��'2�>�)�"���д�>��X�Q8o>
c�%�����q	���>����G���&>�!꼎NA�G��uS�=�驾Ja�?���>�>>y���b.� ��Ƒ>�]3>b��>H��� &�=1~�>
$���	���=['>Ãp<=���+�
')��#m�J�e�Ykq�[��>�'?��>��_���ﾯ�=�i�=a>>M�-=u�9�`�þXa ��,������l>i2��¾�?+~K=yb�>7�=2o�����>��<�1? ň�S���lc�qs<>5�>х�<�ԙ�G�>$Sܾ)�Y=#�������H}��7L��y\>3�����νK��>��w=dP��&D�>�Y�8� =O\پ�bݼ�4/�,�q>��>��>�:�_ ����o>���=|hF>�(�aQ��5�
=�q+�{o�����=�D>�"<>.Ym�מ?�`�=���b��=%���f~�=*1�������>35�<�ؾ(CѾ��q��}	>m|�=���>��ͽm��6���*����>8f$=x�>Y�ؽ!���V'��X��*0>ٻ����pO��y�v��F'?�>%��>#c:� �b��3��E�l�����k����Z�X>W< �n�Q[�����>Igͼe>�����='=i�+�?�;��?�	?�%�>�<o��:�3S�>p`>���C?��������6�?�u;�'��=x����T�>招�{?e"�>u��>���|���N�"�<�;˝�򕆾�V��{`�X�>��r����|��>a���VQ?> �'<Ug3?lZ >G	?���i~=ƫ�=�"��>�15��o?����3T�>v�=���x8��?	?�=�p���ob����Zzj>x&	?9�%=��!ZE<P�'g>l%?e�Z?����M�>��q?��4��,x���>�4������Oֽ�>q���`=i1>��W�)")>�;�����d�X�Eٽ���<��<�<>�A�>E����%=��=ȣ��~]��I������TR>��D<܈�=ɩھ��{��!��g���>��>�<�q��Ւ��3�=c� ��x�=����v�J>���<X�&��@���1�a&����>(��қ�>��_>��^ �=����S��m�>�+�<z�>O?���>�T�=�u��=A{�=G$��M�����ӗ�5k�hp�>���=�.�>�p�<�Y>�=�7�݁�JaP��>>sp��Ĝ�>+��&6�>�}7�FDJ>j8i�/ʊ>}��>"h>M�/��Q����<\�`>��=��8>W�����K���>x�/��������=������=C��>��>�>�>�	>лT>��?z�Z���>��v>���>/�>�1>���>�R��S[>�ڽ|���Č>Jr����*�Z=�K1>4���+����(���>�_<��^=.p�>��c��C��>�߾��
>�]����>Z��=]�m>8,=�5�>v�m> �?��竾���>
�����6�=�s_�� ��5�>9�����m�֭=cL����1�q$�>ա��Ț;<��=[G�uE�J\�>�~?�R>d�>hb=Cü��Q��W����=�@�Q�?TU�����P鵾jS��v>L�%>M��>��<��Cv��"=�] >��[�0�?��><�t8�ͳ��*���L��G�=~��=Ao��>Z�>�m<�Y�?��=��Ƚ�����V=R�?��f%�>s��>m~\<��i>5�=dL!�O64�/�=>"o۽�:�>ܜB=��W=�Q��;R\��nN>1��>�����jt>v��>
�Q>E�辨(F��'�<��=���=����a3?�M�>s�;<��7��M[>4�%>��>��k=�����þ�Ǵ��u����#���r>1SS�<{Q�j3'�������^>޺�<4�B>N|��M�:���3=uO�`)�>z��>�f
>��>=G-?>���9�7됽߿"?�ln>1�">A}���?�I�������>D6>�[�>|��k� ?L������m��=}�=;���B>���Ν��= ����t�.�F�ym�>f;��u~>:��><��>���;��"�$;M��nN�=���>��z����+�̾0y�<9��=�m���U>+%���X��پ�[N��yv>��0>���̗>�<b�X� ��=���֫�Rߢ�IC
>YU�;;�V>A@�>:�x>�;��k�E>K��>�G?�J+?vP=���L>��>T=E>pּ��>U�x,�=�y{�	��>����}H>?,�|>e(�B��Q|?;9��}	*?��\`���>�﴾��?V>K'=�k�$>u�r�  ξn>�����I�K>l !��Eb��4�<�N��|z(=@�E>�̱= j6�z���Y�>�U�qn"=��=k�I>��/}�=M�T>
�D����j8¾$�>�aD������ƺ�'??�B���0ý�����u��ۄ���A >��F?�@�>�E"�[?��>6��>�ń�(       �Z�=M�P=��>y6�>�Ž�U���P�=�	�>�R<?� >H��=�`l��Rx��3A>��m=�?�[#�`�����H>K>�^��w�<��=c�Ѿ|�	���v
ýT�=��>��#>;1�=�ҵ>�Z�=��i>��?���>??#u�>'~ ?	��=(       w`4>#\�w�^>��hZ�>�K�>���=�O>��?�"�	�¾�o>�WD�H�9>^Q���>qcܽy���y��#�> �(=Z�}=��"�=���L�>m���(�_>����>y~I=fo	�l�_n�>�����>��9>/Q=���<��>�]��      l�>���>��>!���;ܑ�t&�_�K�)�S>{<@��T=�E��)�=?���?MZ��'��>�*�>2�o=�C�?/�>�5���՜=�?$>�f��oSʾc�?�Dx=S��>�=��K΃���>Pa澁t>:?>��?���?�>���)?��>�W�����?�\h>����K��>f�>���=[��0F��<=R?���?n���&���?��e�}����h>��ž1����>�v7?�Z��?��>46>h2��<��� ���`���I?���=���J�?�t����Ⱦ˧�>Kӓ�2�>�^0�h�=����>�o���?,�>�:B��r��6�n�?��?>F�,?��	�q�R�:�����H?��>�$��QS�򌓾t����h)��¾m>�>>�\�& 1�%��>E��� ?�ɖ��=	?4)���4?i3�W� �^]f>�!<<^4>�!,>��,>��?����R�>�ny>ꒉ�v����kCC>�>n�>v"�?G'�x�߾G<���xd�g��>��N>�*��~Jv���]���?i@p��}�>Q"L>�*� �=�1�>��(������?4Ip������w��)�l�g���>4�ܾUП�z�A�Ӭ�#CV�$?�>��z����z��=`O>?�>��򾖺��xp�a8x���[����>W�=1Fȼ�R?ۊ�=���e�X>�����P?jc0�Ю�>�N�x:���?[�4�IP�=�5�>߻Ͼ3�?��c�sV��K
�!����eо�v�<�9�>Fwƽ0��4ޥ�2g�=.%�>Ί���?e��>�ߋ���>y��<-,_>^�Y>���>a��>�
�^{��ڷ�>�8�=Em��P���P�??#�C?��m�S�ھ��=�&|>��=�-?�e��>��Ǽ���\��;���;>�e?œd>0��M�ž��=ȹ�H%��90�>�?O���y��>�D����=��!>�J���=ѽ"�>��=��{>�Z�>��?Q�˾y*�=��$?{��>�j�>$ �>�ʦ�L�]>�f>Z�>(���ڨ>�rK=K7��u)>ɚ���8>cY�?����4 ��ܾK�Y?2x=�z����>�����X�sDy>����'�Ŀp?L��\[�7�?漧��N?�0�pr�=B����N{�`P�1��=9j�~͖��̰������t����>�Р<�]??�T>]�����{>�eR�M3=���>(�>�Y[=���G?0�2?�m����>
��>�W��BMb?� �>?�]���=�ʕ>߫��,�j8?��8��� ?s{(�!��o�>�߳��}����>>��>��K?���=a�?���?|�����{b��
�H>Ĕ�����k�e?C4��^�=�.t�ٵ?9/�>.�����B��7�о�?�r���~�<�t~�E�a><�=��\��?�>P��.Q�>@F���Y�L��>��@����}�+���ڽ\
�?�Å<uF�D7�?����Z�>�4����_?B)����=?��?ӻT?��L����!�	�+���=�>�8f?�煾s0.=eg`�գ�=l��>b{�>
�>%�8>F����������       B4 �Ȩh>)=����Ⱥ�»�>>�G>G����j�>J�'���򼂏>�/�>�x%��k����0>8�e�S땾p�\��>���M�I���:���|���J>�"X?m�>pq�>	�	���?f�]>cm�����(�Pɞ�j��>�'H>_�>��ᾞ�m��A=��>A|n�A>�e�<
3>�3��^T���hd���N=G�=�r�=A�ʾ�>���ĕ���?��?䣎>� ���:>���>�%,�w�=@�����"�xu4=����@�6�D��>��p�[�F)�>g��>�f���ʒ�糓>!O��?ipy>ٳ�������>vϴ>�Б>W�3=4<>��:?1�?K߾h�=BEE>�B��>bi߸t\>hI]>zȈ>/>Ǘ�=Bǿ=�?;��頱>�n#>!��z��=FJ1?=���Ľ.U<>���=����k���9>�����>=��=9]�����aY�88,?�>��=Cݫ>���>�+��ҳh��.��*�=_�># n=�=c������>�%p��wn������WV>��޾�:�>����r��zg>k\���o���<hI>f�O��o�>Y���cq�>��ef�oU�6�+����ktH��E�>�o��<�Ⱦ�{�����=&B>��R�P慾�m4�Jd>:�1=%5��]S�>E��[d�s�(�mO�;`�6�'>��)��[�T�8�u���[>�G�>�:�:�i�5d�>G�=xؠ�J=�>�A��4�>����	D}>�4'�;G+���
;��G���3�4*e=��>0F
?��>�k�>F�?��%==��>ƕ5:�Y�>Oȧ>(��=1G?REM��Ð=@�/?R�>� =[�?�
>A;�>ߝ�����=��>�Kt>�n�=M����K��+%���a���l���i�|qb>�h(?�/�>h�B�y�����>�>�F)�(�>�&l?t��2��>Y�K��L�=��k�=]�ʾ�Q���Q�uR�>�r��S�N>.�>�o��r;W]�>x���E=,��>s��=�
�>_P&���>�v>K�b>q�>�/j��7�� ���G�U�}>,����dľ��V>��+?wE>^6�����>�	�<�5�����1`�N}��(���A������@���˾�2����<��<9�c��PY��Xv�:���ц|�.������:�ý��\�E�
>��>���V>H�{��3#߾$��=� >5�c>�䆾ќξ��q��|�O�׽�B>��=>�<�BR>�8\�^8>A�R=þR�S�m>���=2{%>�䶽q���Q�����<'ϥ�d�>��4������˽f\i�P�K>����m덾r�H>�f�>�h�� ��>���ed?<M�����Z߾�0�>�߸���>Vu��"��#?�8?���<����A?���>42?��>�=����ˉ;"،>���ʷ�>;;��W/?�Ǳ>$��9|>q�H���>�n?�%���w���R�o:L�����R/.=	��� ��ד� wT��{�=��7�?��>0�=�i�>U;a�tz�뷾<NľH��k��<'k>�$;>KQz��=��j>0��={"�=���7s�>P�ֽ6��>����������f�>y��=�<@<tվ��*��S��{��>S�=pB�>�Fc=x��>��I�ڼ���8-����ُ_>��"��R��Ş�	�>�g>�K �� ��վ(�V=���=Q�=dJ�>	�N�`zU�|��=�ٓ�:�
>mpF����>ZF>�Z=��v��Z0>�ξM-���e�>���>�!��Z�-��>[���y ��d>�#/���=��>$:�<$A ����=��>Q$ɾ�JĽ�9�(����>j�����ҽ:�;=s�X�轐�T>��J>8�;=b�<��������|�a��~پ�ǵ�^��=X9&?'z>��߾�+��P��^��K=�ζ>� �u�w�
��=3�����=�QȽo��=�X=�F��1�>���>�c�>)�8>H�=lV�����<�U-�����=����ޞ�>��?����M_��$$;= ->�'p>�Z�>�v�_��<�b/�=b�=��>7�?�y�>"����J�>�����Խ�c�>�L���彧����bȼ��<���w>$k>�=>N����53��*���u>�jY���m������
>?=�>�d>M�#���'>a@�`ؿ=�?��*o�=��>F�=�� >��+>���]�Bu���^i9���i=��*>ƍ�=0Č<8������=� ->�o�����<ib���ϽN߲>����K`���n�>�}�e�*�(ǽ\��>XV��k���T=g'0��硾�8>�!/��?}�s��>��>ua<����=u0��cQ�?�>l���}�>���>�B�+��>��e�b]�Y�����?�>>.`Y���?�`�J�H��������=���>�ܴ�C���6)־=O,���>�x��Wc���N����?��W��w>�jQ��h��#���)��ï��w+��Y�n>Te��ډ>��G�l���.�>(����af�7��=�R<X�I��U6�=j� ?��}�5='�g��
�=�.�>�Jv>��*��6���r>o/�(�3?S�=yF�>���У��MI�>"��>�2?�8?�ϣ�=5߳>��:?+&?G���R>e�]?+�(?u���z<$��'$>���>L�����ȼ�8��n�<G�[>j[�p*�ɡ>��7<<��F����˜��*>��> �7?؉�>m����Ǿ�>�@[�F:e>g�¾�Ҿ<����w�y:�>��z�Q��jU���躾��q<.�þ�����	���f�zNb��{žL��=����M�,μ��\>�r��f�ݏy�M�
�笙=��r/�=����_㏾R�u<y|�w�����M=��߾4=>�j�l�۞�=r���"f��z���x�=���%̠=��8�*�?����{8�`���1>��4��0���>�����ƾS�WK�>�r>|"�=����E���c�� ��Ɉ>r�>k׽���\�Z�a=U��VBB��Ƚ"R1������,�ƨ|<�7��?T>=��>�G��\�>�>t:4�°�W*����=;.���c>�h�yu-��$w>�c?�3޼(       =�A��KP��h�>l�>�Ƚ���>�蕾(��=`L{>�?j�2�����g->[����.����=�[N=Xb��w�=GV�=Wf��[�N���?]�_�R�^��=�f%��|�<��>vf(>ݻ�=��d>�&>/�?;0=,���
R=�t>��>��=(       E���]4=�._���ֽzþ��e��V���=��>=�>���=˱�>#�??J��>��=����"�=H�$=���9@���u"�����;q>1����9i>����=l?�C>>&>�+>�d'=�!��kA?\-���&��]�1=K��>�?z|���      � �">�}e����>J���
�P>�K�>��˾�"ｔC����l��=6�T���<�����1cj�!��]�<s�ܳ;d6�> R��O ���>�Ww>��v�|���6�޿t��9X�9���pO�N�>�r��H*>
�Y�Ԁƾ�"ȼF+��?�>D���⳾�݊=�G��)�>j��>4 ��N����=(dP��
b>Š�=mw���ܼʱ����Q>��?����=���>~d��(��U*���T��]?呜��2��U�V����=oE�>mp�>�}{>5��=�������Z>�Z��YO�$(澛��(Ѿ{�;<�u>/@�����ɽ��N?�����=a8?�V>�&�>�j>(��>�����=�_>-\��u]�>b���~�<��k�kP0���=l����@���X�'␽� A�éj�~��>6I>G�j���g���/{$����=~��>��!cR��Q�����'?��[W�>t�|���?Sȳ>%�=��>����W�^a�b��̓>5c�,��О����֜S>��e�\#">Zq�>;]���8�ov�Eі� �>� Ѽ6��<�ȥ�">zj�<�-t��$�D�5��F=>D�?����L?�=L���1��>���>��;���[��Xe=Z�5>\�� ����>�?�>;�M>�v��XT��`e��|{�A_��ʾf���T��6�>IӲ>T������>�cZ���>�f=��#��]>���>��>�!�>N�}>b��=��.Nn����>�3�����sl��=��=�>��_ۼ�m�=Js���,ܽ>?��>��`>��H>�~+��Gr�<j>��=#I	�a������cg�=�%�>x�>�(�=2.@>m"=h�!�� =o�>N��JF�=T^�=]O�C�s>�H�>&]۾!4սE�^��l�>GWP����>��½�F�=lF��A���x=�a������F���>�#����w%>����'�MtǾ�b>��?*g��T�=�.T��ֈ>Q,�>�a=^�v��?AD��#=�����y���t���\=0���q��i�>� �H���6qھc|z>�Q.>m�>:[�����jf>=P����@=��?/�����>cw�a�?c�=
Q���?�?>Y�����v=C)�����;�̀>���!��:W���-��nT>��R�yJ4?�M�>�0Ž����⽥HI����>	A��}@;���D���*����÷�=e�������Gz,��&߾�� >p�u<@ҿ����=����n	��l��ӡ>�;=��>!(�>�I��	Ԏ�l�ʽ�۫�ǻ��Xr�^�o�n�ھŋ�;h=�����o+?�?|k>U�˾�ţ>$a�>��4?+���az>[T1��0��bY����>h�?pɬ>W�#����Rw����<��[����=	#7��<�>>��i�>7>�4Y�}{L>�>�u��=�˘i>�>8��|����7˾�����D���V>�>/�E"���O=�ގ>q	>��=��=�4>b�=�Yt�X��>0�LQ+>�R?p@|�f=�>(       <&>+_>�_~�L����i�I�>s�����>�"ν�M~��>��=�r>;֫j>@%�>`��<XB��">N�=�$�<cy'�AO�Z��>�T�;��>j�S�m���S��<�1%�\��P�>�\>���=Y�>d%���
>���>88{���<�Tý(       �E�>Y�b>�<����7<>�w>�.�>�ϝ>N����t�=n�=��4>}��>�x�>B*�vZ��f��=��E� �?w��u$���=���K�=$/���Á>,� >����)؏���h>�$�=��?th>���p?����>$�{� >��=�      �:?�¾(��>]��>W��>��=�]Q?W��>b�&?�����Z?�!ӽ�j7>���8��>2�?�^m�?t��=�?;�羼��=��> �>a>,��>ɂ����>��ӽyk��LA��A~=ԕ�_�r��B�c��;�b����>6�>�|Y>�h>>���2���)�NS>txӽ�$g���˾��,�M�I�/%Q���羽|�>�Y)�hi�=5�=��>�\?������s�6�Q��>�*���g+�3��>�^�>L"";���-1�>��?l˨�>���2��{��>S��ې�>G����&���GG��2�=���*����7h?z4�-�>ۀ���̿���>�տ�?7聿�$h�x2�=��¾h� ���1=���=o�?��"?��`�='���QF����=�2z>�_>�\�>3��>���<@"�:��
>��R�q��>W��=�5�>]��6vϾ:�?H�x=Z4/>w!=�*�>a��=r�-������>\;i����>B��>doľ�t����>�E�>���>s9O>��>���>��>�3�?�� ?�CȾݙ�>�-���h�>V.7>A��>�.@��"D?�	8?�?��u(�^D�=�>J�H��>��=�[�>p?�>n��{c�X]?,�)>z>>q�>�@?}B���= ?��k���c> �y�S� >Z�'��oa>^l�>`?i�R>c%Ⱦ>����^_��T{���9�n?�w�=��>3��>�\�>�i�>�2G?n�~?��p>������P��|ݾa*��*	�>����"�{O#?;v+?����a)>�v>����K-�=����GNݾ�����վk|��Ar>��\�<-?"�3��	�>c��E���-�?+?�-S?<;�>'�>7U�>'�<车H�>��>�C�6�>E�>���ۆ��F�=��f>���5y5���}�$d�>�4����ž����]G�>��$>#YJ>أ>�����p>>S�>���=�bƾ�H>ξ�F�>[�_A�=�W��V��n�ȽM�e�Wiu>�->�Ǿ!��>hD1�Hψ��ߎ>��O>wx@�ľ�6;?Iw�=��>λ���$��5=�#Q>��>��ξ�m�=0���U!?��žP(���j�>�z�>��U>rF>g#��	?>��=rA�ڋF�*D�>O�>�
ӽ��F>�S;WS:?
1����U���<>�9>��>��ھ�	?M�t�<�}������ю?��.��ve?����/�>��>Y*?PԾ��/���K��[�?�m@=3:�?�Lپ����g��=H�?��q����1��`y>�N�W��=n�C�
�]�@)T>K>�ki=����Bv>�t|��k�>Xi<�;=!�>Щ�-߫>I��=��ؾѡ��k̩��"W�����JLƾ�%"�%�V?�~��&���l<�2?ѯ�?��ܾ${���=f���O� �l\�>��R?��>���>�P�>(��>���?���<A�>u%��(S?��L��2>0`�MV�v�߾%Xr?�����i'��r�=����
��y0e>*1��3��>�=��?�+�>�]̾T?4TI>�9�>9����x=٤?վ0�>!�=       D*C�=� �W�Ͻ��;qH�>;X�U��>X{�={4s>a��>��>�;���y�;Hy���>hG>�_��lw���pe=^&�.�S�E��?�t�=N�=&��?EP���T����i��>�X<�>��>�=�j�>�G�~�N�&
T��� ?�l����
>�Ӿ���<�>n�G>���j�>���>�fk�����
���o���}����K����D�=#2�=+Ys>�Kw�����<�_�Z�?��?�~r����<2A=�����}>�(�7����.���p=�j���x�<)��=�ƨ:��i�<��>s��=����먾�sR>[�M>: ?>^8���J>�ʑ>�-�>������P�H���پ�G�^��=Q)���x
���?hz;?x��>����=��4{"��g�>�q4>|jH�P�_�B�A>�@����c]�����%���in=������'�O��=YDz��3#>Ԃ�i���n?P#�/]1>9/,?�h=(�=��=���=�?�>�?����M�3��<�~Ѿ���>�/�<.�&��(P�ulG>礷=-����c����>���>~j�����Ǥ��7���-`��YS��kT>� �S�<���Y�>MMܾ7�r��譾�t=h�	>�Ϻ>Q�V>}Y�>�1�>	30���>Z�>R=�n#�=	;˾~�>��Yu��qk>g��>s��>-J�=��&>5ŧ�x�)�J�x>	c!�Er����=��~���h����;_�]<���Y�����Ď�<�윾�&��*ӽ�,<����/��<�F>Ӆþ�V=�D�=�:?~� ?H��>�,I>~M4?��C���}��N>�B�>g��6&<5Җ��v�;�v?��<>� ����>��?�3
�<id���>S���A3���3`�%�:<�L�>�>��L+\> ��>�F'���>&(�������U�>������>4]7��SA�7��މ�O�,>{�>��?�P�O�=�V�=w����>Ua=�����S�>���\��[>v��>G/H�� ����E>%���ߌ�=�|���{.�i˃=�����T=5觾�B5�GԽ��=�R�=����֚N�w����!��a}>����~r?>�1�=[S=:�->G�,��!4����>~�a>��輬�?��w�V��<n�Ǿ��|?Q�]>p�>�u�=bڴ�N�N��K�-�*����q�,�0�'?>��>��5?vՍ>}&!��3>�G>n�?"��.������uU۽�G,>�,�>��ݾVf2��N>>6>��>�������';!?:Oڽ��	�#x�� =���=��>;ߎ�?���f�2�=9,="���k><%ݻ�R*�=��=�$���ؽ�ׅ;}>L�?�b>�u���*���ν	'q�gB/>r�E���H�g�=�[,��8Ͼ�u>{�4���ʼ�K�=T��>�?M���Xx�zߟ�Z:���A�>j�(�m똽i����>��8>��H?�m2>jڤ>F���}�?1�׽u(�X'a��l�?�}=L�(�?����W>��?JX����^>g�C>�;��t{�֥D�׽�	��XX> ;@�Y�D>������'��籾��=M&���R=���>��|�5Q�@��k/�;��>*�>�o�=�O뻦��>=A�<��R>p`">(�>=�;#�?������Z�=�I���6����X>�.���g��ש�>O�o�4����a�;)�����=�|�>��>o �sv��=?�j=����gA=��A���B>ߟ��u>�I�;Q�=?�q����>fs>�꼾l9�>��(>[/�=�[��,���>���=�'>�e�;w%�=�*k>� >�4=�B������e��R������>:w�>���j�=����ܐ��`��Ã=�"=8ˌ���?6�����:��(�\D�=� p>.(�>l1?���>-7R>�����>��Frپ�g?h^����;jC��vD�<?���t>�}���>��=�W�=�$>�Gʾm�Ͻ%�j>�k�>*\>pN�>�m9�ᅾ���=O-n>�CY>��ȾU7�>#��}G�=�\��P�>��=����4y��"�̾jV�S��<��ȭ�<p#*>a�=J^'��6T<�荾�,��w��<W�F�Fӽ�_ǽ��Ľ�_��|Z�>�v_�0�_<l�I>87}?%vy��5���J�<��>���>I��>{"��<AP,���r����>����^K��Q��:f����>�l�>��ھyȣ=c�> ]�>/��=_׃>����T'�]6�����>����I�>�>~rh>v�-:��=�8��<��>H�ͻH�=B�i�n�>M�(>�Z����=��?>	��>f�?�(���-��=���=�>�E����?�.�=B��R;?R�_>R����R�����Q���>�?T��>�IR�ր�>cݹ;�D)>x>M��Ƙ���<�_�=f��>֒�>����I��"��T���/��Γ�?�/j<�j�=m��?��d�� =R�>��Ⱥ>�#��˺��K��<m�e?�ὧR����>���=����{�O>�E$�Z��>jx>:�0�2#��i�������<�	��������Y�Y�&��<����=|�d>Ƌr>8?�=�&\>�S���>�A�>W\���˽K72<,�0�oM̽'��Hq8�q����}>=BR����F�F���^T:>���*�=P�>:��=e�	>���jC#��n=���>�>/�0=H"ս+�g<}@=�`��W=>��$���9�]
�>K�����>����X�#'J�)��>Mr�=�ʵ>2�6?�/�����>li?>�5^>��<������>�W(���s�j�������_0�5ݾnJ ����>�W���eQ>�W>�Yx��~þ�ɽ���_��.�=�Ҿ��?
�?ɕ�����<�cZ��T_>�e�>��g���+>1y��C�����>�!򽿬9=ޘ��Q���>)�x��e-�j�Q��eB�=M�=)(�/1��,�[<KP�=��==��S�>�q���ӻ�nZ�>�/�-"��Va�.�<�@���g?�X �FFG��(V>� >FH+>����fڻ���>L��sϺ>}D��k�>�]I>s8�;����fk�>]�\>j��=�[��l�>;�S=xe���w/��������\T\�I�¾8Z�< �*>5e>���}��d6>G>�}�����(       �꺼�S�>$+�wݤ�ûѾ֚ؾnI;=�=��=e>!�˽��U�:�>>S,�9g�>&�һ]	�+�2>q��/Y����߼ ���JI�>5�#�R'�;,�^>{�~>�\~��a���ܺ+>��������>�ߟ���`>���>�I��0?�d>       }��>��*?��>?��>��?#�/?B��>���>fR?���>��2? �>>h�8?u;�>}��>u��=��?�`>�E�?}��>       L5�:ꥎ������<{3�:�W�La�;�;UnA<��5�|o�I�{9ݛx����:;%J��`�9���2�2��(��C@�       ��D�*}ʾC��$22?�f:��f|;���/q�>�2�="+>
D=�������>�𳾷^>;�4��:�=z��>�=r���       g��<��;�)�:L,�;.(<��=s�#=}3<m��<^d�<h�o=Ǚ=��<�Q?<.T=��|<�x�<�c=��<��V<�      ����=tj>3I�q�N>��5>���=V�->k|�����>TQ6�{ͼz�����)>v3
?�<>bq�>{�n>?=9�B<���>��=�7�3ڇ���o�<Ο=�,f��r���BA=܇o>�Q�ۏ3>�|���U��O7>5�<=��0�������ˑ>2�z>VSR>ܓc>��e��[�=��"����=OQ�=��l�K$�=	�q�Ka��>���܋>Oz_�T�ýG"��}�۽v{��2r>k��<�<�>7a��H>�􍾇i�@����=�L�>����L��>胹�����Y���Cݼӄ��\GU����=�	�=� ���*�{������C�;��2m>�>�x�qS�h����(�=�mX>|�>�
�=qr>��Ծ��>�*���'���c���
�uQU�l����@+�]�a���>J@�=�>�����b����z���>�KY=����*�>� ����J��羷_��

���L>�������n}���(F�Þ���)W>���<Ey>S��4LA�H�?�rI=� f>��>���6���l>�g���x2���b��N�=�3�:=x�>Փ��轶��K@r��d�>Um6=�p�>E~R����=�k<�2ݽҢ�>��>P�?~�j�U�T>Ir�=�S�=�!B>���,�&=t���~_�=�9��͡��ކ�>=L��#U>��ϾkXz���=gH�g���E*�<�Ͼ �!>/k�=�� ��`,>c����Y��M��7�R�FY�>��(�5q����;X6�>�� >��5���|>�?)�־�ێ>Kr?�([�=�L��~э>���>��>�@�=;|��:	>��&<N7���>$���=枈�r4�>Xq<���v��>���>6���|}��u<4>��X=�H�sL>�>*<ѽW>��`���������%\P<c@I�I��>��U�L��U��>��ɾ圄>�ڣ>���L!m&�Ν	=�7l�j���'�)�(>�>>.ླྀ^�>�O�͕�>�:�7���Q��4-S>�m���O=�-�X�P>��
>sM�\����=W�9=�����Ż<
�<��ѻ]��[��>���U�=�(>>�N�ݵR��	Ͻ�!� �>�>�>���>��>q1_>�F��11}��)>�K>����a�Ͻ`�7����=F��	��=c�{���u��jd�Q��>r�	����;�>������\��dξ
�ټ�*�>NN>ׂi>-코b�=��>��뽇����.d���>\�=���=�P�|�=H�>E�~���$�v�=a+�p�=�g���x)���>g>/�f���(>r`X�D� �N�s>EhD��m+�bN�>��ξv�=[����HԽO6��]=�v�]�	>���>�j#�aZ@=򢣾�z�>���V<?+>��Ӿ���Ο�=39=0�m���U�K�'�Y�i=W.>Q?�>�^�=O�ǽ��t���=���KZ�=4��=��ܾ�he=i�����;`~�ǌ�>�	V=['�>�T˾�6�?�N��,b>=�;>d ��,����	 ���p>Κ+��%�J�[�툃>$��J��>-K�&vJ��"?�]�>��d�       ��`��5컓�Ѽ�Gd=�5����>��j�����W�,���J�<)�H=M�O=�M�: ��=�j�]��=M>�J=!�<       ��/?��?f�?�T^?�+=?�M�?,L�?@�?�v?x� ?���?顈?I�?[�>,�A?	fp?��?��?�z?w��?       >>��>n-�>�}=>(f�>�t�=�^5>��>�5�>�ĺ<�K�>f0�=��>��>F�>Y�>l�>�5�>$o�>�Ǣ>       �h�ĐJ�s��ϥ�=S�˼H>��l�6'���
V���q�<��=�b�=R�n��Ǯ=�e�;�^=�j>-X:=S�X<       C��>�?�>���>�?@��>Ju�>фT?,?�?���>J�>��m>���>j{?x�U?{��>5�>*�&?���>.�*?       �~�>2
�=9}�=!�˾t�t>�d�>t��z�/>�.���w>�<���x�>�gľ�L >A���q/������/� ��ӫU�       ���=<       ���3�W��o��~�=pϕ>��>3�3?913��2������U�Q��<�1?�S��Q��[�>e��=�<?#þ �1���>��I?F�*?��žo��>u	�>
y��ݨX��N?.����������|:?~0�=�|<Tk?O�X�N���80�ŗ?Uؾ��I���_=�慿�Z��B���?{93�j�t?%?y��>4>�З��A�>�(��1��vzk��Q?��?       �71�r�c� >