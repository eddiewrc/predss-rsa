��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      ���=BtZ?���>��0�ol[��j?��>Ã�����W� =����::���ћ=��=�]>!6���o?�/H�x��>d��:|Z;?������.��?�s;?K�??~}?]�#?��f���e=����>Ź��� 	��Ͻ��l��m���Q>7��>�^���+���꾚�8>�=�����3�e����$�=��ȾƸ�>7放/����+���>J �J�>?P�?1�=�~���y���j?�%?��Z>�N��Ƒ��'W��	>#>�%?�D��'����ؾ�L�>�ce>m��4�?_����Ͼ]�=��k�u��>�&>Ep��L�=��=<�K �c�پ�[>aL�<��ݾ�F�� ���m�|b%?��K>5�?t�Ѿ���>ɛ��yO>�%C?݉�Ǣ���֙>�Ľ�M?q�׾�aq>���>�P�?�T:?
T���?
D�>؉�>�žк)?��,=/RپF�~><��������>T$�> ��=.��>�k��R�W>�/�=�k���`>�����o�<~{�>7d�C��%y�>(p|=�%þB��>�l?>%���J�>6N�=��ؾ����`%�>�M���+ �Ζ��G�/w����6?��J>)a���G?�.f�,�����ƾA�
?���>3�?���3�K�l\�����sg	�_]��g*?*��M־�f�>8��=�>f�4��)���(�=�V�
�ͼ���>�6?���<������>��>�l�$�S���>��>�꼾�f=�H�>�UW�[��ny���b��4�=�>�n>�`2�d�v����=��b�AҽxG&����;��>g�nd�=�Ԝ>_<��-�VT�>��=tြ0���W�4n����j���:�3Ӆ���*>1	?&��>�F�>&����g=�L�>;ז�T�R�t=��4��5潮�:>�F>o*����.>�.ӽ�(�'�B>R�?�C�>2�Y����d�z>�Ǿk��=����E9>p�'���??�o?��>����T�?�U�>hǶ=�=�?�>m\7=��c=B��>�T>}'���ۏ�J����Gn>#Ⱦ��g>L�>;=��a'�<�;BT$�����=�����=�h�>PԤ>2 �>&!����?�d>0F��?<��>�i�>j)>��>���>��O>��?m�|�>I>p>�7���HI?��*?J���S�?�:Y?T(�?��s?B��Aݫ�H>1�tD���$#?���>K��?S�?ocP?�;?��z?@�T?�mM����>g�̿>��p"?���>�%�>8?�^9>�RؾTaȾd�ٽ�(ξb7=l7�>�9>>���lC]>�Ǿ��N������m�>"�=�%>O�B��m�>�*p=>܆�F\>>z�>�-/�\�P�m�+�B�?�>�I"�S���o%��E�=󦊾J�#>o�.?�H�?���f?�R)>��`��Gν�BϾ�T>����鉕�:�D?T�0�Z�t>��i��(�?��4><�"���1?*I]?�~&?Q���=ξ9?x�=�� =l�k�����q>�nk?BR��gm�7r>�Ui?�A!?��>F��=�Qۼ�v)�       q��썾B���	�?���=�`$�DB����\>�Ra>*g>�e��棽E%��+���E��=o�>�)~�=���>����fK�7A���e/��BľE۾������\��H?�,<>�r��\p#���>�X{�^}ľ���hU�> �u����;C��ڜ���>>�>��ؽ|�.>���> �k�^>��o�=7��	�����ξ"F�ՌA�KE�=u��Ve?���>�65>�ٵ>�	�	�>&�־l�ϽS�����>���>�2>>k8�>3�>.�н@C�>b�)=���>�jV�Y>�C�>���N˨>!���W�C� >bX>�"�)e��r�=�b�vy_����ǾK�>i���l$��d =I�,��>-�=h�>}�C�X\��Mc��^B=�3üWz������=�gq>�?k��н%L�>��}>ĺ�L9����ﾾE�>�f��I>c[��;�=����>Q �>��.�����b3>��]���]=(�/>.*C?��=v!{�ӱ>>D{(?S/�>h�<=�ZK;��5���὎2þL��=�l >��7=p�!>������=I��<��f��p6�=��>��>0��S��<&�l ?��>�L?�n�=��>t�>6&?<7�>>�b��O����=??@��2���ý�H���|>.Q�=X�W>hf�F\ҾQ���=�f>ǒ�9���k�)=5=�>A�:=<�]������/'�xn���qr>FzϾvq
��h��6ᨾk�������7��=?L`�"K߽��>��>�;����`�ý�>ά��֓�=|1$=ᮽ�6�<e������Ɩ�=v�?�컾�v+�K(x�c7>�.�=�Φ����=���>�f������A;���>%�4����<��>s�&=�8�e.%�)�u>%�T���>|��k~ >GV?�H�=Xq�>�L��^�k��
?�ad��j`��e�����P�<��KP>���M�>�y�>�	?��=�f��g����d>���=�LM��O>H폾���V�!��?�>TeA����>��s���޽h����>�2�=3�;j�?�4:��@=�G���Ԉ� ��=[��>�J�ZƢ=`�]<N��������=���<���==��=�&�<e�]���=���<̇��ȣ>�f>>��'?�A9��q���,>��>r�S>�.�鄸>��U�	�=ko.?l�x>5aһ_�>\�>���9�?���>V"��m�K<���y�=c(�Ŗo���%��>�>&S�dc��ڔ���О>v�~��`�=t�7��t~��]n=��?>�%�Ey���P���$=/E >�m�=g0~�����
,?�i�>�VZ>8��zY�f��>�)��@�>��5>}2>��B>Z�`>�aM>��=�0�>n��_�׾�J�>A?�žɽ�[ԥ���?�P����s>��?GM��K���!��/��>��p����S�>�K	?#��=r�?2Ƚ�K�5&�>�
�=m�̾8��=7��e�W���<�e'?��?m�ξC��>K]���F$��)�;�W�>�x�=o����l�M>���.=Ժ ����>k��F;�=R.�=��½���=�њ>ɂ8>A	K�=>L5��_оaEپ�Q>��F>�� >Ed�����r��@)� �>���> ���
}�<@�=��1�v�9>i��=�b>�u���I`��Wo>�"[>�TM>D^5��°��!��h@���=��j>�3g>t�o>��>�՛>&^��
�>"�=���	"�m꓾���<�h��~����� ?��?㲛=�������=�>�If��=м��>o輒�r<�ξ~b&����>l�>4!�>>/?���j�>�'���i��J"�N�9<E�P>��O>��.=�����q=#z��>�`)����i=����X���d�>�ݐ>L�>�;ͽB9�>2�=t�%����I�=Ag�����6����>��ʧƽ:`>�}�=Hc�Y#��<ׇ>�U��+�=���E��a��>-�>i4�����>d��:>r�$=��>�L���1�¬1>��Ƚ'%>�Q�\^��l>q-�>�RA��������<���QA
>��=U���g�=2ǈ��O^������.���������>S)'���a=�+��3	�<C��=l��)��>f�Ѿ (۾��J>�\���_>ytL��7�Nl�=b�\��ӗ>e[��j�=Um~��c���MO>V�Ӿ��(<=�#�7�	=� �k:u=(ǋ>��>7>�?�Rb���=l���A���u"����C'��mԾ�	����>x�>֏�>tx����&�םϾ�K�':�%�p>{�7���¼r�Z>oF=\]�>r^t> a!<z�D>M&����l��>�>�j=���>��V��c>8ҕ=��6���l����<4��>�7�>�D?t[�s�<� 
h��oa=`�@���#<�=v�)�=�Ҿ� �M�wl�>���=g^(>R��>Z���i��'D<�Ee�ls �౽��X;������N��!醼�y>�k>/�=<�.V�R"L��`�DlӾ�����)_��7>%�⾿�����>HR�>o=a�>SW�>q��>����m�=��}�^&��;�t>�rg����>�	g>B�����?K��>�����2�>v���$\�=�y���B=�y>�{$>����)�<˽)5=i��;�����C>b+V>e��>�V>aw����& >>D@���=6��F��i���-��髽(2������F���{��W^=k\��"�>�>bD9=��>���>�~�=���>�H;��-���ᾥv�>��>FS?��=�f>>�b�>��k<a����r>2���	/�L�����?��q�+��=��=2t޽`(����>��i>�Ɇ����鶽At���Q�>��6>�f<㮹��V_>��������}}3����X�	��=�嬾fm��Ҿ�����m���?#��>m����>�s>>\e�>�ݘ=~�Խ�f��h/�#v�>�"���a�=L=�̲==��=hZ�q��>�Τ>	�>B!>9�=��!���.�8����V��>>(RJ>(�����T��S@�K�@=J5<>)8���В�O��]�?�A=�^<J �>a�=ݳ�T���]�:�hW�>	J>       �7f�daȽ�����5>�<��Z>W��>�48��>��0!,>(�K���{>����lZ?��T�|�q����7�9��V�=�=F����?������ξ�«�
O�=��齤2�������b>��=C���b��5.?��W� ��>�'�0�>m,�Z)b>�!�=�Ũ��ҟ>J��>���[ko���=������+�=���iܾ(�h<�.н��2�'3�<�ݽrNv��p>#ﾄl��K��jT-= pB�P>aw;�ULg�>->��	�=i�=k�ھ��B�8r+��󾶊>���a>��W����>tS�����<HD ����<mG;N��=,���1����>�̠�p��>�U(���=o˨��w?�ǵ=���=�"��;)�~*�<��>���*�%�a��=P�>��>;ɫ���N>|�
��@�mܾ���>�>&ǧ�c'�o�>/!=�T1�t��;�߳�a]>��%���>�h�so��/(>qa���~���㟾)�����+�6�ؾG�<F�>Zg��m�a�ߟS;7�#����>G��>�
?��>�ײ�rE�>�]-����p$�>N������>8�>=�>1��=�����p��;�=���>��w�#�ɾy=#z����k>XX�>��>D�1�Q8)�=X �#�ӽ��=U��$�޾�wͻcx�>�6�=��H>А7�{�+�h�T����>��̽�����r������:U�>fڽ��K�=s�1���<���=ˉ����%�u>3�)>=>��k��:
>4�{���%«>������d*��Ή>��z���=�p7>��j=���}��pb$����>�u�߻�>%gH��c>o�С.?�[@>֑?��<۰�e�V=����ϝ��dcP��J�<r�]�h�����x<���<�R��H����H<��,�>�n��)�3�W;.�� �����<Q�>Wq����'��0��R�>wM�����!S��н��h��>H<=��@��Wg>I�b�0y�E��c��<��C�����[�=�iƽG��=�K'>�f>�k־]Ͼ%Ц>~@���{u>��s>��鼠��J�C?Q�>'b+>4>��|�N>�"���'A�Yv!�&U?�}�>s�t>6K�>j/��h�V��.3���=`O�>��澯�i�)�>~�M>O�k>����6>Y/�>��= �#>��A>g�X=�>���>�EL�d�;f%>N�>ע��Jv�� �鞔>����=M0��	��[�G��z>��O�v�,��ȽQ�Q>��=1t�>O�]>K>�i�>����8Q?����=Ķc��S��dv=ۼ޼��5��X�> ��=�
d�&�>'�>lY�>2~��)�=���=�h>aȦ=$�/��>Cb>�z��Bd�>�*v��$�=�=��"B>8u?/顽?r�Ñ�>6w�>�q�<g׀>�t���˾k��ӳ>��p=�5?Ne>���>�}�<Z�>�!�=tJ��B�>�������[>�Ҿ>���=�?�ϕ���^�a@��̈́�׼�>�7C�� >���>lC1>�R�>�A�����>�4	?���=T	n������=W� ?hL��-z��Oz�Q�u��ܶ�{#�=����=Kp��;�=Q���5�9>/"�(�	���/>�R�>����{>��׽Æ�>��*՟>��8>'L@<����s
����>͠K���U�Y��������>63�<�O<���>/�{>��W��^Ѿ�}>F�&�;\־��<��=YP��)�x�>��=Z�^>�־џ�s�̽�]��dC��+>�~��a,>Ţ�>�`�>�s=u�����)�=�'_>��ɾe8J>��<�K�=���[d>-9��VY$>C��=n��Z�>_�
���\��>�=74���i���=3������3>ci�=Ek
����>:�;'4>!I8�K�B>3�j>�g��(�<���X#>ӔP�p����ܿ>�/>8�=g񑽪��f=�e���E��=����<�s;��#>�ƺ>�p�>s����@�=F��>�/ >���=�x>��>����$�����	0��e�>�O��ݹ{>.��>�N'���>�莾���<*������>?��>��[�U&ֽG3v>V�!�!OӺZ9b>�����=\��=��==H���d>�K�=7\��Y_>iג�e�ƾ�W+��r�=r�u<��N=�����䬽�۶���>T�����'�a׾�4�=�>�7��w�g�YI>�?��{n>x�����˾�Y0<L�����Α�=�~ܽi]T>.�!��ug>�q|>/�>�ڿ>�yu��Q>������;�˾^�>T�<Ƃ��3�>�ϻ�����U﷾Tg�Q�>a>�߽�D�����O�'ϻ���6� {��
�=��t<��=�%�>w�=�j	>��hF澠������>Q����=�i�=-nn>(N�����=���>�I�� �*=���;��m��>"��>/��=���<�p��%LQ=�`>�Y>�� =����=���+��"Ծ�Z�19�������LLv��K>Y*>��>���>]\L=�+��3t���S<B�R�{��=O�>y���X�= ,>��	> ���9�>���=Z�&�����&>B�1>�M6>��?��Gm>�h�N��̾,0>>�k������k�<�RH>� �>@��p����=��¾{�e>�L�f�>�r�>փ��[�^��+�=A0$?/'p�L�X�@�����p�v@������>�$���]4�D��>��=u�=�&��ޒ�>�M,<���>5�ľUF����<��W>*)����:?�y>F�<iu��>j�+�mj��*W���=s�Q�܏H>���=��j>�5ļȟ ?��s>�Ӣ��&b��3Y���R���A���@�tw7>_?�=�#���?̾F��M��ʃ�Gu�>��W>}Vv>m ����оy6�ώ�>ڪ�=���>���<�+!�?��>����(�< W�>�e�=����!�ͽ�%D>�����8����>k!�>�F�~>��|u���>���]�"�3��N�>�4�>��*>��>b6+��k<<lQ�h�=��>�ҩ>%Q��u�>��w>���>����������=k�=��>v����(!�zh>µ>��K��+�=a^c=�wỐ      ȍ����>s�ھ���P��� ���K>lվ��̽(MB�� �>\��>���>-C�>��˽X�C�]�{)6�r�������>.b?�e?�?�>	�?�R����>в�>� >��P�H'���J�>�/!?��5�o"�>#O(�,_�>c��?�2_>�t����>�y���?ّ?�=��L?Hj�>-`ὄi�>5�վ�.R�̱=���,Ͳ���=��	?^7`�2�x>�QC?�&޽��> ��>%�Ie)>�<���h>������;g⦾>ν�~Ⱦ�;�>KJ9��>|@">H-i��=�?�3>�ٸ=L��CN>��7=ߘ?"�u�fS�<	���K]>�|>?�wQ�����Q���>��0�>�wᾌh��>��?�,�?30��9̾�\���#��hmT�ɗg>�S��_�=�q*��n]�J�����>:&�Y��	�>�����N�>a��>�{>� �>d�U>�6D��f�����}��pt�>��߽2�R>�Դ��u�>q	����j
ƽ���*�N����=��[�����g���	��=w����_����=�V�>jo�=r�ʽw)=dC�>GA)�,�!?~�⾊ϐ>'��>��F�=�^�=�eԾ&�=>��i�k���*�� ?c�d��A>�v=��_>�N`��J��>%�>��=e�>#zd���l>���k�>��=`{���,>�B�>�⾧�>�:9?��M>�f6?B�E�Kƕ?�󾣎?搾��?M> ���>�����w(>���&G���|Ѿ�6%?���>y=�W���>�	�>xQ���=?��>7��rQ�����>�G�9��=U���%H<>�c6��f���>_�=o[�Т1�_n�>�J���FL=sǃ=ml>�3�>��'?8�>���=���>皸=�z��ѽ�+���Ͱ>5���5M#�����*�>�&4���]=��=v��=r>:
�>N>�>ZݼON>�*"=3{T>���K>>i���=sKc��y?�m�=x;�=O��=�ꕾIT��zz�f�=��W�<�<.)�G����>>2>
m�>c�>z���.&?j�������>���۾��j�n;?������>K��ٴ{>� ��<6>>6�~���
_��a�>��>����T?9!i>&O����>�<��;�M-������m��$�>���>Op�{�#>��=�uN�YO���^�Gq�=:G>[6���"\>�Ě�-�>�I���5F�E8|=���<R�=ФR�=Y�>б�>9��<��u=�r=���<��F)���=m���B��>��*�Oiؾ-9�>/"�;)��T�>���U5�=�4=t��>��'>���>#���%�>��׼Qy��M���
#���>l���y�T��=D-.����>�_�>�j�=�x�=P瀿9����cG�-u=�$�=�ӽ�L�=���=�g����=�䓾>�6�dI9�%u;���-?,���׆a?��?���?I�$��^�5���'����: ��?/�Q�����զ�B�=/|&>��]�g�5�eB�#Q����>����	��>�1?!^%��      ��?�l���h��}�έz>�L_?�U��=Q?����;?�)�NH��
� ϥ��4'>�f�=��?���q�4?� ��#7?��?1w
@:&>�m#?K�@�	�?�F��7�M>0�#?-���Q�<ڛ?6�r?P?�Fb?O�ƾ7�@v�=�,?ԃT?�'��?���!#g���ؾ����S"?0oȾ�Z����>���>cDr?��]@P�?J��>��οX�?P�>E�ܿH�S�"5>�p�>�T??��h?�Q�?�6�?�J��7N?�
�4�b����>��߽�*Z���B�A!�>��h�����q�K�@��>q{?NH��\h�io/���	@,v�?��?i����q�s��?�@���ѽ*}�=��h����8W�>�������/�B�o�?��t?fa�?@0k�5@��ӧȿ������]J��-�?�8'>�"�>�Rb?�l��1�Z> i���'�)+�>�7ο���?�(p��l���qc�#D
?�P>Q�?� =����Ա��
7�?�w�?���?��׾
�?7�o="��>�v¾R�C@�������Jߥ?9�+�
�?���GOľHW�����>��-?�.v� +m���[�����$o�)S��1�Խ*{�����>��3yH=��?dr�f�~���a?x��������>�b�p�?-D?�r8?Й���
����?>?�JP<s�Q�`���u��[��:ƿ1���b&��3�>�N2�D8M>�RE=�<��I@����?�����<�:V=�	�ſ�@�M׿Y�ɽ��?8�@D��><k?��Y?�9꾝��?����\=��9@�?D�;����Ι��q�?�v�1	�>VJ�> ��?��>e��W��-ľ߬H?����	"!@࿷�a{̾yP��}z>?�<D�D2x�<�Խ	� ���R<3j�\s�Bb���� ?>�=�EJ?�����ݾ(O��o�>? I���$?_���$�?va?Y	��h����	v�� �jR������?'��?2�-�ҥ5>�%��G��$��&+�;̷���!�³?��"?�$w��?b��?���?\i���y�7���t7���>�9�?��_�����6֯>Mi���ٌ?8D?o�����4���׿���?=��?�7�?��?�۽������(� ��N��?��^��r�?{�ǿ#�0�旟?��>�#x��@�m�c?E��?*��>��a=��N�о؈�>4�];��l�ѿ���?$E?(U�=jQ�?�g�?�\�?�y��I�!?m�U���,�ǰ���?.��>0%> ;�>�j�	V?�u?~[H?Y�?��̿���?�!1���(>�����:e?��?=O��~���lC���-��>�@;�?X�/���o?�#@Pm��Z ��R��>�?ӹ�!J��~o�萠?��?c��?�d?���?���?�F8?|�I�?��N�?�{�>�Xվ���8Z<
�=ފ���ڿ��{�≭?p;@-Օ�cb?`��><?�X�?��P?es�>��t?����sK��w2Q�zփ��?��#�7\3?�ę��zx��a�<ػ������ �?��_��?��޿>9�>�A��
�uHr?啯�y��*�r?\?>�G@RK=�v���dB?T̾(�P���ʿ���>��?Q^�>��e<(       �g��b��F��`Nb�d�<��m=����n���x�>c��<h��>�`�=���>J���?>�_�=��+��Z9>��6���<V"`��@�s�J����= �l>�M4��E>W��y�>�yF��>�Ӡ<t�:�7q���R�8C6>ʈ?O�(�w�}>��c=(       ���=/��=�+�Ƒ̾�B�=ߔu�a��=��:=��?(�o=cڊ>�ˌ��>���>:1����s���4��VJ�x-��j����.��ې�A�$>��X�b��>��3>>�>���@��%D��rY���:> >�'�����t��F��>�������>��=(       �ǂ�)&�>>�>it>�h,>;�>���<����ˣ��nO<�Y�>5f�YVB��NS�P5Q�$@��~�>�� >�~>-O��䳾��=Z�G�"��'����>)-�>ۛ��k��>WL^>��=�@<>F�l�>�Z ?�=>���=ѿ=A�=�J�>       bQ���D����??�^?�A=�w�>���>�ξ�׎>E�����=X�?�?F����d>!%�>7?�՜��7�ǽ(:������*>��|�#����=y��>��:>o][>�KH<z�"?�h�=�؛�zz$>��=���,>�m�=�Ɉ=��>��>�������߾>�}�>C�f��@?C祾�?_���gk��d4�����{ ?��?7�#?��>��)����Zj2�!-{��x<M���;=D�uA���|:=�Y�<��S>�6�ڙ<�Rk@>R�u��9�=�|�=��d��ı�$��)����>��g>��9��B�>��_��p��܋\�4G��>'>�c��M�?G�ewU�d���(�����>ٳ>����S�>p�s=/t�<�i1�m�A>��~>�N?���Q<@>�~[?��?�I��"�$>���:��~���W>L�&?��#>�P�>;˽�?uF7��Z�=�^'=��\>F������>ۋ�=kc}<P����>�.�;3[>ᛕ���h�s*Q>x�b��F��Y¼vs>O ���*�>]g��)!�a��>4��=4p�>�U����#R>p��"��>�ԾϜA��1���Y���t�q�Ծ�.�=En�>c+�y�a>�\E�}����ɻ��1��졽��y��#�>.�V>M�d�C��`��<�iP=��d��ľ[n���=�od��j�=�[l>!:����=~�	>� ���x��.�r�fE=g�M4=�s<��[ր>g���q�?�!�P��L�<�qb����<��1� �C��=��~��2�a> >ʬO�z�=?ӣ�=��4?t�? s��d�>���;_w�=�4�=����/>>s!R�5%�k���0>�I�>��=!���{L>#�<��«=E���0�<vͺ>��؏�>>o�>1S�>R`�U������گ�Ӥ$=�w�>C��>WQ�>��d�e�޾�$�y��[?	\�����>Sc?|�K?7�?�]>y|;?�ٓ�ox2��[���߅.����=��]?b1�'Y�=hlD>��l�*>��ľ�<��+L�������>ɚ�>:�5=O��4�=f�y=`|���x�ףg�ֱ>G>��; m;?X轭!׾��=�D.>j�ھ'�.�lU����M�l0��Sk>���>J���XD>XH�i�n�J�>�V>��>W�4=D������?���>f�z?�M�>�$�<��	?e���A?��y?)�$?��ԼU*��D�>o돽�ř�l��>%| ��:F?��>$�\��gS�O=*?��ս�a?a�@>��9e>ͳ >��C>�4~?0�D>ـ?�`���!7?�!s>��t>g�����y��*�=���=!�y>uɦ=O7A��>����EV>�sq���_>�Ԗ����>�q>y��������>Z׊>~|>����f��>�q�����=jq�>�x>�?��<Lh�>��4��w�Ȟ�<��o<�<�>��>��>��þ[�i���g>󦕾+݂��1�>$L�=4E�>�)K>X�>�>B��a�ޢ�=`�E�Gz��j=>���:�Zt>+��=�K�����'�>=�=qb>��>�3�>d�>�YH���>�S	��S<B�������C>�5=������t=�g�'�=��G>�X��R�>�+�搐<�o>a@?�X�=��<R��=���߸¾����鳾9zA>q�۽롖�{l���g�>.e*><��> S>=��>F�_>�~���0��ï��>�?`�u>�|��[��/!�=nb1>If>���}-���U>��P=�@�>�fl�=�=qƾ*��>���>tX��]z>kH^���Ⱦ.6��/"Z�=n>�����w�>� 0=�u�=`Z̽�ԡ>� ���?�����6{һ�6�b5 �+��}[�<l򽇹e���ǽe\�=.ֶ<_1
>�ˆ�s+��%@>���>L@Ľ�����V>�ߎ<���zU=C�M��Ͼ�=�jٽ���=S����gx�=�>���>�Y���%�>.?�C�<Ng>Lk\>���>�/�=ɠ<=ţ>��>
�	���Q>#�0�ܰ�>d�a��͒� �z>��=A@�=Eٽ����[��2�>��=d��<�B��&31>j����c(�=2>���r;>A�>�̈;+�>H\>��Y��v[��̠�3�W�_'Q>���<�C;��o�>�Ӎ�2l���"���?�k]x��� ��G(>�58>(��-��>��q9@���M	�> �6��)�=ļi���ü�>M��>7پ�2>☭�X��=v�����=?>e�,=:g=��46ӽV͵>���=�о+��s�9>d��>L����
�7-���?�>�>_�˷���պ>�d��{*E���T>��H=�F�>��!�[���%�e>��m����>3�e>1Z�=��i>�-��Į��G���L�>�3���J�>�,�=�>�am��m�>��]EA>L3�!��=U�վ^��<;.>�{>P�"��W��>=5YR=��9��>�dh>a<�r�(<Ў{�>;�Ú=�>����<�j�&��>}>kN&>Dď�F�-��%	��?*�ξ���=4�8>���>]\">f,������!���>�9
<!��>l�V�a�Z�����G�>��=ލ>�{���x�3�9>�� aĽ�HX;0ͩ��4F>Up�=8V�=9bd��>�Lw��>�>w�X>�ܷ�=���^��<��N���ҽ�U�>�b9�i�>T���W�>�t����M;�P
�>�OI>�e&���i��|�V�*��$z>?*f�an���/?I�?8���L��̑�<����=O�
?��)<��>| �428>�k ����k13?�0�7d7>X5�=�	�D��=,i<'����>��!��>/d!��/�����>K�>B����8>j#.?��k�.u����{��bھ�m��M=�as>y=�>�^(?��=��Na=��彣>�D�=������>.��=�[�="�y$>:�>lA�>@�>�N>= ��>FJ>Ο<��N�`�=�e0�>(r=�R5�F�>�`c��yY=���/��=FC>�a����z=�ԝ�� ;<���)��>}+��l��/�ai�������$=/�^m�(=��Y�<���q� �=J�=mPԽk����-:��C?(       j0�=
����=���T�X>o�.��>��\>�Cw�.&>�@>3Ѯ>�O>�g�Գ=]H�*�<��=u[�>R4O��)=Q��>يO���۾Qܗ�ٞs�������4�4>��ҽ��s=�Ѓ>�g>��=j�����7>��=�|=:�l�vU(=(        R����!��<2>�&}>/
>ԣ�='�����O�L���'z�>�O9��(=4�>]q�I>���>��>�h�� �=s�ؾ��>�����1��}h>��=S����R>�^>��>2�t��>�e>*�0�rw�>��0�>j�ɾ�}@�����      �m��A�=��5��?�'Z<b'?���>o��2㳼�@?��k�p��=&R�=�y��&<�H�<�5F����=)�
>�3n?���\�=��9C������>�:���b�әz�R{�>Z0�>f=����T��>��
=�~�����>
yD��������>��>�?��=�־�"P��J>���������H �XH�=%j����>��=�K �@���8gP��R�r��>'(?��?g���j���Ea>9�c<��>�M˾D�>z��>�+�=ZӬ=Ԇ񾛰�=��@��>�>50���c[>Ė>���>k��>c�L>��H���|�4�(������p��7>��^�?��x>�Zk�ڌ�����3�C(�=�!��Q�>�����o=�������}��c�����>3��}�Y�G��+!��)��>��'�v��>�*��󸕾L3�>�U1��Ծ����:�>��cδ>�q�ƶ½���=k�>�(��@����>1˾7�;�7<Y��;�=�~�;��=)".�EQ��.2>�2�=܊A?�Z�.��>�L�>���H��}h�:�$>�ۗ���T�󸁾�^}>+��>t�?X@>F�)�I�=2�?26D>l��>C�>1�P���پ�@>󔔾R�Ǿ�Ql��&?�>�cþ�ug�d���e�=iC�>���>�r=q뎽(�i>Fm�>�X�=�2!�Ɍ>�1?�%���cS�|d��QZk>�l��7��H>��?�NQ��ܾ}�>���>�q�>������)���B��@��5�>�Э�+(�>���gL7<aC�
�w��D9�Z|
?���=N�˾��>6��>�~��L���ﻃ�>8��	�׾��>/��=&/V>NF%?F�/=��r>U$��I���M<M�>�
�>�G(>!��=`~>��h>f3w�Z��>�.��3=ݕ�<�)�=�wW��¾����R��+�>�c�EC/����>_�<�跮��ʫ��`<�>��=���;Q�f��ނ>�(�s����� >�b���.�^�z���>�v�=��]��J�����=��e�Y��;ӗ>�\ӽ^)��K��A s�5%>�򾤃�����>�
���[>���=zY˼~�����>:a�l9(��4��̚���=�<�C?+ec�[� =L4��Py�>�G��ȗ�>8ߚ>����)�#`���� ��i�>*r,���>z!��F?���>���=�.��̐[<6�>���>\D��P����.8>�;>Ʌľ%���݈�>aꂾF󮾑Y>�A���h �DX�D<:?�$��l$�I��>�0d=C�->��v�qE�>�A羓��>�LԾw��>2}�=%Nj>
e�>$?�	�=�L�>���ho����=k+�y�p�ܹM>��v>�[�m�4q>�� =֬��'��}�������y:��Nvt���R�������?�;�Ū�Ggx�R�j?�V����!>"��>
.L�g�Q����>|̇>�T�>�y�;<�$?���=Z��=yIS>{�����>����0L��b�>=qƾR�u�P��>��j>�%��2=����x��7��> ��O���ѽ3`ӾuB�в	>       q*>nR���xt>=V.>DJ<?J>�>a�#>��>�O����>)�@��N�>͟:>~ۄ��B��u��*�>@M�=#�5�����?]��<���=F��=�ܾ�xG>��/�w��>�|�<�����O>Jw�������?��!�3���Y�v���?\�P�aپ��8�u�>a�>tٗ>z�=�^�=�/0>H��>�]Ծ�� =�LS>ģ�>��]<l�=�V�>�8V>*���n	>4�����Y>xaJ<��o>=Y7��c�">s�^>�4'�awo��Φ��eӽ�� �a�¾�4�Bo�>(�?��xm:��J?��R�=���L�>E�^=U~�<n�ȼ��=�v>7rf�a�q"=�c>�7��e�νOE>����E�=vם>�]��O�&�`�½���>���v�>l�佒h��@�(��9��C^�L8�'CC?w>?�?��g�5��b߂=m��=;�	?��>��>�톾j ��>}�P�i[&���> �>�^�=�D��>�$Ǽ�?��)>m"g����/q���f� ���_����@�>��+��է>�b���{�>�?>�u>�i�s��+�����>q��=*8��M�>�������>�+�<�&ξ��>�v��������=T�i�n�>�?���� ��=V���2��}½ݩ���c�>.�L�<wg>�j�K��NE�V�����>k��؄!��c�=i��\W�&U��r=־��t?�ܽ	�7>"���}�?���#��>`8%>j��>�Q�%>]K�>U��`s�R��<M�[>���:���n���\l��8�<+���!���>��?�R�>�򶻤o��=1o >0�8>%@�=�&[���ȾG��.�0��.���1f>�f��_һ���?��+=u}�>. %<6ĸ��|I>;4^;o$?�R�b�l�2���>\>���>v�=�Kx�$��>�ǾB~�=~ʾ"̽r,b��ş��w�=���q@��1��>�Dh�R���>�{k�}�=��W*��G@���c>�*>S��>��ӿ���^>�M�=�>�e@��a���=Q^������j�=��>>�_h>5�w��x�?0m>�r���o=���t1>Kv��V�|�P�>���<�߾lӵ���h��4�=zf>�'�>���Nb <1-��Ӑ<��>���=�k�>%F�6�B����N+*���&>?�ν���8z���̅���'?���>�w�>�ϊ��]�E��-����g{l��Qؾ�V>��,=mJu�HV���?5��hmc�V��=��=��$�(?slF���?*+?��>�@C�}�����>	�8>D���0�P?c�¼�q��	c!?-8����=�ف���>�GD�C��>��>��M>7z;��Ҿ�-A�1佩��C�����@�`�^�+c�>�\������f�>^p��2g>_;}<�?�D>x?���O¤=�G�=��s��>�4���>�83���>#��<�K�����)�>��/����<k�s�ò�m�>x3?O&�=��־�!3�*g��O�=p�?��A?��H��>��l?�LT���>�w��>Pp5�U\��f�j�kR*>5��j3==;%E>�^���%>��W���*�aAY��T����<�d`�J!>)�o>*Ζ�C�;��=7@�.�/��ќ������A>���<}�=]�����z�����`� >��>�#=s�>��E���O>\���&z=�#���/d>q�ѼA����Z���I��~���x>w#�o��>y�p>t�ԽG�=?���%$��I��>��[;h�>��f�Z��>�}�=����e =���=̷��rA6�2��1�;��P�M��>��=�x>�u�;�L>|�=��&�1��J�@B>2���l��> �۶�˓>��[��JN>����>GQ�>A�t>��/�f/N�r�Q=6d1>�ő=X9>���%��E�>��=��[���s�=�����=��>Đ�>��>��>A�>��L>)�?USo��9�>j%]>䥊>��>�6>O	�>���UW>�]���$��bc�>4�<~l��=��F>S��7�ڽw*�\�z>�z<<��=���>�������"�=
�߾�C/>�iY���r>yR�==7Y>���==kz>�J�> �C�����Tғ>�J�M��.>}�g�4����>��Ͻ^h����=
�t�Z�5��n> i���ZD�z�X=�m��f�]��$�>L�>��0>F�>ܸz=��U<R=P�8�j��! =~�<�"??�ӵ��X���ľ�i�>N�>z�*>��>�~Y��F�����<��>7K��,?ox=b�C��I��@T=�{V�1��=-�>Do����>JUȻ�I����=��s���
�kN�:u�?%	�팫>S��>���<NS�>��=�A�;��2��->�>���[�>�f�=3a�=���(yP��V>��>��9R�>=�w>s�3>ؾ�W��J�=�>S񅽙�
?��\>Ql���=��=?JP>�>��>�3�=?? =나��
��i!�G�����^>g�u������%-�n���>�>�[>��
>N����[���D<�~h���o>`�>��?>'
�>u�/?Z�^�4�f�󱙽9�?�F>{�>��$�q�?l9��Q��6��>��l>��>��ʽ�k�>ߐ��t��h��=�K>;A��0I>6��ʦ�����ނV��"C��^�>�u��ϸ>#>x>J�j>s�<��;l<U;5��[�aN�=�H�>�͑վ�$���i��\=2����R>�� ���:��x¾c�X�@�>)�>.��J�>qn����y�Ž� �6���m1>�j|��f[>0�j>�՚>�y��Y_>���>��6?�Z?�s �c>�ŷ>�#>&��<�Y�> �V�(O&=om�
>�X�<��K>bE#?DЊ>wP��om�:�
?�'����?I�s�c
����>ȩU�&��ߴ4>��!���>'�T��#ھ��ྩVg�Se`>e�����+�ń������w�;>]��=���	�m�UZ�>N��F��yB�=�9&>pe�H�=(�=
eU�����s���x�>��7�Gy����s�?�H=�6ʽ+t���2���Dz���>B4B?���>����?���>Eh�>'�P�(       :��=O]^=�7�>���>(���<���O�=�x�>��1?���=0��=��f�V�t�"�>��y=;�?�I6��{�������J>W彽$
�<��
=��׾m;y��t�0�ɽm]�=�
z>��>���=G۰>��=��]>��?8Ԯ>�{?7�>�Z?u�=(       A�.>9��8�o>��D�'Q�>�Q�>G��=�]X>%Y?��Y���Ⱦ��u>6�@���>O�����>���t��Q2��}>#�!=K�=�"�WL����>�a��Qa\>U��&�>�e9=�D��z����>83��>�o/>�68=e�/����>򔽐      ���>E�v>���>߫���������C�Ͻ�:#>�+��FL=������-?�l�?"����ť>ܴ>g�a=�H�?�i>f�ɾ�R=|P>�˳�8�̾�?k�$=Vn�>)r�����cG)=VM߾F�>�>��?P�~?��l��*?O��> {P��� ?�h�>�紾�]�>C��>v5=:n��A�Ծt�L?�T�?�I���L�?�AV�XAľ٨@>�xȾ��A��ώ>N� ?�̉�8��?��>�Y�=t}��I��@f�ij��d�T?1A��񰹾�Q�?�d��D�ھ�4�>؃�B=>|�9���=[a��,4��+?�=�>"�@��p+�Ͼ?τE>�H?T���ڊ���̾�A?/�.>:����{ؾ�1�����nA=���־��>�QK���)�N�> 諭�?ʶ�<�C?+���?���ʋ޾Ƽ9>_t&=-�O>��\>��Z>s�?�A߾�z�>�V�>�����/#�4å�ˌO>��>,�>��?�F��]ƾ�ֽ!�P�қ�>�W\>�=�����GH�~�?�D���>O�!>0�'�X�p=�L�>N���뻾E��>%���|'�.��%����鵝>�/⾔���E�_�}��
I���>CMZ�-[�z�Z>��~>2��>n���F��͍���~[�����)�>A�>=�C���'?�(>�>#����>Z#���H?m������=��Y����(�?kX$�Q��=��>܁��Jz}?��r���+�[ˬ�͔�����"_i=~&�>�ޖ��d8��>Ͼ&�<^�>@q�m?�6�>h��$��>3�S=);>\�E>�hj>� �>2��G�q�b��>�4�=�{ؽ{����<?��B?&�k�/���遝=@�<><��=8�-?�Aӽ|w�>�:#�,�	�����7��RP>�?��=>���$���-Ct=����0��9S�>�U ?������>��׽���=�>:>ٞ���Ƚm
r>>��=�>#� ?��?�{��l�->�%?ꞌ>?)�>��n>�����78>��}>� D>��%ç>!S+=N�5�e4>:�:����=Ƅ?�5u�|��0g��NU?�k=�^�����>�����vY�K��>3���;���>v?����_���?�ս�tL?�?���1���*Tr�-@�}w�=B<�����i���ӄǾx���d�>���=�Z!?�-�=����l�S>1�9��Nb=L��>�X�>�V�;P� �N>A? �:?��a����>j��>w䈾�;_?s�>��Z��a޻뉨>W1��*���?J�H��j�>�}������R>����E��Ѣ�>�)�>�+?���=���?{��?1�����~��j�>ƛ�G����Z?I;D�M��=��<g?1�>:���D�����U�Ǿ9c)�j/����;dK�;4w>"f�=pIg����>�����>WG�< 0`���>�9�D����|�ñ����?e�(���g�P�?����1�>:���X�C?�����\���?��D?pN�1������j�A�=ˀ>M�]?U ��\���U]����=���>�{�>��">�Z\>轉���ľc�       it-�K�=�Q�R�h=L��=F>��2>���I��H>����h�\�=B��>z!��V���(>�}���q�m�w��Q>�I>��u���"��hռ^>vdY?NUx>��>�:��^�?�Y>����c���t7��Q�����>$��=�J�>AQ̾��k�+�<�W�=�f��y>�	�;j�#>�S��H����g�]�&=�=i��=sؾA)��NM� ��>Ï�>w|>��$C>��>L`$�l�=Pջ����֞��4;�/P���v���c�>�����<��>5k>T��������>�cƾR�?I�j>�þc�S�Y'.>dܷ>1��>�' �;*8>%+?\�?'xݾ�^�=�0>t��&��>�'=n?F>9�>7�7>��=�L>rP=D� ?,ȵ�7��>ZR1>	��p�=C/?!���w�=v�&>t1�=]?��H��<��>�4�| �>P��-ޱ�5���⓾p�?dy>�\>w��>�"�>�H�P�H�(1�o�=��>��=EO�=Յ�0b�>ps{���6�+A0���L>����٠w>0L��SF>��ž6̻�PԼ�.�>�L���>/h��ˇ>3���w�����53_�98�&=� ̋>r^�����r믾�@�}_>vā��ok��u=���z>�{=����9�x>�@����Z�ןC��<�;�<�#�������-ݽ'�U<p�C><��>�v�;�+�(��>��O=>S���|�>~���Cy>�8��Fכ>�,*�q�!�_t1>�Ud�9�����=��=�?ѥ�>; ?+0?}ݒ;yW�>�g}��Z>�	R>(?>$�;?T�F��n�<�J?��=Q�<ѵ
?�T�=6�K>��H��A=���>'�'>ߜ�=����P��9'�EaR�C~a�\eݼ�@>1V#?�i�>��U�t������>�s=g�=��DK>�V?x
鼛��>�K���=1���x�>�'߾�>���g��a�>�"���|%>[�>b龌wȾw�>�����L�=0�>�0N=A��>t�*�t�>,�=�TT>���>O.N��<�%�[�;�C7�>�
��"ƾ��>��%?��(>�_���>��=�7�}~��O���l���y��e��'��\�H�[�þC������<H��<Pd�-mD����c�[�.yK�I�Ž^��Ӱ��5P� v0��=K>ڐ�>%;Ծ8m>��վ��D�!Xھ�q�=E�&>�I>��e�-�ɾ%Pj�A�j��Tٽ�;$>|h�=��<ޣs>��w�P�F>�y=_�=�q:a>���=�>�D'�44ڽLZt��7�<l)��~M>qP<D��pr_�_�|�J�r>W�ݽ�r�)�&>�/?�(����>�)6��EK?<b�IFu�4H���?�\��_��>�V��9}��G�>�`�>��?�̠���x
?��Z>��?�>u�v>)���x����>�WɽB�y>p��� ?ؠS>�0o=�o�>HJ���(>���>b�~�~b�A��<����Ƚ0�8�ub�Eb1� /H�h���>������>����>�>yh�Ca���}�=Ο���_B�J�7�b�>6tP>D.����<=`l>&h>c`
>����ȭ>�2����>�e�������,ؾ�̕>�h�=���t�پ\�8��4���ċ> ��=�z�>@>�=���>=��%���e�z�jz���A����J>�a�ŴW�.�~��>��=	v����}Cؾ��|=K�=�.=�w�>�3��cP�U#�=����ѩ >((�xg�>(>�n=��v��>%Ⱦ�����A�>aG�>2�.�������>\�����Mq�>$K���֡=�0�>j̝=D��u��=c�>��;q��<m��F]�k��>�h��������ž���z��X>��U>~NE=��;�Ծpf����(�T��#˾n���{��=A�"?�vm>9ྡྷO��l�1߾#�=�ۼ>�I��Ft�A >�����O�=an��f�=���<�Y��ҟz>���>Z�h>��$>��=�0�ح������9�=ӽ#J�>U��.�����W�<�*�=?�X>���>�ow���A=�y�Y��=?�1>Dq?LԿ>v��A��>�|��
ٽ��}>�������������g�?�t>��T>G��=�}�{�W�+��c�>Ip�<y
=�^���Z�=H��>�h>�彈e
>�j���>`Ӝ�γ�=���=y�~=��C>pU>�9��d���~L�5��s������>0pz=� <a��U�>=4�">� �?N��f6=�"[<pNܽA�>l�=�׌����>���-�����ǽ<��>�o��`�zf=�ۈ����\c>_����o��^�>U�{>��yJ=�@��e��J"�>u
��]�>Ԙ�>9!(�b��>h)�3	\�B���?q�>[�t��?�s�[���q|�=5��>����e���O��V�פ�>�ʍ�+�Y�-ꄾ��?�?g���7>��T�%os�n5�ﳑ��j�������r>x�үH>8fD��|��l>y*��0U2��;�=
����'K���F�<�j?�s-�噛<�Ӏ����=)��>��>R�
3R�a1>p���?� D��_�>�ȗ�`��hN�>�>�)5?R��3�=�~�>�g8?�:2?AE���|>��L?� ?�'�7=���
?�=�Y�>RD�0.����4���f�3��>	�&��Ҵ�B�>� �;i���#����N�>(�>�f%?=�>�x轳 ���}D>t#x�c�E>��¾v�þ��ھ��ٽp��>��N�ջ��\9������<";��H����$R�yW޼�����<�X��:���K�^?> _��=߽qs����͍=N����c=�C.�L<����y;��0��I溫��=��¾�9>K)� `˾-�=�EҾ���C��>�>�܀��">^G1� �>�(ھ��&�
䀾��A=n!A��09�"Y�>���r��	8ٽ)u�>3�=>�b=+�������q�ɹ/�b��=�=n֘�z��E	�<�D�� M�����M�^���{,�t֣=<ݛ�l��>2v�>����%�>��>Q������n���j�3>D4���O�=�xS���/��E�>��?{�;�(       �:W�>Q@���>eN�>kǽ;+�>�]����=^�L>��>PH�����ǚ/>��E0�b��=%6&=��K�)��=@�>�[���K��?��]���V��=a5$�0��<��>;w>�p=�A>b�=�q?�=�H��bbU��l>��=?� =(       /��%�<��T�[e�	�3p��ԃ���=x��>�M�>jV�={�>��A?q��>im�<pê�W�=Um=1����o��ߊ��� ��Rμ��U>�̢���_>�����W�>k{:>.�D>+t?>}�=.iٽ��??�h�ڿ�u��<G��>�#?*�Խ�      Fz���,>�'^���>�����N>-�>��;I-��0��'Ӿb�=mC��26D=�L�i��D�z�Xj�j=T���Qؾ���>�������2�>' �>UB>���_ֿM.�����? q�⾿>5���׭=>�t�q�˾���<�ʽ6>jUl�Rκ�S�l=t��	�><y�>�#��}�?� �<�*F�l�A>�c�=����4�?<泾>s�?.d=SC�=��>�^t�ǣ�i(���9�>��>rת��q���Qg�vR>W[�>���>�Tn>�ʴ=�S�L�hS>�j���F�.������f,ľL�>�kֽ�L<�a�#�|mL?���++>?�7�=u�>�06>�&�>R���5��p>+x��;�>7W�A�<S�N��JB�J�&=�#/�(<��hp�D����zN�q�Y�*��>R��=���ifݾ�9��}& �(>��>��ɵK�Q�����[(?�$��.�>X��9ō?]��>8�=��>0[��%L���B�>� �Oi�>sk]�)׽2������ۏ:>]V�i")>���>�x�����)ܛ�8Ѫ��z�='��ċ=�en��4!>r@�<��K��<���9� 7>�א?�)���=w����>� �>"k.���F�X��<��$>��T��g�?��>��E>_/��,ܣ�^T7��������:��s���}vY����>e�>������>l�=���>��=4�=�M4e>��>,ː>b�>�nB>B��=ȫ��:���\2�>"����Ͼ!�q����=��=��T�:i�.=�Q����ӽ��?�`�>�p>��M>�2��-x���\>u��<n1��?,�rK���=�=�J�>��>1�=��:>���=�N�+�.=KI�>qG����=�!}=���ER>��>D�Ͼ,���䵽��>��^��,�>:jؽу�=Q�Խ�j����<�j<<ʪ��Ho����>W���w���1W>�3⽙�(�>��S�>tl?,���Pӭ=��=�
�>%��>r�u=�q�ڝ ?OR�3�&<I��1K��a��=���Ҥt���> �v
���־��v>�0>��>�#��
��{W>�vG��k=�k?�L �So�>����� ?/��=��T���>ѱ]>
�3>=Tx���h�<9��>=]V���&����$��"e>��N�֯&?���>N���D*'���+�nk����>�gL� �-=[#��	��E�5a���I�>�(_�q"�jb$�?1ƾ�=�Ы=D��&�>������龟�;���>�i�=��>J�>B���s�eȘ�	@ӽ"U��`N������3GѾ "�;�^�<�e��BG/?�
�>&y>/پ�>�>\t�>">?S���m��=o-�?o��By��f]>}�?sɍ>��=wZ�­���a�S�&�M3W=`�3���>��?�9i�>��M>r a�$�E>�G�>b���ON%���n>_4�>Ǫ}���3��Z־!�����*�WAU>�H��1���O��<�"�>�>&ȏ=��9=�=3'�=	聾1�>#�V%�=��>�c}��d�>(       
V=>\h>�x��Y���u׼��>_F����>8߽R(��x
��4׵=s��� Uu>U9�>��<y\�x�->W��=w"=$ 0�������>c�D�iA�>��H��X����<pG:�]䱾�۫>�n>�ڢ=~�>�:��`]>��>$��������F�(       ��>�b>zI<���*$�ڳu>���>��>������T=e��=7���"/>, �>6k>�%����%� >)ly�&�?ɾ���枇=M�d����=B����Lx>��$>���'ɑ��l>FH�=�?�J>ƨ��,��i�>�@��u>G�m=�      I�G?�;���ѯ>��>���>֔3=8vD?�У>.7�>�)���G?6��5Z3>>	���N�>�޸��=�?��='|�>����c >�)�=qN�>��`>�1�>�ҹ�Q��>��޽�Ԧ�{�U�Qs�=)Cq��o_�HE5��l;ւȽ�>}_�>c��>��Q>�h�C�#��<"���@>�
A�)�p�!o���['���7�͗��)=���I�>��3�6+�=�[�=�A�>E-V?��{��H�������>;Ë��zת>��>st`��'��_*�>�T�?Q���ۚ��.��$�>x!��F��>����ᅪ�o9��&�=� ��얿��Z?j��^)�>��k�]����L>�Ͽ��v?	Fz�im��G�=3�ľ�3���j#�=/:?�9#?;{����<=��[ֽA��=v?�>L�@>�)�>w��>l��<���>)>�?�O�6>�>?Λ>Xt�Ï̾o��> 
=s�e=Ə<<k�>N/
=��9��6�����>�Xm�c��>4��>�v���zټ_0�>%S�>���>��3>Yʖ>}ش>�u>QV�?��&?{�̾�	�>Ax�
:�>udK>>�>}�;���-?��)?o�K1�@��F��>!GF�}��>���=��>�?w@7�>f[�oO?�5&>ԾU�]`
>c�L?)Ⲿ��?�zy�2�6>��G�>Y:0�Q<>H<�>�� ?�*�>z�ݾ<势$���=e���u�?b^R�G�>�f�>z�>|�>�'?Z�`?ڍU>Њ��yQ�غ�z����I�>	����-�\�?�+?���h>FK�=��㽔x�=�x����u��T�ξ񒆾V�����S��H,?#�D�|�>�͠��!-�%j!?�R#?��T?�>-%�>�7�>��'�T��>�>�f�>�E#��e�>�F�>/���媾�M4�6�A>E����0�jug�L �>A���d������{�|>�{'>�FP>\�>�]���tN>m�>�Y�='����s>I2оE��>9�Z��>,pD���u�tr�g�Y�feb>�T>�`˾|��>b	1�։���>��E>}�ξ�0=?\�X=��>h㠾W�.�N#�<sA>> �?<ʾ[��������"?X�þ$򴾦^�>�n�>� g>*;s>�.����?W_�=�"���J<�>�)<>F���߁>^�f�N�#?OQ��-
y��_>�y>N, ?����-�?�`��d���<���f�?��E�lF+?j���x>�Vi>�x*?�S;h���%Q�Þ�?(��=���?�Sྷ�}�`�=I,?+H�*��೜�d5B>��Խo�>wA=�/�E���~>w��=��=ɧ߾NN�>��O��"�>���=���=T]�=����>U1=+̾�����괿��?���³ξ��(��F?j��G4���#ƽ�h�>�F?�vپ�!�E>�󱽞���79�>��B?;�>��>���>�� ?=�v?��}<��>����-h>?Y@����>�}V������Ծ�Z[?����,�n�<>ҽ��	��#2g>"C��F�><�<#�?��>��;�T'?�mJ>\��>��5Ҡ=�R�?��ھ
�>�_�=       T��K��� ��ˈ<���>��o�r��>�g�=��b>J=}>l�>=a��" ��������>G�>�
E��2{�]��="'Ͼ&����Њ?�$�=XJ>��?ް@�}�-�Z���A�>�4�_��>�R�=�+s>����KO��S�f�>�.'��o�=V�ƾzj:ϸn>�a>Q۩�2��>�|�>\����Ǘ���žґ��Q�V������=��>��^>CU��<��3���6d?���>�"���
'<��v=�E�e�>ĝ	���`���)p�=5U���,=�'�=�"w<W�`�4:�>n~�=�������46>��>W ?e�,���j>笛>ħ�>.㶾�:�`�����,�o����=����~ ����?ބQ?�٦>����گ�+��&�>��|>����2�e�&�C>�v���D꾅۾I <2J���n����i� �(�h�->%o�_	�=�z���3�� ;?[w��=a4?��=U��=\��=�A>��?CRq?}f��e����=�帾M�>m!=���ЏE���Q>$\�=Ύ�U�>�2��>Y'�>�Es�hޞ�������Z	:�j*o<��m>L��@������Û>�־�K�o ;�J=;�N>F��>5�=�ױ>߈�>�LνƟ�>��>N�����">g?��<��nЕ��`>'Ql>۔�>>=WR3>0t��c��<�P>oa:�y�z��-A=ʹ��`X;�y�<�����S{�+�l�w����=�/��a�H�*��p�I�m���=�$N>=ܾ�h=W�>���>0z?��2>F�=��(?����J��k>���>och����=Y������?Z�&> ?��(��>��>*y��2���>�[a�Zcd=��?��3�=��>޽�ˇ>OF�>�L2�⸜>/���ʥ��A�>ഢ�J�>�u�5�x��	�l�m��
> ��=f�?��_�Ć�=�3>�k��!�>I�c=�Ԋ��K�>��C�Ҥi��BC>TZ�>ߕP�ǌ�fF<>���>)�=��{�������=pQоF�K=)�[���b�r��=��=D�����a��ަT���f>A���0�0>{�=)�v=;�?>Q~�<DԽH�?M3[>�	�<��?�]c����v/;��_?^;�>,��>A�=h9���n�=(0�
�b)��M ��[
?~��>��(?$ԁ>���u>"�8>�?�w������p"���ڽ�?�=�=�>�`����n>9�>�o�>tG��65�Л ?o�;�j���IP�;��=x��=ж�>��Y�A����^�ų�4�<W���$���ظ�@�>�g=;���@ʼ�,�<�>w��>m�@>��澘93�n��p�j�$~�=3S��S�6�E�)=22������>�������=�T�>[A�>�Aվ�D��5������>�24��Tٽ\-�^��>�$>N2E?5�>��>i�¾ٲ�>]��ɞ̾ o����o?V��=D�7�a껼K�(>]�?]̈��uH>�>���]�w���1�>e�Ǔ��R�>v�8���&>��D��O)�4���2)=-ʳ��y�=yʬ>��׼�j�^�1���3<RЯ>��>�o�=�%!<��~>8���D>>�}�>i$B=�oA��ɂ�n��=X�������=>F=��/�����>�{u��B��7��<�N �X�>��>s�>����ϖ�w��=J�J=b���~=ҨK�R%?>�����=!I��%Z=�`��A�>��t>��Ԩ�>%a�=GI>Ё�o>�ֱ>�>І >���6�=&\>>��=�A&=v�o_��лP�Z�������>\m�>C3��-�=e1s�Cΐ�U�u�8��<f�=d�t�'?p�������SfA��=��6>�b�>�j9?k��>s�U>?���k�>���� �S�^?�]��ׯ<�Ά�a���]%��btv>�|T�x��>ml�<�_�=�%->�ʾ�)���Xi>�ѐ>>�=6��>�6μlu��p�=ʡ@>y�e>� ۾)�>����1�=���oC�>׃�;�6��~s��K�Ծ:x��07=M����=�[%>F>B_���<q����p��3UG=�ld��x�������X��C�۽�b�>�P�û̄>�H`?�������
=�@>fڒ>xĪ>7���}��3��B�Y�ܠ�>���=C��?�-kK�抐>��e>�\���|=��>�	�>���=���>v嫾8��h�N;�>�Y�qM�>|�%>�@|>a�<6Ӽ=�T���̞>���<��,=bf��j�> Wa>˹���'�=�=y>�?�>���Z~��Z��=#��;$��=f���u��]?Y �=�[��,?�>���� �<B˵�S������>�V?�b�>��J��]�>[�<�>fcy>��,�1ː��eZ:�1>���>��>�r�fꕾf%��XD��E>�� n�?�q.=13>3�?�4q���=�e:��@�>��ܽ��1��o�=�T?q|$�\��m�I�@��<�;��4�Z>/����>��e><9�n���X�bA��,=���<i�'�OP���S�<��ڋ��=�=I�`>��s>�B>�?@>����p��>���>�Ӎ�<Eڽ���;�������ɇ6��:����h�=YI��\�սX���
mV>�a:��>G��>c��=���="Q����ܽ�
T�h�>��W>n��=n(��`�=���f89�^�4>���,��;��>����w�l>�E��T�"��������>���;���>1�:?�
�y|�>V�.>D�Z>�(6�B���HK�>��#�:����x�8���G�۽�;��%����>M�=�NTA>�>��������<��x����=]o���?o�?E�8�����4�O�{>`*�>�T�{UV>�v���f�>s��.<�=���V�5�xA�>�4�<�u��P�<��I;�L>�,�2ᐾ̖���ߟ=vSG>�z��6�>��߾$�����>.�ڼh�?��\��=�E����?^���"�x�*>1�>z�S>h�k�-<���ӓ>�"�^�>ܟ���f�=c#�=��r=�_��\(w>�$N>Ww>�Kc�� �>�*=ė���YN��������3$�<[�̾t!޺ ?>y1>��H�(��W>S >�ٌ��Q��(       �u����>x� �9,��T�ľ�ʾ��=9�= �>,��>N��(_�UI�>���)��>A��;�,���D>˵ڽ[7ǾŞɼ�Ƌ��O�^��>�M������^>��g>���F��'�<�0������2�>���h�I>���>:�E�ML?J��=       �8�>�s'?מA?��>�?�y,?]X�>s?�>�T?�X�>��1?ܛB>N�??��>���>3F�=�?�p]>K}�?��>       ��c<|�;E���F�к��4���"<��<�v� k�<�\�;���;��<*1��1�D��:��c<v��w�j;oļ���       k�A���־� ���2?�z1�+��;gOs�:��>���=��>�n�<Y�����>Q]���"�=�J5�:�W=7�>B�\=0ƥ�       2' =Q��;�/&;�v�;E*<Uw =��=#�<���<��Z<l�g=I��=F̧<��,<&=u��<<-�<�f=I*�<�"9<�      �����=$ww>l��$�Q>��>`�	>�2>PQ˽ⳙ>"�ѯ������+>� ?@�E>�K�>�OM>u-=��<�>5�=[���[~����{e�={�1����)~=w�`>��M��?>�� @Y��5@>*3�<K�:�T�XE����y>�0�>�}S>�yk>��j�n��=(
��%>�v_=a�M��=zF��/�z˷�W�s>{����������ƂϽ���G�><
=�x�>�<�#>0������nB���=?�>M��f\�>"��uEq������mu��H��OZ��ho�=��=��tmc���}�2�����K��i~>�� >Ə��T߾(�ý��=L�b>�Nv>�Nd=�|>ɩ��`Eo>�����ͥ��W�i䧼��r�鐫�	x/�M�Z����>��>�憽M>�Q��d���a>�^�|�Խc7�>$ν��n��̾�r{����`m>S�hlm�������h�n�4�֔g>��6��>}����lF��p?R9P=�d>>k���ZZ��N>F쵽`u��3`�I�=f��x�{>�\�^n����u�.-�>"��<{ n>�ߐ�p>[�.<�/��®>�Ǯ>�?�B����J>��<��J=̑<>ؿ�'w�<�d����=�.��La�L*�>�E�<��H���c>�u޾1u� ��=XZX�Zk��z�I@Ǿ��>��_;q���)4>f�<��g���$�'���>{I��s���Ⱦ扝>�h">�h�����T[>s�?�S����>�=H�S��=��'P�>6��>���>�/�=|+j��f#>���<�PX����=��	��	c<Ҝw�T͟>R`�;�ΰ�-6�>c��>݉���]۽A�1>]g�<�)e���>4>�'��˅>>td��I^���Ľ�ڂ�t����>&c�ҽЧ>�����'�>I �>���⤽v�+��»�q3F��������C>j�V>o�׽ݦ�>����ɱ>�K%�!���O�y��SM>Y�����=�H�G>�>�
=���N=�9=��<C�r<zJ�<6���P%��| �>Ӡ�ϖ�=��
>h�e�6�W��.���녽Fx�>`��=�z>���>�-�>B�h>��d���M��"�=�.p>e��#�ӄD�9x�=����>�f˼ ���(Kq�&%�>	
�� 8;�>Z>ս"�^�(�о�c���Ŧ>�/C>�u>���F��=���>�潲���C�{��ъ>4�=g�|=�I��@p=,�>�9��a�<�HY���ż���=Z��~ͽ3�>�t�=����ߛ0>�C�����>��t����Ʋ�>�wξ$�H=�䥾yϽ]�-�t��=�Kƽu>�F�>>F%��`�=	���<�>� ����;�<9>�ྍ�9����=�[=�CX�4�^��G��9<=��>C��>GG�=1Uн��~�r�]=��սu�q=�=%޾��o=yk��ek=��޽�ɯ>�Ll=�¨>$���0���
D��vm>��H>ָn�DX�w>��ծ���F>�D��It0��9�k@z>K���6�>����Y�`�k&?싇>N�O�       :�b���n� <_$=.���޹=EUb�V¾�Ғ��f��X�<u�*=$�=S|<�\�=_�Z����=U�>��B<I=       �,?�i�>�?��Z?J3?�?\�?�_�?�1r?���>?��?�
�?�e?��>�>>?;�k?0��?�Ӟ?�o?�߂?       +Z>�X�>YN�>IV0>a��>���=,)>�B�>櫾>`a�<{��>K��=*T�>*��>�~>P�!>���>�ҳ>��>�ʡ>       ?�q�����ӡ�<G��=M��<p�=S8��d@Ӿˑ��� ��$<}75��% >(�<�r�=�S9�:�>�,>��=���<       ���>��	?>2�>_4?�Q ?�̬>�E?�?s?n��>�>N�>�w	?3�?AU?�ܟ>�z�>�� ?/��>��3?       /��>�G�=�M�=\Mž�[m>���>��0�&>�<���Ms>����b�>�����z>�J���7����)��0�x3��{�P�       ���=<        	���R�T+�1��=:�>�U�>�+?��'�f���Ya󾲩I�F<�/?�2K���_A�>�U��d4?A��I�&�)�>�,C?��$?�ƾ)��>s�>c᛾U�I�F?rف�!2��:Ƈ�R� 7?*2��	�<��?o�W�V����2�J?�̾��9��:U=��{�Ґ���\:��d?�E)��LZ?�W	?ˇ�>�� >����|��>��#�۠���7[��N?��?       ��8��k��A>