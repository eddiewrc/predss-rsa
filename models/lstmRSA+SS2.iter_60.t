��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,40)
		self.r1 = t.nn.Sequential(t.nn.LSTM(40, 15, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(30), t.nn.Linear(30,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(K(U_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U54862816q1Ucpuq2�HNtQK ��(��(��tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U55389120q|h2�`	NtQK �<�(��(��tRq}�Rq~��N�be]q(hYh-h.((h/h0U55387840q�h2��NtQK �<�����tRq��Rq���N�be]q�(hZh-h.((h/h0U55386560q�h2�<NtQK �<���tRq��Rq���N�be]q�(h[h-h.((h/h0U55385280q�h2�<NtQK �<���tRq��Rq���N�be]q�(h]h-h.((h/h0U55384000q�h2�`	NtQK �<�(��(��tRq��Rq���N�be]q�(h^h-h.((h/h0U55382720q�h2��NtQK �<�����tRq��Rq���N�be]q�(h_h-h.((h/h0U55333504q�h2�<NtQK �<���tRq��Rq���N�be]q�(h`h-h.((h/h0U55331936q�h2�<NtQK �<���tRq��Rq���N�be]q�(hbh-h.((h/h0U49080400q�h2�NtQK �<�����tRq��Rq���N�be]q�(hch-h.((h/h0U55457056q�h2��NtQK �<�����tRq��Rq���N�be]q�(hdh-h.((h/h0U55455488q�h2�<NtQK �<���tRq��Rq���N�be]q�(heh-h.((h/h0U55514096q�h2�<NtQK �<���tRq��Rq���N�be]q�(hgh-h.((h/h0U55512816q�h2�NtQK �<�����tRq��Rq���N�be]q�(hhh-h.((h/h0U55512176q�h2��NtQK �<�����tRq��Rq���N�be]q�(hih-h.((h/h0U55510896q�h2�<NtQK �<���tRq��Rq���N�be]q�(hjh-h.((h/h0U55509616q�h2�<NtQK �<���tRq��Rq���N�be]q�(hlh-h.((h/h0U55508336q�h2�NtQK �<�����tRq��Rq���N�be]q�(hmh-h.((h/h0U55506480q�h2��NtQK �<�����tRq��Rq�N�be]q�(hnh-h.((h/h0U55582208q�h2�<NtQK �<���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U45437632q�h2�<NtQK �<���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U57742992q�h2�NtQK �<�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U57879424q�h2��NtQK �<�����tRqхRq҈�N�be]q�(hsh-h.((h/h0U57450960q�h2�<NtQK �<���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U59249120q�h2�<NtQK �<���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�K(Uhidden_sizeq�KU
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U68485920r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U68510432r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U68534944r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U68559824r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U71992288r0  h2�XNtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U74830080r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U75401792rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U75453920rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U75471984rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U75490224rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U78302064r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U78316016r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U81066112r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U81077216r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2r�  ub.�]q(U45437632qU49080400qU54862816qU55331936qU55333504qU55382720qU55384000qU55385280q	U55386560q
U55387840qU55389120qU55455488qU55457056qU55506480qU55508336qU55509616qU55510896qU55512176qU55512816qU55514096qU55582208qU57450960qU57742992qU57879424qU59249120qU68485920qU68510432qU68534944qU68559824qU71992288qU74830080q U75401792q!U75453920q"U75471984q#U75490224q$U78302064q%U78316016q&U81066112q'U81077216q(e.<       AɾC$�>����>���>�UӾ2�
>;��>"�e>J|�=N����|���^��� =+�s���='I4>��^=�gb�`�5��/�<nw���	���
Խخ�=�e>&�¾��Ľt7>ZS�;F��s$�>���=�&������o?[>k��>_(����"���=�/���5>N�@��鹾�/P<����j?��Q�D�=�?zr��I��<o�R>�Н=]{�>��='���h��>���=w�=      ��=�P�>�����p^>	�2>���>a�<6���؛��K��q;�����++���/�B��>� >Θ�<�-t=j^��!�<�pi=e�߽��y�iƘ�1՟�eĨ=��,�{>2�K>Щ>�}�=���=�T>�:|�J"���ć�]�>Ϳ��sX�f6��8&�=�Ew>,���G� ���q���=�7=;W��i��<[��1-->��L�Fʜ���>J���m`	��B��xD� ��=7a?�<P�,�]�1�x���ř��>����[��N<1�>���=�5Ӿ��8@*�"�=�v�>
��=!�E�ƛ�=s�>O�>Dw¼/"-�7��M�9Mc���=�����<rý���=$����,�E���V��P���jkA���x>E�>=�{>�T�> �Y��r�>�3�=Q��G?J�(<��>A피����@>;�����>�6�洂>
Ҿ�Ѽ0?Y�=���%� ��,轀&S>f]�>�7D?Gk1��D>�GY=�Pm=�o�>��s����>X�g����>X�����=)֍=N�>�d����.�6�\'y� 4�>t�=U�~�c��=���=+�ͥ<��!>G�Y��>adѽ��Ծ穸���.��a.>8Q0>&�>��u=���?������=*6�����
c����A>��)-?�be>0$>���>�M�=	6�]2g�C��;�&彥db�(��=���>81"��}?�������=�F�=6��>���>8c�>��\�� 1���3=̥>j�>��	?���>f7>��>�'>�W�{u�9����}�;�<K�����Fh?�y�N<�J>�򾼡�=��>�?�B�i�\>qT�����Òr<���=Si��x���6?h��Hv��S�>���>K��>����>�9�����=�j�>PO>[t�<�+>�38=ޥ<�i�ыi�rhֽ�D@?(�g>�R���ߑ>
��>A&>Cg��^A�>*����=gM�>�پ�����
>��=������>�оtA��Z ?��&�3�J<h�9<]�>��>��;�vvȾ�6>+�<Pyʾl6�>�1�;6� >2��=�%D>a�L��!��'�8?M"�=�4�>K\�=�w@�k
a����=��=(�4=�p
��W/�pU�� �I=�~�>2�<�P��]�>������=1�=<�=� �>}Zo>���=߮�>sM���=��n<0��>C�V���?��|�ť������5����>�Lz>b�B?�
=�(>�$�|�����=J�=Q爽�׏>�*&>�k|��7�=��q>'���ڭh>5/����	�=��>P`�>C��K�r�1ξ��=��Z��B�k��g ���=�w=��9���Z>LT�>���>@��>���>�c%=Ă�1h?d��>��n�����C܂�	6�=��>smv�J>Bz�=������5>��[>ig�>/ᓾn Y>�a����=ݥH�}<˻	���(>"��=5&F����>�.̾��<���>�C�=���>H��}�>���>�Ο>EdD;*��@�ھ�W�=J�U)�����>�T��!Y������+�=�r?g��>���=�� ?���t3�t>x?x���>/�>���</��F{������2�=�c�=B�>�h�=#��>�H���ž��	I>���>�Ă����=>�=��M*�>��6>���>�ސ�#�>��0���$>�%>���J��>�	>�C>�.>�G>�� ?���\!>��s>B�a>k�a=�.�>9}��i�*>}<���=3�1=����4�({r�bY��>��iJg�/�o>Ŀ����>uT}>� ��m��-L=fɼ(r��yd#?�>�z,�Ve>� �=��q=����-�@>�箽r���֍V����>�>�Ⱦ�x��*>?���`�sM�=��?z�r�͊>V�!�toF=5�ǽ/ ����$�p`ž#��=�G��@4�>˖>��-���"?M����GM>���>����u��>s�)?��=L�>�? c��ځ�= =x�im�=^6��y�>R)S����-�>G���<��r>}�� ���=�M
>��3��AN>�T����#��}w=���=O+�W6�eS�>WQ=��\=g9��i< �b�>�pp��\>ч?`�;��A?Ө��Q_�<o���`<��S#>7�����F>t1?c�2>�o��,����>�E,>Ý>��/�)��=6߷>�+��W���O��>`?A>��>"B�Ƒ$=})�?�Խ<:*>��>!.>���>�d)��}b>`�������x>�wZ��T�>���s�s;$�1W��v��>�3���ܽ����U�~����=�l>޿�v�ǽ��	�Nv��d�3?c%��aV=�M�=@A����>�U?|ܰ>�Щ��t��31�S�>`
�>�.�eS>�pe?㕇�M�`���w>(,�>l�(��j>�V��^Om�;���7�>�_�迷>��&>h�^�>��ߟ�d
�"_�D�>_�>��k��b�>k:�>0��>�u��龬>Z>	��Ns�?�>H��>lse=��=�=<�8�=28?��"�X�v>T
;7M;�[���k?A�߾��=[#�?,���N� ��>��>X'��o����=�Ӿ�߾y!?�
>��>GV>F�<?���.�y?d�V>�e꼫u6?���>����>\�;pv>W��>�s,�+M>R�>^��A8����>�f����;�>�P*=���=�)����=�4��vA�R�B�������1�	}�=�֪�*�V>C&�>t>�@�b��>W�:~�� ��=����X�Q�)?�aR>��>���>U3\�Q��z�>)���K2��,����]�_�K���1>���>u>����09=$|����`���~�=b>���ɾ�����ǿM�tq�>��>�U>�q��2
��O��9 N���D>��?�Z�>���?�ܾ¹Z�j�=�>�M<?��>Җw=\ao��v�W�<5c��F��)hH>�n�>�u���).=�6���4���ü�L�<)��<M�=X~�>ӽ>��Ê>� >�÷=w*{�_b�>�-b>��s>��i�5�Ļ�J=c�=��T��x#�`qs�B��>�|f�͊�=߶��k�=�t	�v�9:�r`��x���W��x�>	0���>�Zh[��9�؃�փ{�u����_�<�v�=��Ծ��z�G�&��m&>r1>�SW>���>M�>�[޽t�����=�Cu>uT뽾e;?�d�>�Q^�Íd��^��o)�
�'��5$��NR���w�$��xp'<ě�>�@�=Hľ>ߵ^�{oO�j�(>�:;����3���n>��>�+�~#���`�>���B0���)����(�Ҹk�"���'��Ϡ>T���ص���=��i���>�7	�����?�n�>�G����?��>�gB���q?E�x��e�>���>��b�s�����x���?��%?��ǽ&>��!���Ҿk�>�� ��u>���>��U�W�O�Ɲ>a�?�7�>JR,?9<?�ɉ�-� ?��>�ڏ=9��?�̾Ԝ˿;r@=%p�>�:1>��>�ҡ��T(>k�=���99���t��bf>�h����_>�I�=�ݜ��6>���>��弭᡽������̾�4�> �>�o�>��>�A<��@M�R���5�h=F�J=p\ټx־#�\����=���O��>��,>ƅ���,M�>���>�z���O��!
���Y�=q:O>\�>�#�=�bQ<s���P>�
g�wV`>�J�(V���;.�⫁=�/>*�d<p]�>�N�>����Տ=k?��>ä�>���L���=t �Xɗ�,�8�z�n-�����C(f>)�F�O�ܾ�j1;����Ͷ��!kս��[=�ٓ��0>�}������>�2����L�d�[Yl>�=�>�ѽ�4>m�;��;�x5%>�
f���-�����ɋY>'�=L�R=�{�G9H���4��<2�>�F��^���s�=�z�<�~A>��L<�>���j~;�hY�;Z'>����1' �FB�=�+>o���;Ȁ�01�ʍ�s�>�>��=\�R>����1G��f�������|�	��8p���>-��=$�x�������n����>2Q����)�Q�U=���	=�PK=��>6,�=Qký{]�=���<��D���D�oTϽ8q;�A�+���>nO��I�>��H9��J��I��>��f<[��X�=b0�<���>���v�ʾ�:k��.>G=�=���=�AQ>����>r�#>��8=��x>��@�$��<�̥>wJ�>K�D=�j�=u�����#>�KP>V�k?ͽ��>f�ʽz��<L��>�3a=�;l��,>��ї>�<��=��>l٧�%���w����{�&}�>��'>��%>q�+�8�>Y�2��:=�?������j ��ʹd�!�۾��i=CF��p:�_jU��q�(d>RV�,B���վ�q�� 4��*>��<0%��<Y��B��=�F8=m�=>B��>{�=t�=���>Ӥ��;��d���<�>��<�Z0>Jiξ��d��=�c>]��>�I�8�H�$U>�(�>I>��\Y�=�ȣ��E�>%�־�T�=�b[��対�~��+�;�A��D�=Y���Y���j�i�u=0V�=9��~�ؽKxм�
=V �R�;yA>��½��Ƚ�Ҍ� ��>����	D �#=D�[>�����N>��^<$n>�
�>�[�=S�b>_d)���)�B�;U��>�e<�~v>���+�<g#�2RW�8����p>za�=���>��۽Uax���Ǿbt���c�=.��:��ɨ�j�t���=�99��ϩ��� >�-��$sý7 !?��>J+>�jO>��ӻS�~��}=i����%�xT�=�t;��A�>�E>s�> �F�w8���_,=�`�>
ɽ���F� �㥺���$<�P>S/��3�g<��a>о@�����m��&
>P����	����1=��j<�/��<�=�8��א�=�;@��|Y����>iоG1�������N4>F����!�=�<�ݾ7'�>�?���N=֓������ ����>�i�>{B>|=�`���8D�s�s>ng�<�k	�aѡ>W�>Ræ���d>Z�.�(���#"��"���i<d>?^�<o�=+Ǡ>A�q=��c=إ��t>K<��[���ĝ��6=��W�.[�9t���=P,
;&�<�>�T���s�+����ƽ[)��2�>Ξ>>�7���t(>E<�7R���==�&�t|پ�1@<��=Oq���!��#Y�>�=>���==0=�#t�X��=�|f>�W>.��>�m�>o�>Y̾��L>:$�>�o+<�\=ڦ<�&g��6�>�«>R�žT���#q�&Z>V��o|8>lҰ=BdI>T]>;�>����k���0�������������>T�
�4x=�N�>��<q���۾�T>7�ݾ�h�͉�����x�޾���=��U=��=�	Խ��ؾ�'�>��>�j�>	�k�9߼~[�>|S2>�Y�xK<.��=�� =4��<¾F�H>�"�=8[>=�����E�����"������c�>(7;+z��Bpm>Np��k*<��
�<e�>��M��g<�=C �'��=��>}�>��=�0e>c�Q���ľa�g=.�j�=�S�Z(�=�69�5��<�@�>��->y��z:�>y��qKn�2��<ΉQ>d��>�{�=�� ?��>�t=Qg3��m�>o�>�&?��>��q>���P��=�&o�G���2V��f<�d�R>+�>OS~>"qX�N�m�ǰ��8���7>�?S���#���1>���˫���/�&}\=��<e�?�bs=h�Y?���;>w��=����V]>�þ)���P�1>�D�:>��E��Ņ߾p�Ž������>�>�>ϻ�IK���	�>�46���=`g�<v_,?��vk�>}��r�;Q�)?�`^=2�I>tP�����>�q�=\Jj<㓾�(��XO��!ɝ�2���?����?�,����<�Q@?�􈾪N)�?^�>of�>�S�X��6ٽ������>;Ա=�r�l����=�W>#�0>������(�T�D��=O��<jl���s�O>~���}���>tb#�O��S�h��ކ��O�=��>b�>^+�>��g=E��=��=��F=bb�����,�=:X7>�T�=ؠ	��A>�$�=�*�����G�{����>u@C>���!�=N=�|l>��> d����,>w�=�	���H���u�qP¾>��EN)?���<�����>�w��p0=σq��a�>����$K=��k�ur>Ǩ	?��?���%�"�u����>j >�Q������@r=䕾Ц�>��i=��n�bP��:�������">`�>C��<΂�G?&>�ݾD8;��+�>��">��<E	���펾���=��=�v���h=�O5>�r/>'#��!]���>�)�?p#?����ʕ�>`���=��0�R?? �$��m�<R�>9�>�P���/���>�u��L�?S��=V6�>�=Oֶ���U>�X>��2>�>/.=��.�}��=}���Ӿ��	�7>L>�Vo>��>x#
>�.h>.o�=��6�D��=G�U�Uٟ=��%����u7G�����kH��vU=X��>L��>�l>��>������>�.k��=Z�\�ҽ��{�m�D�o�+>UD�>r%����>���= (p=�8?���>u�=oj�>��b��ؽ�:�>77�7�*�u��>"t�=m��=}K�G��>l��T���s.T=�)>��=a�����,o��#n�[�$>H���ѽ�{i���5>!Y!>��?Ҍ>�\&�@��6�S=9l>�Տ�2��2��>muq�.f�<t��>����
5>�-?�N>ը8�����r�=#�>�%ͽ(�=P��#[$=3ؾ��>�?6�bh��0�Z<�4>���=��ٽI�T>��0��b����>�P�=��¾>j>{�>�� �bE>g�>�~�>��󽁈�>`�>,(ʾ�G=�>���"?�k�>Ɨ_>�����K�[���k>�[�ǬJ�H      %���a�>�Q�?<�=\�x?(6�нy>�����?	j?��?Dzl��U��"���4W�N3?�F>�>D��9۷?ي?ǰͿo��>�
�=� ?H�?P�����=���=:n徬?P^��Zٕ�U=>a���.?�R*��-��W@-Nm=����������=5 9�ޯ���銿��5?p�)��(?�y��e�>!�O�b����?i�C�e9�Xvҽ�G���)���0�E����z�P?	��>>d�?l�׾�'C>Qv�?h���Z����W?S���;?q�$��g�ݥ�?���?=�t��.�=Y����4�D��:d�?���?� �� �@�N���:!@��=B�|?�N�/@�+J?[�U�O?+�k�$f=�����s�Ӻ�
�l>7r\�r�c�	Bֽ�oN>�ؘ���n?��	���?9N/�A,�?	_?�1*?bQ�2r�?�߿Du�>8�r��"?��?\����F���>N�"?���?JB��1�{1�?�$�?q�'��A�P=D�?2��?B�?�
B>$_����?���?n����͒���[?A��弿悿��?D��R�??������A��t}�� u?ܚ>��)�jw�>@����d?\�,�����>H���ѡ��e�<��ǙS=14��L���f��>��>`�>k��i�?*�?`O�?�BS?)oS�Y�?S�!�����i�)>���M�^<�E$?t�O?{
�Y�-��T�?�ά?��	�����!�?�I�?��7��Z��U�4�n�?�l�� m�>��?w¼��#��:=�Y�?mk=�̿\�?��x?n�1>�ᕿ��s�
x�?�F���x�V5I?]'��H��??c���ŵ?_?×�������'l?!?����2?�:?�T?�Rp�BJ�=-8@w}�>a@�aM?s?r?���?�	�>���=�վ\4��r�@�������?-	?W�?�:D?�>.�?3�k?Z�>�8[>/��?�j�?�����0@��>���?j���޶��A�>c:���p���<U��y��J%�o���BϾ���?�&ҾR�7?�a?�����Ӿ��*@�ov?���>0���>�����-?�� ��p���῰A�?J[@��>&@+�[�Q����?"��L��Ŵ>�􎽿�?N��??2��$�^k�>�:����>�$��AI˾��9�SH|�#���L���>��v�?h�>��Y>Qv=���4��Ͻ?Z�3?�S�?��?�x�>ä�?��m?&PJ�F�ÿ�_�>5.����E�|���
[�?�駿�)u���?�
��!���i�	�G ���5Ҵ�tB�>/|��Ѩ�" ҿ�o1?�r�>���?���>�{�?�a-;�:6?�*��Rz�?�M��+���ؿ�r��6:��5�@�����PW���?�R�>��@D�P>g6?��?b� @%ݖ���
?�!T>�}�i�ξ�8��z7��*����羏F�=����=��?W�	@�G�����>p&�?��Сu?P+�?.ɿ��t�t��?�VĿ$er���?�r >:���dtN?�Q��5پk��>?��>&n?�\��뜿��>+�>�b�ڨf?ލ�>~�$���"?��a��3?J��??Sh>�-���L�N�P�$�?"-�*�?�۳? �Z��ɣ<��?�:5?|C}�P??�D���)�)�ľ�X�GI>� ��q)@� M���>@��>�>}���Ê>΃?�����?hz�?��?qU�?��?d�4�l#�?���?ş+�,	�>#��h�J��N���VC�Wͽ��>S�ξ�#�?~Ҩ�+�0?�Į>@<>ю?���=�F�:�����Ͼ@���4~������y>ӭ?I�
�2�>���L4?�7����?)u??�/X>��l��t>��D�?q�i�-�B�0&�>�pm��6�>"OF? D"���{>�>�?��^=�0���.�=<�w������g?��7�ݩ�?U�f?��?�<��A믾=�p>N@�3>oa ?���y7�?<(?R@�?��>5����?E�����>X@��!�?��11=j\}=5��j�?���>�.)��f��80�> �?�޿	�%�h@�?;|N?�k�}�x㰿Sֶ�E۾I� ��Y?��F?G���.�=�K�>0a?ݼu?#I�?�1��G`�>1��yM��[�?*�>7Zݽ1F��ޏ?��>ԲK?,�������²=d�?QN���%�?�;��ٵ�>Gc��X��ᒊ?�v��uK�?y�b?�i¿�@~��? �ω��>���:x?
�U��+�?��q?L���c�ɴ�>���>��N?����׼~�+�U��R�)�h7��Cl���q?��?L||���{�m��=��?e=?/,;�����W�>AUh>�n>� ?�Y$�?���>��?i��>p��P? s�?Q�B>B��?�U>'�Z?��@?��\n�>i�>��?��>���>\��=*k�>R�>�G��yʾ�b׿�V?Ш��j�h?h���!L?��+��N���,G<_'G��/��@E=i�4��>���?�U?`N�>Nu������Mֿ�����?�=?�7?;T�]��,�>����Јٿ����"�9E��hz?8�g>N('?!t?������^?��q?z"^�{P��k�x�ž����9-��!�>���$z'>�X2��f@*�?ݻ*�?�?��*<&/?�B=�/%��M`��#?�ꊾ��k?׶?�_�����?&<־��>�ɾ�H @�?E��?!?q�;���w?d�H?�-
?8'��-֩�'C�=q��?m;?Z!�>m_5�o���{��v���v�>w���;��>�]���?%
��Ӑ?��2?�;��=��Lg�#���Vo۾��(�W��oP]���=�Ƙ?+p�� -�>+ξ��'��"���g��h��p>ta�?K��?�D@>���<u��S?�ā?��Rf1�}�@�d�>l-?�x�=Ϧ�>2tF>h<z����?Z��[p���+&�+� @�.���L@�D��@�߿����r���>��=?(G?�A��? ?�f�?ΰ��u*�W�/@��k>�	ӿ7ޚ>�l3>��/?S�Q?q�?�ҽ?7DY?L>��J��&?h�g��l?A%����?�i�?�r�>H�俼�|?4�}��O�=<R�?�۫?��6�֖�>�O��{u?��ǿ�>?�b3>�у?�JX?{*�ʃ?'L���6�?fj_���M>8B�诿�֌?�M��+)��V\-@�@��_?r�T?�mi?GA��n־cɥ�<       ;�=����K��>	4�=�fR>�:��U +>�7��u���`*��u�=��>��>q�>��?X�ǻ\%>BB�>b�?����TF>�>�>	Q>l->�J*�����uֽ�1�� �<g����gf����>�C>u�L��'O>�C����=B�ʽy_�����;D�[\�������"�ɐ>�G�=��e>���=��$>�u�����R���А<ԋ�=�>�H�=J->_f?J�W>ͧ/?<       ����8e�C���dn�>~�=@�o>��Z>����v���g�=��^���s>!�>��>���>(vA�����2�W>�И�%�VlJ�i�;�&��k>;����X=���-�7=�����l	�:c6�Hg$=~�=>1$�9`��b���
=�Qu�����ߍ
�:��=�@�g7"�n&�=s�>��d>jѼ>zv�����c�5�Ҁ�>��=��1=S	`>�rL�0�<�o1�>q��>"��>�	6?�      v>	7z�W�<֋>&���X?��?��@>DhC?Z_=��X��x��>�C>C%Ӿ	�a>�а>��:�>{`Ͼѩ ��;=���<�w���=?�R��j�8=��G>\�0�e"?��>��>�a�����Լ�����_>'ݠ<_M=�)A>�jپ�����;G���9�.�?��2?���^��>�~>��!�>O�����>�!��݉����>�¾��սx���;��?B1$�Ճ�B.�=�|�=x�>5�>�� ?T�>�@>A�y=��/�Z������=�K��h�j>]�5>c^)?�U-?�i7��$e?"ñ��VR��p�>룆?�\��;!�>>��=w����#d?kc�>��ڽU��t
���q�}D���y�>V���#'>�G>�L1?)�.~^?CTi?/�>V��Y��Ὧ>ѓ���;?��X�9����H�ʌT�9'Q>Ļ�>qI�>��j�<��>?O�?��ʽf��ج�>B=e���>���{վݰ�>4��>X��>޳�=�z&?���^�� �D?��C�o"���^?�$5?Q��5+?�$�>~+�<��E�1��=vS/�R=[eY����> W?�����==2D?��?5�T<���=�h��Ҿ']t�T3?�]G����"n�9�u?�k?&�~?"�%?�7?gCq��*����8>:��dK>���>;����w=��+���Y?�Uw�:�W>��M>��5�;O��&��׭��<:��?ȏ�>��=�w?��6�����>-�e?-�?�G�G�>�Fy>)et?_>4褾{�����q��� ?�8t�X�վ�*�cM?[28� ��> ��q���D�?�r��l*�����;n(S��z6�Zھ���G��=K�޾�1>��=����wtB�'T?.��!�=��@���K>$m�39�0��=~�6���*?��*?8�O�@��aK��cV�/���*�?�h�=3�>/<�;"n�=`� ��Z�<	Bܽ�o�<8�=	%���`S�Wnp>z3�>j�!���=9ه��6?����I��ۘ��t�2��=W��%�>�r=���>@&�,�P=�.>
�	�1]�=��$�����[o>��d>Q��O��P�ھx���1ý�L�>�3C��j-�?��s>̟�ro�8>5���|�>���>�Nb��J�>��>�V��;�>��n>Y�>��?b�s��>&i�����P�<�cN�A\t���k�U?Լ�����>p �:�/>j�>/S��M?����+������抵��Ą=�_��^־-�¾"9a>Ĺ�W��1�׽j���*�>A[l>�@>��p���=ɿɾ�]c=y�t>4��=<WF���>*(����=�����&?x��=��\�Np�0s��CDR�I,ݽUH�=X����z ?���>�!
����0mվ����~��?��s���z�>a;d�]�>�?V̈́>�*�<%څ=�$�>L����A=ě��5؉�HCC��7{>q+���ؽ��T�=G���@O�>�7μ���NF>�	�d�*>�r�>�>T�+?Л�<!n>0k߾��=�T�&>�嬾��!?�5>�bm���>;��<#�p;�zվ��C?�D\?������?��>�M>��>y�?�@:?�F?�"���׾&Ȅ�01�֎ >��@�q�>�"�>��=?E>1�?H1�>72�=<�$��]�>T	���>�T澩�a��F?sV�3�?�?��ХL?�I�=~�>aƿ>�Ҿ�`>2NC��O����A� �\�z�]�羪%�>'���U��n���w>&-�%i� �㾔2y=��?�5��&3>�*�>�T����r��3ؽ��='g���B�=(���?��c[f=>�>�A�gH>��<>��>o���(�%> W��fX>�>'D�#����܈��W��I>l��>Ҏ*>l�>�>3��^����%=HB$�LӾ>��?���>i7�|�>�s��s]=]?�]�>O�>�/��v�>ʠ�>嵩=�b2�ݖ���!�r����V���+�2?Cd��5����^]�Wt>�x�o�����>|�������_�w^��l�B?���?x�\?���>���p��>��#��e�=�<?j�>��O<��?F~?+�U���Z=�=�V��>��L���=�E�;����a=^���>j�>�NR=��>�h�1>�>!B�=	K��*�>��.��&���?B�O>(�_>���>��>���)f�2�b�_>r��)Ƹ���ɾ�P>��.>��o>B+T>��g���$>��A�
�B�_�=�>l���kwT>d@�>/����{p>��=����"��S��]Ծ8�`�������
>Y�!?�[&?�"���ZK����>����ˌ�m�]��'徱�&��\5>���X��n쾇��>��7>�s�+��=/tK>yZa�k�e���-=Us�>�.?�$?�\ ���0?�����`������1�>�o?�'ܻO�c�\̫?IW��C¾�zܽX5O��F�>�K�>&m��#Ҿ�V��F��
? ^�>��>�jY>%��=2����ì���>)��=�a��	��>n�����<z�=�Ծ�v�����*���/{����ҾZ�(?�h-?���2�"b�>��x�<;?=?��K><�>Get��;�>�{��@=U� �k�=6 k>��#��(?k��>�rľ%���XO�M�\?3�B>��L��D�>_kH>?R�>J�3��H-?����7<����v�+EӾ	��=�������>�Dh>��>���> ����Ͼ �?>�X���������4�����>00F�d-y�(V�K�/��E?^�?�[�>|�"=��ƾD	�=���>�@ڽ��羞Z>���>����lx�>>IĽ�hP���ͽ�Ǒ>J���O-�>"�ǽvw�>�F�>�=7?�^����>�繽�����>6#K=�I��Y�}?��W.��Vj�$b>+�K?���
iw��d]��GD�u�(?�+�rE���?>�9L?��;	�b=�!'>����.����|I5> DM��З����>"'��
wC>�0��P��>���>�5���y�>�e[��=����J/?үʾ5�$�ʓ+�ѕ���_��Ġ>'�I�����y��?SL��d>�]�K>Č���K?�����F���@��B���)���>�W�>]��=є�9  ?ո?�ê�\��>m0K�E������H�>�Jx9��>�?=:��)���V�{����?��?cC�>|�W��(?/��<����>"��⾜-��b����b젾nE?��?�XI>���>D�>=���`���Q\>f�ؾ��j�u_>{Wؾ��c>g�=�r;)��>��=�e=$�\��Fk=-8,�l?���������h�>�ۮ�ј@?���<�a�='S8?��?�ѽ���>*↾�kK��f�[�?���,�e�0�w���<��E&>i��<e�">�YǾ��>Mn,?��R?����qT���]?gG��[�`=ټ���y�z���t?8m�cԔ�kf�>�l�?`	      B��z	�=҆T�('
�t�پ�4��l�>�vc<��L��_t>��1>�h���%�=�;h>-��=�/�=2��d�������u>�>Ȑ'>(0���������=�پ=��J����=DcH���c=uH���U<C!}��j��-ʨ��	�>/4�=jݹ���(��-h�ǯ��{�w=��k���>�fK>g��=�k�>g�@=��I���tlx>��=�K�%J���7K�Ϫʾ�-�y3�>��6>_�B�x�鼥��=�=���z>s$�=J�+��ޣ��׬>�H�>Ғ�='!��t�^D>d��=*އ��%������=�(�="��>)�7ɵ>F����E	>9����չ�d�h>�D"�C�i>��~���l�I��<�<��~��"�=��=ߓ���K�>&+|<��>�F��ʝ��b�">k"= s�=a睾�}ҽÅ��wU����=�`
�dD�Gj=�SϽ�m�=�T'>��<�f<޴��X��=H �>����ܾ��!��ܽl�>�l��޺>YK�>���>�!|�֏1>̦0>Ȧ�⿏<�q���2��,þ�	=飃���R�>�g���G�Au��Ѻ>�խ>F�>A����=:���.��f�#=��j��� >v:�='�>���:bz>$瓾"����<�9r�=�6>��_=������H�v�<>�w��hl��<��=h�/���Ⱥ�k4Z>�ٍ��2Q>��:>^̟>� >��۾t�`�->�e�=����� >�D�=�b�
�c:�ʽ3&>~�x>�@�=�҆� ���*��2,��>Ӌ�>�]�>��=Ul�>dv���[�=�������>�Bݾ�"��Nھ��>¡���I�>w5�=��I>��:�6ԩ>N��=��a���н�>�	��'����\>�Ó>uZ�������=�̚ ��dI>�д�����{�>���[>��9>cG>Z������;Ċ�� �����>��>B�
�O/E��A�=90'�Q�>��R=�|q>Ξ�>~m�>�٥>��l!K�����>˛�>C(�>��p>�n~=�ԥ>��<�l����!>� p���>�dҽ��;ǯ�t%���R��`>�߽��k>���<�7>�n��W״=��)<����i�*>7.>p�=��'>H��>vA���-����>Ld��^�������w>��
;�8	�����ZȪ>���>�����>�ّ�����m>,���=�]k�ґ��!�>B� >7�=V6ڽ��L=��>/�l>������=�Ϧ>8�F>h
��"��8N��O�3�?�#5>���>��+>�s>�����Z�a��?b>e�
�rVq��k���>�����>>��q>��Q>���[�����P=��-p�=�����|>���j��=章�לE>䊃���:C�Z�~�=eL�@�*>���1��=J��>S=Խn'�>�m���>�n=�NR>P4> ޑ>ɔ�>xQ�:�;>�`>�cy�{Ὤ=L>:�/>?>�S���Fܽ^��>K<1=*�ʽx�<�3
���=���I$���G�K��=Ҳ>΅������~� >�5>���>5O>>GD?
� ��c$>��Q���>�0�>0���Ҏ�Kҡ�8lp�gh��"F�=�OU>�[=�5*����#=5t/����=4[�=0k�IW�>l=�<�7�y��>�2�1�4>��:��Ͱ���=�P����=�s>B�->�.�>�C;>
����U�>Z�(�n� ?��>���="���_����=\��J�>wce��R��Eo��#�>���=����G�<���>�Nk��E�<�<>�Y�6�����=��R����>�Q����>sK'���ǻ�Z+����;��ͽ���㽞�p�.��>�a��T��4�V>�u�<u&o�Xi����=+���Y�>��<*�F���>ݜ?�H�'�<�8L���>�1��)��>�*[=LW��$ >)�?���d����<r*[��,�+�ý�������>���Ϋs=�����=�b�=ڱ�=�4m>��潋Ҝ�j�>!=O>�=�R>���̑o>�i�>���4m�.]��ǚ>��u>q}{>\>��y�S���f>x(	>��u><����H>Ŋ׾��?L�H�Y���s����C>r f�O��=����w	�2��=�|�>>����A�=&��u���t����e>s��;�cL� J�>����Ry=�Z�z5�>(X ?=��>��Ծڱ�= ��<	B>��>��d�Td�=���;nc}�FC۾�dO��a�=�G1>t������EJ�=3:�D��pV�=S0��	&|>�/���>�ns>p�n>���"^���V���6��>\�=I�"��缮d�=�Ԑ=H ~=�qU>��>�$���&�Sށ>S��>����X�l*k���>�̾�s��5�>h!�Qѳ��ɂ��z�>$ML=��m�ӽT"?}�l�u�<bf�<l굽(��>2]�=�[>����|�}쒾���>$�8>�`>Ɨ>>�j�L�����i>J㗽��>�|>L>�����U=�Y�>P�T=J���������>8*�=s��e���a���۾���=�#��{�>���0ք�y>�v��������K����Ճ������ES)>�<�ɾ�� ��m�?���>K�=>�E�1����v�|A��*٫=���K�>A���켽�+�>#���%t�@w><�i�r���Ԩ��o���ӧ�=��H=�½_��=��=&�R=:����.�3�~�������쿨=?9$�H�X�c,>�Ƚ̒������#�����>�K>/��=�:�XO���������>��>�d����z>�Qм�	-=!87>%�>��r>�~(�w��>�S�=��	>H}˽)���EzȾ�2h��Si����bBʼ�R=e�o�R<	?����na>eN?�����>���=��4��J,�(��1�վ�*罰H�>��O�%��=g3����=�>\��= ��!Ǿ�'��m�����U>-�O=�Q���v���𾭀��\�����<dz�_�=���	�5_=�2:�fX�=q��=ɽ{�e>>BZ��O��>�>���>ڈ>{>>PB7>�w�=�:=�q��qȋ�F�����z$�<_�s=�U�>�=#>2G�;�R�=���>�X��?�<L>�!����j>�V$�*�F?��>l��7h�<�V>�^��D¾Ӱ��m J�o����)��Ӣc�]7�>���7�־���A9Ͼ�˿��k����o>.���>��=�c8=M��>�%l>va�>�lc<-4h�:�н0�/?a�l>�ލ�|[�>���=h%>�C�>m%X=/H�=����p^>��!>
�]<�"/>j�o>��i�y[M>f��=��k�9�����;��=>]c�=E�]�"��<�r>K�����,>�D>�%�!=c ʼ^\v��!�<�c�<zqg�
��>��=�sX�L������ڨ=�e?�#������>\�=׫(�|��=G���v����F?��
����=67��˼l������<e�0����ɪ��(�=R�>P�+�\>��D���?��->��+�1#>��᾵��=�����_>Bz�<��G>�^B�0�
>��I��݁���\=�3f����=U��=[�Ľ�������@-���D�� �t�.X��t<	�>����s[<c�>�*=�Tӽ�e>�.P=dގ��)�=��L�f��������P:��z��p�=�IV��H����2�н�A=h��>�xL���>xuN�S����=��޾�
�>C7��.>�g>���>& �����=�H2�g�<��>D�c<�]ͽ�� >�E��"zW>4�%���W�4�Ծ�0�ϧԾ����[�u����>@���y��>>��=��pDf�ԅ����k>�,���x�>��=�"���72>w�g���/���m�����،>39�	r�=<Rq�\����C.X�/�<E4��s���rʾ��G>�t/>U�s�1">�콞���E�>2	>�Z>��>��Ҿb��"���">�V���1۾|����5�=����)�a��?ʥ����>�Z�=�θ=�1V�g�8�r�]�h�΂��77�>���@p=rZ>:̽����k?9	����=�z�>����>zį=�ʌ��u��IpܽB��>2U>�P=�\־��r�
9y>��̌�rj)>G���ъ̽�ǟ>�s>>��2�+s>R��>źf>�䓾��>�}�>Ⱥ彷�>Ä�><ݾ��]>Y�>�|��J>&_��J��r/>��_�(�3����<�W�>�/�
��W:>��>�ɜ����.�j`��>=�>y��>�)j�&<�7�L�>�U���5>��]>��9�RB>��;v`F���-��x���V-���>�o>��̾�\�<U*u>e<m>��Z�*�=a����)=�)x<Љ������= �ɼ��9����>����I�f=d��=��>{v=�V����n�yP�=��>47���c>aH!���>�84��M�=?�`��2�=��2$���1�=$H������G�=��}=5-�>)�����<��k�=�fL>.���6�y�}��!�[�@���K�нJ���3G>SJ�>+E�>ܕھEv�>k9	?a>�!���>*
��跘��)�<����{*a>�(��Ǿ�6�<t�>u��=	�Y>�懽���=hXD>��>���=�F�� ��=>�>8���勽��B= b�>�	�<�e(=�q=�CR��$�ݔB>��I��ih	>Ԩ��&S>
Z]���;>�N<�=�pb>?�I�NP>�����=��:�;���\���y��i���94���x</둽��HV��N>W�l>i+v��L^=���>ǿd���4��m>Z����=�<�<�f��w�;[!�=^�>��>�ί�S����ٰ�8�˞@�H6���ח<�{ �Tٚ���>�Ԭ>��7�Y��dG��4^p���<{F�#�>�v=�y<���=#�>h��="��>� V>�"��_�>�OǽG�=ל�>�I��>���<@��=��'��5ؼ0�L�GĽp�ƾd�=�b���(㻭H��2�V>|��=�k�II>��ȼ������=z^d����=''��2�K=��˾��>�򴽏8'��}�G ���A����>���>�3>�h?!M;�1>�zx�u���->Z>cS��^c��I��Iy==3���7��l��>y�E>Ջ>���D�>�_�=��>?�>�qj=�>��r>�`�<�N+>f�V=�C�=���=[*0�S�b�z)�[�Y���>h۞����M6�<n�|=��=x�>�R>�6�=5�e���;�2�����=�B��[�w:h��yֽ��?�"�>gז�J�=������.>�ʽ��G>�0�>#އ�2�����=�'�<���_Ѿ�ޞ�=,�B�At��42�I�>�#>���{�>dm>,�.>�f>V6M���G>A�@>9�v�<�d��V_d��#�>�K>ztJ<uY=��2�[����L>Z��lg�/c?>�uZ>�F>�S>�;>H�J��qN>Hf����>��q>�>�r�����=�>FyK����=�U�>�R�>�n��>ڈ>JN�>���=���=�W��%�;>5U����S�/�{4�>`f�<�Kl�c#J>��@�\K��Џ<�]�<����b�GWw��p>rPV>�*�����m��
&>0��>�]�a�>�:��"�>K����op�w%���wq<S2��@{��v���;=��i���S>4��������D�=QI�<�.>Շ�>��)>�o�>IHc>��=c�X�(�=��O���
�m�C��y=>�>�=J�<��>�>�;����+=�a��O"����1|5=�G�>.��>!�>���zNK>�ȃ=0ﺼ#��t�=2O���ˢ���e���]�����_�ν�Գ�i>GZ�<+�2="�>���>�Y�=c]�<v���3�>^��t@S��g��n�N=�=��X>:�R�Lcm�����K\<�������=9��>�>� m��\������ƀ��6��J[D������Խ�۽�Y�=`����i�=B�>B��\>�
�>t����=�옾G2�>��<J���쨼�󦾨­>_>�<m�?L�KT�=a&��B��������N���=�L<��L���_=�	�=�F��a�)=[-�>c"k������=�y�qMܽ"��>�@����/�"��><U�>j\3>ɣ��Hn���*
�e�����{�=�u<$�
��*u� t>������۽�,�<o�g����>|�W>e�k�>Q������,��D�?�=>���=�ͻ=  j> �J���}�{��}/�=�X.�H�~����>�݈��?�>o�#�l�\=���;u ����=��l������=#m0>��=��l����>݃���p�5z�=��*�J�.���l� -�=->3(o���=T֩��=�fؽ������
�P��|�>��þ>��.�=.J����q����R��̜�=O���ԼZT��@��<��=�9�	"�>%(��۽R�>m;��޷��L���>rE
�V���(/>��3�%�>C*��v,�=IȈ;��B���?>Q�=�1!��7��ds潬{e���/>=��>�0H��j��G��蓧���
�v�d>��\>Ƹ�=\Bl��ߌ;�1�i��7��>2�S�^J�+\���B<��4>�&�؄���&��`R���Q>��L>K7.=ԝ��%t�=�_>n�Ƚ�*�=���;�R����v�����$���=��W=c\�7MO>�?O>���<Q��x[�>І�����g��~ �=/v7�2����x�|���<��>�ݫ<?4վ��?2��>�C+>��3>���>T���k>�Ӧ>7MѼ̭��f�>���g
w�����n=w�ս�־C<ѻ#>���=ng7���k�0͏������!>�h>*o�=��=l�����>5 ���ս>��>Q�=>���|�=���ˑ�h>�$��M�>SV�<�j >�	��!�(��=�AW�.�>�ļ=xW>o���B�׽�l�=y����n�:se�޳׼u��ь=���I�=ƽ�;2)z>��=r¹����=ީ<hcF>����վ]L:�A �>]ȏ>�&0����>�ğ��%9>:z>>A��J�?�p��R�	>�Ⱦb߸�G�p��<��V��� �=���=���>L�>o�I����䔽	:�<�5�����K�_>z���V�?5n>*�������=2��>��M>O��=����"��m=��>�m�>��=��/���پ�G>]&�ڬ��N���V�=� d>�Tq��:���k��H>�t��'�7�]�)>�M�<�ޗ>pj���p�;�i�>��ҽ��?>~�S��|��6�`�_՚�����>��>d��=Á��\#�:9H�����=���*��yb�>�?�+߽�y>`>�>)�Z��e>�Ҹ�'�>_i=д�<���"���{�=�;���݋��=R���=&p>��=�[Ž�H�=h�<c�=���=pV�Y{���W>O>?tNݻ�#�<����³�>:7�>5��SV�<�(�6I>���=o񽻀�>/����� >|-H����>���>�N�>�f>cI1�ځ�<j�R=��]��HϾg�b>QL������|�<��>��=��.�[�>�>f������=:@9>���i��?�Ż���=�3�W�+�Y~��;�>�9�=�����>�$.�� �/��>��)>+�g���>-��=�˾�ㆾ�>R��>즞�Kr�<�>՛=�㮾�2`�Ik�=�⡼����/�;��=_��>����A�~�>҅�=�A�>��U�
X�����Uߌ=�����q�$>�E�>�."��Ϭ<�z���<�𜽗&{��m���>�>��??F�=�� >�^Ѿ�������>B�=:#��W_>q���2f=^'�=T�=,���(�=+'���z���;����>�>て�޹���!���A��/=B��`p�@V^>�L>�U[�(�=Uω=�v�����>�M����>�,���R���*�c'>���9������)?�.H�<�������>�H�=�?��^�?ݬ=u6>G�M�~��ɭ>PP/>���=ă;^�!�!~�>�:��zI��>Mʽq�;>Z�>r�2�~̅���J$�w^��6�]<�˺=pť>W��1�w�f�.d=\�����l=� ��ki=�,}��;[=��
:��o>U�*>S�E���>�7Y=]�N��~>|o=P�(>�Ǔ��h�����>�)��MN�=�*��ྂ=�G����<��<�|$�Ր����I�[⇾�6>vS�>Ѐ->�E`<��3>��;8�ھ���=�<۽�����e�`ڝ��(�=ط>�����
>, �[J�=��`��`�<�����Z�ok�:-�_>����������>��%?��<� ���!�6���	���lp=	���JZ���=�h�=m�=�9/=���<�₽D�>���=��F���>���>�$�OH>�������=�ᢾ��5��m�=.W�k~� ��<AvK>���=�>���>�Ƣ�CL1=s�:���>+<>aو��>�>� ��%-�~8?�8R=^�ݽJ�!��/k>���>m>��ƽ�oo�����$<�$~�+8>&4L��~D>�M>���>a�`��퓾5�?��ڽ�7��Eؑ��|�>@>�"*?m�k<�>S�-�B;>{��>;q=J�P���)=�r��x�>�����<��H>8�)��bU���7�R�����>׊�>;���"��%���-5	>Q�û�)�=��a=G�>=�u<g��L�>�">r�����;��)�=ʪ�=+�w��p�y���� �Z�/���������������=�F���^��`�=�{������<`��>��4��=�>�qM��9k��*B<�p>$Jڽc#�=�ę�����P=s�]5,>	�R>kt���-ͽ��=;KμŜ�<��c���彛��=��>�'l��-��*�=Pa�>�N�>��=��ͽ���=��)oZ=�'Ƚc��;��D������c�=�Ӈ���h>%��>�~���m<`=v��ʶ��s��\��=|�~>��5�?�=�s>�� �fe��$/��s���?�=�[1�O�=}�>��T>�����
F�{)ھkN�>v���%|��]����(>'y�@I��k>l�?ee?2W�=��#�H���Yo<0��������=s�1>u���d�j�@>}Ǒ��R�>ѵ3�8�?�P*>�k,>�+����˥x<�����>���<�qy�Dh�>}�N������0=ȕ2>���=z�~>ҝ>B =H.����$>	@d>:�c�}��H�1�D�ƾ���>�ώ=�־��>��
=�{f�)�����=Q@��hB����~>���,:>��m>�Y�>oo��D�(>\��=Ļ�>�C��J>��¾<       ����/�=���`b>�(�h�����	>���>�M>��<��>\�>"��ƶ����R>��/>|���	7½IW=7tL=k��� ����۾e2���>F�ھ�ꉼ���>�1�C;<MR;>�q�>�4m=-#�4o>�y����>>o:�>ti)=i��=��=mE>hk�['&>�;�=���4�!>� �:�eE>�UG����>�Z>~uo>�w�>��f��蔽^�>I�T<(�C���`�<       ��(>�?�vj>%�=c����1>�Y=�R�>�.�}P9>n��=��>9>3䫾�K��1�>�T>�z�>ZD��e^=9����?=���v�{<������ˡC�%'	>��T>�e>��V>X�'���,��I�:�X>��ӼE�=K�R>Q\�=��=>�Է��x>�������<p(��;=>7S�>���=+��=��>"~�>�U�>�ǖ>���;�r�>�R�>���>]>��e>�      _���)�>~�8?�S?{"�>%B��#�>U�=i�N�?�5?���RܼG=�s2,?��r�/�?<Ⱦ�=>䟔�=F�*L?yN�>���>u���"�>Ο>��#� �R>`;?g�5>MKI>��o={�|�ۼ����>�L<��?9��>��>/�=��m��#�y?��Ͼ�X��7LY?������� �d,�=��~�lY?�;�;��>u{?�k�~�#=�81��O_>c�t��9h�v�C��^5>�c}>(�?��=6��=��`>]#�>5��>�.��4>M��y��=/����?� c�đH=�
��|�J>h��������>��+��7{?8 ž��wQq=�7o?�@�g?�!;�v�=�M�S\�>��=E,�=]x�<0�"��eS���нU������^����ɾ>_���:�o�������;"�L^>*j�>0}?��>3�E��j�>��l>�)>�5�=���>߆?�H�>��"����81� վ��kg�!ˈ?]I�Bc}>��=~�T=��)��+���uD>�,?ÿ<��d�>�
>��g�!�����u>}���r�=�i/�.>���j�*}2����%�7�8B�(,�	���ْ�
�M>�n�?��⽐�F?9�D?O��tꪽ��ǼC`)?��@���a�4�`@��8����=J:Ҿ�A�?c>�|�?fІ?��l�8#�N��>�M@?���1T`�\J�fp��ʪ����>l���)��>�t����1��؏=b䉿�J%�BT���]D>��9��up�VL��%0��܄�\�?qHb��"?�˻���U����>%�?Qk�=U�G?�|>?2�Q�E^��mQ��",�s����;>�Q9��>�$��6Ɂ?�(C���?#b�>P%�>���=�[>�׾MC�+3��c����(>�eZ�\z�>���>.x;��^���b>;ė���{��T&?�e�r�>�<V��T'��rB?��>�Kp>AԾ������:>@^�>�2��6Y�'p?6�ݾf�n�/��>ڒq>�O��^U?��	�)j�>�Y>s�^>����u�>�">�TB>�?�>0�_>�=�ȲQ>����#>/c<��24���?���Nܩ>�6�$����P?e�T?,�?�]->�;;>��K>���C��>M� ��$�=R>�M?��Ϳ��4�{���Z,?��ƾ����-پjx&��v��
���*�0>�>Q�/��?򃾑L־Q�?����������c��>�Z?���=`�ž��=��U���O��=���I��=�F��CW~?]w�=�0B�˨u�ԓ?R��������Ǳ>T�d?Tw>>��?[�㽍�о�ʜ<�yo�%6H��c?��?��U���S����#�!$����R>P"?�:?�?����Z<3�4�3��_`����=�3�>�r�=��~�A2&?߭=└��[�5��,,w�(����k!?b�?(���Q��V����a�(��{�>��b?�QI?߮��Z<�wY����>��^�7踾��_?�x>�J�}�z���t�߽WV�?:�h��b"?_� ?��"�w6~=2!Ⱦ�T��+A�!S���I?���;o�oա�L�žɤo>�����t�o\� �)<�`�=0`H?b��>����]��/t�>'f�>�쳽��N>e�==���;v����>���>��.>����s����x�B����=�F�F��>�L�PB>,OM�q?�iH?��>{���l�F��d3���=�bY���?� �<p��=f(�?�#>��<�L<�6�<'L=�殽�>�o���F����=Yu ?E��>A�v?��g�tk>���?�Z=�*O>X+1����D�K�>���>p�	?���J�=��H?�0?m}r�
�>�N�>w�>�S����?=Z��G/>7
���Z>V�꽊ظ>ciu��2ƾa�~��=�>ഗ>_�ʾ4f��:�>y#�>��eX�����Z� �@a?x1��F+=�b>��>��ž��^>377?|`+>��>��
?墹=�J>�,=M�#���7�A	d>K8��k�2L�>e�>>�򛽮,>B7?=�wT����>�	?���>b�v������ھT���֘�=�-��3[V��vu�6p�>�谾�>��>��S����n��y�>S&���?H5��������>���>�O�<&�>-�^?�����>�w��"2?�����T?1�	?q�|�yb?�9־������?l�[?r ?�pV��)?��?d)��A��=�<����q�4�pk���>
j�>�(�W�/?��ƾ�9�5a>ܢʾ�.?W�K�I���5@��̽�F���?>'�%> �>~�=�_?ė�>H?|�<���>������<j��>��r���=�g?7c��Z=[���$�%>�!?E&���f���*=0t���&?L�Rf���E�@8��V�?�gy?�8���NI�!�?�^<���Y��rx�Db���_?$ڍ�/:�=r��>gD>�8������٬��!�۴>�x>r?ʼ��>q3���ܾu�==���>�A�>��`��=.���n�?yW�>�v�?<Tf�<�B��S�>*�{��=$?% t<^��=��g��lD��\=� i?�")�ӸE?u~H=����۠%=���=%�x��0�?�_>��z>s;��g>'�$cR�� �A� <��P��S?1����y>��?X�Y>� ��"N�>�S-?�#��(?6<>�v{���޾4%j>�5�>g���|d�TB�<� b>D���z�u��;��B�<8���� �彠��>�+�>&G
?��>�/{?T%�>]X?L��k�ˊ�>���=��d��a�>F~�?�i>�O�� i�)^Z�\���C����`�m�=�+�-��>�G�>+�H�Q�;��K��
�>�A?�W�^ڹ���R>���W�,?�ܦ>�tn�)��T䆿ޕK�Y'?���w'}>(�L��8�<�*�+��>�%?K�	�[C.=�lݾ�?����}I?�r�>�4�?�L��=�>E�|���'�i:;���>�|�����>@��>�\k� �ɼ�>�������������=?���-�q�f7�����܇>�9��戾 މ? �U>Br�>�<w�f�#���?{�	?�u���)>���=���>D)���q�~B�>��?b넾��>&M���0��m�>�?����y���f��W?F�Q?�H�>�ˇ>�9��N�O�����>35�k)O�De̽-�B?HU:���{�<��p���MO�؝K�6K��?���
�?�վ��?����>�G�c��Ŕ�=�� >��e�"������>��J��>�S3�2�M?��2�>�?�Հ��X���½�o*?, ?�ޙ���L���Y�ś���l�;�{?���>�>�&��>�#7?i��>F�<���>�6?�t��=�^�$+��`��*
?�z��꾥QC>D��=H/�?��=&&��aX��\�>�1?�{��}T��_AY�r:t����>�˾Z�ּ!9?=@>ӫQ?g�6�Ş(�֑<����>ɜ�=`	      D�K��^$>��<��>�S�>9,&>���� �>T��>򐱽���=o�>36t���><���n>R^�>(+5>����a2*>ͻ��a��s�������c>��C>��>�K=<H>�������4��~>��]>Yc|>��>���=�;��D+��>��t>ǫ=Ƿ��3虼!�G���R>�����mO�>��>&7+����<w�ɽ��>�R>�rN��;b>��>������>��0�/�>�'����Qv�4É�;�V>xw�>��S>�!]��!=eA������F���9>��o=<۾z����g�d>E�\>C�~���>r9��} ����=�-�<R(�5->)�>Z��'��Jp���>���>�R�=T��j��>]�>pw=���>�D��l[�� 񧽙���m��DEB>���=�g�>_@��)�=�_t��fl>T�.>��@�bݣ�VsG>ڼs>Y5����=���N��=��f�[2�\�G��l��~���#�����?'�=�!>�J��8�X�> ��=�V=��%>��>>M�=	#=�"a=$�,��瞽���=��(>�jR��c�'`>�>o&� )>O>i�Z��<��?Ԍz��6�<`��`��>�D齀1>�`�=���>"����|�>�ӽ��7� ��<b-�� ��>�H�=��F���>��<_g'>�1+?Kȫ��vd>`�K>L��=)���3w�==�����D<�"=�ʅν�=�8K��u׼b�)>3�h�~�Ҽ=G�>n��>��B�v&�=�iݾ��B>�2m�)������>�������;>3o�=�M�<n~=<؆��!��E���>��.>W?�ȕ>��U>�t�=>6=�Y�3>��̽��J����E �>�﷾WA���qƾ���>3��>����D>��>/lҾ�=�>�3k>5���>M	�>��>i�>|2Ǿ=p�� ��7;��?=~#�gS��$�7��q->��|>������Aa>��:>q����ʾ�����IO>�79>L�v�h `=LM�<!����=��>Hy>��z�mj?iׂ>b%Q=�恾�EC>��Ch���:����=E~��V�@>)��<���>��>�n�>o�w>V� >���=�f����^>��I���?ߠ���ἴ��=�Ç=�վ ���~3*=��>
����]>��>/`�=�t����=>��I>�![��Җ�?�> ��=�^��xc��-8�>'p>T��;�%}�s��=��R��e�=��˽��=�1	�_�=�>.�5��=D��=�1�>�M����Ⱦ��콹q�=������G�/��>-��>�(+��7'�wn-���x�ϥr�Y�����=�~�<ꨞ>���>�Gy��1(>��?>|��=3p�cz?��%��*�I���:���9���=������t-����<_���Y=REe���=�ǽ�
d��U>=���>RԾSgd>~��>�>X�a��G��>� ֽ�z����>�d_����<�ӽ=N�=$��=���E�>�bx>��Ǿ�
?2~h>��L�.Q}�]0�=���>`R�=5#>r����B>0��s��

�&����54��}�.����q=L�>�ҧ�<�J�=Ȭ����Ѿ^:/�Á�>m�>A���Z�L>pB=��� N��<�>��=O]>n�=�Խ�n�=����f=F;*���=�%���g�<�چ�+��>��)>�u>j.�p-�=1�>�J�=K�;>�[��P"�
�=0��-O>�ƽ�"=���>)s�>Ã�y`�>|D>�t=?䀁��7�>ڙ=)�>W�!��`�a��=�َ���>8'<�W˽f�ҾD������#�%*;;�"=��Խ�k�&�+>B7���[!=��)>i%���0>�� ���z>������/B:��Y>�4�=��	��#�a��;�F�=�o#=|�@�^��<Y��<��!>`/�=�ʽ/� >t��8�yۼ>���>�3������E��Y��>�˼��=���>r9���>�¾����>�?�4,�K�">]�a��d��S\>c�p�h���+�f���>�F>ݏ>P��>Y�d��A=Ꞽ��a>��Ƚ�ӽ_	:>���y{۽[���Y��>��"?_�!>cJm>�l>�d���O��[�Խ���|>�7t�3֪�3\̻��_�� �>/�e��ֵ����<a�D���g=��>uѼ4H��C$	>�>[p�ɪ���#�l��=dn����4�}NN��%c�m>��ȽF7����=�)�>��a>4�A���=�!,=�f<b:t>71-��}�=�X|����<�ȷ=#��>`��;q��<6��DA�<S�f=���y�<ٴ�>�����V>�ɾu_=���>���GO�c9��m�:S�=֭�=��<­X=W�	>�� 	ٽ�=��5���	߾Ľϻ���=,�=W�=Ëz>%�H��"<ГN�ڸ�>*�=sŽ|~��]������=���i��Ǎ�L!0='W�>u9�>Rŋ�6G���	{>Nw̽*��;�l^>��D>���(�ؽ����?�@���6�vK>s�$>�RE�8���!i?��$��>��z>J/'�nL��D>h��>���=��=��b�XDz>3��<y_>U�==��?U�t�J�>(�=�+{��5>k�>�^>�7?�!�>��">p>~��q�F�1��T��yA>b�k�(l���:�=�3��#��=�G��:bD�hb8=��߼��=�vr�a�>N"<����c��='�=�Q=��&=`C*��d���̽�彳�x��Q�<ܢd>�˽1fF>�<��2=�g>����غ��F>ڞ=4{��j�<_K体����j�,�?=~�=�X>C6�>Ϭ>��>�ʻ>��>A�F=��Ӽ���=��	>�{������	�>i!�>5ԍ<[4�>@���e�M=+�9����� >��}>1���#�|̀�J�"L�>iϾs"x</�<��s���">O��0�4>�m~>J��)��w�����wA<�
�6a��n�L��>ǐ��&�>F�6�O�]>�z?���Ҳ>~h�>����NK����Ǿ$l>���p�M=�ƾ�1�>����R�>#�=��>�.&��uw>*��=Z2��{���>��=��������ol��W�>y9�>���>i�=��<�/�=�F=pľ�	�K���X����PZ��p�
y`�rg�G�>H8��
�?6˾.� ���=k9����ʳ��P}"?���=���>�!*>����2!��P>�z!>��>򚔾"�>��+d��A��;�N>��>�*7>ȳ����޽C�'>߲O��*c=���>�_	=!9�Y����>t�Խ�>eB�"�@�K;>7l>�?���>F�U>�\�>�D�>)�>ǂ�n��8�`����>���=Mu�>Q�	��Q������΢�%�=��P?C,�=��4���%>fm���<�Q�>�醾e� ���վג+�4R�=�%V<���>d��>¼mӖ>�q�<��|�������H�>�T���>�=de9�QX?���>�+=�K�> �=M��<��&<�=�����=��K�-8b?"kH<R>����~�=sgO��*�Y����_���<����u���S�T�> ��%>���<��y�:þ
w2�2>�>�,Ǻ�)���n>۞�>OO���s���	��0�8��;�>��ĳ=��>��)�J���=` ��N�Rj�<�=>�(?�����>�@l�E+������Q��<����~�\�[5�;B�;��~=D�K=<�U>Y�>c��>��>��`���=}g*��v�>��>(���\	��y���>H����G�>�R_��o�=��6=Gg۽�$>o>پF4�=~bN��F��,�N�-t½�=}��b�����
Y=Ɂ�<t�=2�0��{D�Θ�>��ཁ[�>�w�����?؋>lo��a�=�A6=$���ݕ�1�q=��-�w������>a���R��>��p�2�=]I�	_��"�Ex��L�g��5W>*MY�G>���`䫾5bt��?�⢙���r>�p�=��>�<��;���/�/>$n/=�\�<� ?�F9>��� ���AB�o0>�l>���>�$Q�`?�>ە�=����<��C�3�y�����(����D��̘���$�=��>��Z>c0���I�.��>lL���>�,=�M�>��>�)�����?f��������>��?������>)����߰�+5ݻe��5Ԡ�$T4��E]������ը>�ʜ��S���_�	�=���>�\��!N��>��>�"��/�=�UȾ��+>pۇ>ꃱ�3�6��>�d��G=��c=W|</���M�]�\싾1�\�𵷽=2������(���[�
}�>��;(X>&�>",=�/����=�$���V>+�=��:�]���&�>d���������6�-#=�F���]�=.��<��/"�K��>k��=�9�>d3����>^5=7Y(��O �MS�>� �'Е=F	>'U�����b_�<�P?� ;_�HL�="�(<���]p�>duu�j��� K8���<��l��M���,>���:�W>�뾾`�=~��]��=
5˼E����Z>�j)� h���#=1�n=�^Ծ��ݽ�{8�S�y=*C��[�b�
�>Ui>L�N�	�=\�s>"g?�"I>�J�>_9��E�>i�<O8<>���=_�ep=�6�=�+ľi�>4��>�f_��>m�=㝮���>�L@>�{��g�=˝�=H����>����Q��>O�`<�ѡ���.>�o�>��">�<(n׼-@)�Q�<��Qd= �	�dI=��	�.x��Xh>ǳ>� ��f=\<�>�c>�m)��4�>U�����>���=?4�={%+��b�=���e��=�ja����?���ƺ�-�M��}�=7�ý��������f�e>�Ul�\3ý�t�=c�6��1 �760>kd>��ʾxʸ=aX�>�6¼�)�� �>��>�uR��>�O��U�>fu������`]<-�[;�v�hqc=7�>қ��t=i�4�>���<Dx>+Չ��Œ��wn�H�(>��p>��U=n�Ly���>��>����G�=q_l��!����Ѭ>ǥ����b�~��=�;�o>����=���=���=��茾�~�<��������>'���;᳽ҠT���>�_Q��W>\+�>�`9�C��>X��O*�u׭�|�>�ߓ���>1+��F��=�O�=�B�>�l��i���Ry�>o�=�G�>;ý=�QӾ	��>��ֽ܎�>6�/��o�=2#�=����K ���3&�>el�<\�;�����x>mw�;���=����7�=��2`%>�o��`��\���2=��G>`��<�u>}�W>�q>/�9�&"�=*�2>��ǽ��<�b"��q�=\�=�(�=���=����}�<��y��8�>�s��Bi>lS�='H�=�ʾ�(y=�>f=F=��b���=zYu�B�ٽ����\�=�M ���>���)c���l>r�ӽO��=?R��Q�>߰���+�x��=�M����A>/K��Jֽ���5@�<
<���l=��N>�]ͽ��Q>����!��=?��>=BJ��&���u=*��>����=���J=�x�*ah=c�H���侵�>�K��~7��Ś�c>�=��w>i]�>
�<�>����=��Žف5=��<�+>��(>5��)<ƾCL�>S���3F�_+��v��e]�c�G=~,>,o��p6��\�����
���Ǚp>�	�X�|>��2>���2��]��<AT����<D׷�J����Q���o��� �={b��+�=�C�=2��>j�i�B�Y�/*��c[=��=�0S<9��>d��<��8� ����[��3�O���b�3�]='�>�����>�ЍW�`~�>>n�m��<6:���>��/>;gI�����:2����<�"r>4[���,��'��>Yz�>��u>�}}��y�h<]�c�U(�>sdE���)=�پ�4>
��=>C�<l�l>�8��ݥ;I )�qȘ���]��,>X�ɦm��>*��=�V#��d=�`=M派mu;f}/>�:>�4�>��<���!>������Խ�<��+�s���q9��\���9�l��>��J=�v����@��=���> e�=z��:'>8�������j��H1=�ꟾ�V1=#�=0&~�u�#>1<g�ï޽Q���AXz>�=�0�=�7
>ʧo=@��=�W��%�%=�0����p=)��>Ȥp����=���>Vj����I>;����0�<��(�4�#��\ؽ�v�=9��LQ���N���>�`���_��;��&�T��=�$�=�<>�پp��>(O����Eӷ=0�=B��>�KQ>��>S!�=���ԟI�@����q.=�����J�\�%���G=�uE��� >;�<�w>������W�Z�R>�	��h��Cj�Y��A:A��ݓ�k�ݽ�:y;�Ph<�e
=�F�>��=�Ⱦ�"'������W�����>$aʽ�C>��`�j:&�r��Ec����\I��������'���$�i�{>��#=�l1���~����=ꊉ>��>�uu�L�U��>�q���6=��=�$��˜l��wf�l�����>8�=%��Je�b�K��b��:䑽�=`��D=�>������>hv�3&�2a_�`���>:�g�X����矸>��P>��=0#���1߂>7�>1&��;=�1u>v<s�V����>�{�>�ξn��>�#�Fg��^��t◾SP�=bW�>6l�=v�M�����W���<��r>�W�e=��2!N>ő>\�b>��ܽ獖��0�=P˓=�J]���0�n>��!=?��a��=j1�>1�Ľ?��@L=���=���=ߏ�=�
|��Ǐ��c��ԙ��)�
>L>���>�;ƾA�;>ji�=�n�>�>���<� �>���J�<>�E�=���=�`<�#)����1^��1��:j���� >��>Qt��N>����/?�>��A�D�;>��>>O#j=��u>V͗�A�������Nk=÷=��0�ѭ_<���>0D��h|>�����������=j%x�N�����'��/�>�bn>`0�;��<蝳>�߳>f�=��=}�G>�E���=��V>��i����5���ը��:���9��mS>:�I>w+����A=}` ?����W�����}�>cE��Io�;���>��=Y)�<��&>Q@�>ug�>
�>�W����V9>V~D>�S�>��>�>4Z�=	�
<�Í>�w��@�d ����R�T��9=>d<���\�`���݁>u-�>ȣ��g����>Ɠ�%%�=�҇��>6�žy�s�:�>�.�=��-���
?��H>y�0>�5^�5s:>)��<�NO���@>nv��1\=�Rn>��$>4z��*;^�d���'>�^���M�=�S<>2�>�Q��N콀��ȭ?��'=j�+z>���<yht�P36>�� k��E־���}=g�<Vs�>��3=�}=�8>ݩ=�j�>Et�=4M��c���6�=1:(=P�<���>,p�;��>8��>K�>� �Qɽr+W<'�9=�r�=��@��>.4����.��k=����4�>����=����b�D��>B��<r�;a�m=9��  �*x���=s��?N�$�O>��D=�{C>��?���>W(�>X�>探�>`���<k���j��,����>�︗����E
l�*Ѿ�����(~>�e<��=v��|�=����@+�=��v����"�=s7���T>i�ʾ[=��M��أ>.L!>��ʾ�0@>��������B�>���=i��>�Q<� �%c���lѽ�rL>��X=��u=4���> 7=�]��Ym�=zR�=lw[��:���0��|�R>�TѽB֗=n�L>��B>�?�>�SG=�[D>��=wCi>Q�����:�SȾ��)��䉽{2�=��=�"n>�*g���׽�c���3>���>��>��1�кY�9�j<>���Ԟ��m�>���1O��tL���p�]�>UB��G�>꾁���1?z��>p	���о�ȍ������>@��`#i>�=#sν'�>[��>`��=�dn����>N�(>$��>>#U���G>WԾ���=�|(>���MC������=�aY=���>�"�=��>3�>��=�=G'O=�͊��K�>�U��!f�U>и)9['�;U������=��>�m�<S��B�=�ޝ=l�`�nž� ���'��6}��A�=X	���x�/�>�C��H>Of��G�����>1Ӑ>L�j<`�>���D�l�<1�&=7�K�^���3?P>G#�<�(�ZѾ#�N>�ý8i�=���|A�%pF>�C8=Y��H>뵲>�5��H8���Թ=n�?�7��>�xg>�Fu<�Ec���S=�՗���>�pw��*=~m�=�@>�D��<�����>��=i8����r=wd�b׀��NS>m����y�[�`>�0�>PT ? �v>�9r���?����/f�3�,>g�>$�����Kw�>K��>��->y�> s�>�
ν�#�a�,>���=5!�>E���Y�������ޜ��:��=я>^ξ5�=�O�>�w��I9Ž��(��<-�ge�K�=bM*>�h>x��W��S^d���>�ӽŇ>�)��\�=s����q��}���>�%��[g�(ֽ�2>ŝ����=D���7�>W6;�tu	�=��>d慼А��r�=���	><�>c�=>�F�<׫��B4>D�H�QS�>��Q�1t>�����$>���ꉄ�7�;>i���S>�7�<�����?�h�R�?���>?�.�i����ֽn�>�>_'>H�<џ�>�������������>�����Q$<Q���4�Zx��Z��'=>.n�>��T=V��>�Cx��X�= )
><���O��=���D��>4_�#�=�獾�S>)�.i½T�b>i�[>n�ͼJ@�=��^>(䤾W��>p$л`�.H?=s�ȼ0�7=8K��򎏾h��=qE��{y=�H-<�v�s;>�U9�U�=�K��W=N?^>ټY>�T�����GT�<�?`�=��&:2��񋨾_��=���>�E�>OU
=��a�=`��� �b��5>��?�����%�d=>n>�>,U�>��T>N�>�����5�����2򾜀�>�C��
���<F<>��<��>�2>������/�ͬ�>dV��W�>q����_l�]T���j|>O�ֽg�q>�,�=8Pʾt�����>𵽽�գ=�,>q��=ڇ�=�y*���C���>=U��=&G�=���5�H>,�8>��o�:�/>��3����=F+����>�,`�Or�=Z{>���H(������Q�>n�->�B)�Y�<��=[�D��_�w�}����{�q=5K.><       kJ�^�����`=�?E>T�=>,���R\S>��`��ͮ�j(+��|�~ �<)��`!�>岾=w��=�?>�������\E�>�p<��L>mʡ�6[	�B�z���*=��˻c���'���E�j'�>76�>���=�>�V.>nP��|����y>g�<�}��s~�>D�2������|=d��;@Gv=
aJ>�>��2=��8���?#�W�x-.����=�e�=]�s>�������=�|>�      M{�<��>�'">��1?�6�?NvW>��>�ܜ��a��.O��ʔ��b�=[q]>Wt��,6>������T�o4�>��ig��w1-���>c�i��ڽ�|����=��)?p\���k��cb���@?��d��=l��>� �>&��曄=�K5>�١� �>0y���Ҿ�~@�q�%��=��8?��ʽNΕ���y?r�=>��`=Y�,�+�w� >B�Ž�WQ<�\����>V�w?AM�>9h�����uo�֤�>�������>��f=a�t����(f>�j�>5'M���>��ݾ3n �n�Ѽɹ��>z?���b��=�������UνY�پ1:羋��������#������KS>9�M��ݔ��`�<�	H>�\�O�??���KX�>iv$?!"�>�!�=�Ri=���><.>?��>@��=��+:1P?�b�x�����>R�	=�Q=���>O�����!�1��>*��>\��>���}b"��?A��>�>��þ�5>=�ƾ��1�`�>\=���Ɩ�<�����(���|��z2�l��>�!�?}���ד=�M?�%��&w޽�'�H������!{�>�8 ��|9��>�-����G��"�'�;?ã��Pj�z�о�݊�#D�>9��<� �cR��Ʌ�>O�?L5E�lþ>��C�ǉD>��=�3>b���`�q.5?�u�>�$̾A�<2��>Lt3��At�=�>C�-��2�=/����"�����>�H?�����r*? 2�=��>�S2>�S5<>(�=Z�B��I��p'? ��>\�l>sf ?R���o��ʽB
�>��?�?�&������3���><Yl�C��>�<꼕b��Bx�0?����E������>�?���������G�>��~=�&?`�>Bt%��տ>yu�>`���V�>���q�P��0VB>�� >^���6|�ˇ*>KY �yU>��O>�@��_%��e=�=�>pT�=1�X=?�=l������>iSI�*X�����TXȾ��:>���!��ѯ-�������>p4�=�[�@��=3���cp��-L >up���{><?s/'?ѩ��b��!۽������]q������MFƾ��8>Ԋ>�F�o��>5]?��J> ~e�FF,�C�>-��/��,�>彉��W��>d"���5<} X>�(}>U���H�_�Q��� ��=��=t?�>w��o︾����\��>�*>��܍�=��*��1��a�־Ģ>�xf��'��-=Zs��N@��C>`��>�!�=�8�>i�>f��B�>�v�=��`�jX;����=U�=6\U>��B>��O>WI���Ф����=ތ=2������>@����X�O#�>�v@����k>�j)���.�U�#���dش��n�>
��>zQk=�!��uN?��]�Y������>G��F	��>�?Zsw��y���'>�J�e�x��dw�4�Ǿ�N>�&-=b>��Qi3>���/�o>W̯>��<���ྕ5����w��`��-��<������w:>J�>�[�����>��=_�������n�9>3��7��=�=�>�%S���!>�3>Wӑ>�A�[{
���?J���o�Ԭ�<�2�>�n�>��?�໹��]��=cG�>*O>7�"�\�?b����*� �=�VC��
��I�=�>��}�Ǿ�օ>V��b6?��ܾe�p�O?K�>0?��>۽��T.� P����=�BN�^�>i�=wM<�p��y��=�ؽ��9=MY�>��X>X։���?�=�\��w?.>�4�>}W�>�B�hȴ=<;�sw;�,?k��=Q� �y�H�*� ����>	'�<���&��^��~�D�_>|jl>QS>
�?#����c>��<�\�=.�>��=�(4�^:�>�mx���>�Y��#4�ƺ�>�Σ>��8�֐�={�F��&�6�L>�&�� 	>���Cg��֚>�����|�8�����>�E*=�SR����d�J>�
�>Y�|?��>�WO>��=��B��oy=�ޛ=��,>Ǝ8�B5>�-?��1�$�>L�H>ї>kM��TI�)kg>� �A�Q�WzE����>0؁�,��<H���U>;̣>0�0N�>������>�$!>Y-�>/1ݽ4I_?��=�.�?YTZ�k�<2����־����nY>�k��	���՚�f�־���=�»v�����=�M<>3w~;�K3��_�=��">�>�> ������Q*��t�#�Z�ǾK�����>8�>Ц徱����5>�H�� ���5�>�)�ý�=��=��Ҿ��Ⱦc̗<91��'N>�LE?0�n��(��a���l�j��MH����b�^>�����#�>C��,M>}���>�M�B�G�t�>�#�=D��	���j���
�=��=1�=0�>����������&?��1>�?�<�����>Ƀ߻�*�>�ɻ>�ݑ��>�=AB�=$qF>���=�:L�B��=����Fr�>��>�1/�ѝ��<�
��X1�2������M�����d��=������|�m�>�@�L/�>��?�K��W?�( ?�]�>�T�=`��H�B|�>c�_>�_"��2�%�?5���)��=tr`=�p��G��.�T?զ7��R�>�Oe>*/?|�>2��>��?$֓���<jeZ�j{>�%?��\?���=J{*�V9��z�>+������V�y>��=�����>�����$ ��ҽ\|	?�E�>r�&�D0�<���8�>>Ҿ��E��[�v<����	���-�����?����`����>B��=��X���>i�5=m��g����>��ʾ��V풾j��>��=?�D�>�-V�l4?@�>q���j*�;w��_�>��v�>���������P�>��>"���4���ǂ�>�ﾳ$��.>z��>hƧ>A��?�f�>�WV�T�>�?��?���j,>?�ν�>��%?�V�L�B�9�f9L=Xپ��
��u�>�&־�+Y��F����>�G��x$���%>�j=G=����><�e�!�>g��?�i�=y �>۫�=��>��=�>�%/>���=��>�l]�CH̾��!��; �r>���>cH����<R�Z:��F�%j>�}>�g�e��t?L�D>�io=�oھF�.�ew��������+?��%�1w`�4<�=󨱿��O����:�\�Y�>Y��>Ҋ�������H?&���>�kd>堁��?4���>�P���x���3�>;�J���꾯|%��^F?=�;�9ֹ̽+�xy�����>��p>�\������Q.?r�?�3G>��=����	��>�໴J�>�N%�m�����>X�"�啾ԙ�r쒼*��orr�'}�Զ�梮�i����J>Z��>�\%>n!�={��=�n6>6ѡ>�R��x��=�Y1� 1�=����8?�%��qĔ>:�?�*�>	���E<��@=�R?:,>4�쾝\=Eܾ�"B?���<�>�{�=���>߀�>8�w?]*
��@*��O(?�F?<�����=p]�:bI���>�      �^���<�=�վ�ڒ<?�߾��=�J��Rrg>�=(i=i}�=í�=dؒ�����]j>bB;�Py>D�}>u*\�I���&>��*M��-���>ؒ��f���>-�>H��<>ˀ>�-=�,�{W�=�(�=
8?"��>o����R�;��=`-�S�6>��j=K�->��a�;ң>>žz�V>ta�0�ʽ�V<����R5.>���=$p�FO?v�>%K���>�_��\�^��@ =P 4>�񔽍���8�T��	�=Ƶ-=�)>��W�O<���(q�>��>�
��K���[�p�'>��<p�+��y��i��+�=�?.�0�֒9>��q�Х0�+p�>�L>����VDa��Ӓ=�(�=p�>��������������k|S��k�>n
�񃋽%�]>��)>Of�=9�>� ����9��B��3�>��>J&�=�8?v�P>U�`>l��=U���q8�=n��Y=(_U��΂�*��>"�6�
�T�r�D>@Z�>O����Su>�>�������>r�^�ķ��h�z=���G�>�q�>���>վ>/�1?EL��w����ު=�޾�&=�	��L��H���ս-�����=�P?��?-?6>
�>3����偿��L�m2�=��	?f��K���ڼ�Pi>��>����Du�>h#>EB >b:L>a�>�>F5-���y��=|-��4����>�����2>(v�>�G�9���k����澘�h>/b7�.����Y�G�>y��>���=�뎾�9=De��O�{=�̾�T,����>n{�]|�<(�G>v~��M���?�<����[%�ه;nˍ>�Ҥ�O�>4�龀鋿Q���4�>_?8��:�?�щ=D�}�$x?2 ��I������'>@Jc�#d��w̥��	>�s	=.�%�X�Z>�O��yI�?->�W�CW�;��=a&=�}J>B9=�^�����&G>�Q�	�Q���->�A���Qu>_S�=�~������>��>��>�">���>��4>#�8��V���Խ�
��>��>T�3>�O���_>,�����=��7>l��>�$t�� G��H��c�>NZ���(?-����V�f�h�V>B/�=�
�����A>���;8{i�]����C��V�=J׳>w����">tH�=��&���?=���I������1!="�>����F���m�2��b�>�ua=��>ka>�u��P�?b��=m�-���f���^>���1&>dG�V�ݾ{~��~T^������Ç>[��>�aR��l">��=��>���>��p��L����'>F�&��2澺�o��N޽р	?S�=DȾ�����K=z��>��O�&$�(�����>cԽ�,>����>��J>V=3����k�'<T�=�}�����=ǁ���b9> ���U�>��%�.g�R�^��o��V�>��߾(�ѽ�|����� 
? ��=��P�75>ߥ>�*���:�=,:�>'��=)G���=��B����&�>P�޽�)�>�0����>}� >�c����o�>Y_�i�>o�=y,�>%�=]�3>���=14ƾS|�=kJ�><���%��6�>��>y��=VbC�s���M+���FվW`O�â=�c=����!��r_�%�h��l��`�=e>���=@�p>qF@=8�q��E>=����7�&=Jͬ<�N���V�><ߧ���y�~�mp���мH���D�>vʕ> �>�?�|�n!;��*���>�0?��`� �<ND�ew����>i�>�]���˳=|������>C�"��I�=Q�e����='D�>Ę�>��=7�>};�>���[r=T>�w=�">DS1�q��>���E�<���<Sソbi�F�����>�
���?�?�G��D���	��G��]�x=F���v���@�������>|F�����=�d;??#���S=�`#>ť�#p$�l?>����a�,u?$��<��>Ty< ܴ>��F>g�>o:@�[�ݾe�4>!�??���>��>�[���-��?c< �?H?��xi��ߑY����=&�ѽ�Ϝ<B�"����K�0>�n?nH2=��?�>3>T�����>D�=��M狽���>z����=U�9�������1������2��;�>��?<���Q�>�J��v9?�ú���W<�8ھ��-?}���8w�vcB=��%��c��9�;?3�7?����H�>s����P>m��>��%��-�<ώ � f��B�=�J׾���>�؁=5k���	�>kg=�;ؾ^#�;��>�?r��&]>xn��[^�;�.?ZJ�[���t�w�o=�پP��>D9��}'�{y>ez������1�>b4u��>Z*�>�>�()���=��Z=QA�=��J>�RC�H��t���t����Q>!Ƅ�2����V_>L^?]�5>�E�=�6꽁Zr������j9>� B>Cӻ<L�F�6c�=�/�=�/�>�,��Y���D�M����&�=Q�w>'��醱���<Y��ք�>-M>9�������T>i�=D~V��"�� b�>숾j
<���;�a =�����=�o�����=�,=�)�>��p=���>�$
��5��͔<�z����	��>�`�>�7�>n����-z>H�/�Yw�=^$��NZ"�A�=�+�>�)>��.;4&޾HY� ��=���=�`>f��uR>w��}Ɖ>�ښ���>d?j�e>v�K=��ž�qE�vy�� `>Hң��E�>=z�!�>G��/>B�=f�f�j��>�?<%p��s=9�$�D�ھG�N>`��>��۽��`d>�5޽$J�>�d�����>{6H�N�a�>��]>u�*�A��?
���Ac����=8����!c>����u�?C._��B�`�S>��e����=�a�;��=>����н�%G?��N�������9˼��!��c,����ٍ��Vd#��z�>�e?3�9!ֽws\��������dڌ>/)0��S���IR>R;��/Z��$� �6�~��Q�=mʞ><O�QX?J�j�i ̾'�l ��UA�>8�3�j<�>ؾ�0H��ckF�~F�<�R�>e��>��>=�w>m�>g�$<L�2�� ���>=���<0yE��W>�L1�~�=ט�=�Y���=�>þ�:���a���>f>K)�n��=�m���Z>w�7����4v=o��>�$!>���>�|g��S/��r�>LQ��=�������I����b����V��a�?c%<?q�>>�j߼�괆��Ae���?��>~����V�2m�!ົ�4��a�����q�<�: >�Nn>њw��_?�"I=O���J>Y�>!�A��ࡾ��?=!Ҿ�T�=/��>E����p6>�*v=��վ�ʕ>:�����m?�P¾�d>|�j���Z=���=p	>\G�>�
�>��I>;3�=��1>��=�>},c�@W>gH�>6G> L/��2�>J�A�L�M��p�>��<%ʼ�L��c3	?|���N�޽��>c^���>      �鼢�:>x_U>�>�B%�f��[��罵�:�W�.� �<�j ��4�4>���=�������u�8>�Ӽ���y0b>Ľ�= ��>۽~x(=�l{>�˼>s��=�����.����>8�z>
�>E��<g�=j
>J���;���~�>H��=�޻<b�*�vI3>�����U��U������mr]>R6%��~8��{��c?���-���C�_B�=�b�=���=�z�<zw&��>h�>Ͻ�.��>p>�C�=��6>^�<��<�a��B<����<�>��=�z(��A��#>�{��8�]�����Ú�hg>xA�=������ﰽ�3�=�Ӏ��a=��/v���'3�=��>��<�VɽXu>{:��0:.�%�����>c>b(���}�>p楾�a>�r��k%�<�����=�����E��~�=I��=��퀂=�<>��(>9�K>Di=@E�f����>m>�
5��0!>@�q>����<���$>ݘ>,�b>��?=��>Ǻ��}oG��5��y̽c��d�Ѿ-�$�.>?���A�>KS޾��-D?,�Ǿ��{����<��=���V�=1�
>��K>p�ӾM��3=�;�=���x�T?(>'>�:�>?������>��a>��8��d�>��=GȾ�Rý��m>qb׽\2>�7��[<>��>��[�����J�׽tSƼ�!7>g��>~/V�Է��ܷ��������>	5Ѿ��K>k���M��m{�>̆={r�>K�->�/>�5����>��>�����N��#c�>��߾���=��?�w���UW�Fs�$М>��5���>;�w>a���e��$Q6>�$?�J�������>�-$?�;C�
�>�$�*����A3>u�=ٿU�?sD>�VѾ��(���>ܰ���ཱི��"�'?��>ς̽|��>���>�Tg�*�=]���؞���l��j<e?�C����z�ol�>���>vӚ=�?��� �>ᣱ��V�Ĥ�6m��6�C��5�>����7�=�����[6>�vg����>Xm�>��@?	�⾄�>>�B�=�/9>���>�%X���>�{>�4��6ɾ=�/���>�M�>N7��\E�����W$�=EZ>�؁�n�ľprP>S��jf����=��\?F�m�vPڽ�(?�>KE�>�\h>�d=� \��@������Ԛڽ�C=�X>_n�8_�=n�Ѿ�־�z�3��>�򝾝߽�_� <G����̽2��>��>H(4;��>�|�>k��^��=׺�Q?G�=6�˽�[��h�="N��Qf�:�C����=V�W=���>Un�=��^>xw���m?O���>o�N���D��p�� �&)r����=�ɽhG@��>��=l�;���=�����>_n�>��>&>>zB:������F�=D�������=xH�h�>��J>##����r����e�/�=��>^O7?N�=qm>>�E>x܁�R��>M�>DO��c��p>�̾�u?�yk>#@�������=u�>�:K��t#>;ŋ>ƪ��G��>��^�UrH���H<[��?T'����=-"�>Ϣ�5�S�j���4��=��4�PC�>�X/>��ƽ�R�=�_���=p�N�3~�>3*�>�2>T�=� ^�䀩<�����_�>�C��n�p��5?�L�θ��/w�>�>�L ?n̥�dE���b���E���7�>Rŀ=P�&>��Ҕ�{�ٽ]��>|��=����&>2֧=� ��e�B��۞>e�ʽZ��{
=y�5�@�'��rM�P�@�������>V�ҽ�վ� ,����=�,�=3�W>��>Z�^���>�5��:�?"�z>�/=-/J>)�>����o8(�b2�>.�8���.��B�>�=J�f��~�>���>��}>=3=8�-;B�6���n_����>�_1=��2�$�G<7K>6����M>� �>�l�M�>|��>�F+>N!�{�g<VA��s,>���0A>W��=��>O�/��]>N����=���>�,��VH>J<��;����>F��>����+�>l�>9l}��=�Y��μ�΁�5�?��>R��>#���HV��5(<`k��"L��s]�>�2���!?��>����>��V�>cF�tɮ��M>��Ǿ�z����3���2�	z��t7ٽy�F��8>�!�>5𑾳���HR���t�>pLM>2�]>�>�=���0��>C�>�b��{c���|-޽��1�"����c>�ү==��	�2_��BI�@��$z�`|���ֺ=7�μ�Ɠ� I��	�>r�������Y����y��?���B�ӻC�0?�T�=��)?`>]匽M�I��ƃ?Nž`D�=oV�>xm@�d&=w~�>u�`��r<�r�9m>Ǖ�=�/��By�-q�� :��)���4�=46%�=H?���>�("����=��>�9A��U_�Fw8��[�>s�9���>% >�f���T�=�&��8��>�a��-¾c��8k��ݡQ�	!���d>K�9>[�]>�	������|��=a�=H#�>��=�������>�[>A�%>ml�;+>�N�������=�;Hݨ���>sҼVAL���>��C� ~,������>sk�>{-��AE���?�3#��ؽ�z>����e�>���[H�=@a���.>��'�;�>k.K��S��j��uI���p�!/>G��>Z
d�LX���=?jU{>��	�����^v>��>�D�>�Cž-:�'9�!�4>�>�=�b��P:=����v�U����>�=<| Ľy��,j�=�ګ�cz�>�#����;>b���!���f�=0��9i<����5��{��%��_>E�����~�侼A��f���>G�>>�㘾Ymj>�{!>���>�1>O�>�,�è=;�>ȱ>�UX���>��c>!�E�33������	���m�����#[=�9�;�S�=�	�=�H��[��z��=�|D�O�)�8��>�{�=�*���ƥ��C_=R[>H0�h��P�s>`(	���߽0@�?��?p��=�a��qa�>�P��9���K���ӽ� 8��,s>�H��\=����B�$>�S��W�;���`g���:�Γ">9�žr2-=���ڧȾ�LN�=ߖ>��>��<@��>�a��{�`��>�Di<�MO���K=k���jb����I�(�7�X?�	>�p=7O���=�;t�7��K4��Q�-Q>�̾|h:>5R��?�v~>�b�>�q>����|
�6�X>�x�pٽT�����	ku=��=3�R>��9=�=�>*7+?���>ǔ ��'�>ZN�=� >>�{��}	���>���<�%-��_����=ձR��t���;��?��+>�V�>�����>1�8>29��@�;�	��<v��=���>Qv��b��>��y>�ڊ�z�>���=��S��#]>�s=�Lz�/@������~�˽���=*z���>�U���m>���>�*�>��?�/ >51��L�澹�o���P?b,J���=��>z���t����� R��z�d<%ļ�X��Q���`�-�]{�>2h=�)�<�p>
1��  �����Ds=��X��vw�ǻֽ`���	�μ��F=��O>�s���^2��<��=�i�|���b߾�d��>2�����3�&�=d��=iy>lL=�(�;����q�=���=�?����=�>�I�=[@�>�t��I������q>�����p*>tp�>c�>����
/��Ҕ��� >���;/� �
�>H�>�Z=��>��<�\��<	�>�p齪��=�P)=`�G�i�����>��=�(b���Ż�\�=�焾0�ڽ
�C>��,>��>��.>.�����x���e>�W����+�>����o�u>�r�>m�'����=$_��� �����	����]�>�>L޽�������	ķ<ov ��"h���=ر�=f\=�S >�E��Z��>ⵋ�F��> [D��v��Lc�9�> �F�\?ѾF���>�j�>��>�rI�?@=�+���q�B�>Ma=/�=��}$3�;Ѿ@��ya��r�>+���!E�==g�>��R��ub���:>��0>v� �ܒ �� S>�)m=l�Y��q>uMƽdK�=E�K>�*�DD��V|=�;ý�Yӽ2-��R�s�p���:��������� �P鵾=�����<��>�c>?N�>����os|<���>��Ž��y=�+��H9>��=�ᆽM�G>�
s>�
�+��6��[���.��To�=�ｃ�?�b/l>w�2�gk�>�.>>^Oǹ����H��=�>i����{���8�h/�P.�=ч>Q*ҽS�����=��= ���=��3�[�a�[��?�Ì�D�	�fD��N:�P�=(�>W��=�����-?GK|=2;=/�þ�g>Y�l>�:p��*=~[����<V� >�>�<Կ>��d�)�������ǽ� ���@���v���콴�H��,>�#�>�w���֔���=��z��`M���T>�D>l�<Fډ�\b�x���#��	mn�;�`>]\	�s�T=?�>�Ge����=���>�Ot�?8񾆓�<]��>�=w=a������3>p��=�P{���b�M�6��=�r�=�<�HbH=��[=w�=����1%7�8�P�3=�����+K�=�v>�+�ﱴ>��.�9�L����>�:��ݺE��,���K�݂*�:��>�oʼs&>+���2������=XŽ�<Ͳ��t��>N��<���X������jI=�>�>���> ?;��Ä>�9�=b��Ǽ:�U+>򁏾c���1�K>^9/>��|>�4�wݚ<;�=��>[A=Е�=;$!>=�۽�n>��>R#>���=<��=;��w������f,��N=���	�<�P4��,�����P�T>�[Ͻ�m�����>O*�?��>T�.>�3J�"�h>*���ŗ����n�>yQ�=>n�>���:2�w>�kֻ��G���>>ku>+�	?Wu=�-i>�%��L�P>��>�E���(A��F=����
��u��>�4ͽ���w��=ņ>N�>fU����<X�k=�8>U^�<��<��>�����X ����|=�ց>뎾�	�>�<�<�:�>]��>E�>}k��� P>'�νq�>C����|M>��=�+����=Z�)�9ё>�\>�-�=潸�P"�=��о��=o\>߮@=��>�	�=�/���=�Z�>��پ�˽?�#�P��>χ>�|�="Q\� *�=1�!��,W��t���ڞ��W��y��>8�4?�����-�����Yj�ɾ��,>є}���=��ɽP6R=�O�=�ҽ��`��.�%{�>)O��y0I>�4�=�;?d�?�Ʉ$�Q�R:p�'���3���Y>۪���+���T>#�4>� ��Ĩ}>�E�]���s����%=Q��=�u��� .>�C�ϙr��M�=���>�!�>#��bE!�R7I�dԿ��o�=�\e�u��=?>e"��&��=4D�='�$j>}���m9 ��~>�H���&o�4�}=�Z>�[�>�פT�e�0�&T=��������v�V>h2>�|t�+�|�ܵ>N��>���=�	�9b;v���my=�H���F�=2[�>�-�Tl�=	�ֽ��A>#j�>ZX!�M��>�u=O���R +>j��<)��.|?����Y5��
k=v9���XK=�7���=,�`�	��=�tI�0�ڽ��?>NՐ�.g2�wmF=��ν�y%=���<$=uwW�4n�y;�>���;??��4��A�6>`��[��>����t2�>]Ȗ>�o	�Y[�=�t��LZ>ڢ�=vDy<S�C��t�� ��`��>7�=)��+¾h>m>AM�����=z�,��r��@�>� .�z��� �=^��>
"�=(�=;��>�Xݽpӽ�_˼�L=�1�<������ ?%�a>r�Zۨ=S� >�=��P�8?Hz�&���9��=3�Q>����=D;8�@�=�8��q*�q���졽�q�=��þrR�>f���b�c�1+!���G=�@�;���]*����>���&����>hf=�>�{���lZ>�+��SQ��򧑾
����y>2롾��C�&�?��<ϯ=3��v^�T$��k�>�u`�@I�o�߽�C?��>�NH���ϼ-E�>���O�庿1�����t#�>���m���>� g�V�p��F<?��>V�٩��ͼ�>�|�#�J�Nh=S��
E��`�p�	�3=��<��	��v�>�D�Q]����\>�{�>웾|T�=s4�}u��<��7>ǁ���o^>�]��}
���ýXz���B���P>����>�.���R�� >�ہ�'휾�T��=i���>)���E�=jB�>�`��@<Z�[�"���½�V�>�>(�I~��D�@>>�6>dΕ�@�e��R�<�њ>����K$��u.>��F?����5��X�?��6V��:��<1'�* ������A�8>��H>:>�c>�"v>b~L>�UI��&>��E> :��3��|!=��6��=?l��F���}/?��оC?p�G��UO>r�g>��ʽ�l`? ѓ>�6� �=8��fN���>����=��B>}�>T'ž��i�:����p=erɾSK>�.ӳ> vv�\�9��R¾��>ɟ�=�6�>�L�۬ ?&x���&��B���
�,W���M>�`{>�p\>�{�ɺ���=K�}۾)∽�;�>4��=~~F���H>����>�`�����=b;�=���>���=Y�Ծ�j���>i#�>�%�>J��=珺�9�=8�^��=?�}����;°7>�"���=$�<��<;�
�=o(�>���>������=׾s�J[��靾|]�=^{�>)/����ڽo��< |��fK�>���<B>�/��eD��	��(����彅�?3Ͻ` �>�SO��
����W>ī=<s�=*�;�g>c�ݾ���>��=b*�>G�Ě���`x=ɤ��:�=���=��>�C?__�C���=Q�6hu�eI>�6�� O�>,q�������.��j >7$Z�w:�<<       ƛ�<����2%���<a$=��?�co>	n�Hݫ��\d�e��=��?`�>ꓛ:����[(>�a��z��;�8<)�Ѽ\�>#�S>ٚ$>�9�>H��=;��n���RD�yϽ,�?�
w�;#���y�����)�A�à;�1�>U=gD>����G�>q{6>,>Ծ#�s(����>��<;�=�kP>oO���%������(���<s�E����>pv)?�7>���=��3�<       ��=eA��]�4���:>r���ȁM����>�7=-μ���<w"9=ֶ0?`>��w�ͽ�vf��?P�i�a��V�>	+� >�~�=@�~>!.�>�����l���9�h�>q{!��xѾ�s�>B>�����b�;�����*=��*;��ȼ�6��Ol�=�ꑽE�<�А>�������R�j>��>v^�=j�����0=Ob�=.���>���t"`>3�>z ?�;�=���=�S>�      �c�:�*P?"3��f�>Yĕ��i����1?P1��,�=�B�>�߽_�o�A�v�6M�>��>�˾����J>�Mν,���U�!?�t�>���>���.?�>M����(u?��Z=�n��D4,��k�>�G?u�/>&��c�侃�>m�	���>�s��i�>�+?i�g>^ݼ�i_v;��d�|���G���?��>+�>�q�՛��=N>��I��9�=2�=rf�����'����S���S�>:>i�>�\�>b�> ��>���>�z��`6�>H�V����Td����O���9=q h>���oM>����	����>�f�>}S�>|�;>��\�#�n�����&��>^N�=�U��P;U=mkC�v�9?��=go��{ǒ>����\��+m�>/��� �>��#����i�M=q��=Deh�?�
���>w����=8#�>['ؾk1�>��� ��B����>��*>6y�iԽ�7f>�sV?�$<�U��+?�
?�!/�d�%	>�=�>��=����	����,:?ǚS�b�,>�=n�/m���t?���?hI��G�)��={���	��F?��>�Y8��ߜ>�E�>̽,��<6�&��fI�>V������ޗ�>�a^>&-Ѿ�O?5�I�	�������A��M>���j���p��߻hQW�ʜ>ZYP>c����o�ÊW>�}�>M#�C?"L��P����2�U�>��'>�������Q��&2?y�<��=;DU�A��=�����辽��>�8*?��Ҿ�}�=��?{�G�J��)U��ST>��>���/?�>ɮ�J�G>=�>ұؾ<d���Ͻ"l?��=�����>s��=�>s����Tt ?�����q	�5�S���>:پS�>%:�, �>�}?>s�>�^���W>�>���Y��<�M?&��=��?P����Eu?<�Ѿ���<�1�����B?r��>{?�X��>�?8?��=>�0���?Ʉ辟ⷾ���=��>�g���ǾB{?�}�����ef��\�>���<zM.>_��>�L�>?��}��C)�2~>�;��*��v�>R�=u�Z����>���>�I>�f����D�:h�pU|���">+��<�u/>~��>=�ξV�B>!�ؾi 	?Mԛ>�쾞tB�#-�?�Ľ���=���=<O���j�̾�.�������=/E��62o����>�g>���>�;�L>�{>��L?��=;�=�]=(~�U��=͟���J��7��>�f�'�<𔾾���> ��=VU\��e�=�>\� ���<C=���Л�j,:[ED����>w��#೾���=�9�=�����1o>
Q?e;�E�3�b2���z�՗�>��+?�e>9���e�"?��M�{"!���>N�3=�q׾��>��w�{�G��x��;u&>�1>��=�2�=���>�x=��E����=�ֻ~�=|��=�b@?qo�?pо��:>��O?�H�>�WϾ�9>��z>����R�˾78�����G1>+2�>��?�|k�E�>�+���{o>S@�>7���@�{@?HBY�T���律�>tV�=�E6>)a�>��<zM�>���N����1��0�=o\:�u��0���a���E��b>S=>:$���-�����󪍾S=o�{���;?�M;�/4����#�>���=bET>��>Hy�<���=Ƚg�=�zP>"�>r���� ?�s�>�q8>4X�=z��=��<Y�������qW�=�b>VI�;�r��:t��2(���3���lr>�9׽���>����>WP~>�"�=Q���[N=N>�y��=�F=�0�>��^=f�a=n>�2	?^Û>D 3��(ԾP�8>x�r���F�?@��>+s���� >��A>)���X�=�6>�X��I>�ղ=�F��?�>y'�>>�=lf1>�8>e�q�=�>�=$�[[(�PD��D��=ް�;A�>��>?�j=_�>5�?�?�=��ýEj��D�,>��=�2��i�-�O6L�h�h>o�|>8H?�Ȁ=���<�k�>��=����l?�&�>=&��D�o=��ή=�r#��;>�m�=��[��.?>�SO?�:�I�}=���>X��p�]>���+�=\K�=-s>�?Rើ���=�1?>��<s�c>`�@<��%�λA��X�>�%N>&,.>w�>>��ʾc��={¾�=>3�H>�g(�U�p�,�>J�ؽ!������=��S�����h��}J>Z�u;6%2>%��񼿴o='�>�zm������>�>?�+s�d��=QQ�>B/�{���pd�>�"��$�ھ��=� >�����>��8>C��=\Ļ���u�!%l>~��>�JǾ�3>�د>i�Q�m�d��1�>8NG�C��=�9>V�ھ�J?�J��T�O�(>�t��ӌ>�F�>���=�F=2ݾ���>�G�\"D�\��=�/�>�����ٌ>����u����<l2�=ޗ>�z;o��>��������о��S>��:��%��C���#����=��.>q�F��n<��/�=�쐽�!9=ԯ�=���>�kD��#S��">`Y�>߻	?���>�Z�����S
?���K�����\��'�> �j��F= u����=�>F9羦�$��	?߉�F��>́�=;��
&?��%��=�h>w�K?^&��n���J?	�U�����IӾ�>�?>�)��=&5�>9�'?yXr?�p)>�Ek?[I��RE�=�ު�+��=��>ԵC?�8�>�0�� c��#?9>F��Y�+? �B��?E�%?�!D?
E�m�>A�.��t���t^����<���>Xڠ=��p������(V⽾o?��?v�Q?Ĩr�"8�������þf!���I>컽>Lݸ�~T�>���>���=��5> t�>.�0�o�6��d;�����?������:�������d���>þ<�<-��Ȟ�a�=�+J?��������p	�����b�=�"����>5�@��|�����&��:?s=5���)�>M>�&�T��ṽ �,�2���x#�&l ?"�V>"�N>x�W=$h�=%�">�+½��꾍�W=*1�*�Te~=�%;���>S�:!L_��J�=�E�>�T?I=��*����?�l{��N�> 4-�*G�=����F�>��?Q�<yj>@H:����> �g?�U��<3�e���=�b0=u��=JM>?����fܾ�T�?U�*=��qʈ�rn ����=VUe�~�>Q۸��K����>�x>_�'�'$�� �L0?޲�=��u�$6$�䉽����ԥ��9?�S�?/>��+>'��'�p�>��>�eQ�Gƾ�:׼Jݎ>�>�jj>-���6t���D3>��<EHֽ�2��[I�>{�<jbY��̲;�Ù��8��Z�F��j���=��R׾3=r�Mh�>.D�����v�2?e	?D����>�J[>�s}����>�e�>�Or?ծy=�M��N [>�H�=�]�>\��ą>�b'?D�B?��>q�����=      뉈��?Ͳ�� y���v>z��7��>m�9?Д?[Ys�)�z>WeI>�b�=k��=�4>��=��>}Χ���>y�>J`&���>��>]^�=x�3=�А��Ɲ>�֯>{=����L�>D�����=2�d>�a���f> ��1T5���;>WC ?�4�=��߾��x�@v>�">����S�>DKž�,�s�?�w.��]>���>��7�T.����[>uH�>L�S��J�=|�*�l�s	���b,��9?>2ɳ>̭�=�'T>ZW?ȟ>P�?5!l��Q,?��/>d�=Zt ��ѽ�&�>#sv= /�>6��>q;?����"I�)���c)�?�=}��>��>[E�� +����^:�>�>(�=�r�����.?-	��q[==���=�of�+�����w>�X*�M��>	2���>��1���?ٷ���c>��|>��4>V�>E#J>hfH��VC���?>[��<�0��򢬽�7쾊7ݽ���>Y
�=��>�ț>�؅�)�>Wm�>h��<��@>�>���u}���:�N���!�>D�=��=;�_> i�T�>�͘>L~�<��>�IG>O�>��z<~�I����>���>�C�>�����,���ھWD�>N)��M��~Ӫ��ڕ�T�!=t߽<v>���l���$��ָ=4��>��=P���M̾tN"��C��f߼,�־j�='J�>)r����k> �T>��">-���=�<��h=VX�>1�Q��n�=RA,�#��> >zò�%�z=UNl����=�Ф>�z�<���K�=�,��D���̶��GJ,��l>��t�yF���f>��½Vĕ>��^f�>��e��h�>v�/="<?���L��Rd�u��;4<,>�M����q�>p�}��=_�i�l}���GD�RP;>.Q�>9�=�t,��#n��|����[�������<�b<���=��>�9���=g>FV?�4��'�Ⱦ+-��$-�����;>����Bی�*K��ؿ>������>�]~>o[��~�<=����N��<s�?�֨���s9�@�>e����>A�����=
���PྲE�=�������=�lB=H���t:D>t�$<��
�����:>:�F���F�F<�^>��s>�Ӻ>F�ۼ�1���>���<���>6��5>��Ͼ�$<S�ʽ=H��31̽E����k�x�>T2˽��dG$���u>��;�� >�u���?�_4>��=�BO>�]�>�A�N	>���=9&�>��>�*-?�>�ƾfS�>`�+?��^>,�
=~�|���=��U>�O�=[�[?�r�=����>n;>�S*;7�� �k?0��9��=CMC�>A�O����I���i�a;ݽ���>qW�>%F�>�⇽{���&˽­�>�?2���7A�>�G>�����*�����;���i潽��?��A��Ӭ�J�@��}>�[�(r>����W>��<m㊼G*�>>�=�k�>�p�=Ͱ�>�g=��>E��=���>2>J�z��&����>��>Q�Q�1��=}sl����=�M�>���� Q�̢~>��R>��A��1��%^X>h?�[z==4���}
>��>�6�?�S�\�=�2`���>i6C;�v<�Y�ͼ�Qf�Rh̽ڦw=Ghy�5o:�)ݾ<:PC>��>�������,�(�4%><�>��ݾQ񞾵@b>��F=��>�Kw�۵�>!�?<��g=W>4?�J�=Ky>�w>���>h>�>�Z>��[��]޾�;�=7� =̯W�_
����ی��v���A?���͂�>;Z���I=xp�>��{��	?�]7=dH����}<.�>�F�>n&[?���=�<>�b>��ļ��k�ۏ�>�Oc�^�U>�x�����F����7>"�!>�
㽀2R>�L��7�>+��>'o콝�Ѿ��=5�A>�1��e�>%!�>�?��8�2ʾV�?s�>>���>)J?g:�>�I?0�u� ��=���b�K>�V~?�De>���>?1�=�����v>�N�=��>D��>�l����>j?_?)|Ⱦ6�������4i��'b>Ƈ�.����a?�[\>�B7>ތ��H^:>��=��e?���>k(�=Yۓ���=U/����R2�<'�>�4�e��[ ����W�_3�?vm�y8�N�>���=[���>Qe?>�Ӳ��r�>^Z��u|�>���s"�=G?Q?<���w�>?�>w���Y�=è7>"{�=c����Jھ	[#��	@>B!�=�?=�2>��Z�2�k=eR?��Ƈr��5��@����=e0>& 0?%�?7���?4�=�j���?X<�@?e7�>O�>:�<�������>|4*?��z��{>>�J>T3ݾ�@=!Cp>0��>���7>r	%?D�>
�S:�*L��=f=��ܽ�Խy�/�"ᵾ��T? �?dY#=(7>4ƴ��e��nק=�7G��������=S��k�پ�ܾs�?��p��������>�o�>�)��Am>����ow���?�l�>��>0�?R0�eܡ��� ��A��#����W��>W�����=��?��>Ý�u�;,�:��s0>��������r���H�<�(L>�p�=�#t=���,�
?Y�>O�c=
մ<�b=�A��nk$�r�-���=y��32<*�>��>�R�}췾 �>BC@>uK��9���ͮ>. t�s>>����>�!?��j=��;��{>:н�>? v2�A��t���\���;���Mu=��>��=��=~=�w辏���`t�9����?H�?{y1�H� �\�㾭e�>��>���?�|c�C�4>�������<�?��#����煊��b�>�(�=��=�=��>PV���
�iN��o��]{=(=�=S@.?nb>%f
��2���!�=ѳ߽N���6��`&��I���~ld>k�>��:��g�r��<�����>��2>�C���̾^c;�c�����;zt�=>�u:^>&���N>K�?8Ǿ��I���>E?��=66?8���	n�\�\����o���r/?�?�l��M^�=q#����>(��=�ý>�xW>c��=�ך�8�+?4��>'�޾�I�>x���S�4?��>�7�) }�L�4����۾1�7>$�H?S�(?��>�k�>�ǵ����>�J?�>1�i=ƕ�%;=��-��D>�O�>��"=�ͽA�C>^QU��5���|�%��=K��I��=���������z��݅=R�:>2)>>Ԯ���=>i⁾6�k�����(2>����8�)��H�=��ݾ�>ɻ�H�@K��X����>i2?r�&?���<r�q��'�<��d���`>?�,=
e���ҽ��Z�l��<,fg=޽���ԼT�p�#�\�"ϽDL���>:�e;v��=�ܕ��PN�ܰf>\�u>,�"=-�ྖ��~���B>U�w?��꾈���w�=��t� h?S��=/:߾@��<��>��!>?��>���>�j���>��e�|ƚ=Ec���J<< w�V���=�>@�[=���>7׻�-(>]�B> Ë��.>�>����>ge�����>ޥ׾��=��N�=�s#=A$�=E����7>1z>��7������dg�KO>�=� =�u>�˷��� ������J������>�C>t�<�Ҋ�o��=@�<�=�ǯ�@�]>B�@��W>�Z���S	��p<|��=�L�7D|=�R�o��t�
��>Ћ�������=�$I������>8c
��8�t�P=*��>��>yXڽ�e���>����F�=��;��='�N<e�j��o�>c? �Õ�<C E>ײ>�b,>n�5>��>��\��RC���=-2�<Mؽ��=-ѳ=1���Xμ���`Ht>}凾io�=���>�'��$��Ta>�R�<:�s�+�>�M�=�9��4>�ּ>֩d>�͢��U�h�˽� �<���u��=�l��ȓ��T���R�lֶ;�m�0����߼�%>����l�=W�ʽҗ̽d��>r�$#]��i��A�<X�d>؄��繆�vӷ>:T<p�=bZ���9>_Ҏ��&=�>>.ν�W��7���}�=��}D\��,>��c�n���ۤ�:��Ľ\
>�D<
t�
�/��}���l=@�������2�1�-;d͛�G�s]�����<��=�����ʟ>.�d:���=�<;4�5���½�ڻ>��s>�|�>h;��	>=�Չ>�H}=�#�=��"��<6*��a(Ӿ��b�<�;k�c)�> S�>���>�>$����䥽ζ�����>=ܘ;�&>�5�;]~#�&��>*i�=Jdν�v,���>��5�<�����}<<.���z>�~��P+��)��������>��=�Q<kå�~Ź�����U��v��>|;�MO��E�[���� =���G�=�ɲ=hڊ>���=�}�� �>d�=����˽�+k������l����t<�8��b�׌0�+މ<>nw�*/�=`��>*��>��.>���=П{�����Ӂ��.�:����Õ�C"�<c�j��=�r<��Ȫ>м��8��y>�iŻ&�>���>�����=F�0? ���$��,3�=	/�<�t=�g�=o���b=�s>=U:���">BX�>�$8��g=a����>N�>�qG=���Q����>_&���R��
���)�/�g>+�>�a�=�4=>n(��)2�<��bڄ=Ͼ&>$�)>�6۽р�=��u�㤢��O�xTw��E�<��Ѻ�2�=E�޽������=[q��A�>b<���W�W� A&>s=���#�ȍ��G�c�mJ�؇�����~R��N�=��e;"�>X�%>m͙>)]���q�b�%>�������xR�=�#
��B/>�������M�`�����,Y=\��U��+�m��3o`���=t���F>�MƧ�㎝>�o <��_>u��=�,>�Ǘ>@⎽�$z>ek��->�1��'�����>�;�&>.>���<d�p��܉=�V>,�t��~�"̲�d��=_���ܴ���m=�J��R�m;�M
>ݬ��5	!�<29��\�>�B�>TM�=�:+>���"*�<*����/>��[>�#��
e��\!>�����e6�Aԑ>�3�>�^�>"�}>�ľ�3���$��<�>�w���8�>�X[>u7�(I�>�,Ͼi�>!�E>�=��
.�_q>FQ;��}>b>�G�I�l>��>ܮ�g��ܳ�=�Fl��!�=�p�=2��9�}v�g���L>�H>���<�n,>�h�8���
Y>(N>Ž�> C��D���?���,�>��徯�J>D�$>Lr����>6˸>v $>%��>D�K�`Z�>WX�=��%>�\��Q��=mk�>8��>�1��N>U}=->&���>����>��ՈǾ4*>/n�>�ы��'�>|��(�h��> �?]��>��A>�߉<����p>�]���ں>��>��	�\)?bz�>��(��r>�8�j�+?�2��z4�>O��}=�a���->
9�=��$>SH�>�X�>]���u�x>�$ʻ��>@f:���(�7��<�q,�j
?`�>iH^�JǾ
��>N�=��<Ez<!>]=�	>8��'L)>�6�>k��>5v��>��η�����P�<nCd>ھ=S��y����=�mz���9�@��>�f~=/���܃�=4���?<A��>l����
?Y,w���=j�?���<���=JT���ݼ6�
?��=�@���<�,���ꉾHi�>���AY�R�|�\r�Z�=�j��ꥆ�:��;x}e=�+>�^�>�?
[½"c�,�Wq���.�����>º.?i�#��2d>�+>�}�>Wl�=��>�S�����3��Aк:M|����@�>���>x%��?��=�y"?&�>���>��={��=jԱ��q{��RX���Ž����U=U�E���k��f�>@����=F���#-�E�l�nt���>������=�u���*��
� v��W�U>����D��==�=)����=���=ͥ���4>�����m>qq>(�=�+��`>�Ã��R�=^��>�|�= {�=�c�>�pN>-�O��/F�H.>FV=��K��t����`���|�٢?���`��>���>��;�C��C����=c�¼�����>[ >E����N�_=���]�T>u$W>�ჾ������>�$�>�\}��w��gx��O:�wۘ>��T�4��=��|>|��>�>.�!�3�2����=K|O�p4 =�[���0�iw�>�F��	M���I�>"o>�Q�;��F>��??��>U"T��۔�Cֽ��>�k�>&�N^$� a?�q3=�>����
Y�>�G���Z�1e��e����k�
M���������x�>�$����Խ���=�">`w=�0�=&�C>0��>�	?������/=$���Zk>�s��e�ƾ��D=I�f-I>V��=�?,>>>щL=��@>�����y�>�9>4��;4�d>�ܼ^�=}?�#��8X{<!"Y���D=�]���%��9;��>
>�=��=���~#�8�\=��H>�W�>�1�E�<�*L�Qo>��=�+�>D>?8;�=����>F��� �?+�>5�3��>��)�9>e(G=K*�>JZJ�4ݚ<��=@��ƽ�?Hb�����W籹�T#�*�<��C>�pe��޾\ė>"G�>��
>�=�4CK<`�?>�QL����=h�=�Y!�!w�U2V�3g�>H+�=�(>����(B��ñA��Uν>��>ž>,���=>�$>�V>Q����+�=���G�>1T��"���:>dyνc�=>n 7>ި�>iX�>H*?ǅ+��e��q=�y��͜�>Z|����?��?M������=������1>�o�2�&�l{��%����=]J}�h��;�9��> �����=I�����>*� �Vԥ�H�i�K+��R�~�[ZU�i��>���#V>�ʾ�z���s?
��>��>����0��>��6��?�>�'�<��]>�|=�=R�>�RL���:�︾�����H�=F��=p><       I#�>Kh��aH�>��>�O$��zS�>�3;*"���<�z�<4��G��L�Z>=P�>pb�=H��>�a���녾���>�i>��t�_!�=��;�-�����<-�=���=>���/��@�	>J�"�<��=�@̽~�)> �>B���˻>���<��k>��>���=Ł>��9��i�⪅=���=�y�>���>x:A��>P��>����T����@��uZd�N�=Ta�>5d�>Z��><       g�;b�/>��;�l?�Q�>����1>��>�L@>�&��G�<y�>�D�ѽ$2�=7乾��+>'�=�S�>����@T��T˽�Ӡ�R!1�$�N>�ލ�,��=��"^>�y�a���b�*=�v�>hɯ=�j1�RKH=F�p���>hxþ�$o>�u�\���g��;MX,>�l�=8:�=��X>u6�>�๽���>s�?���K��<�.�>�9�=W�����=͙=Yw<>��>ȳG�<       ɺ�>�aK=��i>�?�ؖ�V�n>�`�:�+�5��=u��A$��__�"L����\ѽL1s>���h�s<nR�̥\�(k�=�#�?\SS>�l�>��/��r�s��>�E�>����b�> �ƽ�N>9?:��8޽uW�;�\�>w���VMy>����|�	>g*>��ּ���=�z/>!s ?K�F����>:�>b_�>�{�IH�>Z>�^#;���>�����h���4>��W�m=)>      ��>el">�!˽��<�$;���ۿ? �>��K�շu>��>��s>�,ƻ�x+>����>`w��%��e��qv/<d/`�g�c�e?*
���>잾x������sf����>G�B>xdؽh%o���>���k�"=�gν�B���G�=�J��ū��f>@H>B%O=p����>��=y�X�Rr�C>��>�*���޽X�ż|��>C䒽��
>6w�<�=6s�r�=|�o>Qp���)��M
�;��>_i��^�<���E������q�s�ֻ�t�����k_�)�=dҕ�Y���QD��Z׬��`>�\������I��Q�]�Ͻ��߽���y>=ޏ>[���=߇=�bn>0��=H=W�0�۾��?�=������>��7��eC>��<@��=���=�.����>���>@\7tǛ�!>��?�U��9�� Y|>^n?��ܾ?�D�>�Pg��s����>�=+�N?2Y7��}�G�P���<��2I�4�f�N\��hG׾����R�G�$>M�G?��2=�Y�>�C�>�3�>���`�ֽ���(>>[$	��Z?�/龤�>9�>}SG=^�;?]�Y�0�oP�>0U0�o�b�s�F�ѽ���<���>@P#�(M��ZM�x� ��@�>��?�[>@L�>s�=�k��-�����?"�پ
���Xd.?��V��V�!�k=�>	����%�>�ų���:���ѽ�$b�Z����J���=�u+=W�>�L���x�=�X���!�X�G��:>��D>f\��'}>�W�>���S���W>��%���mp=�E���j��G�.ꌼ���=tX>#*ƾ�9R�%d�F�U��>�����*��B�����e�]�> ���ߖf>�aT�[�4>Q��>?�=TH+>9�K?�r#�R�L�lv=' 
>�q�'@Ǿ�Z5��M>�Dc=Qؽ�淼y+=�т�B�
3��c��+M$��1	�6�>>o暾2��>o�=�E�=��;�>��:`=e���Dh=D���2��P>��d�C�=��?/�>C�c=�k�u���A���R�D�>���=���=���=$��>Kg>�e�>�L�>���=�����0�
j=���>�₾������<�1>tI�t-]>�<;$�k��K���P>� ��)J�Q)��2�����<PC>Nq;�*j>����v��*-�>ND�=��T�U��Y�?c��=�N�����ݒ���c�~�=Ɩ!�d�Ǿl\l��?1�)>ͬ�=�q>����^r1��A>C�>Z~߾��=Z�ݽ������z>���ٔI�K�=����Z�g>:s=�ޘ��{����>�ǹ>�I�=3#�G_�=���>d�$��6�̯@>��x>��=S�.�n�?=(�=C�> �����.��>x��>l�=���?Gp����2>���5�Aj�����?����?݃�?}�Ĺ1D����=���?gd?Bw�
�4��W�W�̾��:��=%;��a>{T?�p�>\��9�/��3��x�'���F������'4�(����;,-Ž�>Ԯ(=˰�y{�b�ʻ`2���>H�ۼ(�]�>ilu≯�<�ޜ=B��<��L;_־�_=�$��C���.�E<��'�� �X���wf��Ʈ��Q׽JM�?�>).�����=n���&Q��r��>�]�>Xu���XJ�,��>��>���>3&�=����3�l��#�^�1=�>QH��	�>�Ԁ�xS>�l>�\��w� �B%�>�"�>T��L+(�Z����w:=	�}?���>�m����=�n�>%��� �=/ʹ�\(龋
���S=Vr�����q;>�(����l��̻>��˾׻��QE>�4���j���z+>'��=nL������$?�ؾF?��>�>v
��:�>u���ԣ=��>!�޽�u|>PdB;���=����痧>~�W="ؓ={aʼJ\�>�n�=���>�	>�3k>A��;|u��}�m�vJ]=�I�s��������h�>)����Ԏ�ka�>Bc�>�9.>���=����ۚ����=�*���錾ó���f��1ʟ>�8����ѾQc��1�ڽm$�;�| =�-�:'�>��~>�V>���EDS=f��+�Ȼ_k�kc>�c�=@?ƙY��C޾F�<#��C��0��>�܉�5˾v� ��z>������>^�����>�w>�J�>��C�,?����T��[b�>�3?b��>�E��B>~`�=�J�'~=�~}��l���w>�.[>.{>�X�>���>�ǅ=K�'>�c�>iCH?k��>�u�ŠǾ�Xd>F�*�6�d�?kq��>�����>6��K�Ⱦ���=�z��{LW>��:?�!d>�q��L*?��Ծ\�b=#q���"Ծ�/�U<m�Ծ��ь��t׾U���>�w�>���=�A>��5�M$6>bC�>9�9��u¾=��>W���H�!�Jf�>/�#?�$��=I?��5�ژҽD�C=l�_>I,'�,e�=]w�`ߡ=�ѕ���I�>}ݾ�l6�����?�C���H�=�x�>؇<>��>鸃��`�΋,��>C��<g=&�̾ٴh����>��K�����%>�
?qO��?��>#Q,��^��l �>f �>J�W�q�D?7Y$�����,��*/�C,.�<�	�C'���ξ��O��ԉ>�j��"̾,M:>}h�>��3?���>�˲�A����Pz�]��1D�.��=�o�=��=Kt���'������c�<����>ga���j>>r	>g�^>u��>�s�B��=�����D=��D|̼	7�=�����=}S>�%.=1��Ҋ��뛾l����P>����(ߛ��齁ñ�<��6ө=$0���Z>ڑu>���5�<<RT�Θ =�ɱ>~ֻ�VD=y��=�-=���H ��Ļ�bqq�,�>[&M��<�>�����T�>�	�>b#h�A3�}�&��H���Y? ��=�LJ=,�ݻ���=-L6����>��? �M>��>=��׾y��/R>��&?�܆�)tK�K˚�m&>*�;?Nj���XX���߾�2K��DӾ��@?��>/�<==7>�5�=jz>��?+%�<M<wj��l�=�#�>��_=r~E?�X�=��?IA2�f��>�(�>k;�����=�#�0u��!��G�?|����3��O�>-�'?o�==���ŗ?7{��jm?Ut�>��>��?�z>�%���G���9�ͿA?fcS���*n/���>,�>��O?�½���)�~��=�Ͼ�����\�]��?wL�?������>�̙?d`y��C�>md���9�>2��<C<��M�����~>p�%���
�Y��$<>1�=a�@>ݍ�>��g>R�>*r�������c>ԏ��o)���O��0<�_��O�>��=jA�=Gx�>�� >D$�����=��M�>:�>tC�:���>�9��J�޾QB����>�=�<t��!�s>Ys�>���g����>B��>���?'�>���L�>r�>6F��c��C��G7<�n���L�}4ܽ��K��ό<�N:���>�8`��ρ�i�?݋�>t���OX'>(3�>(F�n��<�G��9� �V��������}�M5－��>LW4�?'��f��b�[C>�o�U�ʾ��>y_>�o"�'2�>zΞ�4y>�e��~��Fʯ>e�>�`���$w=G�����=8:S>�r�>�7=8��,]�<���u@��gK�:�<�c>�f��ɥk��Z��� ���1��n��ړ�>�Ľ��P�"�=�؄<s\D�1�J�N�>�F��P0�>
��4�;��%>m��2Ƚ��;鼫 �>�L>�Z��E�S�xc�=�˂��T߼����D�>{Q�>�9=��>z��=^�T�@$=>b�g><	��0���B<>�SʽP:>�!A�4�"��dw=ox%�������3\�jt�Ԕ=�*��Z8>��	;������;�$սB�>��>u��=Uk�=�9��A����_>{�i>�"=Vn�<y��J���}�.<V�a>�*�>M��>�?:e��.�>s=��T���1>�;�����D��4���֬<;�>�O�Bä���%���E=�Xj>�.�>�t?5���<��v��=���R���!w��1�nm>˃}�ք'>&I��}�C�wE�=�0c�� 9>��R��w�=��;/	\�=˅��4h����=�*�;�+E�L��=`L׾�i�>�	�=@&���K��c���_2A>���=�-g>���>���<ȉ�g�R�R�B>�\ټW������=D�=<��=��u=~	<�ٴ�ץ<v}�=�R4�D%���Ŭ<�|��Y >�f�=0�}�9tA;|l>U%�>Q��<�Z=R��;qѽ]�>V2>wy-�V4��<f<�`=�p��tJ�>�x>�)���,�>��P>�@��(�>��Ƚ�>8<��t=�1˾�aa>t�@��r�>~��>��ɾ�[a��WY>����;�>VzH��J
�:�>��:�=>��=]r!�[��=_�2�+n��">��=|^:=��F��
�<�Ѳ�AW�0:=Vr������Ϣ���Sy���!@�Pރ>��@��dE��n�=�l >�!�ܻ>V��v:>?��>kP�=HFF�zOm�%��hz=�=�Ak>~[ܽ����0�=�E�G=��0�&>�YS>��=��W�	�>.��>(�=��U�\�U��)#���U>��1�1`e;�I�=��,�;W������M�>V�_�74���Ny ��U��k�=���E�:�aiz��ɾ,R�>=���*Q>����m>U�;�w:=�T�>���~�[�"O/�5�����{>p;ѺC���3f�=Ͼ���=P(k��D����}=�G>o��A�<*�?�n�>�Nx<���=��9��">L3�>��6�zR=]���k5�>��+=�;V��q�>��o=������w���l�a�U=�P>�)>�\>��D�'��<r@X>g�K�9��=�!��?&>�c�����g�=LL3�$�:��{$�/��>V����O#���>U��>�������9F�=��>8μ�4>�m0=�	=GZ0>����W=7�>=�s2>�/=>�ş>�0�EU��ji��Q���`J� �+����<I�!>Gwս����s���>�i����>/!�>*\S=��<��:>+�<�>�j����������n����^>��ýu>#>p	+>֗> �>BG�>��-���S�tq=��K=��-��9&=�{��>j�C�=�=�=�=ѽ��>k��K����I>�_[>?�">;8����`�M>�輧�a�1�5��Ii=�C�>$P>X;�>;�
>����CD�>1!ݾ��1>/�U>Š�>����N�=�ș��:>l���0�=mq�>�)很�#�ጢ<��/���E:���>&%��}w߽:��; :��|X�<��.=��<�?�>+Tx>Բ�>lcݻ��v>T+��9̽;$g������s��G�ݼVR>�;�]R�>\h*>{e�>�ӽ�!>d�8��hY>GC�=�.�;��-��]P���>�� �:^���R>���>&�r>�-������rq;�=t9���=,�� ����>��w�����t+��i>�Ǭ>&�>��(�|�J(�>���U�j�+F޾�۩=޾�=�9Y���+��e�����=i׋�� l=��=]��>���>���='���2��=�|>�F�>m�I����>+��$���{v���2��98�#�^>��= �->�uQ>�f	>8�_>�߼=f
ս}Bk<g[�=�	�M#�>��>?�ξ�{Ǿ��1�>M���z��b_�>5j���.��	�!�>�w��Ul?65X>XX�>n⭾��=���=�?������������H2�g��>뤁>�|�v"m��̴=�i�=�\��[�s>�ԥ�0���wb>�2��Y7��N:� Z	;��>��>ih>�򘾪��>�xܾ�>��|��Z������=���>Z�	����;"7�>���>������޼�h+>?)�>kA�ѫ�>/1�>ͨ��侀��>/�ҽ�Rپ�����7����T� �>֎�,a�=0ܛ�ǯ>JJ�8>it���6�>�ڥ�l�I�7l�=����:��	9ӽEu?>�d�r�u>�a��$;L>j��� wϽ+=␭��">�5�A����xu���=xLe��Й>+���(�Ҿ�K����;>��������o9��W��+?�V	���>Ot=P.�>�V��#5>�B�>3j�qߒ����>��d��?�E?��fm=7Xm�����Ҥ<�~��] �<9��2>k�f��dT>P[�����𞼼�~�=��<����>j��=�|��9�!5|>�_�� ^�Op���9��
�o�V>��>>"��=��r��ֽ�^�8l�>zq��$���Wj=��ȼ]��"ac���c�
>�>�3Ӿ�c�
�߼¤��҃��$1�ݾ���K�>T�=A!|��{�>Xۯ�4e�=Dds�aB���q=$s=� ž\�H�F>6�<���=@c>�څ>.��>��M��ȓ>&���� z>�4�Ӎ��,�;�*�>���>��}��)?�U�>�м�s0�P�������>��������۾<��>xR+�9�?�d����!�$5>��f=O����c��7�>�-ݾA�L�UC.?"��>#��=��
?�i��YX�>��>��>��h�r�>w�i>vwr;x����;�U$��5����>l�*��3F��Ͼ>sʾ��>:,@��,�>�P>��>�5O=�ѻP��=>>W?�$$��<�>�hz��=���8�!9�>Z6O�"�#>܂�>�N>���>f�:<��
?�$㾜�B�$�P>O����.�
�p�>��=T$��J�����??z�9?+�g?V~�>����x�����T>�H}���i�J>H.�=̼�<ģ[�\��=F��=�F�>��5>2p��5(>xܜ��R/���轹$c>q�P�q��<x�>6>^���#�=�R=&Ev�><��>J��<0q�	>8=��	?d52��n7>l!>
J���=����QA=q��S��>�x���x�㋭=q��VD�w��L0Q>9ח>�ʮ�}�������8Op���?>t�=��>����Ǉ:�������>�      r0�?�J>�/�=�&]�M>%�>�m2>��߽�b3��֡��S>4Z�>�fX���4�=���=B ���Ծ�?N��>˵��}���=�b꽉�����L৽C??g�J>?/C>�$�=^b>�<�Pھ9q>˥!?u6߼= ٽ�C=>fϾ���);�=L#.�"�/����>~���H�?���<F���jǼ�+��)�����>F��b��>h��=�r5>��E=�!(�N�|>
�̾��>��U?��a>#��>�h�=�r�>�!�aW�����[���>�0K�a5A�>71��]G>:���@��F(�m�6�Q#��:�]?�B=H�K?.�>�⢾_+?�������eZZ�56��M�!��4��>��m>#X���m��?>����!I�����Ѿ��Z���-���^!�~��?,�?��>d]�= J?��L�8?�f�>#���
�>�Ì���>���:�=sЉ>��;�PPA�u�d�^;�=`.Q���S>MLU�9�?�co=^���	��>�<�= �(=w�l>^����>=��>}�$=�/[�.�e���C=�Ʉ>ms>�
��o#\=)5�>�v��o��>�m�X�+?i��>�?�^>�N��>4I�>
#$���= A?��G�A��><P���M�=��]���2>�	A?�J�>N�G��`i>�6�>1��z�>9}�>���= k��$;<xi�CXm��b4�侥U=�P�㾚/j=l��>&m�=_,�;G�~=�QP�u��>�ж��Z1?�i�q�8��¡=i���1�%���?�&>>��C�<`�X> w�!F >�KѾ�s��>� �p����������
��;���=i��>�=��uY>>�MT�J�??�����ӳ�k�e>����@������tb:��
b=�L>K��*Ԕ>���wLi������=��i�(��>Y;s�X
G�]�5���?I��;�S3�~fT=����������m>c�7= &K�D0��M�Z>?�_>�?p�����5���>��>��]>*�*���h��=������J�c�ֽ�%	�����#�g�6Ž�5�<�e����>,lG���S���<㰸=S0?�>>����򜾉D,��DK���:�jU>���>@��=��1?�O_���>�a~�_&���g?s΍=z�������;���q����h�>�4������P�>S��y���>�G���V�?��G=
�;��1>|3�!�3o�� nA�Μ�-�2�
��-����٢��o=���,�>����]d>?9��)��ľ#?M�x���?����>�M��O1j��둾�tW?��?���_?�9��ꁽ���>�'+?�Uk=��м��?Ŧ>�����`��1��>��>hɎ=n0�<��>v;c�-��>8C=ʼT�>�|�>T|�=�
�d'X�MzO=!���Ӻ�����>��;������8$�'��=u�U���=3����I���(?cm�>�4=�'>o�>����ܹ~X��CC&�2;�=Q?>�N��u>�p6����=6��>�#���Ñ�[���,�X�FϼKɳ<��$?��>�nV>�W=����h��\�Ȼi��<!��D1z>���e���zG�<p������!���k?
�I��"><�>x�3>��b�fz�>��:�+�t9�~ټ����>�^��+(>�������T={�+=/�>���d���h���">��׾G�=�y>>f3q>�"� ��ԼD���-=?W�Ͼd�X=��>̑����?_�W�����xr�o�>��@���>8�|�^p�>cD�>+��>3�>��?�g��e��=G}���'~=)(&?	��>o����e;iW����>g�u��<�z�>t�L>��L>,���>#a�<
*���ZC���h��?N#?5Q��K(>��lv�=~3�[�>�j.��6뽜�T?������B�>V�D�W�=�?�؂��E�<����<�=���?dV�>!E?Ks6������pE�0"������x�=��#Щ>�%b�`6>��	>��Ͼ`Ӕ>��I>\G|��:K>�E'���J>���=LQ>v9?,�(>o�^}?+���!��D.�yu�<�>�5
���6��^m�5>m9��q�>��R�#F7>#���b�>��#�/�����-���a�?HC>nb�<\2�>�I7�R빾G��>$���A))�p�:>Ɔ �i*�>ѥ��{t ? �u� �|�����>������>*��훅�#Ls�u��>����ke��$����0���7�io���&9�ʌ=y4Y<�=ϻ��>�
��{>-�)>��>T�=0�辍/�=�4�b�@���=;!>}?>>[�=0V����>b��>� �>�9�>x��>_�6?�P�Z��f"j�L~W���>��y=���?3����T>E=�=�3�:m>iF=|����',>��/>b/���>�K>>��.��ێ;#��<�Ы>��c<�\���B��(��'��i���>H��=۸	=����Ӿ�켃X2=��n�����>45N�}n��ٻB|%���p==������TPE>^P*�����e^$�R�
l�>�i��a���
Vӽ8	��8=RG=�hܽ�Ue>�����>��ǾΤ�>�|->Qg���2��=3�k�o����h����=�A�������>���E�'>�a�b$(>#��^$:�ۑ��!>���%1�?M�>U��>ˎ��a�:s����=�VZ�8>kFQ?W�-<b�3?/�]>���+��|L��fD3<N�=H��>�pZ��[�=)5��}p��}<��x>vς>}h>Ȓ?/Mվ��B>�hȾ	�g�M@���Ҿ�w;�v? �%��[?��&;->rEb��]�>VV��
��?]�n>�,���	?AG(�gҮ��/>h�~���&?��1������ֽ>�-���[��<;����_���������+��("����>'�> Մ?d	���9���>��ɾ�ݙ<X����<����=Ԇ=�YQ�=��u��;��@��w�n>T����K�У��G�`��Ջ=iA;��	��i>��? ��=�nv>;=�{���z��>��C=��?�[�=z�%>�L�c�!���?��'>E��>�!�>Z���s$>	~R���=z�r=1.(>D#?b8>�]Ͻ�m�>aY�>�>c�+>� >J��>j��2��<�YپSS����>�$�^�>�{ؽ�J�>͆H�4h�>�?�����)>��?4$?wE}<��=#�>��>�
�����>砟?]Sx�)@�>Q� =�XC�?����Ӟ�$+?+�	��j�`=��\���XK>k2þ�V+?0�?>�Ҏ>v���Q%=��S>���C9��Ř�搴��_"�y�Ҿm�<�jY=�_���>�H ����k�n<��>w5>���-ep=?��đ~>y�E�f��d*����P=�=>�m�>�D{>j#C>s�x>bN>=s�����u��=���!
�=C�� ��>�4���8�Eo�>��?���ʹ��1Ᾱ~-�<       ���=H�=�H��1F�==�=f>�쮾4E����9��Rފ=:�$�@��E9=�ɾor?׷��g��Ѥ߾6�ؽXuT��g>=I�?}j�>�p>|d��3-'��;B���>0>��>si(>�>T7�歾&�d=��>sEd>��+=�Y��@k>&��Q��=��=%��=A2�>~�ཁ i>�?́E>�'|=��*>ݙ>��
>��/>�ٻ=V��=h
�=944�F��<       ́@?�p�>��>�a(>1��>ǰ�>Ĳ�>-�h>j�1?���>�h�>HR?���?�7?k�i?f4�>�Q=? ��=�P�>��>�.�==�>vc�>�S?v(�>t�>W�-?
�?�cP?7j?       �!u�Rj;t�;� �<�(���x�d�8�� �W+��#�;��û�f�8�䕻�Z�9��O���J;�P�;[o�� o�n����6��e�;� ���ƻCÛ���P�s��I`�:�U;���<       �����D?�H����-�=#�<๸>�G���V��/CO���=�>���=���a��,�A?ؽ~�>,}.�l�ؼ����r����a���?���ri�=�#�:�5e<8�=��(>       ~��<��;�T�<{R�<���=D1<�
�<o��=`� =!�=�:==�H<K��<_0�<VE�=2�e<���;��U=�m=��=�w0=؝\<L�0=�[><F<�W=ͅ�=5{k=�L<���<X      ޳�=�S��1=�B>2�x�/��Ľ�se����>G��=>�̋=��^��c�-kU>��=x/ >��>�V`<��̼�ʮ��/�>��ټ�4���2���؈��f�<����^y��:��y/4����]���{�>ݕ�>�K�>^��L�o��p��tA�<��=��'���5>�SI�v����������u��A�>����T>�)&��������=�.����h�38���l6?�ᕽ,�=��鼑��>���U�����T�����>+�;>|)�=u�>��</���,��Ҧ<	ې�(���'�=��&��d��p,սd-g�#�V��{h�D�K<��!�d��;�T1� "%>T�M�ϱ?�X��Ҝ=��;6��= ���\M�>Y�}�/ļq~>�k�<ׁ�>,��>�*����{����>B8վߜ=:71>$��<�����v��Z�����N�����������=�����o��}>_�=��=�_�>����Q�e�|�FS<��?�<�C�=�a�=�W4=chc=�-=^�>�B�������*�&-��m�=_��<ඵ=�ZV>!�>k=�I�6�=u����=M[����ͽ%�!;��`.=�y�>�mm=���<f\��:`���w<��;=�g�=�B��������6�!���žq^�v->����G�$Ȁ��H���8*>�bὩ�뽦AC�W��>Y����Խ��/>=4���9�=.�e�9��=i�����j�z4����>�k��9���>,=�O)�u�=���<[K��mq>t}�:��W�=i��=FO{��;�<23����<A����+��I�=�� 촽)�>��>��>�\��<�7�ꬽ,����%>ϋݼX�ǽ$��>党>��<=��/�Vu">:��>��=��@�(u�=��=�L�=�ɬ=��ا"�Oҩ�r��=
��>H`���d>�p�=�U�<��\�R�G>я>GW�i��������䉽.��>�.,�Z>���<���"E=T�>�8���x�=tyʻ"㢽S =��Ƚ~G<>��J�n#��p3���В��ջ���a�F�r�p�z�=����=�Y>=w�=�ߋ�f�>F{�5 �=`Փ���=J�1��Ϸ�^L�>�
�>E� �R�ݼrg�=Ͽ>���=)`����D��b�>�=��`>������=� Y��7�R�S��E.<��Ľ�ۭ>L��=Zֽ]'�=�����נ=j�< ��>�F�X�����=��@>M�b�
�=�.ռ�J��椣��F6���н�O�;R��>&��M�>��绒	!���&>��0���{>�x4>��: ���}=V>�o�>qΝ��4H>�L�?$���(b���i��4"��n�>g���LH>6ژ=6�#���I>C�=dO�=�J�=��>t�>�}N><��>�,�>�z>H+W����>lY�h.}>��=1����m>�m�>IR��н>ꠡ=�W=�ͣ>��>Y�=Y>��>a8�{h���5A��{�<{಺`=>�:r����6�=qL[>*�?�A>0�����>m�����</y���`F���>��=^P>����˽����.>�C�=+>ǽ\}��������<�������sT�=�����νtm�=�$g>���=8~��K+=��
�k�=�>w��=b����_�e>mR�>�!���M�=��D�5��AԼ���=yhϼ��9g�����>pp�>�W���cI>�R���Vo=d�=s�=<�6��/j>O6�=��F��#R>�)>�	>�Q>9*о)O>O�=:3p>4��SM=�=�>#̨����4��CJ�׀Z>:��8u�=�p�=ޮ���y���*�=S&D>N{=M��2)<}��=�%}�/z�=)Ӥ�@Ã�;+��=�)%�=.��O,2��v��wӼ�0��#�>a�Y=�燾�d7��<�>�c>�����O��$,���]�=)�������Ep�>�d����
=�W>j�]><>y�_Z��3mG?��z=�.�>'�>��>�Q�=3%D>t/o�k��)%`>g.���>�Ԏ>� c<����=H���q�3��>�=:�>ÙȽ��c��^�>�t
�Į��l�h>+�>3�@On�>Uu�]X=�L>�"�=u�R>l��=m�J>�����[�[�~�_8�<.�<�J.��ip�ZZ������/�>&9�#>"/=���<�G�>n>����С�8�b�	��>��E=��=ʲ�֌{���W�5>2��_ƼWJ �V\c�ؾ�dȾ�9�= 2��9��U޵���w>|�/>SpV<�4/��=��#=���#�Z�UaW������_>dھy���e���X�N��HüG`U�+8U�����P��8���:>���u�\>       /A��#�]�)�=��@���[�R?��
�=i����`��/�</��=����y6=Q4��*N��+�=�����<lV%>=��       �Z?AX? �?i��?Fh�?ҎF?��)?'�0?2?V�??�On? ?�NS;+�h?!�?�x"?^Ѐ?��?1�N?       �H�>��>��>��T>�Q�>��>+�>��*>���>F�n>f��>�W�>C�jT��LH�>���>}\>�t�>�>���>       �æ�j�L�w�=�1��ۙ�Tt��{��=Fe���.���$Z��>D���Xm=��@���ռ�=��!����:��	>,H��       e�|?�~�?�?���?R�`?�6?˴l?*c/?~��?h�?�i?��?��y>fm->�Pg?�N?�W?56�?��>��@       ����Ռ���>����+=-I��I�@>���c�<B��>d�>�$�u��>�����ʾM���
>_�s�Jc>ض�       Ǖ�=<       !�j��0�e�+���Nڙ=]S��d����>���C��/����??J�>��k���j?���0�>�?C'��R���+?���־Ṛ�X?���R�?�|>-�>?{�?���=�3�TPо">=6'��T?@8?d�%�(�>���o� ��Lx?�?�Q?D�n�c�I?��ʾ�پx�¾��?�e�a�������"�T>1�:����Kq>p辕"�?       �K=���[=�5g�