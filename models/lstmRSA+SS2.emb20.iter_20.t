��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      r��=��>�e>�*��f���(?U0E>�9� P��<�=�t,�f<��3Uݽ��j�6j�=Ӧ�=�mZ?�jm�(%U>�O(=��?�]��!*�8n?H�!?�?x.�>_{�>�A��˧���о��>I��r�O�o�=�Uh�V�$Dp>#
�>�W���:���t�:�>�0i�:����	���x~�=���i�m>�)�RM��B�?��>���w�?gdh��-�=\7�ŉ��R�����>��>v�>�j�$|o�HmH�� f>�>��?��<ˈ�����kj&>�<������M��?@,�_O����ӽz�ӽ��:>#��i�%���J>D���Ͼ���R�!>W����{)�P���9�־�"ڽ��?Jb����>�'ž��E>ďl�\^�=�w)?�ʁ�ug�ĉ>yi7�5�a>x���q�>?Ꚃ?k�?�u>���>,B�>�{�>���kB?K8�=D,M���>>��[�Ѿ0�=yޛ>S(�l�S>	���e����>�b�f��>Ե�z��<h�:>y�׾/X&����>t>签aٺ>z>�鿾iT�>z�>��h��}�<�>���ь]���4���V��0ٽ�0	?&��=0QW�>�?�w��Nc��̾���>f�>?=�>wJ�|�<�#�ږ߽AU���83���>]���:w����>�
=o��<�*��}ۼDv=.]B�=Q@>�Ӹ>������U��>IX#�����=�'�>�ۘ>U!�<퀠=E/>�7��o��k�6���4��=��w=�q>k��c��o��ڼ�L��َ���z]h>�����9�=���>,-�;i<�����= �=d���s����� ��E����*���⼾K�E��V=�.?�z>��>g�ȼ ��=~�>�φ��쬾s2�=3�"�-D���&�=��
>2ZY�9Nr>��(=x�c��>}>�3�>�:�>�iL��Z���R>����*>nY<6�G>)ֽ�?2�a?R��>��8�m��3�?5��=;�˼՜��'��=x�=rي>0}p>+P�=�(F��"���k��>��=I�ǽb��=݁>�zľ��f�����L�Ҿj���6�+�V�(��>��>ț�>����˹>�V�=�[g��nu���g>��>K�>�]d>��>c>j>ض ?l����ܱ=�a=&��=c��>k&?NfV�FXq?qh?;4?��?[Ҿ�8��z�Ͼն��z�><7�>�l?��>!�c?Ә�>�X>?��>�Y��|��>=�����Y�}] ?ē�>��>/?i���վ�󢾐Ɣ�5þ��<��>�3`=���Ϣ=��-��m�v��@�ھ���>�N2>��¾���>>L��<�.(�� �=e�u>�>o�	��>)��m	?���>��'�I�}�P��c~޽dP�����
?�ڣ?a��V�>Zˈ>����Щ���]�G6$>�~�����oR?��P��H=3u��kԝ?�[�=)O�<�#?��k?�H�>� ����*���2?�Ր=�\=Ȟ�=�{���� �9��=�)��n/��>A>��L?~�?-�>Ma��OG��a�       ��o���/���|��?'r
=��#��d۾�~3>I%V>�N>Ču�X���"G�>���]Rf=!�ӽmw<W �>�=�i�ߥ��n
��Ԭ�f篾�j̽�n���?A#�=�0��JH����>W�����?Գ�ɫ>SIJ���ռ)`�荐�ú�>���>�����l>8>>5���M���73>_+'������1��� ����=빟�1�?5�>�>�=~t>���:Ƅ�=������� �fc�>'��>��P>P��>ET�>͚���"�>����b��>�����/@>r5�>�r¼��>l��� �h�
U>6��>�<ּ#)��ug�=�u�+�<��Ϳ��rƾ�%u>�
ڽ�o��T忼߆��pw>�#=��>�'�o�Ͼe�a+�=o�[�̾Ќ���>��>�-<�N�	���>��1>߱����k�𧥾�>�(�ȫ~<�%�	�=�9L��.>���>��i<`��n>H�w����a� >��3?D">ƽ:�,O>�^?r>�>�q�<�2�<��f���=x��D��<�W�=�	�ݍE>�/>�/>/��03	�-��="7�=�U=SQ��Y����@�f��>*��=Ѽ�>:ѭ=C޵>{�>}L ?���>�%P�� �~�>�e��������ͽn��"3B>�K�<7�w>h�F������ѽܮn>팇��W(��r�=�׿>�Y=�er�� ��# ��!Ѿ<�>^Eþ[��T8�<���)�I�/��ٵ�_�C��M�lc*>B�>!��B��W����V�>����	�=��R=����Nq�pt���m<�K>��&?�F��xV�g�l��N>�J��_u����=3B$>��+���{�z�ؾje�>��_��Q�={r�>;��=ם���
��*h>��;��_(>�ʸ�9�?>�j�>�Zt>��>qN��eC��s��>�}&�7����9=��>f~=��=[5¾7�f>�!>�9?g(>�G��;J��V*>8�<ð��-�=���E4�������>޲�iv?v���E`�n����rX>�E�=LT=+2�>�N����>Ԍc���V<K�#>�e�>��G��_G<���;HZ��l�=���=(��=C>�_->�����G��<=L��=���V>>=aE>;\�=a��>(̅;mN$>��=��h>�=4�/��ǌ>�6��.�<=��?�&�>H?L���=q�=xF �IuT�v�>����o�s=@�8��; ��5�*���M�>�z�=� 8�+'���O>O�Y=���=㤹�6h<!C>��<�B��x>�i�t�+�$.=U��>�V>�`ٽ�<�P?g�>��0>�kM�����<�>���<�>.i>��:>f&>D>��>�7����>b���� VU����>:z�b0(��Xh���>�}��l�>��?L�P�?x[�߉��+gq�` >���|fj���>�!�>D)�<���*��e�`�>��=�J��h͡=ȅ�{^M����=)��>iU�>�맾?b���4Q��#о��U=�NC��>��=�'�=U� �|U���X���vŽ�a�>$�M<�ʔ=�)�=�(�[��=J;�>ϋP>QA��23>Z���>u��Ch߾1V>��x>�4>�␾ߤ�;�g��I���^֥>9B�>��;�)��=��=�A"��@>�>=B>c�R�w�,�}A�>"��>E0>�I&�B�T�ؾ�����/<=�ˀ>�5n>��[>Ύ���3>�}�>�VK�7�>b�=V ��d�,�[��i��<�
������"?m
!?E6-=u־�� =5�{>"�<�3� �dY�>�3"=B�=��ľ�%��m�>�ؚ>��>��?N����>:���zi�;Ml�pX�<��2>�6T>�O�=�	���R~<C���5 *��L����;=�_
� ���+�>�>��>J6Խ)��>���=���_����=�?������ƿ���R>�ݽ�ѽ#�k>A]�<�i'�H���,�><�o��\�=U~���ݯ��O�>oi�>i�3��Т���G>^��{>�d�<�>��߽n��W�=�W��1��=����bia>��>�*�R�����<{���3>���=JE����R=n����;�w���,Kѽ8%��>	�V�	Ⴝ���V4=�>�uý���>�$���׾�Y�=v:*���X>C=X����=P>�+�b��>f�W�<�/=d¡��$��?>��ɾ�x����;�\=�)Z��d�=�>>�|>u>��?ŷ���$>$"�����b\�V�ɽ	<[���о�6
�/K�>��>�9�>�슾�8�F�ľu��C T� �C>��8�#�}�,�]>9!�=T��>�>�N��`�#>�:q�6�齿��>�<�=��=�lz>!����c=��40�����h���(�>�rB>��>�*�=A7K�#�e�G��=�c���<�������=�ޡ����������h>]L�=��>�<>>"�3������=+1=�|!������=�M�WS�w��$U��Nfe>�4�>��+=g���D�Q�C�l�)�׾&��KlJ���=�S��Sm��ϣ>���>'�.=�F�>���>���>������=P���	s�s c>�v���>_�t>��HR�>��>��\��)�>����/��=엛��	���P>�.>��4�h�?=�S:�ؽ=���<��?��
�=��>|��>�0�= ���(�9=�=��&���w;����Sľ��r;w.��)
��ڪ��[���%۾c�ü��y���ƾnx��h�>�/=g]�>���>m#=���>p��=	Ue����1ԗ>��=�?���=ZC>Q��>)#�δ9��tl>j)��lS�=�T�����>Б8����=�]g�C6���>7��=���n���0Ϝ�����Ѿw�>!!>�CF;���W�G>���y���f?�ON��Aҽޖ���I���'�s#���߾�(a����>�0�>e���j>��c>���>Ҫ�=t%q<O��<��;�*�z>���������Z�=kB>�Ĳ�W�<=T�,����>-��>�e�>�E�>;�,��̽�Q��h��?Q�l�>]�>ޠ��H�!�j������<��=+����L�����&�
?5�>_��=V\�>�� =����/��o�Zs�>��U>       ��H�/���ًվ�>�=�>�v�>���>���Eg�l��=s7���>[B̽��>!t�������$��r���>�[��g���y���龆"��͡�`5�=�\��;��� {����=�F�=l��*�l+?��|��À>Ë���>����/�>�D>��x�-��>�T�>�.�=#�����=$�x��n|t<�����l�����=�҆<�]ѽt�R��c�|�l��ǵ=8�۾n��\�Ծɿ!>�1���?>�%����|=���jV���<՗��(�%��������|>�Q<a��=V��a�>�	2�p�,=�ھ-(I�I��R�<k���2�߂i>{��=2��>����+�=^��?-B�=|���>'�����^���m�>E����t	���ľ��&>=�>#�>����~=>���<��Ѿ8/�>7�M>��޼�3�M�>O�>أ���\K<ئ��*>]>.9�T�>��U�K�>��>����Ӻ떮�R�N�t2/�ksо�c븐f�>?~����$�=;�X��>���>J�??o>N�ɾ+��>A�G�j���)>��¾�>r��>�6�>�*��O$���=�=,3�>��R��m��I����v����=��1>|��>�CN�Ҥ(��d5���<wѽ��E>C�w�^��8����>�/�=z;>�P2�\�C��wϽ^��>C���"��຾t^��7������>aX���Gl=����ۛ=���<�x����X�R�V>u��=p��=���e�=��i��Uþh~�>J��`P)��h���*>>����=+��=��T=�L����q8����>i�����>_����V>T5�uO%?�C>j��>�p3��q���=�������	sJ��:=��~��U/���<��$=:kJ��^���z,�qn�y�=,��
ǽf�;�O=���>_� >n6��15��߾}��=�4���2��ͨ��D.��2�>Nlf���o��b>]%��$�<]�]�3}}=�˧�t?�<$�=�.��3a�=�>��>�_����߾�}�>�N����>kN>��9qns���-?������=����?�>'�e����;91��[�>$ϫ>�#?>�o�>�����N�B���E6=��>���2�<��#=��d>[�7>@e𽻣�>T��>�6�=[Ó=�� >`�;R�>Ě�>z~��0X=�f�=�~>eh���y��h5ƽU�>y!�L�)<
*������^���'>�fx��xg���r>�W�=�!�>��>�7�>h=�>$��Z[���~�=j�����_��=l�,��I�r�>E51> �%�4�H>/ �>���>S�h+=�w\�����^��=ΰ���>t�i>�p���|�>?ڼm��,-��LB&>	�?��޽�i,���v>2s�>M >ۢ>��
�j����$��`�>̉�=�� ?�z<>�g�>���;�~�>Vp=�T���>[����:��� >���>2��=?  ��|�j�M�-�ݝV��?�>+)���B�=M1�>[ �=�}>3����H�>��?��C=�χ�;��B�>"��>�Ғ�G�b�t8Q�����e����*
>O������=�-���=�G����\>-#�X��N7�=C�>-�r�by>)Խ�*�>���Wd�>�M>��D׾���rZ>bF�/D�����:R�&���{>ƹ��ć<+u�>��p>_�h�Ož|y>���6`ɾą3���=��/�S7&��%�>@��=G�M>U�Ⱦ���Ζ�=�D��<�� P/>�$���>�>_>�[�>2��<�A�� <��u�=F�h>��`�b>���;dm>7�c<n��>����TD>���=�Ϧ�V�>&���:���>����������=�P,��k���d�>&�s�Ҙ�>�s���>|��DŨ=�_V>��s���X=a�}=��@>lw_�{��uo�>���=B�=��R;�yA�=����ٽ��BK��oc=�t��X>�٦>��>wƽ[Y�=���>�2>���=�5�=��!>��ڽ�ӵ������$�}D}>I�0�{�U> �>`�+�LD�>)?��h}:����+ۈ>^u�>
j�8��S>�7���>h��=>b���3Η=~�=�_1�v쯾'�>+>WI�$��=b� �vh��j���<�<�3=d���Vq���ſ��j>�1:�k2$�kGʾ�]�=2�>����H��&>��\[G>흾-$о�ǉ�����2��;O�>�>½,�i>� �Lr�>�E�>��a>%��>����F�8>�e2�����-ǾzNc>E�=1�^�:�>�᷾6ʾ3l����t��T>��j>��߽]����9���,2�%� �̞�<�=:��:S��=.4�>m�?>H�'����Iо��?�(�����=u�=l�5>+�ľ�ݞ=��=>x��.6������\���m�>�}�>a,,=uJ�<	���%�=�ث=��Z>��<��ڽ�*�������2��D��x'ƾu7��S�l'=�r�=���>U?>�<:��3��q���O��<��W�/e>�:=��h��=��>|).>���=��r>�r=�zɽ�h�TM>��=���=��>U�ν��)>/����u��(�ᾲp>;��=�������nQ<;�'>�B�>[�_��|���k{;�#ܾ�a>�����>��>�=� ��6=n�?(���R7z�r�⽂�_����W���c>'��@߼���>%��=������:����>@>�@��>�>��w�۽~i���]>�����+?�Q>�ʹ=��j<!�>g�H�����V�
�,<��}���>gr�=�+{>d!'���>gI�>�������;\�1����X"���4���>�Ȗ<���=�'�Ě���l0���=�ڃ�>�V>�w>����#��u�҈�>�x�=�z�>߿�=Hǒ���>���Rb=�o�>3B!>>�����'��-�=�ُ��Cl��R�>k��>dߦ�XaM>���� �:?>��s�C��)��B�>�v�>�6>���>%�%,@=CDĽ�2�=~,!>��>�CA���>ʆ�>��_>�==�3|��TIƽ���=1��=�X=-C�������=��>������=��>�َ��      յR�)�	?Uv>O���IG�:����0>����|3y�٧8�)c�>�B�>�4�>f��>.�мX������4��t�%�;�ѽ4��>��9>��?3i�=�k�>M� �l��>��R>Y	1�I�� K��iK�>)�>=�m�,>��N��><�d?~'�=h��lM?-���+AX?�b�>��R�3?�� >�X4<��>n+վ߹6�f��=k�$���Z���E>�l�>͜5��-�>��>?!y�Ѝ�>��>���=��=i�%i>)���Į��į½DX��V9G��������>C��P>>�+>�(�,N=A?/�>�W�>L�ܾ���=�ɽ"��=�
��A>d>Ɂ���~{=�u
?�XG�;�����a�BU(���|>�X���>~�Ov�?F��?F���s���C���c��c+���=PxH��	�=q ��i��֠����>�b1�wS��F����p�>���>]E>���> ��>�+��=�Ɂ���޾:pZ>��ڽ��¼n�=�	~>I�ӾM!�����Q����@-��3�=��:�C����*�rY�:rS@�Y����T�=F��>%49�p���=�}�>1?3�ᑼ>؂;�:~>���>Ŕ��D= �=���pb�=��>���%�]����>�AM��#>����k7�>H��U��=)�->��n=��=`��)چ=}�����>1��#[�<�b>�t>�+�ʕ�>�<�>K-d>�4Q?���d�?6O�ow?�j��)?"���ye�>���+O�"[1=��	�����靾z�>?~gE>"#�+u����<��>- ���<?�S=�@��d�J�		�>���n��=V�־��K>W�*�XV���=���=�����ǽ[�>�׼�@���`�=6�	>�Y>�y>2�?j_=3�}=��>I��=�J��`Խ?
<-Y�>�vǽ�'
��Ø��p�>{݇�����
�i>����;�0�>\��>6*>�)�=��Լ]�\>���-�R=���<�=�rK�Щ?��=�^��=�7=�����3����		�=�{���&>�柽�r�լ�=3�;�%�>j��>�ɽ�U�>�5��n����$���n��?H\��Z�>�&�<V �>@�ܽ�x>��߾IS��3E�����>�C>�ث�ۗ�>B�]>3Nν��>�P��]q-�+
;f��L�O�,��>BY���=�q>(��=�}H�&�G�0n���U=y�>�A�����>s�X��s�>��Z�P=�lž��>h8m=U?�<B��\-�>�4�>a�E�U�8="pZ�X��u�R©�c�%��>F� �A�߾��N>��X�5d��{�>���ARc;��G>�W�>�G?�!�>� ��4��>o]���P��i����j(��8�>����"j�	<>�jd:
��>��>#�>T��=��Z��=��}ˁ� f�=	[(��#�un����=CV5�$�:>@y���j��ʖ�Uu ���.?���w]9?��|?�?���1���𦾫�������[�>����z>̟־rݭ=�S�<�0U�����ᾣ�Ͼ�^ͽ���O��(a?K�q?J��      ܒh?�޾
X#>�����>zM?'Փ���? ����ǁ?ӞI��L��^�e����$�>"���@4�?݇����?���L�!?�?�@��&>׹?��	@��?�Ӷ��{�>��?��"}H����?_�e?p?�d?Ž�[@�N�=�3!?+Dc?eI��E�2?�觽�}F���˾Gپ�~�>�w羻�5��?SK�>�?�aI@ʵ?���>I���?y��>)5�O�V����=^��>��/?b)v?ͅ�?A��?�7���N?'�Ǿ8b��n�>��hc��@>�,��>�li�gۿq>�2�?(�~?���<�KW�9F5��G	@$�?��?�^�G��@[�N�?��#�dM��%>�FɾR^�f �y��>O��a^�����<i �?��T?�?܈=�[�h��K]�����;��GԪ?od=c�
?�l?��"�ܬ>*�5��g�>��Ŀ���?�yp�橌�0	|�&?��T>ɞ�?�j���%���y��r��?�ƚ?޿�?�~Ծ_ �?�a�=ގ�>���P�C@R����������?��&��k ?�S��n�x����v>�3?�j��`�H�j��e���AY��摿Ca�T���)�?�f�^uk�E�,?�&k�cJ^�R2j?������k��>��[�b?��? -?̉��k��J�?�m�>�>r�\�7N��y���M��{�ȿ����JI:����>%`�rNI>�:<;��o���2��?Z��Ti4�0p@�vʿ����<ٿ����&�?*�@	�>�$f?ĉL?B(羨g�?���̻=�:@�wO��B��:6�����?�u|���>3�>c=�?�։>t����پC1?�b�ϓ@n�ܾ�&޾.����C?�"K��b��N��K�|����:W6U�{���K��1	?�b�=��[?� �1�Ѿ䜿}�J?��V�B?�2Ѿ颖?8�Z?W�z� ���]{�����ʐ�8��;i?��?��1{N>��(�g���8���?�(Ц���"��?߸:?dy�=u�?#��?���?��:Pɋ�9����Z5�=,V>�G�?�!9�;�
��y�>������?�	C?}���B����9����ؿȠ?Kt�?]��? `*?�/˾�D���|.�Ġ���?��`��r�?���B.2�ܤ�?d��>��a�?s��
��?N��?��?��=��޾!��>�.�Rf���\Կ\X�?�2?7=��?���?���?�оſ?uP���+��1߽��?�'�>.�=y�E>;�Y� MX?ŧ?tCJ?F�?�Ϳj��?&���* >�,i�9�d?��?��Ⱦ���>�B�����>��@��?�"��x? �"@�gؾ�6¾���>��?���"NI����UĢ?�C�?*��?�o?�f�?���?�F?]�N������?�?=g�e�)U9=c��=�ݾ�WҿD\n�\v�?�t@�����?
r�>2�J??��?ĝ^?_Y�>�Ig?��Y�I�=�޾�5�I��oEh?�����>r���lG��
���I����Q�6��?la���?k�׿oM�>\���r�'��-~?�G��`�����d?�O>�dE@��;����^R?"���q��Wɿ��?��?%��>t��(       !��,p|�i�����l������=���j��t>��˻g��>x��=�B�>D���U�>#��=���uS>��!�,[�<2���G�"�H��u=��d>��8��Z>[ѓ���>e�4��'�>4s=�GE�/��<�ֽ
�,>���>nt;��HX>�}/<(       F�=^?�=����ѾNN=�톾�n�=���<^?��=G��>HG��څ=��>^��9�s�h�(�<0�k���?��[k��l[���&>c^�Z��>�e/>��!>J�2�/$��W2�c�H�r�I>��>��3�St��#��.��>����>&ȗ<(       �sٽ���>�Q>�=��3>���>v�<�@����|7��y��>�`���u��?�+��f��A��y�>�V>b>���\��v�=Ȥm��[6���t���>4ؕ>��l�1{�>�V>"�<h�=m�x�r�>́�>?G >�:I=2s�=�̭<��>       �Ҿ}������@�6?s�i?���=u�>�\�>9 ;���>�+�.�ཎ�?Y�>�p�"�9�!B�>O΀�6��cJ=��h�$Ľ>�5��ሓ���_�u�?�gl>=�I���=��?�e�={n��*�g>
�K�,I>�Q=�҉=�� >�
�>�Dv�\!��"�y>�m�>��� a�>�rx�o&�>��
������Ѥ�|��;�=���>Sa�>^f�>�hɾv;����
����=��|�D���2k=�q=<���z�s>�5�W1����>�Y� �>�
>$M,�Ӆ����?���%�>x�_>�4<��T�>�X���~ݾ|W��砽ϧF=q�߾A�	��G�[|�p��uF��O=T>L>>K)��蒋>��׾q=�J��>�-�=E�>����>��C?.�>6�=?�>�=��^_
����>�?�>�|�=��=�5���	�>�x����=꫼S_H>m��<��>��P=n�!��6���%�>��E��*�>f�}��������==Q�~�=�*}="kH>񟅾\��>q�e�P�����?�m2>q��>�|��L�!n+>�B�����>�=پ�ܙ�7�����0��_b��@��N�>�˅>�4���u>�A���d����
>��5��96�����ΐ�>p�>����3��z=G>~a�:���°�=c�=ʿ���)'>��F>u��A�*>��>����e�����,�\���o�"�?es>�3>���V�<I�>I^��w<��ݼ_O�=^ ��l���9�-��&=ƽ3��=�H�F��G�1>	b���.?ś>:C#?､?�:��/k��?m�<�G=
�A>�����`>�Ӳ������}����=T��>�N���9��]�O>,E�� �=�?B����=}��>Lm�i��>fȕ>Zi�>_=6�<�z��v�5��;Cʟ>�H�>���>�"��:F������Ӊ��u ?����`>LE>��>E��>�%=�y?��;�ݾ^�&��Xʾ=	����=�?��־�gZ=�Kb>�݆��H���������O����,=�`q>�>�͸="��i�=�ɭ<MzO��.���r�;�>�hb>ߧ=?͙R���ʾ��5>�yV>؊����)~����E�ǃ���I>���>���D0>bI�N ]��Xo>ugG>��Z>��<�厽���d���̻> �~?��=�=��>�����>�¾Is�>�@>n[�<6�ٽQR�>d�ѽ ���I�>��ܙL?n�Q>�_��ｴ*?��\�À=?r�>���i�=�&M>�|=��?i�A>ds6�.3���?>�*>�]>������9�ċD>�w�=��H>���<l�����>RC��!>x���@H>IH��_H�>�p�>	�n����6�>�.�>��{>�5��}�>WA����=�v�>T��>��!?�� �?:��e�=(�~��j�>JG�>BrX>����˚�z�Q>�y��k�F�:�>��<�]>)'�>��>�|��CԄ� ��>鴾	 3��;>Ͼ˽�u>%�A>�v�NHǾ���>44�=��=�JZ=S�>*)�>o�H�a�F>��"P�熾�������>�S��櫾���=�����E>��$>�ν��n>L�
��FT=��y>v\	?zt�=^�3=i&�k���W��4�7���)��>ͩ*�v$p�X���@�>2�=���>�!^=+�>�'j>�<����$�ﰾ)YL>s�\>{�����>�=��Q>�{>��<A�ž��9>,�2=��H>I�"��1>l�ʾ`�.>�u�>�Nн8����J�$������1��� b=��W�}�>tï=k�<�"�;�>Bw�Փ�vp޻�-#��a�40��4儾��#:g>�G�!��
8���==t�	>����w��>�-�>��E��z��`V1>f;����0�7��=�_f�%֯����=ȷ��L�=׍�I=����>">�,�>�T�����>Nd�>�&)�[�@>l-�>-K�>�<1����>�=>>��<p�A> �=�T�>����rH��J>!~>��0>5΅�ca�)Y��}�>֎+>�=I>���=�7z��ξ��>8m�=z������=$;g>[�;<�A>�>�-�/��������)�rY>>>�`5H��>񡁾��.�����15���l���
�K�*>VIU>�D!�L��>�^=�g�b�>j_�<nq�=U��A���B>̪�>?o��6�c>�{���w>�Ȇ�p�=	܀>��=���=�t��=�d�3t{>�u�=I��L�����M>5m�>� �=�͒�׈�E�~��v|>6���?I��뉵>��� ���wyE>*�Q�׏�>Z�q�j��X>�h���?���=U�H>M3�>��˾�M���<��u>o��5[>X�:�>�b���3�� �=^#�y�>��v�I>y坾J����!r>
#f>-��>Wv�s>*i:�q�y�x>�=�=~��Uͽ�=>�ڽ
���ބ>�h����}��=��>���=��o>�/ľ��3��R��*Z�>��V�7�?�~�=9�>��v>oɼP���}�A��N?l#�O��>] ��h����DƾO06>/�;=�@=>p2�y఼�c>ݸ��P"=��<�����@2>!��=��=N�R����>-��> >}i��ô�h��<3�u��X���߆> �>�=��о�s�>d��	��7�>=�ܹ>q�W>`&�C]�}w��	ٰ�|_>'Y�f�<�(?���>��=R�=�4�ک���F>23�>T�B�s�>���/%><>�I�����>iܾ�b�>,?>�8��">$���͓���P=F���i�=)�4���B+F���\>�_�>l���؁>oX?Fx
�a.�
��Λ��e��&	�������<D>$�>���>��>iړ�ێ��a����R�=�a)�jW��K�>�:>�R�=�Z�"U>�`m>���>���=h�Q=x�>��=���;E���q���k�>��Y=K{Y�O�>.�o��<�����=h�1>�9:����=�[���#�=��^��#|>��⾘>�&�J=�w����*>��A<
��)pd�8���lL�>�龱�{��D>���6��� �7�?(       뀍=�}�~��=K�.�<�h>"1�<u<w>0?>Ӽ���>��E>m��>�G	>�̼���=�hS�:�7G�=	�>�4���<mu�>�����@�������^�f�;!(	���.>�Ͻ��=q�}>~�>%����BG>�μ���S��<(       v~���t�q>v]>�C>	�->j~��m���} �>+�;O�<�í=R<n��J>�>�+>0ǁ�]��=3v��/^�>����vV}���Ͼ5��=���=���f�[>Y9X>���>vTJ�9'|>�/>=U&z���>�~�c|�>5�پ�Nl����      Kĳ�.�=5�� ?Oa=��->ğ>�Bͻ��=0�>uEq��I>�9=�%L����<<Ԣ=��;`�= ��=�O?1$�^>�����L�'6d=��!=B�þU�=&C�<p�Ž��>�4!�q4#�*�>�>�J��M�=�u�=�mm��a^=�M+>	\�>�
�<i"޾��7�	J>��R��2���2$���>���K�>�DҾ5�=�&�0��x��%�k#�=I�	?��?p����n�xUu�q��==O����D���S=~	e>G,�=�}d>H��=[=I{u�`s�>��U>�AK����<.P�="��>�-?��3>�hν�o<���9_"�Rt
��c�Ӑi>z1���?��Q>�<	�T!h=ଫ� X���H�=5������^i�<Η�=��@����*���J��D�>�A��s8���H��xV���W�>1�����>���6&^�Y:]>���<���e/��d�>�Ƙ�"͂>K�� ��8>\\�>��Ƽ�,־S�j>RZ��J�6�g��_�޽�&�=ۏ	�]��=��V���	��5�>B��=_�A>�������>~��>��Ӿ�Ƽ;��z�>���D�i�D�Y���S>��>�{?u�>v!����=&6�>�)>§�>(�9>@�����d�=w�ý�r�O����?�>�c�>�%Ծʜ%��d羕��=M>���>Ɛ >�E����=���>���=եQ�?)�>kP?񂠾ZfM��ͬ���_>V���c��*�=YA.?�qU���/�@(?>��> 0~>Ba�+��$W������>v3���v>B����=����+ܽܩ��}�>~0̽��>��f>t�>����ު�����g�_>DP¾dbԾ�}�>�`�=��V>�?�4=@��=��7�[N#��Sڼ��>�2�>��<>i>��D>b�r>�r�0C>�
��4�=ce�:���<�0=$D���뾞#h�(��>��[��0��In>�6�հ��D���Ƞ3<�9�>��=���xX����>c�� ���y>B�b�S%�8��θ>��<0Z��ɽ�|U[�D_J�ཱྀ<bˠ='̽����6���ׄ��'>��{~�<��e>�P����~>~
>�V�<�Ƭ�EW�=k��^�Ͻ_)�����U���7?4�)��W�=:�7�*�>a|�=��>���>���� �辷K���0�G��=�g1�_��>�)ɾ�`�>���=���=�r&��1� ��>qޱ><�C���Y�x�=>*�>�R̾,�(��>�[���X�ogC>>���Sc�,�콹?���y�ؾ�C�>۴۾�8l=fh%��7�>����w��>�����$o>vJ1>"%>R�>Є�>�+�=J�h>>���轔N=�|I�3Gv>�Z=�w���"�O�>I��=��ས[��n|��}ʫ�6�н�U���پϸB����>|�^>V -�j����?�k�=[�@>zm�>ѴD�M�_�(4z>�|�>u �>n�~=�h�>���=�۽=?�>�衾�>��/�5L��Ǫ>�(��v�f��\>|�.>i)ý��4����=� ���n=�L�=�n�7l1�̘t���q���>       E�X>!n�F>��~>�2?q��>�J>�N>�(���E>F��<�nG>��"=s�Y�%�>�ꗽ�t?��g>,:��A��8��>Nu�<�K�=ӆ�=�,־Ȉ=����m�>�_=F"��[�>�r���+�2C?��I=���:��M���?�>Z=�&˾s0����`�">y7�>+ >f�T=C@%=L�E>�Z�>+'־���<"�9>��>�I�=���;�.�>��<��q��
=�겾�H%>���=�=X0�<�����.=ʖ:>ZSk���R}���9=���l�����:��>pg�=s�,?3�r<V-���v>�}[=]Y%=k����>��=�V�⠭������B1>��_�Dq��!U*>J���~]:��ͭ>�C��Ȳ�
��̋�>8~	����>׌���h��=3�����f=|2��?�>�%�>�w>�h�D�ľ�j^=]G�=oZ�>�%>M�>Xm��*a��!��>.0/��=׾�y>�Nx>�G.>�[��?�/��>8Vy>��<�����'�.DY=e����BB�O"�>� ���4>�'��\�>.��="[>>8����U�NY׽�y>�=#��FS�>�ɮ�[�����>�!ý����멇>_�������x�=6=\�gt>>�B7��P����+=Ǥ��GF��P��Z ���/�>��H���>����:"���N��\ڽr(?��׽1���<��<6�N�bD8�
爾�)4>c 򾵒E?]�ػ�5U>�����;�j�F�ӡ�>m��=�vR>V�'�D56>�J�>#�8�e����T�1�>S���ܽ�yu���\��<�d��L��=I!�>S��>�z�>�?��w��<>��a>˞G>) =>��'�x���k޹������Qj>җ���-��q]?��*=˺�>
��2Ł�#GE���t�z?M"��v`��P�o�>��>TA6>�~y<���>*���~Bg>�����ة�>�H!�����=�ĉ�Wt��p*�>L%	�:\3��Ϙ>o�`�������ؾݽ�Џ�c+>,�q>�l�>���!a����n>�m�<���<s�����ǽj4>[��l���>_a�>׭�>>��k�0?6�.>=�0�*w{=�����O>2ӣ�.�9���=C�c<p1����������2���h>�wW>��1�=�h��6/���<���>|�=6��>������ ��/���7>ڜ�'���r�	�����%?)�>|a�>�8�ӌ>�0����L �rK����b�����(�P>��k>�XF�0�K=��>�A'�d.C��>�6N>������ ?}�;>Ue>�`�>�b>N�>�v�=�l�=٪8>�Iڽ��>�l#�@�J���?�ܽp'�<�P�غ�>!�=�b�>1A	?W�+>�U=뻖��E>���Ƚa�ֽ�ⰾ}�D<��>�/��>GC����ѡ>҅�����=��+=E��>r/>8��>�����9�>:+ѽ�5�!�>=�c����>P4����P>`�z;r���vn= Q�>��{>	j�>��2���7>t�<��J?�V>>\���^�:�G�=X��>�?�Ἶ.9�:Z$?�(�e�=�>�B��^þ��.E'>�X=Rb�=�W>a,I�� H>�G}���i�@j(��Ւ�|�T��nԽIA!>M�=�{��{�+=6��=b-��h��z�?%սi�=�=��{=xw3�p١���콍����9n=�;�>'f�=�E%�=�_�n�4>�����=�x��h�r>D������,��ގ;�}����J
>�L��_�>�?>ѧ��Eێ=D�ȾS��&��>Bȇ���>>�ҋ�,�\>���=Zw(�P�=�o�=�yͽ�>;��1|���>=ODX���|>F�Z<.�W>��F�[^/>H<�C��g��"���AC>������>��=���>�Ƶ��k>��1�ש�>��u>�р>����(Ճ��҃=��>"H�=k7l>�u4�r:b<`�P>��/�C)�����=�ɟ�w�	>I-�>�>	��>�o*>ܜ�=�2">}??�c�}��>G�\>�{�>�S�>��>t�>?V6�_�i>�qp���%; SV>�NM��? ���>#>X����{�*�>�v�=Kݘ=��v>�� F.=�����v���->�@[<t�>t��=d=R>�QZ>
Wa>\��>�bG�8{��hˋ>�a��s����m>ľd�� �[>� �{�2� �<ݲ��ۘ^���B>d�Z��{��+s漼�4=XL��¶>'F�>PZ>��>���=j~�=��2���R�;��;w����{?��-���=,몾��@�J�><�>L�>�]��o����;'��=��,�f��>��=E���ݎ�d>�O�@D�=�^>I�<���>��7�!�p���>��
�o�?����=�|�>,�̾�ߞ>�>3Ώ=���>#�2>UF�=� ���>�i�<{�>$]%=_M4>b<k���E���0>�E�>��S�L�o>�E>]��=[m���2s����X�=�>�=��>�q�=����}�c�W�>��>�H�=�G/>?�A>�+=����x��]f"�!�J���=��_�K@����
���>�l�>et=��y�m����p�p���_�=eJ\>k+D>i}?F��>m�
=!Pt�oa �Ng?�A>��P>�	�.?v��A����#�>cǦ>M��>��*�(ģ>GŲ��̽��>�>���xj>�+&�׀�� /����Qp���n>�˂���>��=8>t�'��Ƭ=]�4���ߣ=<+�>�/v��Ц���e��V�_�'�o9��yQ>w���@�7.��&�5����>=Kw=}	���>��U��'Ž�I�,��݄о�j�>�ҽ��>�>KɊ>:!V�F�>v�>e�.?��>yd�6�#=v`�=��L���p>s�>_��+����΀���]�dּ=�wA>�O?Q�>�����3��>�����>��H���}b�>i�w��KRX=x_���P>3�.��ϴ�g���W�ֽe�}>z(;�%{'���Q�6�<H67��>�*ƹ�P��ݜ�l)r>E۽uT�b�;�_���¾���
%W<��_������l��
�>�-ܾ�	��y��W�>�J>��S��Q2�����
�<�^)>�Y1?8��>��,�?8��>B�>� c�(       Mհ=/A=p�>6��>�F~��|�tX�=b�>��?@�<<��<�i���4q���<;=A��>�q�񧋾�C��G>򖒽�!5=lk@=Gm����k������,+�<�>>5�>%~= �>�d�=p�F>�<�>LU�>�x?n�>ڋ?�\=(       ڝ >�G�x�s>�Sm����>�P>"��=2τ>\��>b��ྂc[>~9=��a�:l�|��>��;���������>!x=Z��=mu�$Bf�� +>�c���V<>�R��q�>Q�"=�@��G��1�>�������=�q>1�;�6���&l>����      6m>/��<D8�>�V��G��*���D=�N���;�<MG�Y��>㈉?� G���>إ>�'�=�ʿ?j��<��ƾ��O�ؼS>Pץ��x��)�?��%��ð>����	p��P�'���H�=��<,�>��p?�,>o�.?���>���s���%? �U>����F�>Ȼ�>d����=-P*�u'?�6?d���Q�
�)?O&�N�߾h�>~����3�i(>1�=���9	�?@?6=�I��I�����׽Ԡ_�t�T?�S�=u���?C�m�,��\��>��n���>P?��$]>�O4�l|ž�>T��>��0�ި�9�]���>	��>��2>0�������?��>Ԥ��5��du��M���3�i�$�㾡�>׭���eE�>�þ�?q�,>��?n��z^�>Q���!���o��=�J�=�@>�1~>y J>�m�>]<��ռ�u�>�Mݽ�./��Ԝ��f>�\�>T��=3	M?Ȋ����,�3�[;z�u��>��>�8�"�A>E�0��rl?ۼ�h?>�G�=�h��������>��_�4��=�>L�پ�\=<0%�������Q-�>��݆ɾ�F�>��R����>e�A����~�?�h>���>�貾NiȾ�������P��g>���=.:�=q?.��>�Ͼ�H�>%)�n��>y�<��M�f9?��~��}�?ۈ�w��{��>��ɾͷ>΂��F̾��/���I�Y���{��=���=~S��Oʶ�E��]4��5>��cڼ
�?x��>��R�;�>���=<,�=��=MO@>HZ�>w#�:[+��u�>���=�8����8?�r2?IY���ӽ���=��=ѯh=� ?�c�����>��9��оs#����mZ�>1�?�B>����?����<�h־,?��^�>��>!���g�>H�L�;&(>��E>�*��1�����e>�ۖ=՟>���>�ɾ>be���^>C}?b��>�>a�=�z��\D)>[�>���>����/��>y�U=i/0�}T>^m�
�<o?b?;�%��&��:�cmA?p��<��}�o1�>�����J�/]'?S����ݥ���e?����-U�'�>��R:��I?7Ĵ��Y�Q[ž\�>��K�:��<���͍���Lþ*p��;�p�q1>��>�n=:~�=�;���b�=G��u��=�(=�ޜ>���EڼL,=?=q*?�_/��	�>���>�`#��h2?�KM>�6�����w>W�ܽI��?�4x����>�b��N�b��
�� }k��.8��m>�̕>�
�>��?=b�?���?��F"2>���U�R<�?���D��[F?/�p��T>������?�TP>�Y�� �^n�������p��h�Ԝ���/=�ӛ>��H�����>El��p�G>��=>�m���ڤ>)��<_8�vB����<�J�?���a���z?��;���>�y���"S?c��xK#�P�3?�@B?\�*�F恽����tK���>��>��Δ��ۓ�4�=���>��> �D>Uɑ>��C�5��5�       ���Fl���&��T>o�&>>�8=�b(>�����ن��>=��>&༭+�@��>Fݘ=�����l�=��j���S���&��<t#��	�`���6<�<��=A�V?"�>�z?����G?B�>=�ƾ�6��������Q�mb�>��3=㩗>,���:=%�)2_�0R>ݒ����R>���;�'^>T?����B�ѯo��Ҳ<�]Q=���<ё���{��MU�#u�>�[�>W�[>!�ֽ�|^>�*K=��辪ك�#��<#%��ҭ��e��(���Ԩ� �+>_�l���d>��>=P�ub�7�Ǿ�FC>a���)?��U>��龌�Q�~�=�<�>y��>�]��]�=�/?�} ?����1�[��/3>D�<(�Q>֫>�Z�=T�
<g'�=ji6>]��=x:�;�>�f�9��>�>�.�5�9>�x?����F�>+=(&�=oQr� '>f��������>Đ%��6��XVϾRɾMw�>�|>2�Z>{Lu>yLZ>���=��ӽ����'r>�	>�J�=�B>>�:�8�<>����S<2zj��cZ>骾`w>X��4�/�yPk>�)�~�� �޽�Q>��)�M#�=��=���="�ɾ�3���sD�;Hƽ�N��_�þ��=>rDC�{?!��떾R/��}Ɣ�_,��}��Y6�V3>*�ؽ,Y���J>��M�l�$�kk�z�=cXK=C���$)���=qP>�C�=�?�>��P���5�?����׾A܀>�ʠ��y*>���o�>ó����D�=��"�>�t։�BӬ��]?³>�0�>� �>BtH��q�>�c	�jZx>V�����U>?�B0�j{��q>�|>�G�����>$���d��b����{=�T>��<Ќ=�Ϫ�g�y=�b6�Rz.��\C�&��=�u>���>5�>�����ዿᢴ>(=�B�G���$?����'�A>��R�#> �ݾ��=��ؾ�0���[��>|\�e>���>��f�Ⱦ�Bt>U��f>hk�>`)�;$�9>�8����@?s(	=���=y�>e����8>���I��i ?�ח�"Ƕ���>KR�>��<�K��l?�	[�G�1��]_��9��#�����<Jdv�\����������MȽ�/�=a��=�X��k���־>pZ��t�Kb>��ؽ�|K�/��=0	n�i�o>Z��>w����9>g!��p��� �̾+��=w�L>��=R�!��a�*�3�q��&qǽ���<+��=���=���<X[�� ~>�Q��c���?>%*�<�3=��L=뵙�-����qo���d3�=Hżt�Z��æ=B�N�22�>F:�<����>�V?�ą�A)�>1�4�і?-2ľ�J&������ݮ>�%J>�S>}�P���4�\���>�	�
9Q�;��=�F����>f=o>G�>���=)�ֽ�$6>,�4�Mź=����=�>a?=1S>T�>UK���>P�>�k����=��=���<u��V�3=�
ҾC��K� ���7=y�t�ߍ>i�$�s��>�|�jB��0����ž��� ֽJ1Խ�S>�P�=g�
<�&`>�>6]L>Ay���X�>��� �>��#��"`��zY�>�P=�Rw���׾w�{�[:��x*G>D/>2s�>a�=Iǲ>�ಽ����j��G��Y����U2>�����z��ނ=���>*����s
�J:����ZC�=&r6=xQ�=�Χ>,m�@<�FJ�=9덾��=@E�v��>��>�=CT��D��=j����呾��>ʼ�>*�v�������>�F����n�>_����^��S�>{->%�ؽs-�=�;>_�ᾷC�>�.�����>��Z�Z`i=*Ꜿۮ#���<�dz>�La>�@6=?��3(������P�A����E�������>�&?�	>��Ҿ��������<ɾ�
�=�B�>U�v�N�n�>�=���e=y!ӽ��=�/ؼ���uL>kϰ>1s�=S~�=xk���Լ��þ�L�������j=�ν��e>U\J��&�(}ӽ�:$�Mi�c-B>���>��e�n�=tw����=:cz>y?r��>&�[�b4.>hd�9F���B>�_彏me��æ�&陽�ើ��>f�?>8y=#A�;���I�(>���3X�=�
����<���>��E>�`���8�=[ ˽Ԟ�=��׽��=�������>1�W>��= �ʽ-4N�u��=�J|��ҽ�(�����=!��<��Y�uv�����\	>t���b�=f�=�/�=�޽�;Y>���=�ő��p�>��c�d��~n���>9���t���=O-�<�ek�S=�y@�E�?�u�B>��t>�*F�tu��o�|��&�����>��#�w�>�&U>�JG�?d�>ea(=��<��侦G�>З�>�[����>����b�7<����=
([>�M �vN_�j1� 	���g>B֞�G�$M,= ~k>e(�Q��>��N���e��}F�@��G���S[�V�>|c'����=.6���2>������(�R�<��y�0��Q.��C��r�> �����������2U>?�>�V{>g� �ZU ���=_賽x4v>�X����q>ו���.Ǿ�5�>�&�>��	?��� ��=���>�?%�+?%4���0�=t�?��%?�,_�@̘=xk��79�<f��>;I=Kc��������@�>2�
�8�=��[>�-%=�M�E>w���h�=ͧ�>�J?��>gg��G]�C�y>k�����b>�ͯ������I���-����.>_}��<M���O���Os<{�m�Ko��to��!�<����ʐ�i�=;I���O�� ��$>ph����缄�1�*���_����%|���=�w��B����H>�^��aS=�7>x�����}>�*$�W^a�<Y%�[�b<����*��byo���t�S?h>����S����#�9�p���c�����w>��B�8��\=�g�>�=���p<�{g�1/�r���̔�c�=D�<��d>q�#�-r�����ƚ�;�8q��HL<�ϲ=��#<��Ƚq �=Y�]�B)�>o�>h����}z>�4z>�޹�랾3C����>�4��$�K=v�k�3���Y>5?�=(       �H��u�Ƚ��t>,n>K����>Zk��Y��=U>���>�+��u�ھ��=����+K�47�=�k<[ ��h=h\8>f�{�QP;����>1�{���}��>j�(���=���>��>f���4U�=��>E9�>4�p;������;R>#	<���(       _�þ�3W;�)�ͩL�聂�[L�-,���<G��>eD�>��]=���>S?�x�>��y� ���A�<��=,� �ZV��웫�sݽ#�Y��zK>H����g>3Nν8��>3�@>�s>�:>��;����J&?����J˾��.�uA�>���>�����      T����9>��Z�hF~>�㐾���>�B�>A�뾜���*�����dzJ=��u>�ҽ���cy�X��'C�=�ý�� ��A8>��<շ��8�l����>��>�,&��9���(ξ��J�2 �E������>�xy��1�=��e������m=X
��7 =L������dl�=3�]�~�>:��>�?��Q������13�
����IG=b�ٽ&I=N�p�Qཥ�>��|>zr>���>Z�V�6@��߹߽������=�о��e�H�t��J>~��>(M�>�,>�,(=������Hl�>Ps��8�Խ±�Y���.���J����>pT��la��o���=?r�Ծ�u�=��?Bx����>e�>��>�Z��콾�J�>�6E��>�``�?$v=�|A��q�������!=��=�\��i������
�4��>�7�<�ˮ��䲾�����i�0A]>Lu>;L�\�.�@����J����>'���<g��>���>�4�<�s�4�d>�᧾�G����)��#ʾ��=ҡ(�2"��W鮾*�ֽ-1h>����.i>�0�>����53��λ�߽0����=Ϲ;�i�T>�GH=�\>n������<���,
�e>�e?����}b=����z6�=)�>M��:'1��顽�?�=3F�h ��v?���>��3>Խ�����fs�WG[����n����4򾆍���>Ԑf>ߜ����>�W�+�>�>�<��O�>@l�>u>�?�>`�>�<a�j��Ľ����>���:\�)x���=��=�m۽���=�����!�¢l��?|�>�)`>~qA>*���r恾1>1
��H�e��lC�{ ��!�,>C �>IX>��=��>��_>S �^6�=맒>2&���5c=���=g� ���=�>�!���=�� ��O��>�V����>����S�=o#
��V�����W�=�{z�MtX����>5�W<U|o��e�=��ս5��󲳾���n��> I�޻=?{���\>f�|>�g>�o�ǌ�> ��-���ᘾ0�����j���A>f��� ���*�>��W5�>Ү��W>��>>X��=��T�����?&>��u<�X<���>2�=�ï�>*�����>DZ>�ǎ��,�>�>ѥ1���½�����Q=.��>�x��[;�@��Fr#�7}�>��T�~ $?
�>M����@���@c�T��;8J�>M���MV�=������x�g�Ȳ�=���>��
�n3��7�dQ����=�>~��<[4�>�}����lnϽ�*�>�p>[ȓ>ϵ>�ȟ�����=����u�=���>x�ʯɾ֖�� R=�=Q"?�\�>k�>�譾�>��?|�0?����0�=���K=�HW��|��=D��?�Y>�z>�D�֍�r��g�Ƚ&q�/;%�o��>`����Mx>�ب>�<w�$��=?�u���w��^>���>��A�Bĝ<2�ƾ������t�>�N��\t=��J<�ts>QuE>�<N=�<=^�\���νc4;���>�IþA������>�넿��E>(       �MS>/�T>��z�qyR����V��>���<?\~>�B��J�罻�þ��w=r�\��&�>�>}( <���r>��=
(8=��%���ҽ���>t��}�>��,��'��i;=.<��NǾ�>?>{�x=8�>{v����>>��>�ʥ���	�.�
�(       ��>�#�>���<������<�j> =�>�Q6>= �Q�=	��=�/�cN#>���>��">B�0�a8Ⱦ�F>�dǼ��?��u���P��=���gq�=�����W�>��3>5x��3��hh>w�=�??�EB>�����o��d�>�����c<�:*=�      >�e?1
��`�>��>z��>~I<R�??`�n>״��U>�y ?�z����g>�8Ͼ�|>X-e�-�K?G�#�ѩ7>&��.q<>�A�=mӱ>-p>j	�>Rj����>g������=�CD��o>�I6����kI~����=!�;�{�>ͦ�>'�>n�D>�#��G��(޾3�%>J�x��e�ۏ3�>���Dk��^/!����鼨>��&� g�=��2>��>ˣF?��#���ǾY
��d�><v�+=��&>N�
?�𛽇xe�C��>�Rb?�[���O��P���,`>�h}����>=u��䣿���/E!>��о��.�m%?�:��H��>�ܽ��~�.ý�c��ak?�B��'N�!��=@�������7<���+>^�?r�?���D��=���Ӝ�<���>�f.>|߷>ݹ��)���Wv��cg>����d���s>�M�>1�"���ʾt�&��
��}�н�!j:Т�>��w��!\��t�<!�>��"�`��>���>h�Y�������>a{�>�7�>P��=3g>�/�>�A>~�S?��>��+�>�-c��G�>�߀>��>�2��?C?�o=��#����>#:1�c��>�7�=� F>e�?4L�����?�">�����~�=��\?0��5>?����m>����>X!>W�9�>�j>m��>���><���놿�c��߯�|����/�>��a˔>5yZ>w�0>{5>�8�>�0?^��=����>��E������;��>-�%� ��$\?5\?P��ލ�=!��=�F�D^�=�_�Qu��mx��;����覿�=�4�\U0?c-j���>u%��bX�=�|+?�&?p�^?�>��>�H�>��ua!���p>f��>�gK��f�>G�Z>>���p���3��f�<e��o���3~�{�z>�ѳ����'u�e�[>���==[C>+g�>&��jMc>�K�>�� >�c�l)�>�nپ��>����d"�=�S �9���+R<=?�0�>��M>xk��q
�>�z5�ܖq�e�o>�L>�p��:޾��4?���<�X�>��d�(���`>��D\=K��>%������Pq��uD�>�-;�E�� @>'�>�6�>�
a>N�þ��?�E�=�%���m�>[�>>�@��徽>�[�>��>�|ؾ��q����>-[+>!?�.�n&?�>���=�����N�?
k�����=C~/���>>�>�|,?E<Ӿ����L�q�45�?lվ=T?����(g/��k>��?�»���\����$>�����I�=�����W��k�>��>��>8#f�t��>���4�>,�=�c�=m�E�y���Ϛ>mݾ=僾���gٺ��["��&�[Kž��<�1�!?��w�$�ս(;<Y�>�2p?�ž�rξ���=ѩ��?��g2�>?R?L?���>��>�?S7?rl;���>�@׾e
?R����E>:�?�*5�gᇾ2$?��u�-4ʾ��?>�e��6�����8>�^��r��>�q��s� ?�֋>�E;^�(?�=>�"�>��뾦�>`E�?�j��kȆ>X/�=       +�Z>"̫=����=(��>�������>'�>]�=�`{>�
�=�e[�Y��<=�<�;�>L��<i��=N�x����={��|l ��kL?s����6H>�s?�E��n�:.�o�[ �>�~���f>�>mQ3= ��=Di���)F��b�>
�p=v�=!�KT���<�>�@}>��Z�Pb�>���>���=9* >�������|�^�d�������>T=>e~>w�������[��z��>y�>�W��]D��C=��C��<i�>8�A���]�������>�Ⱦ�ӽ=k�#>���A����;>�`�<y�n�q��X��6E>�?��܎�>�>Q6>e)��d��A4��ؽ�������=����l=��>]J?��>䞴���������>��>�~þ��L��L]>�KM��j���[þX�D=�K��T����;�PO����}>�ߓ�@=79н{��� ��>S��-���4?G�a=R��=�<>��H>5?Q�(?u�߾R���>�2�����=�}E<�g`�WH)�}!>�`�;ېI�u����>��>�=W��b"������?���
���`=��>h^ԾҢD��ӌ��Ƚ>�f��W-��f;���3�=�0�>}#�>�6}�?	d>�+>Q�;=�H?]]�> ���ص>�3��]��0����=�;>y��>K�R;V�@>��;��a>�>����Y�V�=�ƾu	���(<_1�s߽뭽�e���8>��T�1T�������p�(w׾7�=�K[>:~����=h�>��>���>ͭ��	��+?�F˽9�!�Д��� >���Z�=vM�9�!�J�D?y�=�H��-��>D�>[�A�����n>�7׽�>q�Ծ�=;�>�����֚>Ns?رF� ��=�Ql�hȾ��x>��]�3S�>Kq)��_��پ/�V���,<���<��>�vq�$>�M:>�6���m�>s��ϼ���>���J9��d�>�}{>��m�n^��I�)>&�0�3> ^�O���-2>Ǹ��4;;��2���LD��=n���D��=����SO���U=�I*� >R���ځ�=>�=jY�=���>Pz�:|G�=H�?ٟ&>��%>Re�>HZ@�[�u�FU�=z?���>C�>hV���ґ����>Ĭо�/ɾq�ҽ?�1���>~��>��?e}�=<U���M">ۋb>�?��I��9ľ%PG=�!�<�_"=���>a�v�״�>β�=�X>��h��(9�if�>+s=_���h�A���>M>|��>�Ͻ�뾙NF���<Mo�;h��3=�<�l��/�:>� A<2И�g�Ǽ|m!=��=�Os>��>N}���S���G<�73����S��������*���������?>� � �̽<����>�s�>�1S�u[�������Ho�:�U>�}q��
�)�v�>��>t$?�y>�P>󴾢D�>ۗ���R��iP���.?�C>�3o��B��	�=c��>��]�[>OB�=�Ra�lj}��펾wg�=6I����=Y>h�b��=�S��w��������=�����%->�۪>�o�=�!��3���X`>=�Ҭ>��=T��=�><��>!Y8��g�=���=i��>\9�=�PJ���
�s�l><74�{���L��=K��J�?����>��o�2��C�=�����?>r�>� V>����pQ��1>�����սP�/=�W��=&>���@>�Ɉ��N��='U��M�>�ft>�[�ޒJ>�"��]�>b[ؽ �^�k��>�	>due=#A�^J<i�i>M)><�-=�,p��:��+�����A<,'�>� D>y�^�'4R> b~��c����K����1e�=�*�b�>m��<i�Ƚ���BM�/~>qd�>�03?�X�>��K>B�޾C�>�����!(�^k8?bY;A�=�c��jn":#���b�>FKR�Y�>���<X ]=/J�=�~��\I�=�Á>���=�=�F�>�����p	�.MN=$�<�6�>�����i>2X��2 >G�ҽDc>{>��9����U����+<O�-��=AG�<�:�&L>KRr>���W�뼝Ѿ�r׼��=�H}�R���"��
����4޽�Xo>�n۽��G=�R�=6�>��M=�h�ϑ[���>F�w����=Ln���A �3W+�'��=�>�U¾N�ɽ)q�c�=<�>՛�=�'�g�H='�e>]��>G�>���>�[����`����>��[=�{)>[�j>���>1Ή=W6�=��Ⱦ8�>ބ'= )�;�ƽ=�V>�s>��V���%>>�>��=Tlȼ�Ah>iC=����)N�=�e�����b)?Y�?>�$&��$Q>��=����;�< ��0�[�č'=kI?�?�>)h+�Dn�>V�?�����_>�rn�1t��P=� g>�iU>dJ�>E�a�����:홊�+=�R�?$�=�Dm>C�V?
D��ε�=k����=�H�����Bb>�
?o�>:Ƣ��S:�p� :"M��|a>k����>ӄ�>*�O�� T�J�y��<ı�=�;2>^�[��)=oR�Xe��J#=�=�1c>{�Y>��a>�N>�ڐ�7�@>��>_#��A���誡:�Wy����'3^���:�\�a�O7>��F���C�y���_>T@�=��.>��>� >X7=4�B���	�*��=��)>S�>+�>ͯ��e  >a�;=wIL��I>�\����9N >�Ff��N>&�!���H���u��[L>����>8�>;�?�^�u��>�Z�=M�Q>h��f�پ֢�>a�1ƾ"�=K�(a���ؾ�&i�=�>Fex��R>��ɽ'�ƾ���\<0#��#�>4l�+�?���>��c=�7��P�̽�;r>��>��F��`�>"5��������>�9ɽ��~>U2��K=ԝ�>�˲=vFо�@>�#�=�XE>�ԾV�Z�#w��sf�<�?�����0�>�U��HƧ��/�>,��x_���x���>_*&�e��>:����E��)�0=�L`>ӡ�> ������q�>-����>)f��fp�l���>>�%���=��>e�)>�o��m�>���Wv�DFV��.�5��<�=�.̾e��e >l4#>M�ܾ��	��dB>��>G搾���(       ��7�	x�>�>�����?z���5��g�.<��c�>�>{[!�Io���=K޻���a>G���L�gl,>Ji!��׾o��������.��h�>F~���;�,:>�=>_�{��6S�0��=05�}{����>����è2>��>�|2�_5�>h�!>       B>m>4L!?�~*?���>("?��8?�i�>:��>9�S?/s�>�/?�p:>�K?���>��?�=��?	y]>*S�?���>       �ф�8i<�h5<x�d;z����<�m�<��<:+���>޼����і�<�p������[0<�v=L�u�K���Y�;       �(�;O���Wؽ�&?�d�}=�c6��L�>h�=,�+>��b<i� �B��>����Y>��P��c=|c>B��;K֞�       �+�<�5 <i�<m�;�<���<˰�<�m�;&��<��<�a\=�M�=*]�<u{1<�A=�$�<��<���<��;p#<�      ���1��=Ӭ�>�1��l5>�A#>���=p��=������>xZ1��� ��T���[>�^�>\�f>���>U.>���;.�=�H�>[�=�� ��f��V!"<�ȥ<��d�F�Ƽ�'�=�0>�%�D
D>�H��S���K>�߷<v�^��p\�����W�^>@�[>͗>RP>�<`���z=���Ad!>\����P];����X�=)�ľ��>2a�<qQ��J͋��-	����$9�>�=��x> �>���=s��[|=�$b���=͑>��P���>�ἆ��<��O<��6t��>�۾�Q�<���=C\���P �8�������i�>i�#>��=D_���m��U=�Ho>6�Q>s����t>|#��>Km>ЬȾ6 �{Pn���=�Ȓ�,����p5Z��1�> ">Ñ齙̾���H����SV�=��&��U	���>!�����N;;���y����!��l�>���i��B����9�����
�>���:��=<l�='�V��?zZ���)>��=W��#�	��AQ>�{����[��K���=.��ǂ=�aR���B��5ͽ2���8�>H�!��}>Te�&�*>װ��CT�x>��>[�>=͔=�N>_8��}<ʁ	>��m����<sř��@�Eb�$4:��e>�ː;x����@>o���KH�>�3>J�`�����N��0l��	!5>km�=%u�����=B�c=���=��~��],{>�1�w���(������=Q>��վ�|ӽ�>v"?������>HR��q�D>E�N���`>��>褈>aZ7�-S�8х>��=�;ż�i=��:H����t�x�>�5;���ý�\�>܅>����->�L���.q���>c'>x���f>�2j���߽��׽��1��e<���>I)��'.��n�>~���c>�G�>7�P��?����j0���C��Q*��n�����=S�I>]Oڽp��>\�<�3;�>�3�<�.���)��ٚO>p��y=c�پ62>�B�=��=D�O%O< <ʤq=�<6�	=�N��G�U����>���M=�a>.i[�?k�0K��I���T�>֝=��5>$��>c[B>eW>�����ɽ��V����>�8m��ɥ���:�j�=&��Є>�QV��t�O����Y>� �3&��΋�=J��qlF�/sվľ<T��>ɘ>�؃>o����=�9�>Bc��8��["H�Ȁ>�=�L�<�3E��QP=b�>�������	�'���7�aMg=���ӂ=<!0>��==���p>. �iU��>�>W����ž�y>�lþ�b��S�����������=}֬��_:>�1�>X;X��7>5a���K�>�t��#� C>J[ɾ{Yy���=-n����-��Y�~3�c��<��	>$]�>�<�нQN��b�=⼰��{�= 6V<����	=��ƾ<a>�H��G�>�U�=��>��e�A��{r��">2`&>b��(L������"�=�x>� û��R�e����t>Uٝ���>�6ý�m��>&? 3a>��6�       :�<+��<C� <4���>��+=b��3�Ǿ������z���ʎ�=,J���鍹��>����d޽{F�;�==JP=       ��"?1>�>��v?�P?��?���?�4�?F��?sj?uT�>e��?!�?��?�y�>�5?��_?xb�?�?�?�Z?�>x?       �+>�>�>�>�w>3l�>�: >��'>}��>{�>�k;=~#�>�;�=t(�>&͗>��>�8>�7�>q}�>���>&��>       X�=n� =�ƫ;���w�><�R<Pw������dX�г���<�ݖ=M�,�t(���>��o�ɽƽI�v<D�=�@?=       �q�>�e	?-�>vcG??��>J�>6�3?��>��?�?I?ķm>��?��?�V?=��>,�>n��>���>wy;?       ܵ�>t^�=u��=�L����b>9��>�2&��W>��K���o>�6ս�>���� >���7}���l3�8������\C�       6��=<       ��쾮�?�`4����=�:E>GW�>#@? ��tj��#v���'������(?.V5�Y�z�qL�>���##?sك�H����>*a1?@z?]�ɾ���>3�>j���<>����>�Lh�+���&x��G�$,?!�<�Ä�<���>k�Q�U���.��?Q߲���)�--=�_����)�<՞�>m��w)?m��>��>�&>�*��ق�>Ņ���{���@�y��>�	?       ����q�0��=