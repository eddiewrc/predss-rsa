��
l��F� j�P.�M�.�}q(Uprotocol_versionqM�U
type_sizesq}q(UintqKUshortqKUlongqKuUlittle_endianq�u.�(Umoduleqc__main__
myNN
qUpredCombined2_new.pyqT�  class myNN(t.nn.Module):
	
	def __init__(self, name = "NN"):
		super(myNN, self).__init__()
		os.system("mkdir -p models")
		self.name = name
		##########DEFINE NN HERE##############
		self.e = t.nn.Embedding(21,20)
		self.r1 = t.nn.Sequential(t.nn.LSTM(20, 10, 3, batch_first=True, bidirectional = True)) #(batch, seq, feature)
		self.f = t.nn.Sequential(t.nn.BatchNorm1d(20), t.nn.Linear(20,20), t.nn.BatchNorm1d(20), t.nn.ReLU())
		self.rsa = t.nn.Sequential(t.nn.Linear(20,1), t.nn.Sigmoid())
		self.ss = t.nn.Sequential(t.nn.Linear(20,3), t.nn.Softmax())
		
		#metti un batch norm dopo la RNN
		#metti un'altra RNN dopo la prima?
		
		######################################
		#self.getNumParams()
	
	def forward(self, x, lens):	
		#print x.size()		
		e1 = self.e(x.long())
		px = pack_padded_sequence(e1, lens.tolist(), batch_first=True)	
		po = self.r1(px)[0]		
		o, o_len = pad_packed_sequence(po, batch_first=True)	
		#print o.size()
		o = unpad(o, o_len)
		o = t.cat(o)
		o = self.f(o)
		orsa = self.rsa(o)
		oss = self.ss(o)
		#print o.size()	
		return orsa, oss
		
	def last_timestep(self, unpacked, lengths):
		# Index of the last output for each sequence.
		idx = (lengths - 1).view(-1, 1).expand(unpacked.size(0), unpacked.size(2)).unsqueeze(1)
		return unpacked.gather(1, idx).squeeze()
	
	def getWeights(self):
		return self.state_dict()
		
	def getNumParams(self):
		p=[]
		for i in self.parameters():
			p+= list(i.data.cpu().numpy().flat)
		print 'Number of parameters=',len(p)	
qtQ)�q}q(U_backward_hooksqccollections
OrderedDict
q]q	�Rq
U_forward_pre_hooksqh]q�RqU_backendqctorch.nn.backends.thnn
_get_thnn_function_backend
q)RqU_forward_hooksqh]q�RqU_modulesqh]q(]q(Ue(hctorch.nn.modules.sparse
Embedding
qUA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/sparse.pyqT6  class Embedding(Module):
    r"""A simple lookup table that stores embeddings of a fixed dictionary and size.

    This module is often used to store word embeddings and retrieve them using indices.
    The input to the module is a list of indices, and the output is the corresponding
    word embeddings.

    Args:
        num_embeddings (int): size of the dictionary of embeddings
        embedding_dim (int): the size of each embedding vector
        padding_idx (int, optional): If given, pads the output with zeros whenever it encounters the index.
        max_norm (float, optional): If given, will renormalize the embeddings to always have a norm lesser than this
        norm_type (float, optional): The p of the p-norm to compute for the max_norm option
        scale_grad_by_freq (boolean, optional): if given, this will scale gradients by the frequency of
                                                the words in the mini-batch.
        sparse (boolean, optional): if ``True``, gradient w.r.t. weight matrix will be a sparse tensor. See Notes for
                                    more details regarding sparse gradients.

    Attributes:
        weight (Tensor): the learnable weights of the module of shape (num_embeddings, embedding_dim)

    Shape:
        - Input: LongTensor `(N, W)`, N = mini-batch, W = number of indices to extract per mini-batch
        - Output: `(N, W, embedding_dim)`

    Notes:
        Keep in mind that only a limited number of optimizers support
        sparse gradients: currently it's `optim.SGD` (`cuda` and `cpu`),
        `optim.SparseAdam` (`cuda` and `cpu`) and `optim.Adagrad` (`cpu`)

    Examples::

        >>> # an Embedding module containing 10 tensors of size 3
        >>> embedding = nn.Embedding(10, 3)
        >>> # a batch of 2 samples of 4 indices each
        >>> input = Variable(torch.LongTensor([[1,2,4,5],[4,3,2,9]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
         -1.0822  1.2522  0.2434
          0.8393 -0.6062 -0.3348
          0.6597  0.0350  0.0837
          0.5521  0.9447  0.0498

        (1 ,.,.) =
          0.6597  0.0350  0.0837
         -0.1527  0.0877  0.4260
          0.8393 -0.6062 -0.3348
         -0.8738 -0.9054  0.4281
        [torch.FloatTensor of size 2x4x3]

        >>> # example with padding_idx
        >>> embedding = nn.Embedding(10, 3, padding_idx=0)
        >>> input = Variable(torch.LongTensor([[0,2,0,5]]))
        >>> embedding(input)

        Variable containing:
        (0 ,.,.) =
          0.0000  0.0000  0.0000
          0.3452  0.4937 -0.9361
          0.0000  0.0000  0.0000
          0.0706 -2.1962 -0.6276
        [torch.FloatTensor of size 1x4x3]

    """

    def __init__(self, num_embeddings, embedding_dim, padding_idx=None,
                 max_norm=None, norm_type=2, scale_grad_by_freq=False,
                 sparse=False):
        super(Embedding, self).__init__()
        self.num_embeddings = num_embeddings
        self.embedding_dim = embedding_dim
        self.padding_idx = padding_idx
        self.max_norm = max_norm
        self.norm_type = norm_type
        self.scale_grad_by_freq = scale_grad_by_freq
        self.weight = Parameter(torch.Tensor(num_embeddings, embedding_dim))
        self.sparse = sparse

        self.reset_parameters()

    def reset_parameters(self):
        self.weight.data.normal_(0, 1)
        if self.padding_idx is not None:
            self.weight.data[self.padding_idx].fill_(0)

    def forward(self, input):
        padding_idx = self.padding_idx
        if padding_idx is None:
            padding_idx = -1
        return self._backend.Embedding.apply(
            input, self.weight,
            padding_idx, self.max_norm, self.norm_type,
            self.scale_grad_by_freq, self.sparse
        )

    def __repr__(self):
        s = '{name}({num_embeddings}, {embedding_dim}'
        if self.padding_idx is not None:
            s += ', padding_idx={padding_idx}'
        if self.max_norm is not None:
            s += ', max_norm={max_norm}'
        if self.norm_type != 2:
            s += ', norm_type={norm_type}'
        if self.scale_grad_by_freq is not False:
            s += ', scale_grad_by_freq={scale_grad_by_freq}'
        if self.sparse is not False:
            s += ', sparse=True'
        s += ')'
        return s.format(name=self.__class__.__name__, **self.__dict__)
qtQ)�q}q(Upadding_idxqNU	norm_typeqKhh]q�Rqhh]q �Rq!hhUnum_embeddingsq"KUsparseq#�hh]q$�Rq%hh]q&�Rq'Uembedding_dimq(KU_parametersq)h]q*]q+(Uweightq,ctorch.nn.parameter
Parameter
q-ctorch._utils
_rebuild_tensor
q.((Ustorageq/ctorch
FloatStorage
q0U36048080q1Ucpuq2��NtQK ������tRq3�Rq4��N�bea�Rq5Uscale_grad_by_freqq6�U_buffersq7h]q8�Rq9Utrainingq:�Umax_normq;Nube]q<(Ur1q=(hctorch.nn.modules.container
Sequential
q>UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/container.pyq?Tn  class Sequential(Module):
    r"""A sequential container.
    Modules will be added to it in the order they are passed in the constructor.
    Alternatively, an ordered dict of modules can also be passed in.

    To make it easier to understand, given is a small example::

        # Example of using Sequential
        model = nn.Sequential(
                  nn.Conv2d(1,20,5),
                  nn.ReLU(),
                  nn.Conv2d(20,64,5),
                  nn.ReLU()
                )

        # Example of using Sequential with OrderedDict
        model = nn.Sequential(OrderedDict([
                  ('conv1', nn.Conv2d(1,20,5)),
                  ('relu1', nn.ReLU()),
                  ('conv2', nn.Conv2d(20,64,5)),
                  ('relu2', nn.ReLU())
                ]))
    """

    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)

    def __getitem__(self, idx):
        if not (-len(self) <= idx < len(self)):
            raise IndexError('index {} is out of range'.format(idx))
        if idx < 0:
            idx += len(self)
        it = iter(self._modules.values())
        for i in range(idx):
            next(it)
        return next(it)

    def __len__(self):
        return len(self._modules)

    def forward(self, input):
        for module in self._modules.values():
            input = module(input)
        return input
q@tQ)�qA}qB(hh]qC�RqDhh]qE�RqFhhhh]qG�RqHhh]qI]qJ(U0(hctorch.nn.modules.rnn
LSTM
qKU>/usr/local/lib/python2.7/dist-packages/torch/nn/modules/rnn.pyqLT<  class LSTM(RNNBase):
    r"""Applies a multi-layer long short-term memory (LSTM) RNN to an input
    sequence.


    For each element in the input sequence, each layer computes the following
    function:

    .. math::

            \begin{array}{ll}
            i_t = \mathrm{sigmoid}(W_{ii} x_t + b_{ii} + W_{hi} h_{(t-1)} + b_{hi}) \\
            f_t = \mathrm{sigmoid}(W_{if} x_t + b_{if} + W_{hf} h_{(t-1)} + b_{hf}) \\
            g_t = \tanh(W_{ig} x_t + b_{ig} + W_{hc} h_{(t-1)} + b_{hg}) \\
            o_t = \mathrm{sigmoid}(W_{io} x_t + b_{io} + W_{ho} h_{(t-1)} + b_{ho}) \\
            c_t = f_t * c_{(t-1)} + i_t * g_t \\
            h_t = o_t * \tanh(c_t)
            \end{array}

    where :math:`h_t` is the hidden state at time `t`, :math:`c_t` is the cell
    state at time `t`, :math:`x_t` is the hidden state of the previous layer at
    time `t` or :math:`input_t` for the first layer, and :math:`i_t`,
    :math:`f_t`, :math:`g_t`, :math:`o_t` are the input, forget, cell,
    and out gates, respectively.

    Args:
        input_size: The number of expected features in the input x
        hidden_size: The number of features in the hidden state h
        num_layers: Number of recurrent layers.
        bias: If ``False``, then the layer does not use bias weights b_ih and b_hh.
            Default: ``True``
        batch_first: If ``True``, then the input and output tensors are provided
            as (batch, seq, feature)
        dropout: If non-zero, introduces a dropout layer on the outputs of each
            RNN layer except the last layer
        bidirectional: If ``True``, becomes a bidirectional RNN. Default: ``False``

    Inputs: input, (h_0, c_0)
        - **input** (seq_len, batch, input_size): tensor containing the features
          of the input sequence.
          The input can also be a packed variable length sequence.
          See :func:`torch.nn.utils.rnn.pack_padded_sequence` for details.
        - **h_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial hidden state for each element in the batch.
        - **c_0** (num_layers \* num_directions, batch, hidden_size): tensor
          containing the initial cell state for each element in the batch.

          If (h_0, c_0) is not provided, both **h_0** and **c_0** default to zero.


    Outputs: output, (h_n, c_n)
        - **output** (seq_len, batch, hidden_size * num_directions): tensor
          containing the output features `(h_t)` from the last layer of the RNN,
          for each t. If a :class:`torch.nn.utils.rnn.PackedSequence` has been
          given as the input, the output will also be a packed sequence.
        - **h_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the hidden state for t=seq_len
        - **c_n** (num_layers * num_directions, batch, hidden_size): tensor
          containing the cell state for t=seq_len

    Attributes:
        weight_ih_l[k] : the learnable input-hidden weights of the k-th layer
            `(W_ii|W_if|W_ig|W_io)`, of shape `(4*hidden_size x input_size)`
        weight_hh_l[k] : the learnable hidden-hidden weights of the k-th layer
            `(W_hi|W_hf|W_hg|W_ho)`, of shape `(4*hidden_size x hidden_size)`
        bias_ih_l[k] : the learnable input-hidden bias of the k-th layer
            `(b_ii|b_if|b_ig|b_io)`, of shape `(4*hidden_size)`
        bias_hh_l[k] : the learnable hidden-hidden bias of the k-th layer
            `(b_hi|b_hf|b_hg|b_ho)`, of shape `(4*hidden_size)`

    Examples::

        >>> rnn = nn.LSTM(10, 20, 2)
        >>> input = Variable(torch.randn(5, 3, 10))
        >>> h0 = Variable(torch.randn(2, 3, 20))
        >>> c0 = Variable(torch.randn(2, 3, 20))
        >>> output, hn = rnn(input, (h0, c0))
    """

    def __init__(self, *args, **kwargs):
        super(LSTM, self).__init__('LSTM', *args, **kwargs)
qMtQ)�qN}qO(Ubatch_firstqP�hh]qQ�RqRhh]qS�RqThhU_all_weightsqU]qV(]qW(Uweight_ih_l0qXUweight_hh_l0qYU
bias_ih_l0qZU
bias_hh_l0q[e]q\(Uweight_ih_l0_reverseq]Uweight_hh_l0_reverseq^Ubias_ih_l0_reverseq_Ubias_hh_l0_reverseq`e]qa(Uweight_ih_l1qbUweight_hh_l1qcU
bias_ih_l1qdU
bias_hh_l1qee]qf(Uweight_ih_l1_reverseqgUweight_hh_l1_reverseqhUbias_ih_l1_reverseqiUbias_hh_l1_reverseqje]qk(Uweight_ih_l2qlUweight_hh_l2qmU
bias_ih_l2qnU
bias_hh_l2qoe]qp(Uweight_ih_l2_reverseqqUweight_hh_l2_reverseqrUbias_ih_l2_reverseqsUbias_hh_l2_reverseqteeUdropoutquK hh]qv�Rqwhh]qx�Rqyh)h]qz(]q{(hXh-h.((h/h0U35597936q|h2� NtQK �(�����tRq}�Rq~��N�be]q(hYh-h.((h/h0U35786784q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hZh-h.((h/h0U36453024q�h2�(NtQK �(���tRq��Rq���N�be]q�(h[h-h.((h/h0U36457152q�h2�(NtQK �(���tRq��Rq���N�be]q�(h]h-h.((h/h0U35249248q�h2� NtQK �(�����tRq��Rq���N�be]q�(h^h-h.((h/h0U35000640q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(h_h-h.((h/h0U36722368q�h2�(NtQK �(���tRq��Rq���N�be]q�(h`h-h.((h/h0U36847552q�h2�(NtQK �(���tRq��Rq���N�be]q�(hbh-h.((h/h0U36846272q�h2� NtQK �(�����tRq��Rq���N�be]q�(hch-h.((h/h0U36844992q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hdh-h.((h/h0U36843712q�h2�(NtQK �(���tRq��Rq���N�be]q�(heh-h.((h/h0U36842432q�h2�(NtQK �(���tRq��Rq���N�be]q�(hgh-h.((h/h0U36841152q�h2� NtQK �(�����tRq��Rq���N�be]q�(hhh-h.((h/h0U36839872q�h2��NtQK �(�
��
��tRq��Rq���N�be]q�(hih-h.((h/h0U36838592q�h2�(NtQK �(���tRq��Rq���N�be]q�(hjh-h.((h/h0U36789376q�h2�(NtQK �(���tRq��Rq���N�be]q�(hlh-h.((h/h0U36787808q�h2� NtQK �(�����tRq��Rq���N�be]q�(hmh-h.((h/h0U36912928q�h2��NtQK �(�
��
��tRq��Rq�N�be]q�(hnh-h.((h/h0U36911360q�h2�(NtQK �(���tRqŅRqƈ�N�be]q�(hoh-h.((h/h0U36969968q�h2�(NtQK �(���tRqɅRqʈ�N�be]q�(hqh-h.((h/h0U36968688q�h2� NtQK �(�����tRqͅRqΈ�N�be]q�(hrh-h.((h/h0U36967408q�h2��NtQK �(�
��
��tRqхRq҈�N�be]q�(hsh-h.((h/h0U36966128q�h2�(NtQK �(���tRqՅRqֈ�N�be]q�(hth-h.((h/h0U36964848q�h2�(NtQK �(���tRqمRqڈ�N�bee�Rq�Ubidirectionalq܈Udropout_stateq�}q�Ubiasq߈Umodeq�ULSTMq�U
num_layersq�Kh7h]q�Rq�h:�U
input_sizeq�KUhidden_sizeq�K
U
_data_ptrsq�]q�ubea�Rq�h)h]q�Rq�h7h]q�Rq�h:�ube]q�(Ufh>)�q�}q�(hh]q�Rq�hh]q�Rq�hhhh]q��Rq�hh]q�(]q�(U0(hctorch.nn.modules.batchnorm
BatchNorm1d
q�UD/usr/local/lib/python2.7/dist-packages/torch/nn/modules/batchnorm.pyq�T�  class BatchNorm1d(_BatchNorm):
    r"""Applies Batch Normalization over a 2d or 3d input that is seen as a
    mini-batch.

    .. math::

        y = \frac{x - mean[x]}{ \sqrt{Var[x] + \epsilon}} * gamma + beta

    The mean and standard-deviation are calculated per-dimension over
    the mini-batches and gamma and beta are learnable parameter vectors
    of size C (where C is the input size).

    During training, this layer keeps a running estimate of its computed mean
    and variance. The running sum is kept with a default momentum of 0.1.

    During evaluation, this running mean/variance is used for normalization.

    Because the BatchNorm is done over the `C` dimension, computing statistics
    on `(N, L)` slices, it's common terminology to call this Temporal BatchNorm

    Args:
        num_features: num_features from an expected input of size
            `batch_size x num_features [x width]`
        eps: a value added to the denominator for numerical stability.
            Default: 1e-5
        momentum: the value used for the running_mean and running_var
            computation. Default: 0.1
        affine: a boolean value that when set to ``True``, gives the layer learnable
            affine parameters. Default: ``True``

    Shape:
        - Input: :math:`(N, C)` or :math:`(N, C, L)`
        - Output: :math:`(N, C)` or :math:`(N, C, L)` (same shape as input)

    Examples:
        >>> # With Learnable Parameters
        >>> m = nn.BatchNorm1d(100)
        >>> # Without Learnable Parameters
        >>> m = nn.BatchNorm1d(100, affine=False)
        >>> input = autograd.Variable(torch.randn(20, 100))
        >>> output = m(input)
    """

    def _check_input_dim(self, input):
        if input.dim() != 2 and input.dim() != 3:
            raise ValueError('expected 2D or 3D input (got {}D input)'
                             .format(input.dim()))
        super(BatchNorm1d, self)._check_input_dim(input)
q�tQ)�q�}q�(hh]q��Rq�hh]r   �Rr  hhUnum_featuresr  KUaffiner  �hh]r  �Rr  hh]r  �Rr  Uepsr  G>�����h�h)h]r	  (]r
  (h,h-h.((h/h0U41566944r  h2�NtQK ����tRr  �Rr  ��N�be]r  (h�h-h.((h/h0U42910192r  h2�NtQK ����tRr  �Rr  ��N�bee�Rr  h7h]r  (]r  (Urunning_meanr  h.((h/h0U44845536r  h2�NtQK ����tRr  e]r  (Urunning_varr  h.((h/h0U45871744r  h2�NtQK ����tRr  ee�Rr  h:�Umomentumr  G?�������ube]r  (U1(hctorch.nn.modules.linear
Linear
r  UA/usr/local/lib/python2.7/dist-packages/torch/nn/modules/linear.pyr   Ts  class Linear(Module):
    r"""Applies a linear transformation to the incoming data: :math:`y = Ax + b`

    Args:
        in_features: size of each input sample
        out_features: size of each output sample
        bias: If set to False, the layer will not learn an additive bias.
            Default: ``True``

    Shape:
        - Input: :math:`(N, *, in\_features)` where `*` means any number of
          additional dimensions
        - Output: :math:`(N, *, out\_features)` where all but the last dimension
          are the same shape as the input.

    Attributes:
        weight: the learnable weights of the module of shape
            (out_features x in_features)
        bias:   the learnable bias of the module of shape (out_features)

    Examples::

        >>> m = nn.Linear(20, 30)
        >>> input = autograd.Variable(torch.randn(128, 20))
        >>> output = m(input)
        >>> print(output.size())
    """

    def __init__(self, in_features, out_features, bias=True):
        super(Linear, self).__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.weight = Parameter(torch.Tensor(out_features, in_features))
        if bias:
            self.bias = Parameter(torch.Tensor(out_features))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = 1. / math.sqrt(self.weight.size(1))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.uniform_(-stdv, stdv)

    def forward(self, input):
        return F.linear(input, self.weight, self.bias)

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', out_features=' + str(self.out_features) \
            + ', bias=' + str(self.bias is not None) + ')'
r!  tQ)�r"  }r#  (hh]r$  �Rr%  hh]r&  �Rr'  hhUin_featuresr(  KUout_featuresr)  Khh]r*  �Rr+  hh]r,  �Rr-  h)h]r.  (]r/  (h,h-h.((h/h0U49952752r0  h2��NtQK ������tRr1  �Rr2  ��N�be]r3  (h�h-h.((h/h0U49977264r4  h2�NtQK ����tRr5  �Rr6  ��N�bee�Rr7  h7h]r8  �Rr9  h:�ube]r:  (U2h�)�r;  }r<  (hh]r=  �Rr>  hh]r?  �Rr@  hhj  Kj  �hh]rA  �RrB  hh]rC  �RrD  j  G>�����h�h)h]rE  (]rF  (h,h-h.((h/h0U50073024rG  h2�NtQK ����tRrH  �RrI  ��N�be]rJ  (h�h-h.((h/h0U50097536rK  h2�NtQK ����tRrL  �RrM  ��N�bee�RrN  h7h]rO  (]rP  (j  h.((h/h0U53446864rQ  h2�NtQK ����tRrR  e]rS  (j  h.((h/h0U56284656rT  h2�NtQK ����tRrU  ee�RrV  h:�j  G?�������ube]rW  (U3(hctorch.nn.modules.activation
ReLU
rX  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyrY  T  class ReLU(Threshold):
    r"""Applies the rectified linear unit function element-wise
    :math:`{ReLU}(x)= max(0, x)`

    Args:
        inplace: can optionally do the operation in-place. Default: ``False``

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.ReLU()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, inplace=False):
        super(ReLU, self).__init__(0, 0, inplace)

    def __repr__(self):
        inplace_str = 'inplace' if self.inplace else ''
        return self.__class__.__name__ + '(' \
            + inplace_str + ')'
rZ  tQ)�r[  }r\  (hh]r]  �Rr^  hh]r_  �Rr`  hhhh]ra  �Rrb  hh]rc  �Rrd  Uinplacere  �h)h]rf  �Rrg  U	thresholdrh  K Uvalueri  K h7h]rj  �Rrk  h:�ubee�Rrl  h)h]rm  �Rrn  h7h]ro  �Rrp  h:�ube]rq  (Ursarr  h>)�rs  }rt  (hh]ru  �Rrv  hh]rw  �Rrx  hhhh]ry  �Rrz  hh]r{  (]r|  (U0j  )�r}  }r~  (hh]r  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U59636176r�  h2�NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U59649552r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Sigmoid
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T3  class Sigmoid(Module):
    r"""Applies the element-wise function :math:`f(x) = 1 / ( 1 + exp(-x))`

    Shape:
        - Input: :math:`(N, *)` where `*` means, any number of additional
          dimensions
        - Output: :math:`(N, *)`, same shape as the input

    Examples::

        >>> m = nn.Sigmoid()
        >>> input = autograd.Variable(torch.randn(2))
        >>> print(input)
        >>> print(m(input))
    """

    def forward(self, input):
        return torch.sigmoid(input)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ube]r�  (Ussr�  h>)�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  (]r�  (U0j  )�r�  }r�  (hh]r�  �Rr�  hh]r�  �Rr�  hhj(  Kj)  Khh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  (]r�  (h,h-h.((h/h0U62436688r�  h2�<NtQK ������tRr�  �Rr�  ��N�be]r�  (h�h-h.((h/h0U62447792r�  h2�NtQK ����tRr�  �Rr�  ��N�bee�Rr�  h7h]r�  �Rr�  h:�ube]r�  (U1(hctorch.nn.modules.activation
Softmax
r�  UE/usr/local/lib/python2.7/dist-packages/torch/nn/modules/activation.pyr�  T|  class Softmax(Module):
    r"""Applies the Softmax function to an n-dimensional input Tensor
    rescaling them so that the elements of the n-dimensional output Tensor
    lie in the range (0,1) and sum to 1

    Softmax is defined as
    :math:`f_i(x) = \frac{\exp(x_i)}{\sum_j \exp(x_j)}`

    Shape:
        - Input: any shape
        - Output: same as input

    Returns:
        a Tensor of the same dimension and shape as the input with
        values in the range [0, 1]

    Arguments:
        dim (int): A dimension along which Softmax will be computed (so every slice
            along dim will sum to 1).

    .. note::
        This module doesn't work directly with NLLLoss,
        which expects the Log to be computed between the Softmax and itself.
        Use Logsoftmax instead (it's faster and has better numerical properties).

    Examples::

        >>> m = nn.Softmax()
        >>> input = autograd.Variable(torch.randn(2, 3))
        >>> print(input)
        >>> print(m(input))
    """

    def __init__(self, dim=None):
        super(Softmax, self).__init__()
        self.dim = dim

    def __setstate__(self, state):
        self.__dict__.update(state)
        if not hasattr(self, 'dim'):
            self.dim = None

    def forward(self, input):
        return F.softmax(input, self.dim, _stacklevel=5)

    def __repr__(self):
        return self.__class__.__name__ + '()'
r�  tQ)�r�  }r�  (Udimr�  Nhh]r�  �Rr�  hh]r�  �Rr�  hhhh]r�  �Rr�  hh]r�  �Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�ubee�Rr�  h)h]r�  �Rr�  h7h]r�  �Rr�  h:�Unamer�  UlstmRSA+SS2.emb20r�  ub.�]q(U35000640qU35249248qU35597936qU35786784qU36048080qU36453024qU36457152qU36722368q	U36787808q
U36789376qU36838592qU36839872qU36841152qU36842432qU36843712qU36844992qU36846272qU36847552qU36911360qU36912928qU36964848qU36966128qU36967408qU36968688qU36969968qU41566944qU42910192qU44845536qU45871744qU49952752qU49977264q U50073024q!U50097536q"U53446864q#U56284656q$U59636176q%U59649552q&U62436688q'U62447792q(e.�      �y>!�%?`�>2Jǽ>�"� �W?`&�>��J��v�='��:���&�=�m�<V�>���<@Pf?ٜ����>z)�<�I=?��V�(�<ˆ?��2?#�E?��?XT?R/��A��1ľ-�>�fV��p��O���e��ú +e>_c�>�/b�!�@�2@���>>L�~��{��{�e����=������>��m���
�*7I����>ũѽ 4?����}�=Y4;���x�콨�?<?�<a>E8���x��9P��`>ݍ�=�?Z��'�̻��Y~}>��>�Q����?�J-��o��י7�-#�{�>@*�=�Y[����=�-������8���.>��=�Q��e��<1ž�4�ʒ*?#��<Ѹ?��˾�>i����9>�N?��徸c�j�>�����@�>t� ��\K>���>�?]Y3?O��<g��>�.�>�q�>�龒�=?T�+=���� �e>�f��|Ѿ���>0V�>RVu;�S�>�꯾���=a��=�q;�ms>��k�6=�տ>��; ���>c1�=T���R�>�>�6߾3w�>(I>���_���Ԣ>�Z���e־���L=O�������?��>UY�u'9?u���QȎ���־��?n��>	?�>�ľM߽Ɔ��Bӆ�^e��:H���
�>�B�0���$�>IZ*=]��=�:�����M�=/������rF�>k��>[�����/V�>�E�=;Rr���<yE�>��>b�*��=���>�B>�����s��T�X���=�,�=�D>�`#�?B�� �=���`�Ľ�L'��+�ꦗ>��|��7�=�>�>S�ýl���(y>���=��J>9�����t��/�ݽ���������p��T2>5?�>�>��>�±�{�=D�>7ڌ��G徟�i=[�"�`�����>T#>>-M���%U>\<)�>^)���Q>�4?ݠ�>�dK��
]�RXd>`���>�_Ľ�;!>�-���%?!(l?dǺ>%�5��A�?V�L>9�=[3_�Z`�=��<޷>�>���= 5~� ��X���+L>�s����]>n��>a����'��j<Q���i��8<��e0�<�Js>�E�>t�>¼D���>��>Eh:��sn�ت>���>I��>�-�>���>M�>>c�?�솾
^)>���=u&3=��,?��?Y܉���?тj?c�?ZQY?g��{*��!���gj�79?>��>�B�?�p�>�S?��?�yr?��/?�7i����>I���?����?�r�>���>�X2?���=6bҾb���SĽ,�Ծ��u=;�>	C>H��l�*>�������I�龾��D��>w!>���=g#q�M��>[̈<�a2���N>zY�>O��<g�����,�X??L�>A�"��Yܾ�r���<Áξ4�=��&?l»?���8'�>��*>�)��%�@j��#*F>\���O���ԄI?�)>��9L>�6�/�?j8>�,��M,?V�m?T�?/ե���<��7?�c�=�`c=��s<9����+��x�>~D��DsW�.0>��V?V�?'��> �=S�ǻ�$+�       �2��܎��g��%a?��j=��5�C���8�A>��[>.�b>��h��#��G����U���=�����>=���>7��2���M�d�$��z��Ažf���|�t��?k� >��ҽ��I��j�>1���¾u@���(�>�<�� Ƚ5	���s���2�>�߯>/g����L>�g�>��r�V�N��=}c��׾���ھS�Y;�q�=�S���?垞>�)>`߭>�FD;5�>�̾�
�l��G�>
�>G�>>�J�>[o�>t���v��>��Ź =�>)�g��wN>�z�>ĵ���	�>Ի��A�f���>�ny>G�м�[����=�1f�S':��ֲ� ξ$^�>�սMβ�C��<^0�	��>o�=o��>lF�_��a���M=��	��`޾�����	�=��> o���z��ٻ>Ze>*׽����r׾ʠp>E�.��=������=c�����n>_Μ>Vծ<����>�_{��l[<x#>2vD?4��=S���/>
�*?Iz�>�G=bcu�VC���A�?&���M�=K��=�1�<��>�Z=�o�=z�<<9����A���=X�G>�˪=ni��Mپv�8�@�?�>*�>|�=+��>Q,�>�{'?�`�>e�ʼ���D�>ik��Y	�"�����A>O��=p�m>Y��9�Ͼ�Ȍ��`s>�v���z���=���>�D*=`_�7=�w�����4)�>�Y;���r9����A���n�}��=}NT��s���Y�=^h�>�$½I6�����ي>Ǟ~�1��=�9;<iɦ��57=7���P����=�8?����]F�s���q6>�P3<%��L��=䲙>�/l��;����վ�s�>�zͽML�=�g�>��9=P�i������>ƪZ�w(}>��Ⱦ5�0>��>_�O>3�>����)ꕾ�>?.I�n��R�<�ZԼ�������=F��� ��>u`y>��?A��=ݩ־�[��NUQ>�8=�xY��B6>q�(�K�罺�'�
�>O�v�$��>�nO��´�����,50>
A�=r?=b�?9��V�=ʿ�����4��=��>� A�mOi=�;�g��J�;��	>P�=f>��=\�h�t�=�K=B���Ó>�;>`�>�p ?R���t=n�=˕�>�>�S��Ģ>(�����C=�K)?X1�>�Y�;���=�X�=�~T�iϽ��z>'���;N�<�[�;;�C=��뽛'w�#�"����>X L��uu�1�b�2Ƌ>>��+�=t�N4�����=��>��85ӽ)<�0�
����=�(>�3>jJ����� �$?�˽>A�D>��$�t*��n��>Fq(����>$>�)>��Y>q�O>>��>�9:�C �>ľ�U߾�3���1?z��%z���5����>��D���>�T?F�������,d�5>�L
�yⒾjܤ>�?k�=f��3t��
�ͽՍ�>T�=Go־���=��ݽ��u��ݐ=G?c�>`麾u�ٽs�X��e꾪t=���=-��>��=ܴ<)9�#���@<�����>��c� ��=���=l���:�=0�>�8>�~��(AD>5�s��̾�2�1�Q>�?`>��">ج���0�"R��Q�-U�>0~�>	tg�P�?=��=8���Ϩ>�	>g&>�y��/J���|>�j>;�8>�-�a�S�%���� ��ם=�Ey>]�j>��`>����	>:ْ>�R���Z�=�j�=�H6��'����D��<'&J�8Պ�M?��$?)�_=m�þ�%��Fp>��`�".��D�>*����<$,̾��/�Δ�>M�>E��>Ys?I_��X��>�Z��X��J�W��_u<�C>(�S>z
D=�م���E=D�����#����O=3� ����+��>�ߏ>Ҍ>�˽�v�>K�=�������0��=R߳�2㠼,�����>����̽ �j>{�6=r,M��!���d�>���R�=����c����>���>-~3�7����->!�k>��&=���>��½˸�p�>���`�=h�8��^>���>Y�L�������<�h�7K>e�=���=犾J�Y����������/ �>4R[�F�<�%����p<� >w�����>�"�d����=>PM(�}PN>ΠU��>t�w�=�MQ�]��>:�`��J+=A�����˽�.H>��Ծ���<����c=��ѽߢV=��>�u�>|�>��?>�3��$>���@[�� ���1ʽ���<�ؾ�����>>�>�ͳ>�~���\Q�w�̾��Ƚ�m?���]>Å8�9�����N>$��=B��>ټS>�:˺�m7>�:������\;�>!��=�6�=��>V^��l�=�蟻.%2�Fa�֞<�ɖ>�[�>Q�?H�m���I��}���H=��/�i:<�݋�,g�=� ̾	�ߝ�=C�>��=�q=>�Al>�1v������t=Qe��_}!���ཊ�9<��ͽ�p��f�'y�`�u>E)�>9=�������\v��پ���]M��y">DƾP���Q�>�q�>Ow;=$��>��>I��>������=Y���bѕ���c>���4t�>��>q=����>�$�>j)Ѽ��>c@���==����;�AZ>�&>i��H=�����=�PH<��)�-�'>�b>>a?��&>�Y^�7Qx<N&�=��3��d:=e�s�!Zվ��~��5��_�/3��7뼗Ⱦ���h5=X�ھ1�[�>r�C=��>$�>�z�=�/�>�4={�G�������>�>��?�� >��6>�3�>��<��w���v>�þ���g�����>|�L��>1��<�s���	�3l>�#$>zj$�ϸ��k��l�ﾟ0�B�>�_0>NP�<������2>�� �q�����>��� �#���{Y!=儙�1ap��׼�\���s���?��>���G�#>��D>�ٙ>���=����>�� )��7�>H�ǽ!��r��=	�>���<_-=�ƚ�ER�>�$�>�#�>�bU>ӻT=�
�^�0�����_*���>l1o>6ѽ��3��zY��;=��>����*����
�ʇ?Q<�=�L=�~�>���=�E����\�78]�:�>`}>       "Ua��總����&>L�Q=��g>3��>�TC�d�/}>f�����K>�@��p.	?�u�:۾�6C�'b�k����=�|n�ȕ��2;�)������ث��C�=��ܽR+���x��}�>V��=�;X����n'?��[��q�>qH+��>����q>���=,���W�>��>��<W�����={�[<ڻ��=�dԽ!�׾�̇=��E��Y/���(�&^۽gbu����="��fyӾ��ﾇ�=��"��
>�����J<NC���=/�h=�˾�.�;Y�^Z����>��"<���={T`�}�>4>u�q =J���'x��s�/�=�j��CO��x>�d-<���>��\1C=��ν3�?">. ��$����3�B+a�9�>�h��x�|���1�>���>=&�>F��
�->����;�ܾ4)�>�� >+1��p�&����>]��=�ƽ	�<솲���f>�"��t�>I^��vr��>dؽ�ᅼ�9��JBq�;�)�P�ؾJ컬��>mԋ�\P-����<q(+�s��>���>��?��>Fɾ>�>�7��Q�����>GS��
��>���>���>Z��<=��V;�d�=�	�>̵,�?¾hA9�/����X>}�>�>�V1�6Z���-��\�r*ܽ��/>���M���3[��ݻ>���=�.K>��8�^�-��I>�g��> ���)�z��Y;�qb�E������>�U½-k�/C�=ggļl2�<IwE=�Ӎ��܄��~>�(:>݋�=B�o����=�%��C����6�>��ž��z���ݼ��>*�M�t��=�j'>�i$=fj���n�"�"�I<�>����>�i=��s>�|���/?r�>>���>���I�Ծ�'9=�������=o�"�<��n����<��<��<��b�1���|¼�x��>n�#���B��H,���\���=~>�L����(�����Tf�=��������؏�B���G��>��U=-���d>��Q�m	��TP���<��;�������=�k���
 >$E*>=}>�оɤ̾�V�>ʍ���q>�4�>�󻼛��ό<?�=�1>r�	�Q�W>�h��􈂽�-����>��>�fl>]��>�P�C�K�\�:��q�=lo�>j���-�8v>*	i>�M>"��k+l>�Q�><�=���=R�>!x�=��>�{�>l���*+�� >��z>�9*�-U�����>+�> 빽4mS=�?��\���գ����K>��W��&���ڽ<kV>e΅=�Ĉ> C�>^	f>c�>b����}>��C
>�ʊ�������=/I��}���Ժ>[�>O�T��>�ɏ> ��>�������;Hn�����="��=��'�!ش>SA>l_��У�>��5zG=<��NF6>p?����� ��m�>Mڙ>��=O��>�����ʻ����<�8�>�h�=z�?^X>���>���׋U>N
�=�a��V�>����U��)gO>^&�>m��=@	?�3��öo��lL��p�'�>���xK>Y%�>p>��>������>'E?�-�=<d{�	WJ���
>�*�>�	���Rq��nn��т�����"f�=�g��ԩ�=��:6�=B���NL><��E�P�k�>2��>��w���>�����Č>��y��f�>>5>����B���
����>bI�����;:��!��ӂ>�7<�ک<`��>��}>�WW�[%Ͼ�<r>
R"���վϘ?<Ң>���G)�FE�>F��=�:Y>��о����sr�L�Z�16༩[1>溪�x*.>�s�>���>�[Y=I��|%�d��=��k>��ľ=�V>[�<Y/�=/�5<���>z���e6>|V�=�ޠ��4>���~MX��*�=���)���ea=Ѳ���k=H��=UC��P�>�E<�>,�r�>H�_>r��\=�o<��0>z4Q�.踾�2�>���=���=��+�����.W=yc��C��8����=ob�с>�C�>�0�>(���=��>�/>�0�=r��=V�%>9��l����Ƣ��.-����>�,.�?�v>-��>@�/���>tS���-�<N���J�>��>o�T�-�ս:�t>cq��5<��W>H���_�=���=�k'=�0���S>�:>-��0�>>fvڽCzؾ� ��yi=���<��.=^p�������&���o�>�Kt�3R-�n�վ2[�=[u�>�����g�� 6>E�6�_ZX>(H���;�k����*:��Y�=���]X_>��)���{>�'v>8�)>�Ѹ>�z�HF>�b'��캻Yfƾ�w>��<9�����>��ξ �¾�����뺽��>�z�>e侽6���ӽ�w�\����1�{���'�=R8˺f��=�'�>��&=7>�����d��~!���>�ə��=3�=�<Z>:y澌_�=�[�>j���	�<^���z�����>n�>�=��<,S��졙=��>}W>��x<jYҽ�j����ƍ̾Os��%&������&��"�?�pk�=l&>��>���>�h<T?���8��q�<M�{�>�Y�=�3��س=b�.>*[>�	=�˖>G5|=B���L,>: �=�>~%?>�
��f>WC��4x��2ξ-C>���=����.ܦ�|.�<�A>�m�>����fg�[�Z=F���dQ*>gx ��]�>R٨>�aX�[u@���=p?p���<P�3����r�{�/̼L��>{����ѥ��|�>vp�=2U�<m}i�L�>39<y �>�Eʾ{PĽ�B<_�W>x����5?|V.>��F=cf ���>Iy=���|�^��ԝ=�8g��+>B�=��n>{M���'?B��>)���s��G�M�=k�:�D���X��)>W�=���<Cվe��B�9�[p���>N2E>0M>�g����Ⱦ�V�n�>^�=JJ�>�9�=���J�>�t��h�Z<ӡ�>�D�=�J��	��Ѷ1>�h���cL�@q�>)Y�>4!��8&e>h���l�ۋ">�p��:���z����>	��>i�+>���>-�(��,=�����y=��=�\�>=f�!ɽ>9 �>�*x>�?.�_���^/޽gX�=��=a��=�Wɽ�!�2�>��>�����=L/�=V8��      �>{�$?Z�1��ʡ�8$���8���%@>Q�о6�!��9��ǘ>���>���>^��>�O����ﻨ��,-�3E&���齧x�>)�<��ݕ?���=�}?�t���>�>�a=�,�9����>�>���>��2�V�>J�5y�>q>�?��%>a���>�4�s]�?k% ?���<�K? s�>6�Ľ�>1Ҿ�K�v�=���Ŏ��r>P��>��c����>��G?�~�6��>N�>��<5�>2+���S>��߾VR��'�A��-���������@��>&m+�܁>|1>��U�T~=�?��@>�89>>���5�>'����>���(r�=Q$���66>�o+?�&O�;%����W�/��Y�>>�.���iƝ?s�?�1�:�f*'�%��� CG�سB>S�k��=���$At�������>���U��ݯ�,���3�>�b�>��:>��>"�Z>7��P��D�3����t�>^��]>�m5����>�0���=
�����u��b=���=�Q�ŕ�%9��j�=O���������=Z�c>I[3=����tl�=�w�>,#E���
?��Ѿ���>���>�0�ɑi=���=�JᾀU�=�+>9x4������μ?!�f�'�">'�~<�$�>Xl�*���W�>��=ь�=��g�J�@>&ν�U�>���=�T=�K�F>/�>��>e�>�A?:X^>��=?-�;�O�?�\��??(��~?AQ��W�>����,��a�=����b^�pк�ա5?��j>��L<^ݭ�b��=�C�>���:�?�F>柬�:��/�>����H�=��㾌�J>�?<���h�b> ��=��	��&���H�>��6�t���k�u=���=ZUd>�>�K?$�=�]�=�n�><&�=N��=Nƽځ7�u޸>R���D��[��P��>��������h>�i�=s�=�x�>���>Z��<�1>���<2]Y>�L���=Pe��θ=�
R�D�?�-= �=W�\=���<�c]��"�=�cl��ϩ=�ؼ���J���>��=���>��>����q�?���
3��G���̾��g��9?#I�$z�>�a��*�>#C��U�f>/� ��K������>j�>s��z�?a�S>��h�>�8n�08�܄�o�ؼ��7���>&���8�%9 J>jr�=W�K��L��_�Й=4�J>�Ð�k��>ȅ��e�>����+P�_��+�=)j=	��=��#��Ԑ>?~�>�q�<�B.=P�>� >1��d��T�=/��N#�>3'���ھjer>@�v�f�#�.�>]��4"=���=��>|p�>�>
۞��ۘ>��i4n�������^=&��$�>e���N|���=��>�0��>�Ѯ>4S><��=7-v��e��IN�Yǐ=X��=H|�fy-=t*�=<�0>�坾��&��v�;45��/?�d̾t_R?�3�?
��?��Q�վ�ԟ��G"��P�"�>@��;�=+������=Vo�=ri\�-(��
����L��'% �zPؾ��?|�M?:�!��      }!�?O2�|мe� �-Q>��N?W!����g?����j�U?S"4� �C�S=�<�����>&�b;ͥ�?4���(?�į��"1?���?}
@��>��?�G	@Jj�?ܔ����W>�?2� �J	Q�`'�?�m?�M?Ob?�E�� �@y�=#?��\?�u¾��)?es��za��ݾ�>�R�?�p޾��O��;�>���>��?��S@��?�&�>Hs����?�һ>B�ݿ=:U��>���>;&8?l?��?��?3?J�8�M??�־._b����>����`�C�>��״>׈j�!=��G�J?_e{?��N;*Uc��[2���@�k�?��?��d���¿`��U�?��J��M ���>�q�q~c�E���u�>����������Q<�"�?��f?28�?�� �\���ǿt�5�t�Ig�]�?��>���>=�e?������>�I�l�&���>�˿[f�?O�o��&��TZm���?�N1>�u�?��<S��N�����?r{�?�`�?,�g�?�U�=���>m��^RC@�0 ����{Z�%��?��'�S� ?*!��AϾ����Q�>��0?x��g��\b������Ci�+Z���_콃g�����>J�����;3X?�@p���g�rc?���9����>@a�	J?&z?��9?����������?{v�>�y=k�R�d+��)�y��盿��ſܕ�n�;�T��>��=T>�)="��Rᐿ}�?: ��0�9�DM?�#pǿ���ؿaC��� �?��@�M�>ݠh?��T?�Z辋��?�a�ΫF=˒:@`G�/˦�Gt��J��?Efx�(��>��>3G�?�b�>ׯ��P�+�Ǿ�V=?�T���� @a5;�t־0��z�A?��@��Zp��/��X=Ž�=��k��i� ;����?H�p=	AQ?��3Tؾ�9���C?5�U�~%?�?Ǿ:e�?�C^?*�{�DC��dqu��� ��%��i���T?%�?H2��2<>��X~�����)8��]��a��[�?�#)?�����?���?\��?T���1���f9����7��r>+խ?ǟU������>65�� �?K�D?�S��h��^ȼ�p�ֿu=�?!�?��?�`#?�Ǻ��첿�.*�{H��e�?��a�A=�?k�Ŀ�/�Fá?^�>æo����gm?�t�?���>)�=���ȷѾR�>�|1�A��2�տA�?N�?	�=���?�V�?. �?�Yƾk?�|S��*�9{˽�I�?��>���=�k>X�_�0gU?ҍ?��A?I��?su̿���?�)�/�>x|u�|\g?�Ɣ?�-ž�(	�l>����q��>�@��?͠'�A�q?�"@���ϩ�N��>�e�?R���]L��2����?���?��?k i?/�?@�?��>?h�H����Mg�?G�>�}޾��̺�<y�=�J���׿�y�,��?xj@f^��m�?O;�>N??���?�RS?K�>��f?�ý�"��T߾��L���Ѐ?����"?�*��[By��o��ƙ�����S��?������?v�ۿd��>�� ��E�$�t?Q����!��i;o?s�M>^�F@��=葎��8N?��ξ3CX�7uɿ��	?
��?	N�>m_��(       T���B:���|��k�p��l�<_�D=�����h��Gd�>#�.<���>�`�=���>^rA�܇6>�k�=�"��5>d�2�%qJ<48g�G�E�%MB��=��a>��4�2qG>������>kA����>�qc<$G�l�ѽcd��3>�?��8���o>�I=(       ޗ�=]6�=�q0�U�ӾJ��=r�����='�=l�?,S=ⴋ>a̙�4K�=w�>�ģ��p�h�+��%N�0���r��P��M��->P�T��k�>��3>gX>��$�/�ۙ>���[���4>��>K�(�b���飠�ى�>	�4��>��i=(       �(���ʻ>��t>���=�p8>���>8�Q<��k;N��\ �-��>��ؾk5I�$5���e����Ή�>S�/>�t>��A�C���㰩=� �^w"���7��H�>|V�>�K���9�>\Q_>���=�q>�q����>N�>.�0>�.�=��==Đ=���>       z�ᾍc �	q���$?��i?�rV=3?�>@ �>�m�@�g>o[$���<X?O"�>��(��=�إ>��n�Yw�۵�ehm��P���'>��M�����Ֆ<� ?�S7>.z�=�RA=�%?R��=�|��>H>rO��.*>qZ=(�Y=% 
>���>����Rz�>1��>Dk*�:�?$��-�?2�&��z-�����.Խ��>��"?�Q?EM�>�j��Gս���v���1��<��̽����U����'=�n��$i> ���k��Ig>!ht��>�9�=}�H�E}ٽ$������;�>�+i>�i9�g��>CMl�fž��<����8�=�'����;�8���"��N�E9X���~>��->�/����>�y���j+=1�%�;�->�qD>^?.̓��mF>�UC?���>�n���m>���<�?"�y;y>��?U�'>�I\>z����0�>/���˝=��	<��4>���s�>��=���9F���.��>��>���>�,\�?(��p�>�_��P ��}�<m>h�����>l�m�aa½zL�>�'�=\�>ީ]�\�4�K�V>�¨����>��;5tU�S(���0ֽ��c����m��=��>�|I��6j>W��������<i�#���ս�N����>\h8>ik���>�|#<���=EBv�#o¾�*/=�I=����LA�=V1P>27�� !> �[>A<��������*��[�<��L���>��=[͵�mH>ܼ߮i#5������=H|��Ծ;Ȼ�����mN���f�=��g��-���">�CC��9?9��=(i-?�y�?����$�>sY[<���=Y�
>>�"��8O>��}�����ν�>9Ң><��<EJ��#�4>�x��ݷ=�OE���(>q��>sm��>�>��>ς�>P�5<�[����/�U`���4<[o�>FO�>���>�O���¾����V��Rm ?Qx�4T�>)jt>�~7?`�?�H>�^0?�g��H�'�z�V�oB�Z1�V�=ЙL?����=�GP>��y�I�r�Y���E÷���ý~���>�V�>�C�=����=k�*=�w@�)<{�q����>�[>>�<�4?9�9'Ӿvu>+�J>C����M�?+��u_�t��aCl>���>3��5>&���`��܂�>�hL>�@~>�=� u������0�G'�>�E|?:`l>n�=�8�>a�0o�>2kھ�1
?�X�>�����-� ��>�&��=�����>�
��{ZM?�>�d�r�����'?�!���%W?�;>��xp<>T^:>`�>�Ń?ώV>vcB�F���'?eF>d��>�s���Od�M�!>���=��b>I4�=f��Vm�>A��q�A>�'��ZAh>nk���5�>�`x>*�������eT�>�z�>s~�>�v���X�>D/���>R��>���>>�!?�tA<[��>�)+�����U�=e��B��>Y�>���>&2���D��^_>�/����r��>��=��>J��>�;�>�����@W���e>(8����U��8>
���:e>�><����侕��>���=4L�=ƽ`>}�>r�>�I�ٟ�>a����<���S���>��=�}����@�=&F=�Q�>��6>���{�>o����*=Z�Z>%�	?�G8=%C%=w�=�`������0��8���r>|3
��Q���'��6�>�3>��>xC=e�>
�g>f}�� �_������-�>n�>$ ��H���԰=b�@>��>e����ʾ��C>�K=BȂ>�:I�/�=]���p>D�>ƺ����B=�A�Wj��𺊾 �"����=��p��>�D�=�KM=�	��H�>�~��G@��C�J��;_�#�JY���<��,<T<�����T��V�=���$>�F�� ���n>�ܷ>v`���Z� GB>hG)���N����=NW�)¾���=+8ٽ�\�=������P�=��>H��>E'��T�>Ƽ�>��;�F
x>�y>��>N�=�t<Ȭ>��>�����S>�l����>�#�և��e�|>��>oY�=y�ǽ�����
����>�>��=e=v>���o�վ���=J�>�۬�?��=��>�*&<҅)>DY>,�D��][� ����B� 7>��p:�M@�ш�>�ꈾ��G�{��`k?�l�Ҫ	���)>�0E>Z7$���>TY~<�4���?�>a�ݻU�n=��d�(��I�>"�>��Ծ;C>ӫ�~��=�
�����=�I>u*=��=L��
u$�Cg�>�=%�;ЫѼ]'G>a�>ɳ/; ������)�0o�>/���k ��.θ>ࣾf�ļ4kU>��_<�Ň>lK%�
�|�ei>��c�*��>J4>r��=�0�>j.u�S�&��߼RK�>��p�EK�>q��=��>��L�!�ὒH�=�}���*8>ӳ!�l�>
�ž+��<LOD>J��>�H=�T��H�=��N<��U���>'�=���>�mD�<�$����<��>�-�Rv��%�y=<T�>���=#P>�㹾fw��-����>S?��k6�>�&>�Ĭ>6�\> ����i���=�>.L�;��>ߢ5��������U�(>oG�<C��=��	e��V�W>����������h<>i���>�h�=~��=^�g�ZȮ>���0g�>��D>���������J�<G�Q��\��>��C��|�=9���Ϟ>˚��U������\��>k�W>�N&���g�J�s�&��Mr>�����n(?�W�>./`��D��v��:��7�@�:>t ?*ŉ��4>Cۆ�
�8>�jؽ�w�?����]>T�.>�񕼩>lg&<줾���=Pk�գ�= )D�]6]�zg۽��>`�w>ZҽB�0>�|)?w#��OD��ݣ���|��BξI�ս֪�<)p>�e�=���>�?��R�-v�<"!��	d
>yj�=yQ��*~>+��=�B�=�s�Dm;>�Z\>� �>�b>c�B=��>ѝ�=6<�];�h���}�>��:=еR����>p���\ =mך�ح�=��:>��h�Ͳ=-������<='��y�>v��&�]��4ϻ�8�;�:�P�=,��eR�hu���K�=�[������^>����˅�g����-?(       �"�=	P��d�=��J�^>˪ݼ=]�>��>>ۃx�$�>�J>9��>go=>a�����=��O����j�=w��>i�A�"�.=`�>�[,���ھb:���_n���$�i����B>��ƽʥQ=qZ�>��I>z���8�н�	1>\� =M��Cz��h�<(       �դ��F!���.>�q>>��= �����m��"��&��>�$<��=�	>��eJ>�թ>���>;>�����=-V��C��>�����h��X�=Be�=�-��\>�vl>���>�:�Wz�>�h�=.
V�R��>�+��v�>i$پY�N�G���      W���ټ=$4��r?&�!=��>>�s��PJ<4��>�o��I�="�m=��b�;{�<��=|ٓ��b�=�>��c?�[����=����{_��70;��b>T���"e���:��[>	�<>տ��y !�W��>:a�=�o���ya>n.=�I��Xғ>-O�>Cb�>�w�='�Ҿ:J��b>�d��DƎ�dj���=�k:�>�<'��9�0��K&0���N��s4>��?�c?<F�����N�=�$*=!�5����T>'l�>�A�=R32>!:��W�Z=6j��F�>[�[>�p���=l��=o_�>�#�>��7>�D.��\R�� 	��
�zt#�𾲽��q>b��~��?n�Q>�KQ����B㗾�|�78=����8.�I���*�=��I!���O�Fn߼D��>������Y�8�f���Q��>|�X����>�ھ�V�����>[q����Ҿ$A�#��>�ѽ�Ѥ>%P�(S�קD=c��>�Ǩ+���>V菉����/�O�D��.�=�	����=[@G������>a�=;�?u��]��>%��>*�ؾ�YL���B���<>�,��`2=���s���f>e�>+m�?�M>��$�9��=*�?�
,>���>��>�燾�޾X>��O����Q�S��s�>��>�wǾ�<Q�EG���E�=�_M>?A�>b>�j��zX >:g�>���=�Y'��T�>�1?+NþrT�[��	 N>�����֞�4�=+d(?Q�K�Q���t?���>�$�>-�|���ѾA�f�3�M�I j>{����'�>9��@�<���iT��e7�J��>��='���r�y>ѫ>�o�\�����2:��#>���W�ξǴ>�&�=��X>��&?63=��d>A�s��� ��Ɲ�>��>��(>��+>��l>n0�>uvf�bI�>���n�m=�<٤�<��V�����{���2W�e>�>��_�l7��Ƈ>����߶������;<ў�>xС=�ể�u��d�>l$�Ja����
>k����Ȁ�D�>�.�=�d���d����<B,a�my�;��H>0L�����2�T�v{����>\꾎?���c>R6���k>[��=V��D��z�=����.���\��ҽ�^�>�A?B�&���K=�I|��ǥ>vu�;���> ՚>.�������(پ��g�>�;8����>���T]�>T*R>���=6�ƽX���>���>�Ӂ������A>��>bо�m�nt�>�Y����{�n_>Ē�����w��(�%?C鰾}���+�>�|����=�ve�踑>Gn����>�v��[p�>��>t�B>\S�>sD�>�R�=�\�>D�ž9�ѽ�=�9�}�_��mR>��>qXt�
����J>�K=�Ū��OнW��fи�"G���dc�P���L�>I��=6O��ë��YYP?��<��'>���>7:W��a� Wi>���>[��>�^=D?j0	>�
�=d�p>�v����>�4��{�V��H�>�þނ�5ҟ>:7Y>�;]�<��c�*<�ý���H�:�$���䳥�Sڽ>       ��,>?�9S>�:U>ۇ/?4��>{�->�!�>�վ
��>��ҽ��>�*S>�0��?ۡ<Ʊ����>`"2>�d+���L�?�Ơ<8��=�8�=��۾��'>[����>��:<�H����D>$J����н?�p��C�E�.�n���?��;���c���&��X><q�>�U�>�Z=V��=�*>%�>z��:査��9><�>��+=Xa�<�B�>�">����H�=�
���K>�x$=!+N>z�
=:���ey�=ȺQ>a����j�\�����ӏ:��ߨ���½c{�>���@��<T+E?�l&�����h�>v�9= ��<�d���Z�=���=�����̾<���D�A>��%����W>-�v�{�<=��>���M����Q��>��:v�>_����<�"�,��Η���ܺ]D���?�B
?�h�>s=�G�뾮�X=cQ�=aq?g�>>Wk�>�꛾���9�p>y�Ľ/����>qy>�t�=�n��;�>�e�{:�>4�T>@�^��׽�<��C�;�����2�>FI)�gq�>
�̥>�A>��">Eu���E�㰳�Ľ]J�>�C�=u��^ܻ>w1 �'��'��>��@��Ǿ2��>����9�ｦϗ=d�`��>UK@��Ʀ�.��= �6���(�M�>�Y\��Mn�>��I�f7_>p��7>�b�˽XV'�x9?��	��L.�S�=�c~�!�V�]�2B�=�~�f�a?�la�0�=>h���H�������>@/>��t>��žQ>~�>�A��)"Ҿ�E��Ӑ�>�UC��?���.�P���M$�<����� ��&��>C��>(;�>�/k=H��ϼ�=�N>t�2>N�> �t�;��u��)�j�����+f>˳��Q-��5�?r�;=ɲ�>L�{��ҩ�t��=�P��*7?��;���h/���~>д�>!\�=��3��a�>Ѡ��>�Ⱦ[b��k#�JɎ��F>*{���ؽ�4�>�5�	�����>��q�faw=V����M�����jK>!9?>8R�>F����?��J?]>P8+=��=~>���(�`�;>B��絪��N�=!V>���>�҃�p?o3>=\F�z�4=�����J>y����d���>]g<g�!����;V��ю=��5>�3{>�
�Q�M�a��4�<���>��=7#�>PA ��w��E��_H,�?�>�*��"��_߅�n�����-?�+�>�_�>I���[W��4����g菾�:m�&�þ�U>X��={�`��_=�]?�ܥ��xڽ�r�=P�>5n(�Q9?)s�=��?��?0�>���/V�<�>Ѿ)>�㯼�@?*4�vغ�� ?��1|,=?
\����>G�V=��>��?@,<>��<�����9��e���位Ӻ��s߼�|R�.Y�>��:���ɽ�Y�>����>�=�?�#>�F?���qx>Ni�;Ce��8�>+�����>�N��s�s>t]��#���瑼u�>��	��o>?[b���N�G�|=2�C?�=������!�T��\ƍ=�?N�,?���ׁB>x�U?�6��l˽-8�>c�2�R��1iL��9>�j�?�=k3@>v�c�s$;>2[n��wD�p�8�2����{NX�P�>�u<>����Q��l�=�7��]N�xiνXzɽ��/>aI�<ʑ=�	v�M����� ���A9�=���>�f�=�B��N��Z�'>Ef ��F@=N>����e>|`ؽ�:뽔��������}+L>6�4�%c�>?�_>�_��hȋ=L���n�����>�@���6>��t�b��>Q�=���i�<Nf�=������/�g��($=�E����>�0=m�f>��ܼ�gC>8�b=t6��0N��ｧBB>�h����>�"=�Z�>h;���S>E@:�Ey�>v�>9zi>=v��Z��_s=vg#>1a�=y�H>�ۗ�ޫ����>�oM��E���=y����=�F�>��>a�>B\>�>ҡ>>:
?.�s�� �>!pO>�>��>�z*>θ�>��*�%�M>P���r��m>D�=ў��>��=x;>N��]���+�E�a>U�E=bѶ=*1�>����ۖ޼�'�<_@ӾoJ/>���<�nb>�>�= O>�Z=>�|k>=��>�L��<�����>#�־�ꑾ5�Y>�+r�l�Խ;N>��SxO���=O���DG��U>V؍��m��r-S<�� �5Y����>}I�>�>i��>,j=�#A=J%K���!�!<��=���?K^���w���[���d��9�>��&>v��>�T_��]��q1�<��>�UC�B�> ��=�j5�ݜ�З�=r*d�e�=��G>�7S�t��>u�ϼ�J���>}澼��$�z�@�ŗ?������>p%�>�b�<�n�>��'>�5j=�)��>�������>k��=�� >d�� ��qT>#��><.���~>�J^>�i>j�ʾ��e�01�����=�V>���;\��>�
7>@L��{/���i=/�G>h>�Mj>&>��>=�$��Q���H7�2�o�y�>�y��ݟؾ��$���[�>d��>�b�=�淾H%S�wiG����f�">�U�>:T>��>�#?R3<;�������	�?�">�!>3�.���4?$�)�Ե���>�Y�>7-�>QL���q�>�Ͷ�<5��X�=�R>�[�!�J>�?p��:���᫾m�9�=S�8$�>B��8�V>�]6>��F>�Y�`x�<�2��w��O
>TQ�>�Ą�'����V|��U��(=t���	S>���h �`���6�^�՗>�i�=�K���'�>?[���>V	�C�L_;�hl>�I�YX�>OE>F��>��_�9q�>���>.&+?`?V����@�=�?�>�\=L�=,��>#Ԥ�
��l��Q�=���=�)O>�-?��>��q��=���>�̔����>��M�c�#�Hp�>��۽hi�-q>�y��=�>��9�8�ɾؾݓ��
q>��Խ����1���</��� >n}=LL��<����>�T��½��\=;U�=63�Zr=��c=ՙP��s/�}��C��>��$�ײ��F���+�?�f�=�෽P�r�b���QC���$>L^A?���>�־˩?�>R��>1�"�(       =e�=� V=�Ģ>�F�>R�ͽ��ܽ?�=7|�>��+?g%(=Q�U=q�i��{�7�=͢Z=�� ?wI����d��\�I>0��)�=ݾ=��ھ��w�"�c��ϽRY�=�p>n>yb�=5��>'p�=��R>zR�>U�>F�?�^�>MU?9��=(       ��1>u�彄�j>s���:�>��>�[�=?dn>6\?�C̽�#Ӿ�fr>�G�-��=�����>t�������/H>{3=Ւ=�`!�e��~�>O:���Y>mj"�XO�>��=�V*���ͳ�>f#_�p�=�'>��=�,�V��>�Ž�      �{�>��%>���> ����%e����!�m%�=&���<O�w���?y��?<ps��ؘ>+�>˫e=�+�?��=E�ؾt	��N�m>l�������?+�< ��>T�7��y���=���KӾ�L�>|��=$�>��z?n�8=�?2?��>0CK�x��o�?H��>����dA�>J�>��*;�mJ�(�fD??�?v� �ίԾ�"?d�D�]Qؾ�,>'.ɾX��gĄ>~ԡ>�E��x��?���=���<W���⭾19��1��h�[?Y��y����?k`�����~x ?�]{�3A>z�>�Zܭ=�l��M媾���>�
�>ٍ<��������� ?�T8>�f�>���Q͙�-ܾ��??BxN>����٩�]��?��0%��`�j�>�;1�R���w�>����?Jp�=�m?k$���>����Q���n>��~=P;Y><��>�S>�O�>�ȾM��=���>�q��V$�<�����T>�K�>z�>�À?[dֽ�/ľ�
Ͻ3�C��p?�r�>z7��$��<��4�~ٗ?�y~�I�h>#>;��,�=�~�>=�ҽ{������>_S��<�������Ľ�Y�x��>�ᾦ���/T��g"���@�<��>U�>�2����.�>���>�3�>PԾ��_���\>�Ur޽�b�>��=
��_1-?=:{>#S���(�>`�w7?t	��~���Q���[��ߎ?��vw>=>��>����{??�ub�wf���a���d����r��=��2>�$�d����������B>!h8��
?�^�>M�{�s��>���=�3>�%>8JP>z,�>Q`��hZ���>��=�ӽ+����w;?��B? �g��ol�x�=�O�=�x`=s+?�������>�pA�nb�c� �6�)��&w>�?6">:@�
����X=�/Ҿf��V�>ߦ�>I��]ʩ>��j�=��B>�h��;����sj>`�=��>���>I��>ڀ����R>�R?9�>Ob�>C
W>-�����">q9�>�R�>���$Щ>$@F=�5��S4>\�����=TV�?h�A��\�T��QqR?ǋw=v���� ?�$ �[\�޻>����{���k�t?��1lV���>�_ʽ�J?J���1���I��fj�dc-��}=BJ��]����鲾�@ž���S>��=<�>��=�h����D>,5�ѥ�=�s5>�j�>%��yх��F?+�9?�lU����>.:�>%�s�6T?q.�>wW�Y@��࡯>���"���?;5T���>��������ǿ=z��T�߾���>"�>^�?��=j7�?EЪ?B+�A�f=G�;�"y�=]И�|���D{Q?�8S���>a�=��?�jy>�F�\Q��Y�ν~���>�"��R���ּ�N�<�b�>L=�~�zk�>*z��N�j>���=��g��#�>F�2��7ҾiK����T��?��нy�Q˝?c��ӭ>�����c8?of��舽 l�?�7?�NH��Z��<`R��%b>��4?y��m�A��0��5��=cѯ>H��>s5>vń>i�u������e��       �a�&�6Bz�'�=�f>J�=�� >��������[>^@� ��r!�<%<�>I�нm	ƾ��(>�꓾AH��;���A>�
Z��c�B���F�!��=�^?d,p>h_�>6� ��,�?�KS>�=����o�4�NQN���>��=E�>����j㸽�T�;� >��p�iL_>��;��&>v��z���.d����<�*F=v+=�e�#z���ɽ�i�>�=�>9�o>ic�lF>�C->��*=�[⾎`ɽ��h�e���\������>T���Z%�=U�>le�=A��&̾�>�����?��i>~8о��E���'>�-�>n��>�i���1>&�?TP?y<Ҿ��F=�&>�H)��=�>�~�=<�A>gJ�=&�	>|��=��
>+ƈ<��>������>�>&>nm7�v>��&??���\8>�u>K=�I�BJ�=	��=���n�>۽�
Y��6ھb���Ej�>��r>��;>嬗>[x>�c۽0w.���'��=�=�9�>�	>���=0-�F:�>�.s������̽C>�����GZ>d����h�8>����쟾`��LC�>�ED�?�>��ؽ�kB>�g	��a���� ��������?U|>�D�>�R	��}�l�5ڢ=���#�I���B�� X>���<<Wƽ��[>������K�I����sI<�|�=A���t���K�E����=B>)�>⅒<���W?׆6�T�;l̗>~)��gTR>�7�\?�>�2/�CO�L�F>j�q�򏝼]�==�?a�>e�?r~�>I�&����>=��?`>�8�=C�8>��.?
8�S2�J��> ��=h�����>
�<�q�=ju��4��<-��>dS�=V�>#��@/`<*3��+L��_����<��>�,?��>���Qƞ�l��>�b��@��G>&�L?g� �}3>$NN�A>@-��.�$>q �+�<nd��>[x���=i�>�����Ⱦυ�>J���V2>T��>�4�<���>���y?P:�=q�">�ο>vh<�m�=,�̽Sy?�V��>~߾���ž��>�V?���=0z���t�>�"=�n7�8�}�k���c���=��1�V��n�b��������C&=o�*=�߽J�H�����.:�$沽v4�=6����.�l��4N��pr>ڍ>MǾ`�	>�幾��r�6J׾��=��E>��>�F:��a���]`���J��n���n�=}(�='�i=Y�9>3p��O^>J=�` ��GM>���=�7�=ٷI������|;�,�;�����>�M��OI����������v>'&��6/��>��?��G����>��K�١;?����S�e���).�>����(�>o\�����W�>?׏>�甽x������>��=3�?[D�>�֣>�U
=��4����>θ��Y�=>_I�S�	?)��=��7>O��>K�~��(>�O�>ߞ����Z=A��=�������������'�S��d�S��=�����>�h�t��>5nX�E�b��;=Ÿ��`�t�н)��=��J>0ǳ�fK�<�yj>�>`A>�+����>��e��>~�hn����ʾ��>g�=�B�۾�	W��u��`|>�>,��>���=/"�>ū���墳��q��H��J ����1>�g��,�_���>;;?�>���=���1��R[پ1w�=K��=�Y|=,��>v���]=�)m�=J<��.��=��3�#��>��>Pl�=�J}����=�e���j�����>�>'Q@��B��f�><s��+�zZ�>���p=�P�>��=����@�=�>;�`>
]����4�>%X��H���D��Z��	�/�U'\>��X>x�g=<���xj��A����<��㠾����.l��86>a- ?�HL>c�Z)8�Fk���Ͼꈐ=?=�>���z�a��z	>y����η=o����=���:}_��Pz>�8�>�0F>կ>;/I=���������%�CQ�	�u=������>����jQ���~����;Ps=A8L>o��>��x���(=� �m��=P�V>
F?Z�>�쑾�3v>��e���:(V>����^@:�k����O��.y��%I>eE@>u]="�~ɑ��X��!,>�폾�iw=d��8�=�ӫ>z�w>&Z��E�=K���)>ʾ���=�LU=ۣ�=7�K>���=7u�|i������Q�!���=a�L�>�=	;�˰���=�Z;>�V��,;��= �O=�ݽ+-�>�c�=О���h�>�鱭��ǽ�&�>�!���`��m=��ͺ]�����=�cOZ�툍>Ŷx>�ӧ�C��~q��wƾN�>[G��Rl>��q><FX��;�>�^�<V�z�w�jr�> �>�����s?ڦ��%��E��/`�=x��>�]���,��ё����W��>���,�y�3�oO�>��G��t>��V���y�2���͎��'D�|�d��̅>����>&�7�S�}���C>Qǒ��������=Z����}8��g�:���>Td�+Q::���C> �>X�>����A�#>�}��O��>Nj�����>m�b�Y���f��>2��>�q-?�oܽ8��=v��>
'?k(<?ߵ���>	�7?aS?�8���h=���f^=E�>ۘ�<��c���+��$ѽ�L�>�e�U�:��>8��<��S�ڼ���V<�=���>��?�ɘ>fO����m>_"��CA>=���a���ƾ�j���z�>�{��1��� ��by���,<������6�6A��E����朼���@��c���� �7�Ž�8#>�ﾸl̽7]����h�<�07��=b	��!��Ĺ�<kMｹ�"=u��=F�3t8>X5������<�)���<rْ�"��=+���=�=k �+$�>n`��ޕ���zp��6�S��.��ߊ>�E��V������@�>=��=�0=2p��b�#�B0���[Z��u;�Б=�i>z}Y�!$��iؼ�,��">��c�RuW=�a�<���}�=u���,��>0Q�>^,9���>���>�ٽ	���
��Fi>`"-���=$w]�\�)���>��?�"	�(       �kk�X.{�@�>�ʅ>�Z���ϑ>z�����=��1>
��>�7��P!���(>�١�Pu5�]B�=Ս=5�<��ʙ=O>W ����E�~��>�g�WA����=��+�ZF7=�S�><|>���<'*>l�=�/ ?���<ȹ��@d���`>��l=X���(       O�þ���<��J�e�;�S��r���&���=)��>�G�>�+�=,��>�@:?�ϯ>���o<�����=�ˋ=�����\��\��h���A9>W$��>PY>�C����>�rD>D%Y>�M>rz=�3�Џ=?az�c�;d���/+�>Ҳ?0�н�      �龪�8>��U�@��>�Έ��[>"˥>Y�Ӿ`;
�j��e�þ��V=%�y�Qi�=}6�s��Ӱt�K�=Z����⾬��>��J�����>��?��>��ᾊ�ͿcD$��iY�*U�)	����>>��MsK>+�{��2˾#Y�=59׽C�=(<������v�a=c\��40�>�}�>�t ��&�7���I;��{>�!>���$�H=�|6�p�=֓?\�=���=84�>� ]���:��߽6�þ=�|>����>�Yށ�]S8>�G�>$��>:+`>�D�=���9�ܾSc>VU���M3�ǅ߾�����%ɮ�㛬>1�
��b���C���E?������=p?,���lz>փO>�ت>����>Ki�&Qv>�V���H>U�<a#=1�4�X�U��f��0�Q��Ǚ;�ͅ�H����:j�rF���>�=PV�x�;vo���A#�.v_>vG�>���R�?�m$G���m��?�$����>�c=*�?���>ny=��t>_V���[_+��쾥��>M��-˽U������
q8>H?���L>�>�Qa'���ؽ��ѽ'"�=k����=�C_��$8>U��<�7�&�
�=�,�Tn%>�?����=q����b>G��>��"�(/W��m��(>���Ra��?�?H��>@�<>F
q�2C��+�U���w�~���𱯾FN���l�d%�>ٳ�>�?�����>�޽�a�>oC�=ۦU��)p>���>ª�>*p�>A><�`=��c����ޥ>
;�I�����z����=(�=���Y�<L��y��Nݽ�<�?�o>���>_=T>A߽Y-���H>z;��*(�q�E��K��ʠ�=��>D�3>��o=�&>T>����ֹ=���>_����:===m=| 	�I5>S�>��ž����齀
�>Q�T�'��>�Hɽ1\�=Զ��נ�Kyݼ�U�<�ꕾ󅞽O{�>#��������>E�½��(�7=���H�<�P	?V�M�}=c�#�G|>�q�>���=Z={����>E��w�8o*��F��Ek��l�=C�޾�z�S�>z����+�i�;<�u>�09>	`>�)����M>�r���b=�f?�� �lJ�>Ԍ����>8�=��}��w�>s�r>=J.���/��N����=�>̜�^����ѽl+�-�s>/vO�?u?�һ>2���E�����.�1��>.7U�F�=J�
��DC�բm��Ӥ<���>i�����t�Ȏ��|�= �1>9���(6>w�;;�̾�l��"S�>���=��>`�>5���5c�Q������	B=F\���ׇ��~ξ�B�<���<��<<��,?�g�>�ˀ>��Ծ��>G��>��F?����{�=\���:U�Tt8��5>���?�HS>D��=�;
�����N����~�P;1���>i�j����>�Ou>Ӥp���9>���>�q���O*�'p>2��>Io��Ƅ�'�྘V���%�5AF>�:���;�<ˑ�<z��>��>^�p=�`.=�X���=��x�i�>i������S�>ھ����a>(       _�I>B6">@ ���샾'ϥ��~�>ǌۼ�,�>X	������S��j/�=$�����>���>.�w;��w��L>#��=��l=i�/��
��T�>�࠼	�>AXG�Cۨ��;=xE��̾
��>	!>�q~=^��>fǽ�n>y�>?̗��4D��q�(       L"�>��g>�4�;�T������-zl>��>�'�>+��|T=l��=9b!��)>�s�>JnN>�4�������>����?h���,����=���ɪ=᫾�Gw>34>0������uOk>.��=̝?e�:>ο�+�8��b�>�����=i�&=�      ��\?�ҳ��>88�>���>s�=)'=?�>��r>��0���4?�
��i->����+�>I����b?D65=���>����\#>]��=���>�yr>��>�����ٯ>���}���]��%/>�W�+O>��:��\�=~꺼tr�>%��>�i�>��d>�&��B�,���\>��z��bi�:�����n$��f������F�>�C�F�=mA�=3��>��J?�U�-]����5Z�>Ǒ<���ü��>'��>�R弛��o��>�a�?Ѩ���Ԩ�u[���ܯ>0���w��>򘄿2㦿��&�F��=���/6|�,�J?����UR�>��a�ܥ�l[��ҿʿuO?re�0N^�l�=������	�f�,>��?�?�����;��}�W
�T��=+��>۫F> �>��F>���;�w�5�;>"9$�ʀ�=�>�p�>�|�ȾuQ> H�<�:���Qz<U�>U�<�UG���<�>X
]��ޠ>\��>���������>tɹ>ت>�>5Ϡ>���>�N->�7m?��?��Ӿ[��>��a�$:�>��U>��>~$/�$�?��!?�d��ۻ�1
���>��D��?�>X��=]h�>��?�x��M�Z�?��6>I2���=y}V?2ϵ��o?�ꉾU1>Bѝ�_^>e�/�/��=��>8�>�ɬ>�]�I��ߩ���X��������>S�0�6��>\��>�g>q�y>#4
?��B?��@>b����A�	��݂���?�>�S(�<�!�3�?�k%?���ː�=���=m5�Ԁ�=S�a�8Q�S����;,�����-�5
J�Bl+?�S��I�>͏��>�<�)?�E'?�lQ?Na�>SY�>O�>s�&�	����>��>c����>��>�׾>׬�?�7���>���G-���y����>#Ѫ�P.���`����i>� +>*�U>���>d���	>>͆�>��=a����̂>WҾ'��><�J� >v3�J�,�,�!�^�P�Ja>L�u>��Ⱦ%w�>ŷ2��눾�>��#>��ͽ<ؾ\W:?��h�Y\�>?���Ӂ �\`�<��>�6?]�ʾ�A��`N����>��ʾw4��28k>sϚ>�;t>��k>~��?���=��A���2�>�q>9w����> �н��?��ž���� ԍ>خ3>�i?31� �?+�@�a卽���n��?y������>čt��9Y>ӓD>��&?��ľj+��z>R�b�?3�=,}�?\��#QI���>�? 6���-��O'��k�>��Ͻ�z>�%6�`N0�[��>X�>��>鶱����>/z�jX�>}X�=Xv>��<r}�G�>�0o=Jy��[���Ļ��V1�_Lq��iž=C0���8?�s���
��$��5��>ٛu?��վ}�Dz�=�:r�W �6�>�5?ë�>X:�>�$�>�C	?S�\?LqG<c��>".����*?��IX,>�P��! ��%����>?5������TD>I˖���ɾQ�U>߅����>C&���?ET�>Sw;��,?��?>&�>rI�2`�=_�?B����>(ڨ=       Un�<�t �;��2=�&�>bv��=�>��E>�h>�Sk>�)
>ش|�`=�04b�Sy�>I��=A�۽٩}��>�=�q������ݵ�?г�=0�5>��~?�[)�|�
��Kc��T�>��'���>�p�=�?0>�!�<��]�g�N��!�>�-<�$n�= �ܾ��4��>��p>�ɓ����>57�>�;�?<U2��qw���?��:`������>�� >?E>�(�>���n��L�?)s�>�:��[�ɼ=��=�D־�Ȇ>��ɾ���&OR����=����rK^=�:>�=b�e��s|>^>�=�����<�*�>n4?���|��>s@�>¸>��;B�'�A*�mX�����bR�<���	(�=�?�`?�0�>X(ܾ�V
��]�"�>Q�>#����VY���:>Y֋�B�⾀)ھڻ4=/�;����'MW��{�#�b>��}�3�=���|<����?�#T�װ�<9;?��=���=-�>P~B>"i?�W?E������%>�Z��_?�>{�=׹ͽV;��GJ>`=�8��D���յ>
��>�j�8���"?��f��h��̒�=W�U>�(쾩Σ�&ӂ��U�>�!Ǿ�$��־��I=OYv>Z��>T{<B��>K��>��c���	?q��>J3H<��h>�)%�"0��Xժ���?>%�<>;��>��;CkT>����p�>�k;>�?9�q�U�jX =4C���)��*Ƴ<�>㽿�O�{�����}��,>ʹ{�J�U�����id[����<b�Z>�!վ�u=�>���>K�? 8�=��;x�?�Ą���Y�=�ޖ>^�Z�U�>�+<��~�6H�?��>
>��
��>���>85�i���>`i�Bd!>~Y$�O]�=�X�>Ö��B�>���>�W;�R�p>����>���o�>i���e�>F��"�Y������fO�1>-��=
?*�h����={�,>�����_�>J <��c�n�>n?j��!}����>��>�9b��痾�7>4�7�u� >z���Ǩd��g�=��þW�=j ������AE��Y�<\��=A����y��ޘ<<"y�=>����g>��>���=o�z>��;�ؼɦ?�D>4�=8�?rQ�;��Ҿf!@?��>�^�>-��^��4=>��㾟�������"�Ǌ�>U��>�T?B�O>
���m>��A>�b?�ٲ��&�E������V�=��v>���<`Ž�}�>e��=c{�>�􂾤�G��?��k<j:辱�<�J$>8��=J`�>�*��3��R�/[������aѾ�j=)k����0>��r;����L�5 4=@��=�ε>d�>&۾(Se�x����X�ȉ�=�3˽K�)��@=�o�y�3��>�ټ������J�;mV�>+��>j<��U�W�oC�w�����>��?�۲��|	����>�:>p�9?y)>36_>�"ξ	>�>r׽��������V?�N'>>
d�X�.�NM>�i�>ǵ���E>J >o����y��[�H�<	bl�!��=��H�]�>����E3�/���XAn=-���(\>�>E"�<<ק��bO���<���>u�>l��=��W<�fr>�z����&>3��=TƟ>@k�=9G�$���+�>�GO��L����8>�!�}/'���>{g�+��o��<Gz߽��>>��>�Fp>�e�������=�+D=�ν���=T2U��
0>?��9�?=��黿o=Љ�d�V>ͬy>c����	�>�Ȕ�ߕE>�˲�Y�A��R�>nz>kf�=�����H=��P>��=�T=|c�s{5�vF��!y���ݽ��>�Uh>P7�H�>� ��q���Cw�mݷ�#��=�AM���>tx)���B�T��<j�@>���>O�;?)C�>�X>]Q徃י>�����/Q?�ྒྷ�9=&f���8�����z>�]>�El�>F�<��S=N�6>'�ξ8�e�?ru>�u>�N�=�>(���X�w�_=�W>�Xe>r{��[�>B�(��q�=#"]�=��>�'��]%����齬ܾ�",���=�p�2=}00>��->j���p�<�l���� ��E�=��j�S�
�7���J�����T�>0(�Td�N��=�??`�%��a����[=}
>�9>
O�>7�D����?<��&�a�>)��a����ɫ�<'O�>��@>v��X]=`��>���>v��=n�>���,4�<)��G�>F6l�JAo>fL>�.�>f�K=LX�=r���V�>ڍ�<'c=ȳݽ���>�\r>������(>H j>	�2>t�˽�=�I�=Ĥ��,ĺ<�P���ս�f?�0>W1���>}>��Ʉ<T���xy����P>[?uk�>��/��g�>�7\=�;�=�k>�L������m�:�%1>$�>1�>�RO��ݚ�����q��k�B�$S�?�ė=q\>�4{?��d�K��<��%���W>J�-�#H�����=[AF?�~�=�5���J�<&�����| L>�k�� �>6Zk>�&?�|�����`�;C�q=�=�g@�E�<?1:�*�V��`�<f�R=	�[>V1i>�8>�h3>vҨ�'��>��>�[f��
�����;>挽����H(O�%p��ij"�Ӛ)>E�t�:E�l �ӣl>�I<��>	q�>��=]�=6����D�V�Z�>q��>���=��;�G��=V�޼0�a��!>tU���)~?>�hV���u>I�ֽk��7Z��ns>�W޽��>��8?��bʧ>l>'�b>}�-��3�|_�>���K��	�<"J��|�k�˾��@�.:�>�Oʽ֒=>�{�=����R�5X�<�W��hU>����?�?���>��1��,��0�檃>�N�>��I�Km>: �������ؐ>~nڽ5>Cx�#����;�>*�2=2�@�=�˅=�S>ݧ�Mo����=؎�>ά�B��>����UR��c�>�e׻1�Y�6�d����=�QQ��$?�|�7С����=�cD>}r{>&���t��Bd�>����Ů>Zg����V=��=j.�=Y
���U>��<>`�>�e���>J�<b���(�j�#���b��!ڂ=�lӾB���*�>rz4>^�4_"�p�>[�>%ѓ��Ճ�(       1�5�
Y�>r��6~ɽeK��fL��pM�<�i�<��>:��>�<
���W�=��>8t��?͍>��;�V ��qO>:J��r�ؾ����0���b��#L�>qmj��	�/2J>�AV>�-x���m�Ȅ =�8�C��>���X.>c�>��@�,�
?�U�=       {+�>�"?|S@?u��>�?�()?1�>���>g$T?bۻ>c�3?��B>g�H?�Υ>��>��|=�?��O>�^�?1�>       v1};�%�<��1��y��Q���k&���<��<��R<6Ș�R'��PF)=��Hl��J�����<j��<ѴP<��&���:       A��Y�پ�R�f3?��9�L�W<+�����>: >�+>� B=w
���>ʍ��M��=�,�nF�=�ޚ>���<�u��        ��<ܱ<�;��;��<@�<�/=��<{@�<��9<yXn=���=�̤<>9.<p��<c{}<d��<s=={�&<�=(<�      n0�b�=}��>L&���D>/>>�>9�(>Πҽ@]�>��[�@�<R���>4�>\ K>l�>j�9>��=�!=�E�>�s�=n ���R��@�X��Ж=a��sy4�:Õ=r�F>t�T�4dD>���RcY�x�C>dA�<'tA�Mz9��6���`f>��~>4":>}�s>��v����=�l��>ʍ��|�C���H=�G(����=�}¾��G>&I������4���`Ͻ4��6��>�_D=س�>H�=��>O戾X4���P�y��=2��>2�t���>�@��'X��Q$W� .[������٦��ǁ=��=��
���ʽ��!��􊾲�t���>p',>�'�<��žqý͎�=�Nv>��H>8��<17�>�M��ȴg>y�վ�վ�/H����<3�}�k���Ɖ5�i�Q�e��>��>Ffؽ?]ܽ*/q��r�>�
��yϽ���>t.�1��뺸�]���b�̽�υ>ľqji������n�B0� p>��P@�=J��<߄P��?t�=�Ye>��=�&��v᫼�q!>�6����9��xG��~�=GS��
�O>��:�]'9�>9	��=c�jt�>� <��x>ʫ��<�*>�� :nt�)��>���>�?��<[dJ>�
c����<�8*>1��[�<u�ѽ��=	����}G�s��>��<�{��e�X>c_𾡱k���>�b�T�R���3� ���v>�ᘼSB��<�Q>��S=E�s�3�����l��>�S~�S\�$���OF�>W-<> ���i�#�1[R>�f?�?u�Xi�>�G��<�=��g��c�>t��>Ee�>���<'M���O>��V=�?����=~����»�3m��o�>�w���[�>�\�>�����S�$�.>�#T<�v�+v>��>4���r�a>�a��@!�+�Ƚ����<��(h�>�yy��'ý���>�ࣾ#��>V�>aLἹ߻���'���𼕋6��b�1}�!��=7_Y>��ؽ���>��	�W��>/���z���A�&�II>Pս��=}�۾��">K��=��=��뾠N�<��8=��E=�V=K�<s�/�$�����>m��S=Q>@<q�H]_��ü�����.+�>��=�Xd>y��>�ˌ>DV>�}7�������<���>�"��Yѽw�U��@�=ؒ�j�9>-	�����"�����}>�	����;�>a.h�#�b�eWо�b|��,�>��+>�K|>ީ��'��=N��>D�Խ˅���jq��n�>Z|�=M2=�DX�Y�T=�<�>�X��@Q<��H�߶)�w>�=SĽ���]�(>?4�=x���g�&>�G&���vn�>������Ľ�Ԓ>af̾���< ���GmԽI<��V�=$`��V>���>�:���=���a��>;���^.��L>L,ԾO��=���<5M�ZZ�p�E�L2=5V4>���>?W=&�ͽp<�����=:�����r=a�R=�پ��O=�몾jV�=:t޽+�>Rx=�~�>�`��aȤ��8�S>$CO>���B�y�HЬ�gt���>�����D���Dux>f����#�> [��+�j��R$?�Sz>�RI�       :�߽�߽[V���N=���=�bH=�����ξ��=�s)��%� b=.�>�8�<���=l�� �0=Q�>wO�;ʧ�=       Y4)? m�>��{?H�V?Q)?�h�?C=�?��?Qn?��>�Z�?�?�?x�?���>�,;?+g?E�?��?�e?�#�?       A>�´>��>��(>�>��=�y)>��>D�>O/=��>��=p͛>ڞ�>ǟ>R�(>��>ea�>)��>�Ԟ>       ��޽J!�DټL�=�ag=6w0==�����̾
=�q��e�l�a�J=6�	>[��<P��=cW���q=��%>��V��=       ��>c?�$�>�v@?���>�v�>��;?3��>�N?�r�>���>�E�>�?H�?��P?��>U��>�=?�>�>��6?       &��>g��=sp�=�4����f>�8�>�f���>V���?p>�߽���>uŽ��>]M��^ ���5�u�2����tJ�       ��=<       ,��K�k����=8�}>�p�>��#?�q���mԾ�:�$|�:�a-?K�B����Gw�>�j��p-?-����q�>��;?i�?i�Ⱦ���>���>�N��R�=�A�?"u���Q@���tE3?�8����<�W ?7X���<?3�^�?+x¾t�*���I=��n�>��$\��S� ?0!�q�C?c?5��>o�#>db����>`��`���O�IO
?C8?       T'���s�?��=